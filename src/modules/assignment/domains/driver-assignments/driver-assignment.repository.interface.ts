import { DriverAssignment } from './driver-assignment';

export interface IDriverAssignmentRepository {
  insert(driverAssignment: DriverAssignment): Promise<DriverAssignment>;
  update(driverAssignment: DriverAssignment): Promise<DriverAssignment>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<DriverAssignment[]>;
  getById(id: string, withDeleted: boolean): Promise<DriverAssignment>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
