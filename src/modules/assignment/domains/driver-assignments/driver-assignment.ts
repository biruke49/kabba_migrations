import { Driver } from '@provider/domains/drivers/driver';
import { Group } from '../groups/group';
import { Job } from '@job/domains/jobs/job';

export class DriverAssignment {
  id: string;
  driverId: string;
  driverName: string;
  driverPhone: string;
  groupId: string;
  jobId: string;
  status: string;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  driver?: Driver;
  group?: Group;
  job?: Job;
}
