import { Parent } from '@customer/domains/parents/parent';
import { DepositedBy } from './deposited-by';

export class Payment {
  id: string;
  parentId: string;
  amount: number;
  companyEarning: number;
  isPaid: boolean;
  paidAt: Date;
  expiresAt: Date;
  depositedBy?: DepositedBy;
  tradeDate?: Date;
  tradeNumber?: string;
  transactionNumber?: string;
  status: string;
  method?: string;
  reason: string;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  parent: Parent;
}
