import { Payment } from './payment';

export interface IPaymentRepository {
  insert(payment: Payment): Promise<Payment>;
  update(payment: Payment): Promise<Payment>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Payment[]>;
  getById(id: string, withDeleted: boolean): Promise<Payment>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
