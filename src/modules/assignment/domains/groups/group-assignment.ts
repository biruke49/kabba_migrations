import { Kid } from '@customer/domains/parents/kid';
import { Parent } from '@customer/domains/parents/parent';
import { School } from '@customer/domains/schools/school';
import { Driver } from '@provider/domains/drivers/driver';
import { DriverAssignment } from '../driver-assignments/driver-assignment';
import { Group } from './group';

export class GroupAssignment {
  id: string;
  groupId: string;
  kidId: string;
  parentId: string;
  schoolId: string;
  schoolName: string;
  driverId: string;
  driverName: string;
  driverPhone: string;
  status: string;
  amount: number;
  paidAt: Date;
  expiresAt: Date;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  driverAssignments: DriverAssignment[];
  kid: Kid;
  parent: Parent;
  group: Group;
  driver: Driver;
  school: School;
}
