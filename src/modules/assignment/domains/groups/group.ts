import { Category } from '@classification/domains/categories/category';
import { DriverAssignment } from '../driver-assignments/driver-assignment';
import { GroupAssignment } from './group-assignment';
import { Driver } from '@provider/domains/drivers/driver';

export class Group {
  id: string;
  categoryId: string;
  name: string;
  numberOfSeat: number;
  availableSeats: number;
  status: string;
  driverId: string;
  driverName: string;
  driverPhone: string;
  isFull: boolean;
  kidsRoadDifficultyCost: number;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  groupAssignments: GroupAssignment[];
  driverAssignments: DriverAssignment[];
  category: Category;
  driver: Driver;
  async addGroupAssignment(groupAssignment: GroupAssignment) {
    this.groupAssignments.push(groupAssignment);
  }
  async updateGroupAssignment(groupAssignment: GroupAssignment) {
    const existIndex = this.groupAssignments.findIndex(
      (element) => element.id == groupAssignment.id,
    );
    this.groupAssignments[existIndex] = groupAssignment;
  }
  async removeGroupAssignment(id: string) {
    this.groupAssignments = this.groupAssignments.filter(
      (element) => element.id != id,
    );
  }
  async updateGroupAssignments(groupAssignments: GroupAssignment[]) {
    this.groupAssignments = groupAssignments;
  }
}
