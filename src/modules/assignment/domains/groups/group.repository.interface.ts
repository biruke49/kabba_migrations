import { Group } from './group';

export interface IGroupRepository {
  insert(group: Group): Promise<Group>;
  update(group: Group): Promise<Group>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Group[]>;
  getById(id: string, withDeleted: boolean): Promise<Group>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
