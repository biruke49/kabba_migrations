import { Group } from '@assignment/domains/groups/group';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { CategoryResponse } from '@classification/usecases/categories/category.response';
import { ApiProperty } from '@nestjs/swagger';
import { DriverAssignmentResponse } from '../driver-assignments/driver-assignment.response';
import { GroupAssignmentResponse } from './group-assignment.response';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';

export class GroupResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  numberOfSeat: number;
  @ApiProperty()
  availableSeats: number;
  @ApiProperty()
  isFull: boolean;
  @ApiProperty()
  kidsRoadDifficultyCost: number;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  driverId?: string;
  @ApiProperty()
  driverName?: string;
  @ApiProperty()
  driverPhone?: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty({ isArray: true, type: GroupAssignmentResponse })
  groupAssignments: GroupAssignmentResponse[];
  driverAssignments: DriverAssignmentResponse[];
  category: CategoryResponse;
  driver: DriverResponse;
  static fromEntity(groupEntity: GroupEntity): GroupResponse {
    const groupResponse = new GroupResponse();
    groupResponse.id = groupEntity.id;
    groupResponse.categoryId = groupEntity.categoryId;
    groupResponse.name = groupEntity.name;
    groupResponse.status = groupEntity.status;
    groupResponse.numberOfSeat = groupEntity.numberOfSeat;
    groupResponse.availableSeats = groupEntity.availableSeats;
    groupResponse.isFull = groupEntity.isFull;
    groupResponse.kidsRoadDifficultyCost = groupEntity.kidsRoadDifficultyCost;
    groupResponse.driverId = groupEntity.driverId;
    groupResponse.driverName = groupEntity.driverName;
    groupResponse.driverPhone = groupEntity.driverPhone;
    groupResponse.archiveReason = groupEntity.archiveReason;
    groupResponse.createdBy = groupEntity.createdBy;
    groupResponse.updatedBy = groupEntity.updatedBy;
    groupResponse.deletedBy = groupEntity.deletedBy;
    groupResponse.createdAt = groupEntity.createdAt;
    groupResponse.updatedAt = groupEntity.updatedAt;
    groupResponse.deletedAt = groupEntity.deletedAt;
    if (groupEntity.groupAssignments) {
      groupResponse.groupAssignments = groupEntity.groupAssignments
        ? groupEntity.groupAssignments.map((kid) => {
            return GroupAssignmentResponse.fromEntity(kid);
          })
        : [];
    }
    if (groupEntity.driverAssignments) {
      groupResponse.driverAssignments = groupEntity.driverAssignments
        ? groupEntity.driverAssignments.map((kid) => {
            return DriverAssignmentResponse.fromEntity(kid);
          })
        : [];
    }
    if (groupEntity.category) {
      groupResponse.category = CategoryResponse.fromEntity(
        groupEntity.category,
      );
    }
    if (groupEntity.driver) {
      groupResponse.driver = DriverResponse.fromEntity(groupEntity.driver);
    }
    return groupResponse;
  }
  static fromDomain(group: Group): GroupResponse {
    const groupResponse = new GroupResponse();
    groupResponse.id = group.id;
    groupResponse.categoryId = group.categoryId;
    groupResponse.name = group.name;
    groupResponse.status = group.status;
    groupResponse.numberOfSeat = group.numberOfSeat;
    groupResponse.availableSeats = group.availableSeats;
    groupResponse.isFull = group.isFull;
    groupResponse.kidsRoadDifficultyCost = group.kidsRoadDifficultyCost;
    groupResponse.driverId = group.driverId;
    groupResponse.driverName = group.driverName;
    groupResponse.driverPhone = group.driverPhone;
    groupResponse.archiveReason = group.archiveReason;
    groupResponse.createdBy = group.createdBy;
    groupResponse.updatedBy = group.updatedBy;
    groupResponse.deletedBy = group.deletedBy;
    groupResponse.createdAt = group.createdAt;
    groupResponse.updatedAt = group.updatedAt;
    groupResponse.deletedAt = group.deletedAt;
    if (group.groupAssignments) {
      groupResponse.groupAssignments = group.groupAssignments
        ? group.groupAssignments.map((kid) => {
            return GroupAssignmentResponse.fromDomain(kid);
          })
        : [];
    }
    if (group.driverAssignments) {
      groupResponse.driverAssignments = group.driverAssignments
        ? group.driverAssignments.map((kid) => {
            return DriverAssignmentResponse.fromDomain(kid);
          })
        : [];
    }
    if (group.category) {
      groupResponse.category = CategoryResponse.fromDomain(group.category);
    }
    if (group.driver) {
      groupResponse.driver = DriverResponse.fromDomain(group.driver);
    }
    return groupResponse;
  }
}
