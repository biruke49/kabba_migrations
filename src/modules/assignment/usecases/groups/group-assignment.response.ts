import { GroupAssignment } from '@assignment/domains/groups/group-assignment';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { KidResponse } from '@customer/usecases/parents/kid.response';
import { ParentResponse } from '@customer/usecases/parents/parent.response';
import { SchoolResponse } from '@customer/usecases/schools/school.response';
import { GroupAssignmentStatus } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { DriverAssignmentResponse } from '../driver-assignments/driver-assignment.response';
import { GroupResponse } from './group.response';
import { Address } from '@libs/common/address';

export class GroupAssignmentResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  groupId: string;
  @ApiProperty()
  schoolId: string;
  @ApiProperty()
  parentId: string;
  @ApiProperty()
  schoolName: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  kidId: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  driverName: string;
  @ApiProperty()
  driverPhone: string;
  @ApiProperty()
  amount: number;
  @ApiProperty()
  paidAt: Date;
  @ApiProperty()
  expiresAt: Date;
  unseenTexts: number;
  kid: KidResponse;
  parent: ParentResponse;
  group: GroupResponse;
  school: SchoolResponse;
  driver: DriverResponse;
  driverAssignments: DriverAssignmentResponse[];
  static fromEntity(
    groupAssignmentEntity: GroupAssignmentEntity,
  ): GroupAssignmentResponse {
    const groupAssignmentReponse = new GroupAssignmentResponse();
    groupAssignmentReponse.id = groupAssignmentEntity.id;
    groupAssignmentReponse.groupId = groupAssignmentEntity.groupId;
    groupAssignmentReponse.schoolId = groupAssignmentEntity.schoolId;
    groupAssignmentReponse.schoolName = groupAssignmentEntity.schoolName;
    groupAssignmentReponse.status = groupAssignmentEntity.status;
    groupAssignmentReponse.kidId = groupAssignmentEntity.kidId;
    groupAssignmentReponse.parentId = groupAssignmentEntity.parentId;
    groupAssignmentReponse.driverId = groupAssignmentEntity.driverId;
    groupAssignmentReponse.driverName = groupAssignmentEntity.driverName;
    groupAssignmentReponse.driverPhone = groupAssignmentEntity.driverPhone;
    groupAssignmentReponse.amount = groupAssignmentEntity.amount;
    groupAssignmentReponse.paidAt = groupAssignmentEntity.paidAt;
    groupAssignmentReponse.expiresAt = groupAssignmentEntity.expiresAt;
    if (groupAssignmentEntity.kid) {
      groupAssignmentReponse.kid = KidResponse.fromEntity(
        groupAssignmentEntity.kid,
      );
    }
    if (groupAssignmentEntity.parent) {
      groupAssignmentReponse.parent = ParentResponse.fromEntity(
        groupAssignmentEntity.parent,
      );
    }
    if (groupAssignmentEntity.group) {
      groupAssignmentReponse.group = GroupResponse.fromEntity(
        groupAssignmentEntity.group,
      );
    }
    if (groupAssignmentEntity.driver) {
      groupAssignmentReponse.driver = DriverResponse.fromEntity(
        groupAssignmentEntity.driver,
      );
    }
    if (groupAssignmentEntity.school) {
      groupAssignmentReponse.school = SchoolResponse.fromEntity(
        groupAssignmentEntity.school,
      );
    }
    return groupAssignmentReponse;
  }
  static fromDomain(group: GroupAssignment): GroupAssignmentResponse {
    const groupAssignmentReponse = new GroupAssignmentResponse();
    groupAssignmentReponse.id = group.id;
    groupAssignmentReponse.groupId = group.groupId;
    groupAssignmentReponse.schoolId = group.schoolId;
    groupAssignmentReponse.schoolName = group.schoolName;
    groupAssignmentReponse.status = group.status;
    groupAssignmentReponse.kidId = group.kidId;
    groupAssignmentReponse.parentId = group.parentId;
    groupAssignmentReponse.driverId = group.driverId;
    groupAssignmentReponse.driverName = group.driverName;
    groupAssignmentReponse.driverPhone = group.driverPhone;
    groupAssignmentReponse.amount = group.amount;
    groupAssignmentReponse.paidAt = group.paidAt;
    groupAssignmentReponse.expiresAt = group.expiresAt;
    if (group.kid) {
      groupAssignmentReponse.kid = KidResponse.fromDomain(group.kid);
    }
    if (group.parent) {
      groupAssignmentReponse.parent = ParentResponse.fromDomain(group.parent);
    }
    if (group.group) {
      groupAssignmentReponse.group = GroupResponse.fromDomain(group.group);
    }
    if (group.driver) {
      groupAssignmentReponse.driver = DriverResponse.fromDomain(group.driver);
    }
    if (group.school) {
      groupAssignmentReponse.school = SchoolResponse.fromDomain(group.school);
    }
    return groupAssignmentReponse;
  }
}
export class GroupAssignmentDriverResponse {
  @ApiProperty()
  profileImage: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  enabled: string;
  @ApiProperty()
  name: string;
  unseenTexts: number;
}
