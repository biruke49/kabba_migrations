import { GroupAssignment } from '@assignment/domains/groups/group-assignment';
import { UserInfo } from '@account/dtos/user-info.dto';
import {
  DateOfWeek,
  GroupAssignmentStatus,
  KidTravelStatus,
  TransportationTime,
} from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class GroupAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  kidId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolId: string;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolName?: string;
  @ApiProperty()
  oldPrice?: number;
}

export class CreateGroupAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  kidId: string;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolName: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  @ApiProperty()
  @IsNotEmpty()
  days: DateOfWeek[];
  @ApiProperty()
  amount: number;
  @ApiProperty()
  status: string;
  numberOfKids: number;
  currentUser: UserInfo;

  static fromCommand(command: CreateGroupAssignmentCommand): GroupAssignment {
    const groupAssignmentDomain = new GroupAssignment();
    groupAssignmentDomain.groupId = command.groupId;
    groupAssignmentDomain.kidId = command.kidId;
    groupAssignmentDomain.parentId = command.parentId;
    groupAssignmentDomain.schoolId = command.schoolId;
    groupAssignmentDomain.schoolName = command.schoolName;
    groupAssignmentDomain.status = command.status;
    groupAssignmentDomain.amount = command.amount;
    return groupAssignmentDomain;
  }
}
export class CreateGroupAssignmentsCommand {
  @ApiProperty()
  @IsNotEmpty()
  assignments: GroupAssignmentCommand[];
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  currentUser: UserInfo;
}

export class UpdateGroupAssignmentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  @ApiProperty()
  @IsNotEmpty()
  kidId: string;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolName: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  amount: number;
  @ApiProperty()
  paidAt: Date;
  @ApiProperty()
  expiresAt: Date;
  driverName: string;
  driverPhone: string;
  currentUser: UserInfo;
  static fromCommand(
    updateGroupAssignment: UpdateGroupAssignmentCommand,
  ): GroupAssignment {
    const groupAssignment = new GroupAssignment();
    groupAssignment.id = updateGroupAssignment.id;
    groupAssignment.groupId = updateGroupAssignment.groupId;
    groupAssignment.kidId = updateGroupAssignment.kidId;
    groupAssignment.parentId = updateGroupAssignment.parentId;
    groupAssignment.schoolId = updateGroupAssignment.schoolId;
    groupAssignment.schoolName = updateGroupAssignment.schoolName;
    groupAssignment.driverId = updateGroupAssignment.driverId;
    groupAssignment.status = updateGroupAssignment.status;
    groupAssignment.amount = updateGroupAssignment.amount;
    groupAssignment.paidAt = updateGroupAssignment.paidAt;
    groupAssignment.expiresAt = updateGroupAssignment.expiresAt;
    return groupAssignment;
  }
}
export class DeleteGroupAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  currentUser: UserInfo;
}
export class DeleteKidGroupAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  currentUser: UserInfo;
}
export class ArchiveGroupAssignmentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class UpdateGroupAssignmentAmountCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
}
export class UpdateGroupAndAssignmentDriverPhoneAndNameCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  driverName: string;
  @ApiProperty()
  @IsNotEmpty()
  driverPhone: string;
}
export class AddApplyingDriversCommand {
  @ApiProperty()
  @IsNotEmpty()
  groupAssignmentId: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
}
export class CalculateGroupAssignmentPaymentCommand {
  @ApiProperty()
  @IsNotEmpty()
  initialFee: number;
  @ApiProperty()
  @IsNotEmpty()
  perKiloMeterCost: number;
  @ApiProperty()
  @IsNotEmpty()
  distance: number;
  @ApiProperty()
  @IsNotEmpty()
  numOfDays: number;
  @ApiProperty()
  @IsNotEmpty()
  transportationTime: string;
  @ApiProperty()
  @IsNotEmpty()
  kidTravelStatus: string;
  @ApiProperty()
  @IsNotEmpty()
  capacity: number;
  @ApiProperty()
  @IsNotEmpty()
  roadDifficultyCost: number;
}
