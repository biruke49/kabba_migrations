import { Group } from '@assignment/domains/groups/group';
import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateGroupCommand {
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  numberOfSeat: number;
  @ApiProperty()
  @IsNotEmpty()
  availableSeats: number;
  @ApiProperty()
  isFull: boolean;
  @ApiProperty()
  kidsRoadDifficultyCost: number;
  currentUser: UserInfo;
  static fromCommand(command: CreateGroupCommand): Group {
    const groupDomain = new Group();
    groupDomain.categoryId = command.categoryId;
    groupDomain.name = command.name;
    groupDomain.numberOfSeat = command.numberOfSeat;
    groupDomain.availableSeats = command.availableSeats;
    groupDomain.isFull = command.isFull;
    groupDomain.kidsRoadDifficultyCost = command.kidsRoadDifficultyCost;
    return groupDomain;
  }
}
export class AutoCreateGroupCommand {
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  kidId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolName: string;
  @ApiProperty()
  @IsNotEmpty()
  kidTravelStatus: string;
  @ApiProperty()
  @IsNotEmpty()
  transportationTime: string;
  @ApiProperty()
  @IsNotEmpty()
  categoryCapacity: number;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  numberOfSeat: number;
  @ApiProperty()
  @IsNotEmpty()
  availableSeats: number;
  @ApiProperty()
  isFull: boolean;
  @ApiProperty()
  kidsRoadDifficultyCost: number;
  currentUser: UserInfo;
  static fromCommand(command: CreateGroupCommand): Group {
    const groupDomain = new Group();
    groupDomain.categoryId = command.categoryId;
    groupDomain.name = command.name;
    groupDomain.numberOfSeat = command.numberOfSeat;
    groupDomain.availableSeats = command.availableSeats;
    groupDomain.isFull = command.isFull;
    groupDomain.kidsRoadDifficultyCost = command.kidsRoadDifficultyCost;
    return groupDomain;
  }
}

export class UpdateGroupCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  @IsNotEmpty()
  numberOfSeat: number;
  @ApiProperty()
  @IsNotEmpty()
  availableSeats: number;
  @ApiProperty()
  isFull: boolean;
  @ApiProperty()
  kidsRoadDifficultyCost: number;
  currentUser: UserInfo;
}
export class ArchiveGroupCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
