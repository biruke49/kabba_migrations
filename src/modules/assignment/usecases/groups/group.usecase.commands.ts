import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { GroupRepository } from '@assignment/persistence/groups/group.repository';
import { UserInfo } from '@account/dtos/user-info.dto';
import { ParentCommands } from '@customer/usecases/parents/parent.usecase.commands';
import { ParentQuery } from '@customer/usecases/parents/parent.usecase.queries';
import {
  GroupAssignmentStatus,
  GroupStatus,
  KidTravelStatus,
  ParentStatus,
  TransportationTime,
} from '@libs/common/enums';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  ArchiveGroupAssignmentCommand,
  CalculateGroupAssignmentPaymentCommand,
  CreateGroupAssignmentCommand,
  CreateGroupAssignmentsCommand,
  DeleteGroupAssignmentCommand,
  DeleteKidGroupAssignmentCommand,
  UpdateGroupAndAssignmentDriverPhoneAndNameCommand,
  UpdateGroupAssignmentAmountCommand,
} from './group-assignment.commands';
import { GroupAssignmentResponse } from './group-assignment.response';
import {
  ArchiveGroupCommand,
  AutoCreateGroupCommand,
  CreateGroupCommand,
  UpdateGroupCommand,
} from './group.commands';
import { GroupResponse } from './group.response';
import { GroupQuery } from './group.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { ConfigurationQuery } from '@configurations/usecases/configuration/configuration.usecase.queries';
import { AppService } from 'app.service';

@Injectable()
export class GroupCommands {
  constructor(
    private readonly parentQuery: ParentQuery,
    private groupQuery: GroupQuery,
    private parentCommands: ParentCommands,
    private groupRepository: GroupRepository,
    private configQuery: ConfigurationQuery,
    @InjectRepository(GroupAssignmentEntity)
    private groupAssignmentRepository: Repository<GroupAssignmentEntity>,
    private eventEmitter: EventEmitter2,
    private appService: AppService,
  ) {}
  async createGroup(command: CreateGroupCommand): Promise<GroupResponse> {
    if (
      command.name &&
      (await this.groupRepository.getByNameAndCategory(
        command.name,
        command.categoryId,
        true,
      ))
    ) {
      throw new BadRequestException(
        `Name already exist with category ${command.categoryId}`,
      );
    }
    const groupDomain = CreateGroupCommand.fromCommand(command);
    const group = await this.groupRepository.insert(groupDomain);
    if (group) {
      const createGroupCommand = new CreateGroupCommand();
      createGroupCommand.name = command.name;
      createGroupCommand.categoryId = command.categoryId;
      createGroupCommand.numberOfSeat = command.numberOfSeat;
      createGroupCommand.availableSeats = command.numberOfSeat;
      createGroupCommand.isFull = command.isFull;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: group.id,
        modelName: MODELS.GROUP,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return GroupResponse.fromDomain(group);
  }
  @OnEvent('create.kid.group')
  async autoCreateGroup(command: AutoCreateGroupCommand) {
    //Create group name
    if (command.kidTravelStatus === KidTravelStatus.Alone) {
      const date = new Date().getTime();
      // console.log("🚀 ~ file: group.usecase.commands.ts:94 ~ GroupCommands ~ autoCreateGroup ~ date:", date)
      const groupName = `G-${KidTravelStatus.Alone}-` + date;
      if (
        groupName &&
        (await this.groupRepository.getByNameAndCategory(
          groupName,
          command.categoryId,
          true,
        ))
      ) {
        throw new BadRequestException(
          `Name already exist with category ${command.categoryId}`,
        );
      }
      const createGroupCommand = new CreateGroupCommand();
      createGroupCommand.name = groupName;
      createGroupCommand.categoryId = command.categoryId;
      createGroupCommand.numberOfSeat = command.categoryCapacity;
      createGroupCommand.availableSeats = command.categoryCapacity;
      createGroupCommand.isFull = true;
      createGroupCommand.currentUser = command.currentUser;
      const group = await this.createGroup(createGroupCommand);
      const groupId = group.id;
      // Create Group Assignment
      const createGroupAssignmentsCommand = new CreateGroupAssignmentsCommand();
      createGroupAssignmentsCommand.groupId = groupId;
      createGroupAssignmentsCommand.currentUser = command.currentUser;
      createGroupAssignmentsCommand.assignments = [
        {
          kidId: command.kidId,
          schoolId: command.schoolId,
          parentId: command.parentId,
          schoolName: command.schoolName,
        },
      ];
      const groupAssignment = await this.addGroupAssignments(
        createGroupAssignmentsCommand,
      );
    }
    //If kid is going to travel with siblings
    else if (command.kidTravelStatus === KidTravelStatus.WithSiblings) {
      let groupId = '';
      const query = new CollectionQuery();
      const groupAssignmentCheck =
        await this.groupQuery.getGroupAssignmentByParentCategorySchoolId(
          command.parentId,
          command.categoryId,
          command.schoolId,
          command.transportationTime,
          query,
        );
      if (
        groupAssignmentCheck &&
        groupAssignmentCheck.group.availableSeats > 0
      ) {
        groupId = groupAssignmentCheck.groupId;
      } else {
        const date = new Date().getTime();
        //Create group name
        const groupName = `G-${KidTravelStatus.WithSiblings}-` + date;
        if (
          groupName &&
          (await this.groupRepository.getByNameAndCategory(
            groupName,
            command.categoryId,
            true,
          ))
        ) {
          throw new BadRequestException(
            `Name already exist with category ${command.categoryId}`,
          );
        }
        const createGroupCommand = new CreateGroupCommand();
        createGroupCommand.name = groupName;
        createGroupCommand.categoryId = command.categoryId;
        createGroupCommand.numberOfSeat = command.categoryCapacity;
        createGroupCommand.availableSeats = command.categoryCapacity;
        createGroupCommand.isFull = true;
        createGroupCommand.currentUser = command.currentUser;
        const group = await this.createGroup(createGroupCommand);
        groupId = group.id;
      }
      // Create group assignment
      const createGroupAssignmentsCommand = new CreateGroupAssignmentsCommand();
      createGroupAssignmentsCommand.groupId = groupId;
      createGroupAssignmentsCommand.currentUser = command.currentUser;
      if (
        groupAssignmentCheck &&
        groupAssignmentCheck.group.availableSeats > 0
      ) {
        // this.eventEmitter.emit('update.kid.fee', command.kidId, 0);
        createGroupAssignmentsCommand.assignments = [
          {
            kidId: command.kidId,
            schoolId: command.schoolId,
            parentId: command.parentId,
            schoolName: command.schoolName,
            oldPrice: groupAssignmentCheck.amount,
          },
        ];
      } else {
        createGroupAssignmentsCommand.assignments = [
          {
            kidId: command.kidId,
            schoolId: command.schoolId,
            parentId: command.parentId,
            schoolName: command.schoolName,
          },
        ];
      }
      const groupAssignment = await this.addGroupAssignments(
        createGroupAssignmentsCommand,
      );
    }
    //If kid is goint to travel with other kids
    else {
    }
  }
  @OnEvent('update.group')
  async updateGroup(command: UpdateGroupCommand): Promise<GroupResponse> {
    const groupDomain = await this.groupRepository.getById(command.id);
    if (!groupDomain) {
      throw new NotFoundException(`Group not found with id ${command.id}`);
    }
    if (
      command.name &&
      groupDomain.name !== command.name &&
      (await this.groupRepository.getByName(command.name, true))
    ) {
      throw new BadRequestException(`Group already exist with this name`);
    }
    const oldPayload = { ...groupDomain };
    groupDomain.categoryId = command.categoryId;
    groupDomain.name = command.name;
    groupDomain.status = command.status;
    groupDomain.numberOfSeat = command.numberOfSeat;
    groupDomain.availableSeats = command.availableSeats;
    groupDomain.isFull = command.isFull;
    groupDomain.kidsRoadDifficultyCost = command.kidsRoadDifficultyCost;
    const group = await this.groupRepository.update(groupDomain);
    if (group) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: group.id,
        modelName: MODELS.GROUP,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: { ...oldPayload },
        payload: { ...group },
      });
    }
    return GroupResponse.fromDomain(group);
  }
  async archiveGroup(
    archiveCommand: ArchiveGroupCommand,
  ): Promise<GroupResponse> {
    const groupDomain = await this.groupRepository.getById(archiveCommand.id);
    if (!groupDomain) {
      throw new NotFoundException(
        `Group not found with id ${archiveCommand.id}`,
      );
    }

    groupDomain.deletedAt = new Date();
    groupDomain.deletedBy = archiveCommand.currentUser.id;
    groupDomain.archiveReason = archiveCommand.reason;
    const result = await this.groupRepository.update(groupDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: archiveCommand.id,
        modelName: MODELS.GROUP,
        action: ACTIONS.ARCHIVE,
        userId: archiveCommand.currentUser.id,
        user: archiveCommand.currentUser,
      });
    }
    return GroupResponse.fromDomain(result);
  }
  async restoreGroup(
    id: string,
    currentUser: UserInfo,
  ): Promise<GroupResponse> {
    const groupDomain = await this.groupRepository.getById(id, true);
    if (!groupDomain) {
      throw new NotFoundException(`Group not found with id ${id}`);
    }
    groupDomain.archiveReason = null;
    groupDomain.deletedAt = null;
    groupDomain.deletedBy = null;
    const group = await this.groupRepository.update(groupDomain);
    const r = await this.groupRepository.restore(id);
    if (r) {
      groupDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.GROUP,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return GroupResponse.fromDomain(groupDomain);
  }
  async deleteGroup(id: string, currentUser: UserInfo): Promise<boolean> {
    const groupDomain = await this.groupRepository.getById(id, true);
    if (!groupDomain) {
      throw new NotFoundException(`Group not found with id ${id}`);
    }

    const result = await this.groupRepository.delete(id);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.GROUP,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
  async activateOrBlockGroup(
    id: string,
    currentUser: UserInfo,
  ): Promise<GroupResponse> {
    const groupDomain = await this.groupRepository.getById(id);
    if (!groupDomain) {
      throw new NotFoundException(`Group not found with id ${id}`);
    }
    groupDomain.status =
      groupDomain.status === GroupStatus.Active
        ? GroupStatus.Inactive
        : GroupStatus.Active;
    const result = await this.groupRepository.update(groupDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.GROUP,
        action:
          groupDomain.status === GroupStatus.Inactive
            ? ACTIONS.ACTIVATE
            : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return GroupResponse.fromDomain(result);
  }
  async calculateParentpaymentOfAllKids(
    parentId: string,
    query: CollectionQuery,
  ) {
    query.includes = ['category'];
    let total: number = 0;
    const kids = await this.parentQuery.getKidsWithParentId(parentId, query);
    for (const kid of kids.data) {
      total = Number(total) + Number(kid.kidFee);
    }
    return total;
  }
  async addGroupAssignments(
    command: CreateGroupAssignmentsCommand,
  ): Promise<GroupAssignmentResponse[]> {
    const groupDomainn = await this.groupRepository.getById(command.groupId);
    const groupDomain = await this.groupQuery.getGroup(command.groupId);
    if (!groupDomain) {
      throw new NotFoundException(
        `Group Assignment not found with id ${command.groupId}`,
      );
    }
    if (command.assignments.length > groupDomain.availableSeats) {
      throw new NotFoundException(
        `Number of Kids(${command.assignments.length}) can not be greater that the number of seats(${groupDomain.availableSeats})`,
      );
    }
    for (const c of command.assignments) {
      const kidInfo = await this.parentQuery.getKid(c.kidId);
      if (kidInfo.assigned === true) {
        throw new NotFoundException(
          `${kidInfo.name} has already been assigned`,
        );
      }
      if (kidInfo.categoryId === null) {
        throw new NotFoundException(
          `${kidInfo.name} does not have category selected`,
        );
      }
    }
    for (const c of command.assignments) {
      const kidInfo = await this.parentQuery.getKid(c.kidId);
      const parentId = kidInfo.parent.id;
      //Distance Between Kid And School In KMs
      const distance = kidInfo.distanceFromSchool;
      const initialFee = groupDomain.category.kidInitialFee;
      const config = await this.configQuery.getConfigurations(
        new CollectionQuery(),
      );
      const numOfDays = config.globalConfigurations.kidsNumberOfSchoolDays;
      const perKiloMeterCost = groupDomain.category.kidsPerKilometerCost;
      const roadDifficultyCost = groupDomain.kidsRoadDifficultyCost;
      const capacity = groupDomain.category.capacity;

      //Calculate Amount
      const paymentCalculationCommand =
        new CalculateGroupAssignmentPaymentCommand();
      paymentCalculationCommand.capacity = capacity;
      paymentCalculationCommand.roadDifficultyCost = roadDifficultyCost;
      paymentCalculationCommand.perKiloMeterCost = perKiloMeterCost;
      paymentCalculationCommand.numOfDays = numOfDays;
      paymentCalculationCommand.transportationTime = kidInfo.transportationTime;
      paymentCalculationCommand.kidTravelStatus = kidInfo.kidTravelStatus;
      paymentCalculationCommand.initialFee = initialFee;
      paymentCalculationCommand.distance = distance > 8 ? distance : 8;
      //Calculated Amount
      const paymentAmount =
        await this.appService.calculateGroupAssignmentAmount(
          paymentCalculationCommand,
        );

      const singleGroupAssignmentCommand = new CreateGroupAssignmentCommand();
      singleGroupAssignmentCommand.kidId = c.kidId;
      singleGroupAssignmentCommand.parentId = c.parentId
        ? c.parentId
        : parentId;
      singleGroupAssignmentCommand.groupId = command.groupId;
      singleGroupAssignmentCommand.currentUser = command.currentUser;
      singleGroupAssignmentCommand.schoolId = c.schoolId;
      singleGroupAssignmentCommand.schoolName = c.schoolName;
      singleGroupAssignmentCommand.amount = c.oldPrice ? 0 : paymentAmount;
      const groupAssignment = CreateGroupAssignmentCommand.fromCommand(
        singleGroupAssignmentCommand,
      );
      groupDomainn.addGroupAssignment(groupAssignment);
      const kidAssignment = await this.parentCommands.assignOrUnassignKid(
        c.kidId,
        true,
        command.currentUser,
      );
    }
    const available_seats =
      groupDomainn.availableSeats - command.assignments.length;
    groupDomainn.availableSeats = available_seats;
    if (groupDomainn.availableSeats === 0) {
      groupDomainn.isFull = true;
    }
    const result = await this.groupRepository.update(groupDomainn);
    if (!result) return [];
    for (const c of command.assignments) {
      const kidInfo = await this.parentQuery.getKid(c.kidId);
      const parentId = kidInfo.parent.id;
      this.eventEmitter.emit(
        'calculate.total.payment.of.parent',
        parentId,
        new CollectionQuery(),
      );
    }
    const groupAssignmentResponses = [];
    for (const groupAssignment of result.groupAssignments) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: groupAssignment.id,
        modelName: MODELS.GROUPASSIGNMENT,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
      const groupAssignmentResponse =
        GroupAssignmentResponse.fromDomain(groupAssignment);
      groupAssignmentResponses.push(groupAssignmentResponse);
    }
    return groupAssignmentResponses;
  }
  // async getGroupAssignmentByParentId(
  //   parentId: string,
  //   categoryId: string,
  //   schoolId: string,
  //   query: CollectionQuery,
  // ): Promise<any> {
  //   if (!query.filter) {
  //     query.filter = [];
  //   }
  //   query.filter.push(
  //     [
  //       {
  //         field: 'parentId',
  //         value: parentId,
  //         operator: FilterOperators.EqualTo,
  //       },
  //     ],
  //     [
  //       {
  //         field: 'schoolId',
  //         value: schoolId,
  //         operator: FilterOperators.EqualTo,
  //       },
  //     ],
  //     [
  //       {
  //         field: 'amount',
  //         value: 0,
  //         operator: FilterOperators.GreaterThan,
  //       },
  //     ],
  //     [
  //       {
  //         field: 'kid.categoryId',
  //         value: categoryId,
  //         operator: FilterOperators.EqualTo,
  //       },
  //     ],
  //     [
  //       {
  //         field: 'kid.kidTravelStatus',
  //         value: KidTravelStatus.WithSiblings,
  //         operator: FilterOperators.EqualTo,
  //       },
  //     ],
  //   );
  //   const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
  //     this.groupAssignmentRepository,
  //     query,
  //   );
  //   const result = await dataQuery
  //     .leftJoinAndSelect('group_assignments.kid', 'kid')
  //     .leftJoinAndSelect('group_assignments.group', 'group')
  //     .getOne();
  //   return result;
  // }
  async assignGroupAssignment(id: string): Promise<void> {
    const config = await this.configQuery.getConfigurations(
      new CollectionQuery(),
    );
    const groupAssignment = await this.groupAssignmentRepository.find({
      where: { parentId: id },
      withDeleted: true,
    });
    if (!groupAssignment[0]) {
      throw new NotFoundException(
        `Group Assignment not found with parent id ${id}`,
      );
    }

    for (const grouAss of groupAssignment) {
      grouAss.status = GroupAssignmentStatus.Active;
      grouAss.paidAt = new Date();
      const currentDate = new Date();
      const days = config.globalConfigurations.daysAfterExpiry;
      const addedDate = 30 + days;
      grouAss.expiresAt = new Date(
        currentDate.setDate(currentDate.getDate() + addedDate),
      );
      const result = await this.groupAssignmentRepository.save(grouAss);
    }
    this.eventEmitter.emit('update.parent.status', {
      id: id,
      status: ParentStatus.WaitingToBeAssigned,
    });
    // return GroupAssignmentResponse.fromEntity(result);
  }

  async deleteGroupAssignment(
    command: DeleteGroupAssignmentCommand,
  ): Promise<boolean> {
    const groupAssignment = await this.groupAssignmentRepository.find({
      where: { id: command.id, groupId: command.groupId },
      withDeleted: true,
    });
    if (!groupAssignment[0]) {
      throw new NotFoundException(
        `Group Assignment not found with id ${command.id} and group id ${command.groupId}`,
      );
    }
    const result = await this.groupAssignmentRepository.delete({
      id: command.id,
    });
    if (result) {
      await this.parentCommands.assignOrUnassignKid(
        groupAssignment[0].kidId,
        false,
        command.currentUser,
      );
      const groupDomain = await this.groupRepository.getById(command.groupId);
      if (!groupDomain) {
        throw new NotFoundException(
          `Group not found with id ${command.groupId}`,
        );
      }
      groupDomain.availableSeats = groupDomain.availableSeats + 1;
      const group = await this.groupRepository.update(groupDomain);
      this.eventEmitter.emit(
        'calculate.total.payment.of.parent',
        groupAssignment[0].parentId,
        new CollectionQuery(),
      );
      if (group) {
        this.eventEmitter.emit('activity-logger.store', {
          modelId: groupDomain.id,
          modelName: MODELS.GROUP,
          action: ACTIONS.UPDATE,
          userId: command.currentUser.id,
          user: command.currentUser,
        });
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.GROUPASSIGNMENT,
        action: ACTIONS.DELETE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  @OnEvent('delete.kid.group')
  async deleteKidGroupAssignment(
    command: DeleteKidGroupAssignmentCommand,
  ): Promise<boolean> {
    const groupAssignment = await this.groupAssignmentRepository.find({
      where: { kidId: command.id },
      withDeleted: false,
    });
    if (!groupAssignment[0]) {
      throw new NotFoundException(
        `Group Assignment not found with kid id ${command.id}`,
      );
    }
    const deleteGroupAssignmentCommand = new DeleteGroupAssignmentCommand();
    deleteGroupAssignmentCommand.id = groupAssignment[0].id;
    deleteGroupAssignmentCommand.groupId = groupAssignment[0].groupId;
    deleteGroupAssignmentCommand.currentUser = command.currentUser;
    const result = await this.deleteGroupAssignment(
      deleteGroupAssignmentCommand,
    );
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.GROUPASSIGNMENT,
        action: ACTIONS.DELETE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
      return true;
    }
    return false;
  }
  async archiveGroupAssignment(
    command: ArchiveGroupAssignmentCommand,
  ): Promise<boolean> {
    const groupDomain = await this.groupRepository.getById(command.groupId);
    if (!groupDomain) {
      throw new NotFoundException(
        `Group Assignment does not found with id ${command.groupId}`,
      );
    }
    const groupAssignment = groupDomain.groupAssignments.find(
      (groupAssignment) => groupAssignment.id === command.id,
    );
    groupAssignment.deletedAt = new Date();
    groupAssignment.deletedBy = command.currentUser.id;
    groupAssignment.archiveReason = command.reason;
    groupDomain.updateGroupAssignment(groupAssignment);
    const result = await this.groupRepository.update(groupDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.GROUPASSIGNMENT,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  @OnEvent('update.group.assignment.amount')
  async updateGroupAssignmentAmount(
    command: UpdateGroupAssignmentAmountCommand,
  ): Promise<void> {
    const groupDomain = await this.groupRepository.getById(command.groupId);
    if (!groupDomain) {
      throw new NotFoundException(
        `Group Assignment does not found with id ${command.groupId}`,
      );
    }
    const groupAssignment = groupDomain.groupAssignments.find(
      (groupAssignment) => groupAssignment.id === command.id,
    );
    groupAssignment.amount = command.amount;
    groupDomain.updateGroupAssignment(groupAssignment);
    const result = await this.groupRepository.update(groupDomain);
  }
  @OnEvent('update.group.assignment.driver.phone.and.name')
  async updateGroupAssignmentDriverPhoneAndName(
    command: UpdateGroupAndAssignmentDriverPhoneAndNameCommand,
  ): Promise<void> {
    const groupDomain = await this.groupRepository.getByDriverid(
      command.driverId,
    );
    if (groupDomain) {
      const groupAssignment = groupDomain.groupAssignments.find(
        (groupAssignment) =>
          groupAssignment.id === groupDomain.groupAssignments[0].id,
      );
      groupAssignment.driverName = command.driverName;
      groupAssignment.driverPhone = command.driverPhone;
      groupDomain.updateGroupAssignment(groupAssignment);
      groupDomain.driverName = command.driverName;
      groupDomain.driverPhone = command.driverPhone;
      const result = await this.groupRepository.update(groupDomain);
    }
  }
  async activateOrDeactivateGroupAssignment(
    id: string,
    currentUser: UserInfo,
  ): Promise<GroupAssignmentResponse> {
    const groupAssignment = await this.groupAssignmentRepository.find({
      where: { id: id },
      withDeleted: true,
    });
    if (!groupAssignment[0]) {
      throw new NotFoundException(`Group Assignment not found with id ${id}`);
    }
    groupAssignment[0].status =
      groupAssignment[0].status === GroupAssignmentStatus.Active
        ? GroupAssignmentStatus.Inactive
        : GroupAssignmentStatus.Active;
    const result = await this.groupAssignmentRepository.save(
      groupAssignment[0],
    );
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.GROUPASSIGNMENT,
        action:
          groupAssignment[0].status === GroupAssignmentStatus.Active
            ? ACTIONS.BLOCK
            : ACTIONS.ACTIVATE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return GroupAssignmentResponse.fromEntity(result);
  }
  @OnEvent('activate.or.deactivate.group.assignments')
  async activateOrDeactivateGroupAssignmentEvent(id: string): Promise<any> {
    const groupAssignment = await this.groupAssignmentRepository.find({
      where: { id: id },
      relations: ['group', 'kid', 'group.category'],
      withDeleted: true,
    });
    if (!groupAssignment[0]) {
      throw new NotFoundException(`Group Assignment not found with id ${id}`);
    }
    //Calculated Amount
    const config = await this.configQuery.getConfigurations(
      new CollectionQuery(),
    );
    const numOfDays = config.globalConfigurations.kidsNumberOfSchoolDays;
    const paymentCalculationCommand =
      new CalculateGroupAssignmentPaymentCommand();

    paymentCalculationCommand.capacity = groupAssignment[0].group.numberOfSeat;
    paymentCalculationCommand.roadDifficultyCost =
      groupAssignment[0].group.kidsRoadDifficultyCost;
    paymentCalculationCommand.perKiloMeterCost =
      groupAssignment[0].group.category.kidsPerKilometerCost;
    paymentCalculationCommand.numOfDays = numOfDays;
    paymentCalculationCommand.transportationTime =
      groupAssignment[0].kid.transportationTime;
    paymentCalculationCommand.kidTravelStatus =
      groupAssignment[0].kid.kidTravelStatus;
    paymentCalculationCommand.initialFee =
      groupAssignment[0].group.category.kidInitialFee;
    paymentCalculationCommand.distance =
      groupAssignment[0].kid.distanceFromSchool;
    const paymentAmount = await this.appService.calculateGroupAssignmentAmount(
      paymentCalculationCommand,
    );
    groupAssignment[0].status =
      groupAssignment[0].status === GroupAssignmentStatus.Active
        ? GroupAssignmentStatus.Inactive
        : GroupAssignmentStatus.Active;
    groupAssignment[0].amount = paymentAmount;
    const result = await this.groupAssignmentRepository.save(
      groupAssignment[0],
    );
    if (!result) {
      return false;
    }
    return true;
  }
  async restoreGroupAssignment(
    command: DeleteGroupAssignmentCommand,
  ): Promise<GroupAssignmentResponse> {
    const groupAssignment = await this.groupAssignmentRepository.find({
      where: { id: command.id },
      withDeleted: true,
    });
    if (!groupAssignment[0]) {
      throw new NotFoundException(
        `Group Assignment not found with id ${command.id}`,
      );
    }
    groupAssignment[0].deletedAt = null;
    const result = await this.groupAssignmentRepository.save(
      groupAssignment[0],
    );
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.GROUPASSIGNMENT,
      action: ACTIONS.RESTORE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });

    return GroupAssignmentResponse.fromEntity(result);
  }
}
