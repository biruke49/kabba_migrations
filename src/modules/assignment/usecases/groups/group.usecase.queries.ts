import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import {
  GroupAssignmentStatus,
  KidTravelStatus,
  TransportationTime,
} from '@libs/common/enums';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import {
  GroupAssignmentDriverResponse,
  GroupAssignmentResponse,
} from './group-assignment.response';
import { GroupResponse } from './group.response';
import { ChatQuery } from '@chat/usecases/chat.usecase.queries';
import { GetIdsCommand } from '@chat/usecases/chat.commands';
import { ConfigurationRepository } from '@configurations/persistence/configuration/configuration.repository';
import { JobEntity } from '@job/persistence/jobs/job.entity';

@Injectable()
export class GroupQuery {
  constructor(
    private chatQuery: ChatQuery,
    @InjectRepository(GroupEntity)
    private groupRepository: Repository<GroupEntity>,
    @InjectRepository(GroupAssignmentEntity)
    private groupAssignmentRepository: Repository<GroupAssignmentEntity>,
    private configurationRepository: ConfigurationRepository,
    private eventEmitter: EventEmitter2,
    private dataSource: DataSource,
  ) {}
  async getGroup(id: string, withDeleted = false): Promise<GroupResponse> {
    const parent = await this.groupRepository.find({
      where: { id: id },
      relations: ['groupAssignments', 'category', 'driverAssignments'],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      throw new NotFoundException(`Group not found with id ${id}`);
    }
    return GroupResponse.fromEntity(parent[0]);
  }
  async getUnassignedGroups(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driver_id',
        operator: FilterOperators.IsNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupEntity>(
      this.groupRepository,
      query,
    );
    dataQuery.andWhere(
      `groups.id NOT IN ${dataQuery
        .subQuery()
        .select('group_id')
        .from(JobEntity, 'job')
        .getQuery()}`,
    );
    const d = new DataResponseFormat<GroupResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => GroupResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getGroups(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupResponse>> {
    const dataQuery = QueryConstructor.constructQuery<GroupEntity>(
      this.groupRepository,
      query,
    );
    const d = new DataResponseFormat<GroupResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => GroupResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getGroupsWithNoJobs(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupResponse>> {
    const dataQuery = QueryConstructor.constructQuery<GroupEntity>(
      this.groupRepository,
      query,
    );
    const d = new DataResponseFormat<GroupResponse>();
    if (query.count) {
      d.count = await dataQuery
        .leftJoin('groups.jobs', 'job')
        .where('job.groupId IS NULL')
        .getCount();
    } else {
      const [result, total] = await dataQuery
        .leftJoin('groups.jobs', 'job')
        .where('job.groupId IS NULL')
        .getManyAndCount();
      d.data = result.map((entity) => GroupResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedGroups(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupEntity>(
      this.groupRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<GroupResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => GroupResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getGroupsWithCategoryId(
    categoryId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'categoryId',
        value: categoryId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupEntity>(
      this.groupRepository,
      query,
    );
    const d = new DataResponseFormat<GroupResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => GroupResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  //Group Assignments
  async getGroupAssignments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupAssignmentResponse>> {
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<GroupAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        GroupAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getGroupAssignment(
    id: string,
    withDeleted = false,
  ): Promise<GroupAssignmentResponse> {
    const groupAssignment = await this.groupAssignmentRepository.find({
      where: { id: id },
      relations: ['group', 'school', 'kid'],
      withDeleted: withDeleted,
    });
    if (groupAssignment.length === 0) {
      throw new NotFoundException(`Group assignment not found with id ${id}`);
    }
    const item = groupAssignment[0];
    const response = GroupAssignmentResponse.fromEntity(item);
    return response;
  }
  async getGroupAssignmentByGroupId(
    groupId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'groupId',
        value: groupId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<GroupAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        GroupAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getGroupAssignmentByParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'parentId',
        value: parentId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<GroupAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        GroupAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getGroupAssignmentDriversByParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<GroupAssignmentDriverResponse[]> {
    query.select = [];
    query.select.push(
      `driver.name as name`,
      `driver_id as driverId`,
      `driver.profile_image as profileImage`,
      `driver.address as address`,
      `driver.email as email`,
      `driver.phone_number as phoneNumber`,
      `driver.gender as gender`,
      `driver.enabled as enabled`,
    );
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'parentId',
          value: parentId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'status',
          value: 'Active',
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'driverId',
          operator: FilterOperators.NotNull,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );

    const data = await dataQuery
      .leftJoin('group_assignments.driver', 'driver')
      .getRawMany();
    const countResponses = [];
    for (const c of data) {
      const getIdsCommand = new GetIdsCommand();
      const newQuery = new CollectionQuery();
      getIdsCommand.receiverId = parentId;
      getIdsCommand.senderId = c.driverid;
      const unseen =
        await this.chatQuery.getTotalUnseenChatBySenderIdAndReceiverId(
          getIdsCommand,
          newQuery,
        );
      if (c.driverid) {
        const countResponse = new GroupAssignmentDriverResponse();
        countResponse.driverId = c.driverid;
        countResponse.name = c.name;
        countResponse.profileImage = c.profileimage;
        countResponse.address = c.address;
        countResponse.email = c.email;
        countResponse.phoneNumber = c.phonenumber;
        countResponse.gender = c.gender;
        countResponse.enabled = c.enabled;
        countResponse.unseenTexts = unseen.total;

        countResponses.push(countResponse);
      }
    }
    return countResponses;
  }
  //   async getAssignedByParentId(
  //   parentId: string,
  //   withDeleted: boolean,
  // ): Promise<GroupResponse> {
  //   console.log("🚀 ~ file: group.repository.ts:173 ~ GroupRepository ~ parentId:", parentId)
  //   const group = await this.groupAssignmentRepository.find({
  //     where: { parentId: parentId, driverId: Not(null) },
  //     relations: [],
  //     withDeleted: withDeleted,
  //   });
  //   if (!group[0]) {
  //     return null;
  //   }
  //   return GroupResponse.fromEntity(parent[0]);
  // }
  async getAssignedByParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'parentId',
          value: parentId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'driverId',
          operator: FilterOperators.NotNull,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<GroupAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        GroupAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getUnssignedByParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'parentId',
          value: parentId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'driverId',
          operator: FilterOperators.IsNull,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<GroupAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        GroupAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getGroupAssignmentByKidId(
    kidId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'kidId',
        value: kidId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<GroupAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        GroupAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  @OnEvent('calculate.total.payment.of.parent')
  async calculateTotalPaymentOfParent(
    parentId: string,
    query: CollectionQuery,
  ): Promise<number> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'parentId',
          value: parentId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'status',
          value: 'Inactive',
          operator: FilterOperators.EqualTo,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    let totalAmount = 0;
    const result = await dataQuery.getMany();
    for (const res of result) {
      totalAmount += res.amount;
    }
    this.eventEmitter.emit('update.parent.payment', parentId, totalAmount);
    return totalAmount;
  }
  async getGroupAssignmentByParentCategorySchoolId(
    parentId: string,
    categoryId: string,
    schoolId: string,
    transportationTime: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'parentId',
          value: parentId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'schoolId',
          value: schoolId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'amount',
          value: 0,
          operator: FilterOperators.GreaterThan,
        },
      ],
      [
        {
          field: 'kid.categoryId',
          value: categoryId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'kid.kidTravelStatus',
          value: KidTravelStatus.WithSiblings,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'kid.transportationTime',
          value: transportationTime,
          operator: FilterOperators.EqualTo,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const result = await dataQuery
      .leftJoinAndSelect('group_assignments.kid', 'kid')
      .leftJoinAndSelect('group_assignments.group', 'group')
      .getOne();
    return result;
  }
  async getArchivedGroupAssignments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<GroupAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<GroupAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        GroupAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getExpiredGroupAssignments(query: CollectionQuery): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'status',
        value: GroupAssignmentStatus.Active,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const result = await dataQuery
      .andWhere(
        `to_char(expires_at,'YYYY-MM-DD')<= to_char(current_date,'YYYY-MM-DD')`,
      )
      .getMany();
    if (result) {
      for (const groupAss of result) {
        const parentId = groupAss.parentId;
        const notificationData = {
          title: 'Payment Expired',
          body: `Your Kabba Kids payment is due, Please pay today`,
          receiver: parentId,
        };
        this.eventEmitter.emit('create.notification', notificationData);
        this.eventEmitter.emit(
          'activate.or.deactivate.group.assignments',
          groupAss.id,
        );
      }
      return result;
    }
  }
  async getExpiredGroupAssignmentsToNotify(
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    const config = await this.configurationRepository.getAll(false);
    let daysAfterExpiry = 0;
    for (const c of config) {
      daysAfterExpiry = c.globalConfigurations.daysAfterExpiry;
    }
    query.filter.push([
      {
        field: 'status',
        value: GroupAssignmentStatus.Active,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const result = await dataQuery
      .andWhere(
        `Date (to_char(expires_at,'YYYY-MM-DD'))-current_date <= ${daysAfterExpiry}`,
      )
      .andWhere(`Date (to_char(expires_at,'YYYY-MM-DD'))-current_date > 0`)
      .getMany();
    if (result) {
      for (const groupAss of result) {
        const parentId = groupAss.parentId;
        const expiryDate = groupAss.expiresAt;
        const month = expiryDate.toLocaleString('en-US', { month: 'long' });
        const date = expiryDate.getDate();
        const year = expiryDate.getFullYear();
        const finalDate = `${month} ${date}, ${year}`;
        const notificationData = {
          title: 'Payment about to expire',
          body: `Your next Kabba Kids payment is due on ${finalDate}`,
          receiver: parentId,
        };
        this.eventEmitter.emit('create.notification', notificationData);
      }
      return true;
    }
  }
}
