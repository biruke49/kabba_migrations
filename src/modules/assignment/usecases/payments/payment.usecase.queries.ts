import { PaymentEntity } from '@assignment/persistence/payments/payment.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentResponse } from './payment.response';
import { PaymentStatus } from '@libs/common/enums';
import { CountEarningsByCreatedAtResponse } from '@order/usecases/bookings/booking.response';

@Injectable()
export class PaymentQuery {
  constructor(
    @InjectRepository(PaymentEntity)
    private paymentRepository: Repository<PaymentEntity>,
  ) {}
  async getPayment(id: string, withDeleted = false): Promise<PaymentResponse> {
    const city = await this.paymentRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!city[0]) {
      throw new NotFoundException(`Payment not found with id ${id}`);
    }
    return PaymentResponse.fromEntity(city[0]);
  }
  async getPayments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PaymentResponse>> {
    const dataQuery = QueryConstructor.constructQuery<PaymentEntity>(
      this.paymentRepository,
      query,
    );
    const d = new DataResponseFormat<PaymentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PaymentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getPaymentByParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PaymentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'parentId',
        value: parentId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<PaymentEntity>(
      this.paymentRepository,
      query,
    );
    const d = new DataResponseFormat<PaymentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PaymentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getCompanyEarning(query: CollectionQuery): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'status',
          operator: FilterOperators.EqualTo,
          value: PaymentStatus.Success,
        },
      ],
      [
        {
          field: 'is_paid',
          operator: FilterOperators.EqualTo,
          value: true,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<PaymentEntity>(
      this.paymentRepository,
      query,
    );
    const data = await dataQuery
      .select('SUM(company_earning)', 'total')
      .getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async groupEarningsByCreatedDate(
    query: CollectionQuery,
    format: string,
  ): Promise<CountEarningsByCreatedAtResponse[]> {
    //const now = new Date();
    // const startOfYear = new Date(now.getFullYear(), 0, 1);
    // const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    let formatQ = '';
    if (format === 'year') {
      formatQ = 'YYYY';
    } else if (format === 'month') {
      formatQ = 'YYYY-MM';
    } else if (format === 'date') {
      formatQ = 'YYYY-MM-dd';
    }
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'status',
          value: PaymentStatus.Success,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'is_paid',
          value: true,
          operator: FilterOperators.EqualTo,
        },
      ],
    );
    // query.select = [];
    // query.select.push(
    //   `to_char(payments.created_at,'${formatQ}') as created_date`,
    //   'SUM(payments.company_earning)',
    // );
    const dataQuery = QueryConstructor.constructQuery<PaymentEntity>(
      this.paymentRepository,
      query,
    );
    const data = await dataQuery
      .leftJoinAndSelect('payments.parent', 'parent')
      .leftJoinAndSelect('parent.kids', 'kid')
      .select('SUM(payments.companyEarning)', 'companyEarning')
      .addSelect(`to_char(payments.created_at,'${formatQ}') as created_date`)
      .groupBy(`created_date`)
      .orderBy('created_date', 'DESC')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountEarningsByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.total = d.companyEarning;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupEarningsByCategory(query: CollectionQuery): Promise<any[]> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'status',
          operator: FilterOperators.EqualTo,
          value: PaymentStatus.Success,
        },
      ],
      [
        {
          field: 'is_paid',
          operator: FilterOperators.EqualTo,
          value: true,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<PaymentEntity>(
      this.paymentRepository,
      query,
    );
    const data = await dataQuery
      .leftJoinAndSelect('payments.parent', 'parent')
      .leftJoinAndSelect('parent.kids', 'kid')
      .select('SUM(payments.companyEarning)', 'companyEarning')
      .addSelect('kid.categoryId', 'categoryId')
      .addSelect('kid.categoryName', 'categoryName')
      .groupBy('kid.categoryId')
      .addGroupBy('kid.categoryName')
      .limit(query.top)
      .offset(query.skip)
      .getRawMany();
    return data;
  }
  async getArchivedPayments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PaymentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<PaymentEntity>(
      this.paymentRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<PaymentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PaymentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
