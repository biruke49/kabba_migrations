import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { PaymentRepository } from '@assignment/persistence/payments/payment.repository';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PaymentStatus } from '@libs/common/enums';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { GroupCommands } from '../groups/group.usecase.commands';
import {
  ArchivePaymentCommand,
  CreatePaymentCommand,
  UpdatePaymentCommand,
} from './payment.commands';
import { InitiateTeleBirrResponse, PaymentResponse } from './payment.response';
import { ConfigurationQuery } from '@configurations/usecases/configuration/configuration.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';

@Injectable()
export class PaymentCommands {
  constructor(
    private paymentRepository: PaymentRepository,
    private groupCommands: GroupCommands,
    private readonly eventEmitter: EventEmitter2,
    private configQuery: ConfigurationQuery,
  ) {}
  async createPayment(command: CreatePaymentCommand): Promise<PaymentResponse> {
    command.amount = Number(command.amount.toFixed(2));
    const config = await this.configQuery.getConfigurations(
      new CollectionQuery(),
    );
    const driverCommission = config.globalConfigurations.kidsDriverCommission;
    const driverCommissionAmount = (command.amount * driverCommission) / 100;
    command.companyEarning = Number(
      (command.amount - driverCommissionAmount).toFixed(2),
    );
    const paymentDomain = CreatePaymentCommand.fromCommand(command);
    const payment = await this.paymentRepository.insert(paymentDomain);
    return PaymentResponse.fromDomain(payment);
  }
  @OnEvent('create.payment')
  async createNewPayment(
    command: CreatePaymentCommand,
  ): Promise<PaymentResponse> {
    command.amount = Number(command.amount.toFixed(2));
    const config = await this.configQuery.getConfigurations(
      new CollectionQuery(),
    );
    const driverCommission = config.globalConfigurations.kidsDriverCommission;
    const driverCommissionAmount = (command.amount * driverCommission) / 100;
    command.companyEarning = Number(
      (command.amount - driverCommissionAmount).toFixed(2),
    );
    const paymentDomain = CreatePaymentCommand.fromCommand(command);
    const payment = await this.paymentRepository.insert(paymentDomain);
    if (payment) {
      this.eventEmitter.emit('pay.manually', payment.id, command);
    }
    return PaymentResponse.fromDomain(payment);
  }
  // async updatePayment(command: UpdatePaymentCommand): Promise<PaymentResponse> {
  //   const transactionDomain = await this.paymentRepository.getById(command.id);
  //   if (!transactionDomain) {
  //     throw new NotFoundException(
  //       `Transaction not found with id ${command.id}`,
  //     );
  //   }
  //   transactionDomain.amount = command.amount;
  //   transactionDomain.status = command.status;
  //   transactionDomain.transactionNumber = command.transactionNumber;
  //   transactionDomain.updatedAt = new Date();
  //   const transaction = await this.paymentRepository.update(transactionDomain);
  //   return PaymentResponse.fromDomain(transaction);
  // }
  async archivePayment(command: ArchivePaymentCommand): Promise<boolean> {
    const paymentDomain = await this.paymentRepository.getById(command.id);
    if (!paymentDomain) {
      throw new NotFoundException(`Payment not found with id ${command.id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.PAYMENT,
      action: ACTIONS.ARCHIVE,
      userId: command.currentUser.id,
      user: command.currentUser,
      reason: command.reason,
    });
    return await this.paymentRepository.archive(command.id);
  }
  async restorePayment(
    id: string,
    currentUser: UserInfo,
  ): Promise<PaymentResponse> {
    const paymentDomain = await this.paymentRepository.getById(id, true);
    if (!paymentDomain) {
      throw new NotFoundException(`Payment not found with id ${id}`);
    }
    const r = await this.paymentRepository.restore(id);
    if (r) {
      paymentDomain.deletedAt = null;
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.PAYMENT,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    return PaymentResponse.fromDomain(paymentDomain);
  }
  async deletePayment(id: string, currentUser: UserInfo): Promise<boolean> {
    const paymentDomain = await this.paymentRepository.getById(id, true);
    if (!paymentDomain) {
      throw new NotFoundException(`Payment not found with id ${id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.PAYMENT,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    return await this.paymentRepository.delete(id);
  }
  async approveChapaTransaction(paymentId: string, verificationData) {
    const paymentDomain = await this.paymentRepository.getById(paymentId);
    if (!paymentDomain) {
      throw new NotFoundException(`Payment not found with id ${paymentId}`);
    }
    if (paymentDomain) {
      paymentDomain.status = PaymentStatus.Success;
      paymentDomain.isPaid = true;
      paymentDomain.paidAt = new Date();
      paymentDomain.transactionNumber = verificationData.data.reference;
      await this.paymentRepository.update(paymentDomain);
      await this.groupCommands.assignGroupAssignment(verificationData.parentId);
    }
  }
  @OnEvent('pay.manually')
  async approveManualransaction(paymentId: string, verificationData) {
    const paymentDomain = await this.paymentRepository.getById(paymentId);
    if (!paymentDomain) {
      throw new NotFoundException(`Payment not found with id ${paymentId}`);
    }
    if (paymentDomain) {
      paymentDomain.status = PaymentStatus.Success;
      paymentDomain.isPaid = true;
      paymentDomain.paidAt = new Date();
      await this.paymentRepository.update(paymentDomain);
      await this.groupCommands.assignGroupAssignment(verificationData.parentId);
    }
  }
  // async declineChapaTransaction(paymentId: string, status: string) {
  //   const paymentDomain = await this.paymentRepository.getById(paymentId);
  //   if (!paymentDomain) {
  //     throw new NotFoundException(`Payment not found with id ${paymentId}`);
  //   }
  //   if (paymentDomain) {
  //     paymentDomain.status = status;
  //     paymentDomain.isPaid = false;
  //     paymentDomain.paidAt = new Date();
  //     await this.paymentRepository.update(paymentDomain);
  //   }
  // }

  async initiateTeleBirrPayment(
    payload: CreatePaymentCommand,
  ): Promise<InitiateTeleBirrResponse> {
    const transaction = await this.createPayment(payload);
    const response = new InitiateTeleBirrResponse();
    response.transactionId = transaction.id;
    response.outTradeNumber = transaction.id + ',parent';
    response.message = '';
    response.code = '200';
    response.appId = process.env.AppId;
    response.receiverName = process.env.ReceiverName;
    response.shortCode = process.env.ShortCode;
    response.inAppPaymentUrl = process.env.InAppPaymentUrl;
    response.webPaymentUrl = process.env.WebPaymentUrl;
    response.subject = 'Pay for kids with Telebirr';
    response.returnUrl = process.env.ReturnUrl;
    response.notifyUrl = `${process.env.API_URL}/api/${process.env.NotifyUrl}`;
    response.timeoutExpress = process.env.TimeoutExpress;
    response.appKey = process.env.AppKey;
    response.publicKey = process.env.PublicKey;
    return response;
  }
  @OnEvent('update.parent.transaction')
  async updateParentTransaction(
    command: UpdatePaymentCommand,
    currentUser: UserInfo,
  ): Promise<PaymentResponse> {
    console.log("🚀 ~ file: payment.usecase.commads.ts:191 ~ PaymentCommands ~ command:", command)
    const transactionDomain = await this.paymentRepository.getById(command.id);
    if (!transactionDomain) {
      throw new NotFoundException(`Payment not found with id ${command.id}`);
    }
    transactionDomain.amount = command.amount;
    transactionDomain.status = PaymentStatus.Success;
    transactionDomain.isPaid = true;
    transactionDomain.paidAt = new Date();
    transactionDomain.tradeDate = command.tradeDate;
    transactionDomain.tradeNumber = command.tradeNumber;
    transactionDomain.transactionNumber = command.transactionNumber;
    transactionDomain.updatedAt = new Date();
    const transaction = await this.paymentRepository.update(transactionDomain);
    console.log("🚀 ~ file: payment.usecase.commads.ts:205 ~ PaymentCommands ~ transactionDomain:", transactionDomain)
    await this.groupCommands.assignGroupAssignment(transactionDomain.parentId);
    return PaymentResponse.fromDomain(transaction);
  }
}
