import { Payment } from '@assignment/domains/payments/payment';
import { PaymentEntity } from '@assignment/persistence/payments/payment.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ParentResponse } from '@customer/usecases/parents/parent.response';
import { DepositedBy } from '@assignment/domains/payments/deposited-by';

export class PaymentResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  parentId: string;
  @ApiProperty()
  amount: number;
  @ApiProperty()
  companyEarning: number;
  @ApiProperty()
  isPaid: boolean;
  @ApiProperty()
  status: string;
  @ApiProperty()
  paidAt: Date;
  @ApiProperty()
  expiresAt: Date;
  @ApiProperty()
  depositedBy?: DepositedBy;
  @ApiProperty()
  tradeDate?: Date;
  @ApiProperty()
  tradeNumber?: string;
  @ApiProperty()
  transactionNumber?: string;
  @ApiProperty()
  method?: string;
  @ApiProperty()
  reason: string;
  parent: ParentResponse;
  static fromEntity(groupEntity: PaymentEntity): PaymentResponse {
    const groupResponse = new PaymentResponse();
    groupResponse.id = groupEntity.id;
    groupResponse.parentId = groupEntity.parentId;
    groupResponse.amount = groupEntity.amount;
    groupResponse.companyEarning = groupEntity.companyEarning;
    groupResponse.isPaid = groupEntity.isPaid;
    groupResponse.status = groupEntity.status;
    groupResponse.paidAt = groupEntity.paidAt;
    groupResponse.expiresAt = groupEntity.expiresAt;
    groupResponse.depositedBy = groupEntity.depositedBy;
    groupResponse.tradeDate = groupEntity.tradeDate;
    groupResponse.transactionNumber = groupEntity.transactionNumber;
    groupResponse.tradeNumber = groupEntity.tradeNumber;
    groupResponse.method = groupEntity.method;
    groupResponse.reason = groupEntity.reason;
    if (groupEntity.parent) {
      groupResponse.parent = ParentResponse.fromEntity(groupEntity.parent);
    }
    return groupResponse;
  }
  static fromDomain(group: Payment): PaymentResponse {
    const groupResponse = new PaymentResponse();
    groupResponse.id = group.id;
    groupResponse.parentId = group.parentId;
    groupResponse.amount = group.amount;
    groupResponse.companyEarning = group.companyEarning;
    groupResponse.isPaid = group.isPaid;
    groupResponse.status = group.status;
    groupResponse.paidAt = group.paidAt;
    groupResponse.expiresAt = group.expiresAt;
    groupResponse.depositedBy = group.depositedBy;
    groupResponse.tradeDate = group.tradeDate;
    groupResponse.transactionNumber = group.transactionNumber;
    groupResponse.tradeNumber = group.tradeNumber;
    groupResponse.method = group.method;
    groupResponse.reason = group.reason;
    if (group.parent) {
      groupResponse.parent = ParentResponse.fromDomain(group.parent);
    }
    return groupResponse;
  }
}

export class InitiateTeleBirrResponse {
  @ApiProperty()
  transactionId: string;
  @ApiProperty()
  outTradeNumber: string;
  @ApiProperty()
  message?: string;
  @ApiProperty()
  code?: string;
  @ApiProperty()
  appId: string;
  @ApiProperty()
  receiverName: string;
  @ApiProperty()
  shortCode: string;
  @ApiProperty()
  inAppPaymentUrl: string;
  @ApiProperty()
  webPaymentUrl: string;
  @ApiProperty()
  subject: string;
  @ApiProperty()
  returnUrl: string;
  @ApiProperty()
  notifyUrl: string;
  @ApiProperty()
  timeoutExpress: string;
  @ApiProperty()
  appKey: string;
  @ApiProperty()
  publicKey: string;
}
