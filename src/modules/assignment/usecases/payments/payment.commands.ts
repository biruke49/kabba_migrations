import { DepositedBy } from '@assignment/domains/payments/deposited-by';
import { Payment } from '@assignment/domains/payments/payment';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PaymentStatus } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPositive } from 'class-validator';

export class CreatePaymentCommand {
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  @ApiProperty()
  companyEarning: number;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  depositedBy?: DepositedBy;
  @ApiProperty()
  tradeDate?: Date;
  @ApiProperty()
  tradeNumber?: string;
  @ApiProperty()
  transactionNumber?: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  method?: string;
  returnUrl?: string;
  reason?: string;
  static fromCommand(command: CreatePaymentCommand): Payment {
    const transaction = new Payment();
    transaction.parentId = command.parentId;
    transaction.amount = command.amount;
    transaction.companyEarning = command.companyEarning;
    transaction.tradeDate = command.tradeDate;
    transaction.transactionNumber = command.transactionNumber;
    transaction.status = command.status;
    transaction.method = command.method;
    transaction.depositedBy = command.depositedBy;
    transaction.tradeNumber = command.tradeNumber;
    transaction.reason = command.reason;
    return transaction;
  }
}
export class UpdatePaymentCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  @ApiProperty()
  companyEarning: number;
  @ApiProperty()
  tradeDate: Date;
  @ApiProperty()
  tradeNumber: string;
  @ApiProperty()
  transactionNumber?: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  method?: string;
  returnUrl?: string;
  reason?: string;
  parentId?: string;
}
export class ArchivePaymentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class PayWithChapaCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  amount: number;
  parentId: string;
  depositedBy: DepositedBy;
  status: string;
  method: string;
  @ApiProperty()
  returnUrl: string;
  reason?: string;
  static fromCommand(command: PayWithChapaCommand): Payment {
    const transaction = new Payment();
    transaction.parentId = command.parentId;
    transaction.amount = command.amount;
    transaction.status = command.status;
    transaction.method = command.method;
    transaction.depositedBy = command.depositedBy;
    transaction.reason = command.reason;
    return transaction;
  }
}
export class PayManuallyCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  amount: number;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
}
