import { DriverAssignment } from '@assignment/domains/driver-assignments/driver-assignment';
import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';
import { ApiProperty } from '@nestjs/swagger';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { GroupResponse } from '../groups/group.response';

export class DriverAssignmentResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  driverName: string;
  @ApiProperty()
  driverPhone: string;
  @ApiProperty()
  groupId: string;
  @ApiProperty()
  jobId: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  driver?: DriverResponse;
  group?: GroupResponse;
  static fromEntity(
    driverAssignmentEntity: DriverAssignmentEntity,
  ): DriverAssignmentResponse {
    const driverAssignmentResponse = new DriverAssignmentResponse();
    driverAssignmentResponse.id = driverAssignmentEntity.id;
    driverAssignmentResponse.driverId = driverAssignmentEntity.driverId;
    driverAssignmentResponse.driverName = driverAssignmentEntity.driverName;
    driverAssignmentResponse.driverPhone = driverAssignmentEntity.driverPhone;
    driverAssignmentResponse.groupId = driverAssignmentEntity.groupId;
    driverAssignmentResponse.jobId = driverAssignmentEntity.jobId;
    driverAssignmentResponse.status = driverAssignmentEntity.status;
    driverAssignmentResponse.archiveReason =
      driverAssignmentEntity.archiveReason;
    driverAssignmentResponse.createdBy = driverAssignmentEntity.createdBy;
    driverAssignmentResponse.updatedBy = driverAssignmentEntity.updatedBy;
    driverAssignmentResponse.deletedBy = driverAssignmentEntity.deletedBy;
    driverAssignmentResponse.createdAt = driverAssignmentEntity.createdAt;
    driverAssignmentResponse.updatedAt = driverAssignmentEntity.updatedAt;
    driverAssignmentResponse.deletedAt = driverAssignmentEntity.deletedAt;
    if (driverAssignmentEntity.driver) {
      driverAssignmentResponse.driver = DriverResponse.fromEntity(
        driverAssignmentEntity.driver,
      );
    }
    if (driverAssignmentEntity.group) {
      driverAssignmentResponse.group = GroupResponse.fromEntity(
        driverAssignmentEntity.group,
      );
    }
    return driverAssignmentResponse;
  }
  static fromDomain(
    driverAssignment: DriverAssignment,
  ): DriverAssignmentResponse {
    const driverAssignmentResponse = new DriverAssignmentResponse();
    driverAssignmentResponse.id = driverAssignment.id;
    driverAssignmentResponse.driverId = driverAssignment.driverId;
    driverAssignmentResponse.driverName = driverAssignment.driverName;
    driverAssignmentResponse.driverPhone = driverAssignment.driverPhone;
    driverAssignmentResponse.groupId = driverAssignment.groupId;
    driverAssignmentResponse.jobId = driverAssignment.jobId;
    driverAssignmentResponse.status = driverAssignment.status;
    driverAssignmentResponse.archiveReason = driverAssignment.archiveReason;
    driverAssignmentResponse.createdBy = driverAssignment.createdBy;
    driverAssignmentResponse.updatedBy = driverAssignment.updatedBy;
    driverAssignmentResponse.deletedBy = driverAssignment.deletedBy;
    driverAssignmentResponse.createdAt = driverAssignment.createdAt;
    driverAssignmentResponse.updatedAt = driverAssignment.updatedAt;
    driverAssignmentResponse.deletedAt = driverAssignment.deletedAt;
    if (driverAssignment.driver) {
      driverAssignmentResponse.driver = DriverResponse.fromDomain(
        driverAssignment.driver,
      );
    }
    if (driverAssignment.group) {
      driverAssignmentResponse.group = GroupResponse.fromDomain(
        driverAssignment.group,
      );
    }
    return driverAssignmentResponse;
  }
}
