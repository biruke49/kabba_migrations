import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { DriverAssignmentRepository } from '@assignment/persistence/driver-assignments/driver-assignment.repository';
import { GroupRepository } from '@assignment/persistence/groups/group.repository';
import { UserInfo } from '@account/dtos/user-info.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { UpdateGroupAssignmentCommand } from '../groups/group-assignment.commands';
import {
  ArchiveDriverAssignmentCommand,
  CreateDriverAssignmentCommand,
  UpdateDriverAssignmentCommand,
} from './driver-assignment.commands';
import { DriverAssignmentResponse } from './driver-assignment.response';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GroupQuery } from '../groups/group.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { JobRepository } from '@job/persistence/jobs/job.repository';
import { JobStatus, ParentStatus } from '@libs/common/enums';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { AppService } from 'app.service';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';

@Injectable()
export class DriverAssignmentCommands {
  constructor(
    private driverAssignmentRepository: DriverAssignmentRepository,
    private groupRepository: GroupRepository,
    private jobRepository: JobRepository,
    private groupQuery: GroupQuery,
    @InjectRepository(GroupAssignmentEntity)
    private groupAssignmentRepository: Repository<GroupAssignmentEntity>,
    private eventEmitter: EventEmitter2,
    private accountRepository: AccountRepository,
    private appService: AppService,
    private driverRepository: DriverRepository,
  ) {}
  async createDriverAssignment(
    command: CreateDriverAssignmentCommand,
  ): Promise<DriverAssignmentResponse> {
    const existingDriverAssignment =
      await this.driverAssignmentRepository.getByDriverIdAndGroupId(
        command.driverId,
        command.groupId,
      );
    const driverDomain = await this.driverRepository.getById(command.driverId);
    if (existingDriverAssignment) {
      throw new NotFoundException(`Driver has already been assigned to group`);
    }
    const driverAssignmentDomain =
      CreateDriverAssignmentCommand.fromCommand(command);
    driverAssignmentDomain.createdBy = command.currentUser.id;
    driverAssignmentDomain.driverName = driverDomain.name;
    driverAssignmentDomain.driverPhone = driverDomain.phoneNumber;
    const driverAssignment = await this.driverAssignmentRepository.insert(
      driverAssignmentDomain,
    );
    if (driverAssignment) {
      //Update Driver Id in group Assignment
      const groupDomain = await this.groupRepository.getById(command.groupId);
      groupDomain.driverId = command.driverId;
      groupDomain.driverName = driverDomain.name;
      groupDomain.driverPhone = driverDomain.phoneNumber;
      //Assign Driver To Job
      if (command.jobId) {
        this.eventEmitter.emit('open.or.close.job', {
          id: command.jobId,
          status: JobStatus.Assigned,
          currentUser: command.currentUser,
        });
        const jobDomain = await this.jobRepository.getById(command.jobId);
        jobDomain.driverId = command.driverId;
        await this.jobRepository.update(jobDomain);
      }
      for (const groupAss of groupDomain.groupAssignments) {
        const groupAssignmentCommand = new UpdateGroupAssignmentCommand();
        groupAssignmentCommand.id = groupAss.id;
        groupAssignmentCommand.groupId = groupAss.groupId;
        groupAssignmentCommand.schoolId = groupAss.schoolId;
        groupAssignmentCommand.kidId = groupAss.kidId;
        groupAssignmentCommand.status = groupAss.status;
        groupAssignmentCommand.driverId = command.driverId;
        groupAssignmentCommand.driverName = driverDomain.name;
        groupAssignmentCommand.driverPhone = driverDomain.phoneNumber;
        const groupAssignment = UpdateGroupAssignmentCommand.fromCommand(
          groupAssignmentCommand,
        );
        groupDomain.updateGroupAssignment(groupAssignment);
        let check = true;
        const parentGroupAssignments =
          await this.groupQuery.getUnssignedByParentId(
            groupAss.parentId,
            new CollectionQuery(),
          );
        if (parentGroupAssignments.data && parentGroupAssignments.data.length > 0) {
          const parentStatus = {
            id: groupAss.parentId,
            status: ParentStatus.PartiallyAssigned,
            currentUser: command.currentUser,
          };
          this.eventEmitter.emit('update.parent.status', parentStatus);
        } else {
          const parentStatus = {
            id: groupAss.parentId,
            status: ParentStatus.Assigned,
            currentUser: command.currentUser,
          };
          this.eventEmitter.emit('update.parent.status', parentStatus);
        }
      }
      const result = await this.groupRepository.update(groupDomain);
      const notificationData = {
        title: 'Group Assignment',
        body: `Group '${groupDomain.name}' has been assigned to you`,
        receiver: command.driverId,
      };
      this.eventEmitter.emit('create.notification', notificationData);
      const groupAssi = await this.groupRepository.getByGroupId(groupDomain.id);
      //Send notification to parent
      const notificationParentData = {
        title: 'Group Assignment',
        body: `Driver has been assigned to group '${groupDomain.name}'`,
        receiver: groupAssi.parentId,
      };
      this.eventEmitter.emit('create.notification', notificationParentData);
    }
    return DriverAssignmentResponse.fromDomain(driverAssignment);
  }
  async updateDriverAssignment(
    command: UpdateDriverAssignmentCommand,
  ): Promise<DriverAssignmentResponse> {
    const driverAssignmentDomain =
      await this.driverAssignmentRepository.getById(command.id);
    const driverDomain = await this.driverRepository.getById(command.driverId);
    if (!driverAssignmentDomain) {
      throw new NotFoundException(
        `Driver Assignment not found with id ${command.id}`,
      );
    }
    const oldPayload = { ...driverAssignmentDomain };
    driverAssignmentDomain.driverId = command.driverId;
    driverAssignmentDomain.driverName = driverDomain.name;
    driverAssignmentDomain.driverPhone = driverDomain.phoneNumber;
    driverAssignmentDomain.groupId = command.groupId;
    driverAssignmentDomain.status = command.status;
    driverAssignmentDomain.updatedBy = command.currentUser.id;
    const driverAssignment = await this.driverAssignmentRepository.update(
      driverAssignmentDomain,
    );
    if (driverAssignment) {
      const groupAssignments =
        await this.groupQuery.getGroupAssignmentByGroupId(
          command.groupId,
          new CollectionQuery(),
        );
      const group = await this.groupRepository.getById(command.groupId);
      group.driverId = command.driverId;
      group.driverName = driverDomain.name;
      group.driverPhone = driverDomain.phoneNumber;
      await this.groupRepository.update(group);
      for (const groupAss of groupAssignments.data) {
        groupAss.driverId = command.driverId;
        groupAss.driverName = driverDomain.name;
        groupAss.driverPhone = driverDomain.phoneNumber;
        await this.groupAssignmentRepository.save(groupAss);
      }
      if (oldPayload.driverId !== command.driverId) {
        //Send notiication to new driver
        const notificationData = {
          title: 'Group Assignment',
          body: `Group '${group.name}' has been assigned to you`,
          receiver: command.driverId,
        };
        this.eventEmitter.emit('create.notification', notificationData);
        //Send notification to previous driver
        const oldNotificationData = {
          title: 'Group Assignment',
          body: `Group '${group.name}' has been re-assigned to another driver`,
          receiver: oldPayload.driverId,
        };
        this.eventEmitter.emit('create.notification', oldNotificationData);
        //Send notification to parent
        const notificationParentData = {
          title: 'Group Assignment',
          body: `Group '${group.name}' has been re-assigned to a new driver`,
          receiver: groupAssignments.data[0].parentId,
        };
        this.eventEmitter.emit('create.notification', notificationParentData);
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: driverAssignment.id,
        modelName: MODELS.DRIVERASSIGNMENT,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: { ...oldPayload },
        payload: { ...driverAssignment },
      });
    }
    return DriverAssignmentResponse.fromDomain(driverAssignment);
  }
  async archiveDriverAssignment(
    archiveCommand: ArchiveDriverAssignmentCommand,
  ): Promise<DriverAssignmentResponse> {
    const driverAssignmentDomain =
      await this.driverAssignmentRepository.getById(archiveCommand.id);
    if (!driverAssignmentDomain) {
      throw new NotFoundException(
        `Driver Assignment not found with id ${archiveCommand.id}`,
      );
    }

    driverAssignmentDomain.deletedAt = new Date();
    driverAssignmentDomain.deletedBy = archiveCommand.currentUser.id;
    driverAssignmentDomain.archiveReason = archiveCommand.reason;
    const result = await this.driverAssignmentRepository.update(
      driverAssignmentDomain,
    );
    return DriverAssignmentResponse.fromDomain(result);
  }
  async restoreDriverAssignment(
    id: string,
    currentUser: UserInfo,
  ): Promise<DriverAssignmentResponse> {
    const driverAssignmentDomain =
      await this.driverAssignmentRepository.getById(id, true);
    if (!driverAssignmentDomain) {
      throw new NotFoundException(`Driver Assignment not found with id ${id}`);
    }
    driverAssignmentDomain.archiveReason = null;
    const driverAssignment = await this.driverAssignmentRepository.update(
      driverAssignmentDomain,
    );
    const r = await this.driverAssignmentRepository.restore(id);
    if (r) {
      driverAssignmentDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.DRIVERASSIGNMENT,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return DriverAssignmentResponse.fromDomain(driverAssignmentDomain);
  }

  @OnEvent('delete.driver.assignment')
  async deleteDriverAssignment(
    id: string,
    currentUser: UserInfo,
  ): Promise<boolean> {
    const driverAssignmentDomain =
      await this.driverAssignmentRepository.getById(id, true);
    if (!driverAssignmentDomain) {
      throw new NotFoundException(`Driver Assignment not found with id ${id}`);
    }

    const result = await this.driverAssignmentRepository.delete(id);
    const groupAssignments = await this.groupQuery.getGroupAssignmentByGroupId(
      driverAssignmentDomain.groupId,
      new CollectionQuery(),
    );
    const group = await this.groupRepository.getById(
      driverAssignmentDomain.groupId,
    );
    group.driverId = null;
    await this.groupRepository.update(group);
    for (const groupAss of groupAssignments.data) {
      groupAss.driverId = null;
      await this.groupAssignmentRepository.save(groupAss);
    }
    //Unassign Driver To Job
    if (driverAssignmentDomain.jobId) {
      this.eventEmitter.emit('open.or.close.job', {
        id: driverAssignmentDomain.jobId,
        status: JobStatus.Close,
        currentUser: currentUser,
      });
      const jobDomain = await this.jobRepository.getById(
        driverAssignmentDomain.jobId,
      );
      jobDomain.driverId = null;
      jobDomain.status = JobStatus.Close;
      await this.jobRepository.update(jobDomain);
    }
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.DRIVERASSIGNMENT,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
}
