import { DriverAssignment } from '@assignment/domains/driver-assignments/driver-assignment';
import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateDriverAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  @ApiProperty()
  jobId: string;
  currentUser: UserInfo;
  static fromCommand(command: CreateDriverAssignmentCommand): DriverAssignment {
    const driverAssignmentDomain = new DriverAssignment();
    driverAssignmentDomain.driverId = command.driverId;
    driverAssignmentDomain.groupId = command.groupId;
    driverAssignmentDomain.jobId = command.jobId;
    return driverAssignmentDomain;
  }
}
export class UpdateDriverAssignmentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  @ApiProperty()
  status: string;
  currentUser: UserInfo;
}
export class ArchiveDriverAssignmentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
