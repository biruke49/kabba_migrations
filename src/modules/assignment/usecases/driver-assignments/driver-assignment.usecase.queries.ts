import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GroupAssignmentResponse } from '../groups/group-assignment.response';
import { DriverAssignmentResponse } from './driver-assignment.response';
import { GetIdsCommand } from '@chat/usecases/chat.commands';
import { ChatQuery } from '@chat/usecases/chat.usecase.queries';
import { SchoolResponse } from '@customer/usecases/schools/school.response';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { GroupResponse } from '../groups/group.response';
import { ParentResponse } from '@customer/usecases/parents/parent.response';
import { KidResponse } from '@customer/usecases/parents/kid.response';

@Injectable()
export class DriverAssignmentQuery {
  constructor(
    @InjectRepository(DriverAssignmentEntity)
    private driverAssignmentRepository: Repository<DriverAssignmentEntity>,
    @InjectRepository(GroupAssignmentEntity)
    private groupAssignmentRepository: Repository<GroupAssignmentEntity>,
    private chatQuery: ChatQuery,
  ) {}
  async getDriverAssignment(
    id: string,
    withDeleted = false,
  ): Promise<DriverAssignmentResponse> {
    const parent = await this.driverAssignmentRepository.find({
      where: { id: id },
      relations: ['driver', 'group', 'group.groupAssignments'],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      throw new NotFoundException(`Driver Assignment not found with id ${id}`);
    }
    return DriverAssignmentResponse.fromEntity(parent[0]);
  }
  async getDriverAssignments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverAssignmentResponse>> {
    const dataQuery = QueryConstructor.constructQuery<DriverAssignmentEntity>(
      this.driverAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<DriverAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        DriverAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getMyAssignments(
    driverId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        operator: FilterOperators.EqualTo,
        value: driverId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverAssignmentEntity>(
      this.driverAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<DriverAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        DriverAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getAssignmentsWithDriverId(
    driverId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        value: driverId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverAssignmentEntity>(
      this.driverAssignmentRepository,
      query,
    );
    const d = new DataResponseFormat<DriverAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        DriverAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getKidsAssignedToDriver(
    driverId: string,
    query: CollectionQuery,
  ): Promise<GroupAssignmentResponse[]> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'driverId',
          operator: FilterOperators.EqualTo,
          value: driverId,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.EqualTo,
          value: 'Active',
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<GroupAssignmentEntity>(
      this.groupAssignmentRepository,
      query,
    );
    const result = await dataQuery
      .leftJoinAndSelect('group_assignments.kid', 'kid')
      .leftJoinAndSelect('group_assignments.parent', 'parent')
      .leftJoinAndSelect('group_assignments.school', 'school')
      .leftJoinAndSelect('parent.parentPreferences', 'parentPreference')
      .getMany();
    const groupAssignmentResponses = [];
    for (const res of result) {
      const groupAssignmentReponse = new GroupAssignmentResponse();
      const getIdsCommand = new GetIdsCommand();
      const newQuery = new CollectionQuery();
      getIdsCommand.receiverId = driverId;
      getIdsCommand.senderId = res.parentId;
      const unseen =
        await this.chatQuery.getTotalUnseenChatBySenderIdAndReceiverId(
          getIdsCommand,
          newQuery,
        );
      groupAssignmentReponse.id = res.id;
      groupAssignmentReponse.groupId = res.groupId;
      groupAssignmentReponse.schoolId = res.schoolId;
      groupAssignmentReponse.schoolName = res.schoolName;
      groupAssignmentReponse.status = res.status;
      groupAssignmentReponse.kidId = res.kidId;
      groupAssignmentReponse.parentId = res.parentId;
      groupAssignmentReponse.driverId = res.driverId;
      groupAssignmentReponse.amount = res.amount;
      groupAssignmentReponse.paidAt = res.paidAt;
      groupAssignmentReponse.expiresAt = res.expiresAt;
      groupAssignmentReponse.unseenTexts = unseen.total;
      if (res.kid) {
        groupAssignmentReponse.kid = KidResponse.fromEntity(res.kid);
      }
      if (res.parent) {
        groupAssignmentReponse.parent = ParentResponse.fromEntity(res.parent);
      }
      if (res.group) {
        groupAssignmentReponse.group = GroupResponse.fromEntity(res.group);
      }
      if (res.driver) {
        groupAssignmentReponse.driver = DriverResponse.fromEntity(res.driver);
      }
      if (res.school) {
        groupAssignmentReponse.school = SchoolResponse.fromEntity(res.school);
      }
      groupAssignmentResponses.push(groupAssignmentReponse);
    }
    return groupAssignmentResponses;
  }
  async getArchivedDriverAssignments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverAssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverAssignmentEntity>(
      this.driverAssignmentRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<DriverAssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        DriverAssignmentResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
}
