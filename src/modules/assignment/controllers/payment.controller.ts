import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { PaymentRepository } from '@assignment/persistence/payments/payment.repository';
import {
  ArchivePaymentCommand,
  CreatePaymentCommand,
  PayManuallyCommand,
  PayWithChapaCommand,
} from '@assignment/usecases/payments/payment.commands';
import {
  InitiateTeleBirrResponse,
  PaymentResponse,
} from '@assignment/usecases/payments/payment.response';
import { PaymentCommands } from '@assignment/usecases/payments/payment.usecase.commads';
import { PaymentQuery } from '@assignment/usecases/payments/payment.usecase.queries';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { PaymentMethod, PaymentStatus } from '@libs/common/enums';
import { StartChapaPaymentResponse } from '@libs/payment/chapa/dtos/payment.dto';
import { PaymentService } from '@libs/payment/chapa/services/payment.service';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';

@Controller('payments')
@ApiTags('payments')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class PaymentController {
  constructor(
    private readonly command: PaymentCommands,
    private readonly paymentQueries: PaymentQuery,
    private paymentRepository: PaymentRepository,
    private readonly chapaPaymentService: PaymentService,
    private parentRepository: ParentRepository,
  ) {}
  @Get('get-payment/:id')
  @ApiOkResponse({ type: PaymentResponse })
  async getPayment(@Param('id') id: string) {
    return this.paymentQueries.getPayment(id);
  }
  @Get('get-archived-payment/:id')
  @ApiOkResponse({ type: PaymentResponse })
  async getArchivedPayment(@Param('id') id: string) {
    return this.paymentQueries.getPayment(id, true);
  }
  @Get('get-payments')
  @ApiPaginatedResponse(PaymentResponse)
  async getPayments(@Query() query: CollectionQuery) {
    return this.paymentQueries.getPayments(query);
  }
  @Get('get-payments-with-parent-id/:parentId')
  @ApiPaginatedResponse(PaymentResponse)
  async getPaymentByParentId(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.paymentQueries.getPaymentByParentId(parentId, query);
  }
  @Get('get-company-earning')
  @ApiOkResponse({ type: PaymentResponse })
  async getCompanyEarning(@Query() query: CollectionQuery) {
    return this.paymentQueries.getCompanyEarning(query);
  }
  @Get('group-earning-by-created-date/:format')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupEarningsByCreatedDate(
    @Query() query: CollectionQuery,
    @Param('format') format: string,
  ) {
    return this.paymentQueries.groupEarningsByCreatedDate(query, format);
  }
  @Get('group-earning-by-category')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupEarningsByCategory(
    @Query() query: CollectionQuery,
  ) {
    return this.paymentQueries.groupEarningsByCategory(query);
  }
  @Post('create-payment')
  @UseGuards(PermissionsGuard('assignments/pay-for-driver-assignment'))
  @ApiOkResponse({ type: PaymentResponse })
  async createPayment(
    @CurrentUser() user: UserInfo,
    @Body() createPaymentCommand: CreatePaymentCommand,
  ) {
    return this.command.createPayment(createPaymentCommand);
  }
  @Delete('archive-payment')
  @UseGuards(PermissionsGuard('assignments/pay-for-driver-assignment'))
  @ApiOkResponse({ type: Boolean })
  async archivePayment(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchivePaymentCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archivePayment(archiveCommand);
  }
  @Delete('delete-payment/:id')
  @UseGuards(PermissionsGuard('assignments/pay-for-driver-assignment'))
  @ApiOkResponse({ type: Boolean })
  async deletePayment(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deletePayment(id, user);
  }
  @Post('restore-payment/:id')
  @UseGuards(PermissionsGuard('assignments/pay-for-driver-assignment'))
  @ApiOkResponse({ type: PaymentResponse })
  async restorePayment(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restorePayment(id, user);
  }
  @Get('get-archived-payments')
  @ApiPaginatedResponse(PaymentResponse)
  async getArchivedPayments(@Query() query: CollectionQuery) {
    return this.paymentQueries.getArchivedPayments(query);
  }
  //For Kid payment
  @Post('pay-with-chapa')
  @ApiOkResponse({ type: StartChapaPaymentResponse })
  // @UseGuards(RolesGuard('parent'))
  async updateMyWalletWithChapa(
    @CurrentUser() user: UserInfo,
    @Body() command: PayWithChapaCommand,
  ) {
    command.parentId = user.id;
    command.depositedBy = {
      id: user.id,
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
    };
    command.status = PaymentStatus.Pending;
    command.method = PaymentMethod.Chapa;
    command.reason = 'Pay for kids with Chapa';
    const transaction = await this.command.createPayment(
      command as CreatePaymentCommand,
    );
    if (!transaction) {
      throw new BadRequestException(`Unable to process the payment`);
    }
    const transactionDomain = await this.paymentRepository.getById(
      transaction.id,
    );
    if (transactionDomain) {
      transactionDomain.transactionNumber = transaction.id;
      await this.paymentRepository.update(transactionDomain);
    }

    const userName = user.name.trim().split(' ');
    let firstName = ' ';
    if (userName[0]) {
      firstName = userName[0];
    }
    let lastName = ' ';
    if (userName[userName.length - 1]) {
      lastName = userName[userName.length - 1];
    }
    const formData = {
      amount: command.amount,
      currency: 'ETB',
      email: user.email ? user.email : process.env.DEFAULT_CHAPA_EMAIL,
      first_name: firstName,
      last_name: lastName.trim() ? lastName.trim() : 'No name',
      tx_ref: transaction.id,
      callback_url: `${process.env.API_URL}/api/payments/chapa-callback`,
      return_url: command.returnUrl,
    };

    const paymentInfo = await this.chapaPaymentService.initializePayment(
      formData,
    );

    const response = {
      checkout_url: paymentInfo.data.checkout_url,
      status: paymentInfo.status,
      message: paymentInfo.message,
      transactionId: transaction.id,
    };
    return response;
    // return {
    //   firstName: firstName,
    //   lastName: lastName,
    //   txRef: transaction.id,
    //   publicKey: process.env.CHAPA_PUBLIC_KEY,
    //   secretKey: process.env.CHAPA_SECRET_KEY,
    //   currency: 'ETB',
    //   amount: transaction.amount,
    //   email: user.email,
    // };
  }
  //For Kid manual payment
  @Post('pay-manually')
  @ApiOkResponse({ type: StartChapaPaymentResponse })
  async payForKidsManually(@Body() body: PayManuallyCommand) {
    const command = new PayWithChapaCommand();
    const parent = await this.parentRepository.getById(body.parentId);
    command.parentId = parent.id;
    command.depositedBy = {
      id: parent.id,
      name: parent.name,
      email: parent.email,
      phoneNumber: parent.phoneNumber,
    };
    command.status = PaymentStatus.Pending;
    command.amount = body.amount;
    command.method = PaymentMethod.Manual;
    command.reason = 'Pay for kids manually';
    const transaction = await this.command.createPayment(
      command as CreatePaymentCommand,
    );
    this.command.approveManualransaction(transaction.id, command);
  }
  @Get('chapa-callback')
  @AllowAnonymous()
  async chapaCallback(@Body() paymentPayload: any) {
    if (paymentPayload.status === 'success') {
      const verificationData = await this.chapaPaymentService.verifyPayment(
        paymentPayload.trx_ref,
      );
      if (verificationData) {
        // console.log(verificationData);

        this.command.approveChapaTransaction(
          paymentPayload.trx_ref,
          verificationData,
        );
      }
    }
  }

  @Get('initiate-telebirr')
  @UseGuards(RolesGuard('parent'))
  @ApiOkResponse({ type: InitiateTeleBirrResponse })
  async initiateCorporateTelebirr(@CurrentUser() user: UserInfo) {
    const paymentCommand = new CreatePaymentCommand();
    paymentCommand.parentId = user.id;
    paymentCommand.method = PaymentMethod.Telebirr;
    paymentCommand.status = PaymentStatus.Pending;
    paymentCommand.amount = 0;
    paymentCommand.reason = 'Pay for kids with Telebirr';
    paymentCommand.depositedBy = {
      id: user.id,
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
    };
    return await this.command.initiateTeleBirrPayment(paymentCommand);
  }
}
