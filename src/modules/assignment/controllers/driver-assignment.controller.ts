import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import {
  ArchiveDriverAssignmentCommand,
  CreateDriverAssignmentCommand,
  UpdateDriverAssignmentCommand,
} from '@assignment/usecases/driver-assignments/driver-assignment.commands';
import { DriverAssignmentResponse } from '@assignment/usecases/driver-assignments/driver-assignment.response';
import { DriverAssignmentCommands } from '@assignment/usecases/driver-assignments/driver-assignment.usecase.commands';
import { DriverAssignmentQuery } from '@assignment/usecases/driver-assignments/driver-assignment.usecase.queries';
import { GroupAssignmentResponse } from '@assignment/usecases/groups/group-assignment.response';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('driver-assignments')
@ApiTags('driver-assignments')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiOkResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class DriverAssignmentController {
  constructor(
    private command: DriverAssignmentCommands,
    private driverAssignmentQuery: DriverAssignmentQuery,
  ) {}
  @Get('get-driver-assignment/:id')
  @ApiOkResponse({ type: DriverAssignmentResponse })
  async getDriverAssignment(@Param('id') id: string) {
    return this.driverAssignmentQuery.getDriverAssignment(id);
  }
  @Get('get-archived-driver-assignment/:id')
  @ApiOkResponse({ type: DriverAssignmentResponse })
  async getArchivedDriverAssignment(@Param('id') id: string) {
    return this.driverAssignmentQuery.getDriverAssignment(id, true);
  }
  @Get('get-driver-assignments')
  @ApiPaginatedResponse(DriverAssignmentResponse)
  async getDriverAssignments(@Query() query: CollectionQuery) {
    return this.driverAssignmentQuery.getDriverAssignments(query);
  }
  @Get('get-my-assignments')
  @UseGuards(RolesGuard('driver'))
  @ApiPaginatedResponse(DriverAssignmentResponse)
  async getMyAssignments(
    @CurrentUser() user: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.driverAssignmentQuery.getMyAssignments(user.id, query);
  }
  @Get('get-assignments-with-driver-id/:driverId')
  @ApiPaginatedResponse(DriverAssignmentResponse)
  async getKidsWithDriverId(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.driverAssignmentQuery.getAssignmentsWithDriverId(
      driverId,
      query,
    );
  }
  @Get('get-kids-assigned-to-driver/:driverId')
  @ApiPaginatedResponse(GroupAssignmentResponse)
  async getKidsAssignedToDriver(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.driverAssignmentQuery.getKidsAssignedToDriver(driverId, query);
  }
  @Get('get-kids-assigned-to-me')
  @ApiOkResponse({ type: GroupAssignmentResponse, isArray: true })
  async getKidsAssignedToMe(
    @CurrentUser() user: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    const driverId = user.id;
    return this.driverAssignmentQuery.getKidsAssignedToDriver(driverId, query);
  }
  @Post('create-driver-assignment')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: DriverAssignmentResponse })
  async createDriverAssignment(
    @CurrentUser() user: UserInfo,
    @Body() createDriverAssignmentCommand: CreateDriverAssignmentCommand,
  ) {
    createDriverAssignmentCommand.currentUser = user;
    return this.command.createDriverAssignment(createDriverAssignmentCommand);
  }
  @Put('update-driver-assignment')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: DriverAssignmentResponse })
  async updateDriverAssignment(
    @CurrentUser() user: UserInfo,
    @Body() updateDriverAssignmentCommand: UpdateDriverAssignmentCommand,
  ) {
    updateDriverAssignmentCommand.currentUser = user;
    return this.command.updateDriverAssignment(updateDriverAssignmentCommand);
  }
  @Delete('archive-driver-assignment')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: Boolean })
  async archiveDriverAssignment(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveDriverAssignmentCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveDriverAssignment(archiveCommand);
  }
  @Delete('delete-driver-assignment/:id')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: Boolean })
  async deleteDriverAssignment(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.deleteDriverAssignment(id, user);
  }
  @Post('restore-driver-assignment/:id')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: DriverAssignmentResponse })
  async restoreDriverAssignment(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.restoreDriverAssignment(id, user);
  }
  @Get('get-archived-driver-assignments')
  @ApiPaginatedResponse(DriverAssignmentResponse)
  async getArchivedDriverAssignments(@Query() query: CollectionQuery) {
    return this.driverAssignmentQuery.getArchivedDriverAssignments(query);
  }
}
