import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import {
  ArchiveGroupAssignmentCommand,
  CreateGroupAssignmentsCommand,
  DeleteGroupAssignmentCommand,
} from '@assignment/usecases/groups/group-assignment.commands';
import {
  GroupAssignmentDriverResponse,
  GroupAssignmentResponse,
} from '@assignment/usecases/groups/group-assignment.response';
import {
  ArchiveGroupCommand,
  CreateGroupCommand,
  UpdateGroupCommand,
} from '@assignment/usecases/groups/group.commands';
import { GroupResponse } from '@assignment/usecases/groups/group.response';
import { GroupCommands } from '@assignment/usecases/groups/group.usecase.commands';
import { GroupQuery } from '@assignment/usecases/groups/group.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { AppService } from 'app.service';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Controller('groups')
@ApiTags('groups')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiOkResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class GroupController {
  constructor(
    private command: GroupCommands,
    private groupQuery: GroupQuery,
    private accountRepository: AccountRepository,
    private driverRepository: DriverRepository,
    private parentRepository: ParentRepository,
    private appService: AppService,
    private eventEmitter: EventEmitter2,
  ) {}
  @Get('get-group/:id')
  @ApiOkResponse({ type: GroupResponse })
  async getGroup(@Param('id') id: string) {
    return this.groupQuery.getGroup(id);
  }
  @Get('get-archived-group/:id')
  @ApiOkResponse({ type: GroupResponse })
  async getArchivedGroup(@Param('id') id: string) {
    return this.groupQuery.getGroup(id, true);
  }
  @Get('get-groups-with-category-id/:categoryId')
  @ApiPaginatedResponse(GroupResponse)
  async getGroupsWithCategoryId(
    @Param('categoryId') categoryId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.groupQuery.getGroupsWithCategoryId(categoryId, query);
  }
  @Get('get-groups')
  @ApiPaginatedResponse(GroupResponse)
  async getGroups(@Query() query: CollectionQuery) {
    return this.groupQuery.getGroups(query);
  }
  @Get('get-unassigned-groups')
  @ApiPaginatedResponse(GroupResponse)
  async getUnassignedGroups(@Query() query: CollectionQuery) {
    return this.groupQuery.getUnassignedGroups(query);
  }
  @Get('get-groups-with-no-jobs')
  @ApiPaginatedResponse(GroupResponse)
  async getGroupsWithNoJobs(@Query() query: CollectionQuery) {
    return this.groupQuery.getGroupsWithNoJobs(query);
  }
  @Post('create-group')
  @UseGuards(PermissionsGuard('manage-groups'))
  @ApiOkResponse({ type: GroupResponse })
  async createGroup(
    @CurrentUser() user: UserInfo,
    @Body() createGroupCommand: CreateGroupCommand,
  ) {
    createGroupCommand.currentUser = user;
    return this.command.createGroup(createGroupCommand);
  }
  @Put('update-group')
  @UseGuards(PermissionsGuard('manage-groups'))
  @ApiOkResponse({ type: GroupResponse })
  async updateGroup(
    @CurrentUser() user: UserInfo,
    @Body() updateGroupCommand: UpdateGroupCommand,
  ) {
    updateGroupCommand.currentUser = user;
    return this.command.updateGroup(updateGroupCommand);
  }
  @Delete('archive-group')
  @UseGuards(PermissionsGuard('manage-groups'))
  @ApiOkResponse({ type: Boolean })
  async archiveGroup(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveGroupCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveGroup(archiveCommand);
  }
  @Delete('delete-group/:id')
  @UseGuards(PermissionsGuard('manage-groups'))
  @ApiOkResponse({ type: Boolean })
  async deleteGroup(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteGroup(id, user);
  }
  @Post('restore-group/:id')
  @UseGuards(PermissionsGuard('manage-groups'))
  @ApiOkResponse({ type: GroupResponse })
  async restoreGroup(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreGroup(id, user);
  }
  @Get('get-archived-groups')
  @ApiPaginatedResponse(GroupResponse)
  async getArchivedGroups(@Query() query: CollectionQuery) {
    return this.groupQuery.getArchivedGroups(query);
  }
  @Post('activate-or-block-group/:id')
  @UseGuards(PermissionsGuard('activate-or-block-groups'))
  @ApiOkResponse({ type: GroupResponse })
  async activateOrBlockGroup(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockGroup(id, user);
  }

  // Group Assignment
  @Get('get-group-assignment/:id')
  @ApiOkResponse({ type: GroupAssignmentResponse })
  async getGroupAssignment(@Param('id') id: string) {
    return this.groupQuery.getGroupAssignment(id);
  }
  @Get('get-group-assignments')
  @ApiPaginatedResponse(GroupAssignmentResponse)
  async getGroupAssignments(@Query() query: CollectionQuery) {
    return this.groupQuery.getGroupAssignments(query);
  }
  @Get('get-group-assignment-with-group-id/:groupId')
  @ApiPaginatedResponse(GroupAssignmentResponse)
  async getGroupAssignmentByGroupId(
    @Param('groupId') groupId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.groupQuery.getGroupAssignmentByGroupId(groupId, query);
  }
  @Get('get-group-assignment-with-parent-id/:parentId')
  @ApiPaginatedResponse(GroupAssignmentResponse)
  async getGroupAssignmentByParentId(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.groupQuery.getGroupAssignmentByParentId(parentId, query);
  }
  @Get('get-group-assignment-drivers-with-parent-id/:parentId')
  @ApiOkResponse({ type: GroupAssignmentDriverResponse })
  async getGroupAssignmentDriversByParentId(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.groupQuery.getGroupAssignmentDriversByParentId(parentId, query);
  }
  @Get('calculate-total-payment-of-parent/:parentId')
  async calculateTotalPaymentOfParent(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.groupQuery.calculateTotalPaymentOfParent(parentId, query);
  }
  @Get('calculate-total-payment-of-all-kids/:parentId')
  async calculateParentpaymentOfAllKids(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.command.calculateParentpaymentOfAllKids(parentId, query);
  }
  @Post('add-group-assignments')
  @UseGuards(PermissionsGuard('manage-group-assignment'))
  @ApiOkResponse({ type: GroupAssignmentResponse })
  async addGroupAssignment(
    @CurrentUser() user: UserInfo,
    @Body() createGroupAssignmentsCommand: CreateGroupAssignmentsCommand,
  ) {
    createGroupAssignmentsCommand.currentUser = user;
    return this.command.addGroupAssignments(createGroupAssignmentsCommand);
  }
  @Delete('archive-group-assignment')
  @UseGuards(PermissionsGuard('manage-group-assignment'))
  @ApiOkResponse({ type: Boolean })
  async archiveGroupAssignment(
    @CurrentUser() user: UserInfo,
    @Body() archiveGroupAssignmentCommand: ArchiveGroupAssignmentCommand,
  ) {
    archiveGroupAssignmentCommand.currentUser = user;
    return this.command.archiveGroupAssignment(archiveGroupAssignmentCommand);
  }
  @Delete('remove-group-assignment')
  @UseGuards(PermissionsGuard('manage-group-assignment'))
  @ApiOkResponse({ type: Boolean })
  async removeGroupAssignment(
    @CurrentUser() user: UserInfo,
    @Body() removeGroupAssignmentCommand: DeleteGroupAssignmentCommand,
  ) {
    removeGroupAssignmentCommand.currentUser = user;
    return this.command.deleteGroupAssignment(removeGroupAssignmentCommand);
  }
  @Post('restore-group-assignment')
  @UseGuards(PermissionsGuard('manage-group-assignment'))
  @ApiOkResponse({ type: GroupAssignmentResponse })
  async restoreGroupAssignment(
    @CurrentUser() user: UserInfo,
    @Body() command: DeleteGroupAssignmentCommand,
  ) {
    command.currentUser = user;
    return this.command.restoreGroupAssignment(command);
  }
  @Get('get-archived-group-assignments')
  @ApiPaginatedResponse(GroupAssignmentResponse)
  async getArchivedGroupAssignments(@Query() query: CollectionQuery) {
    return this.groupQuery.getArchivedGroupAssignments(query);
  }
  @Post('activate-or-deactivate-group-assignment/:id')
  @UseGuards(PermissionsGuard('activate-or-block-group-assignment'))
  @ApiOkResponse({ type: GroupResponse })
  async activateOrDeactivateGroupAssignment(
    @Param('id') id: string,
    @CurrentUser() user: UserInfo,
  ) {
    return this.command.activateOrDeactivateGroupAssignment(id, user);
  }
  @Post('assign-group-assignment/:id')
  @UseGuards(PermissionsGuard('manage-group-assignment'))
  @ApiOkResponse({ type: GroupResponse })
  async assignGroupAssignment(
    @Param('id') id: string,
    @CurrentUser() user: UserInfo,
  ) {
    return this.command.assignGroupAssignment(id);
  }
  @Post('change-group-assignment-status/:id')
  @ApiOkResponse({ type: GroupResponse })
  async changeGroupAssignmentStatus(
    @Param('id') id: string,
    @CurrentUser() user: UserInfo,
  ) {
    return this.command.activateOrDeactivateGroupAssignmentEvent(id);
  }
  @Get('check-payment-expiry')
  @AllowAnonymous()
  @ApiOkResponse({ type: GroupResponse })
  async getExpiredGroupAssignments(@Query() query: CollectionQuery) {
    await this.groupQuery.getExpiredGroupAssignmentsToNotify(query);
    await this.groupQuery.getExpiredGroupAssignments(query);
  }
}
