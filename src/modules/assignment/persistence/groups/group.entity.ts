import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { GroupStatus } from '@libs/common/enums';
import { JobEntity } from 'modules/job/persistence/jobs/job.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DriverAssignmentEntity } from '../driver-assignments/driver-assignment.entity';
import { GroupAssignmentEntity } from './group-assignment.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';

@Entity('groups')
export class GroupEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ unique: true })
  name: string;
  @Column({ name: 'category_id', type: 'uuid' })
  categoryId: string;
  @Column({ name: 'number_of_seat', nullable: true, type: 'int' })
  numberOfSeat: number;
  @Column({ name: 'available_seats', nullable: true })
  availableSeats: number;
  @Column({ default: GroupStatus.Active })
  status: string;
  @Column({ name: 'driver_id', type: 'uuid', nullable: true })
  driverId: string;
  @Column({ name: 'driver_name', nullable: true })
  driverName: string;
  @Column({ name: 'driver_phone', nullable: true })
  driverPhone: string;
  @Column({ name: 'is_full', default: false })
  isFull: boolean;
  @Column({ name: 'kids_road_difficulty_cost', nullable: true, default: 0 })
  kidsRoadDifficultyCost: number;
  @ManyToOne(() => DriverEntity, (driver) => driver.group, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @ManyToOne(() => CategoryEntity, (category) => category.groupAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'category_id' })
  category: CategoryEntity;
  @OneToMany(
    () => GroupAssignmentEntity,
    (groupAssignment) => groupAssignment.group,
    {
      cascade: true,
      onDelete: 'CASCADE',
    },
  )
  groupAssignments: GroupAssignmentEntity[];
  @OneToMany(
    () => DriverAssignmentEntity,
    (driverAssignment) => driverAssignment.group,
    {
      cascade: ['remove', 'soft-remove', 'recover'],
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  driverAssignments: DriverAssignmentEntity[];
  @OneToMany(() => JobEntity, (job) => job.group, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  jobs: JobEntity[];
}
