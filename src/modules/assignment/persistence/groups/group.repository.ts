import { Group } from '@assignment/domains/groups/group';
import { GroupAssignment } from '@assignment/domains/groups/group-assignment';
import { IGroupRepository } from '@assignment/domains/groups/group.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { GroupAssignmentEntity } from './group-assignment.entity';
import { GroupEntity } from './group.entity';
import { KidTravelStatus } from '@libs/common/enums';

@Injectable()
export class GroupRepository implements IGroupRepository {
  constructor(
    @InjectRepository(GroupEntity)
    private groupRepository: Repository<GroupEntity>,
    @InjectRepository(GroupAssignmentEntity)
    private groupAssignmentRepository: Repository<GroupAssignmentEntity>,
  ) {}

  async insert(group: Group): Promise<Group> {
    const groupEntity = this.toGroupEntity(group);
    const result = await this.groupRepository.save(groupEntity);
    return result ? this.toGroup(result) : null;
  }
  async update(group: Group): Promise<Group> {
    const groupEntity = this.toGroupEntity(group);
    const result = await this.groupRepository.save(groupEntity);
    return result ? this.toGroup(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.groupRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Group[]> {
    const groups = await this.groupRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!groups.length) {
      return null;
    }
    return groups.map((user) => this.toGroup(user));
  }
  async getById(id: string, withDeleted = false): Promise<Group> {
    const group = await this.groupRepository.find({
      where: { id: id },
      relations: ['groupAssignments', 'category'],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroup(group[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.groupRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.groupRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getByName(name: string, withDeleted = false): Promise<Group> {
    const group = await this.groupRepository.find({
      where: { name: name },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroup(group[0]);
  }
  async getByNameAndCategory(
    name: string,
    categoryId: string,
    withDeleted = false,
  ): Promise<Group> {
    const group = await this.groupRepository.find({
      where: { name: name, categoryId: categoryId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroup(group[0]);
  }
  async getByCategorySchoolAndParentId(
    categoryId: string,
    parentId: string,
    schoolId: string,
    withDeleted = false,
  ): Promise<GroupAssignment> {
    const kidTravelStatus = KidTravelStatus.WithSiblings;
    const group = await this.groupAssignmentRepository
      .createQueryBuilder('group_assignment')
      .leftJoinAndSelect('group_assignment.kid', 'kid')
      .leftJoinAndSelect('group_assignment.group', 'group')
      .where('kid.categoryId = :categoryId', { categoryId })
      .andWhere('kid.kidTravelStatus = :kidTravelStatus', { kidTravelStatus })
      .andWhere('group_assignment.parentId = :parentId', { parentId })
      .andWhere('group_assignment.schoolId = :schoolId', { schoolId })
      .andWhere('group_assignment.amount > 0')
      .getOne();

    if (!group) {
      return null;
    }
    return this.toGroupAssignment(group);
  }
  async getByCategoryid(
    categoryId: string,
    withDeleted = false,
  ): Promise<Group> {
    const group = await this.groupRepository.find({
      where: { categoryId: categoryId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroup(group[0]);
  }
  async getByDriverid(driverId: string, withDeleted = false): Promise<Group> {
    const group = await this.groupRepository.find({
      where: { driverId: driverId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroup(group[0]);
  }
  async getByParentid(
    parentId: string,
    withDeleted = false,
  ): Promise<GroupAssignment> {
    const group = await this.groupAssignmentRepository.find({
      where: { parentId: parentId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroupAssignment(group[0]);
  }
  async getAllUnassignedByParentId(
    parentId: string,
    withDeleted: boolean,
  ): Promise<GroupAssignment[]> {
    const accounts = await this.groupAssignmentRepository.find({
      where: { parentId: parentId, driverId: null },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!accounts.length) {
      return null;
    }
    return accounts.map((account) => this.toGroupAssignment(account));
  }
  async getAssignedByParentId(
    parentId: string,
    withDeleted: boolean,
  ): Promise<GroupAssignment> {
    console.log("🚀 ~ file: group.repository.ts:173 ~ GroupRepository ~ parentId:", parentId)
    const group = await this.groupAssignmentRepository.find({
      where: { parentId: parentId, driverId: Not(null) },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroupAssignment(group[0]);
  }
  async getByGroupId(
    groupId: string,
    withDeleted = false,
  ): Promise<GroupAssignment> {
    const group = await this.groupAssignmentRepository.find({
      where: { groupId: groupId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroupAssignment(group[0]);
  }
  async getByKidId(
    kidId: string,
    withDeleted = false,
  ): Promise<GroupAssignment> {
    const group = await this.groupAssignmentRepository.find({
      where: { kidId: kidId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroupAssignment(group[0]);
  }
  async getByGroupIdForUpdate(
    groupId: string,
    withDeleted = false,
  ): Promise<GroupAssignment> {
    const group = await this.groupAssignmentRepository.find({
      where: { groupId: groupId, amount: 0 },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!group[0]) {
      return null;
    }
    return this.toGroupAssignment(group[0]);
  }

  toGroup(groupEntity: GroupEntity): Group {
    const group = new Group();
    group.id = groupEntity.id;
    group.categoryId = groupEntity.categoryId;
    group.name = groupEntity.name;
    group.status = groupEntity.status;
    group.isFull = groupEntity.isFull;
    group.driverId = groupEntity.driverId;
    group.driverName = groupEntity.driverName;
    group.driverPhone = groupEntity.driverPhone;
    group.numberOfSeat = groupEntity.numberOfSeat;
    group.availableSeats = groupEntity.availableSeats;
    group.kidsRoadDifficultyCost = groupEntity.kidsRoadDifficultyCost;
    group.archiveReason = groupEntity.archiveReason;
    group.createdBy = groupEntity.createdBy;
    group.updatedBy = groupEntity.updatedBy;
    group.deletedBy = groupEntity.deletedBy;
    group.createdAt = groupEntity.createdAt;
    group.updatedAt = groupEntity.updatedAt;
    group.deletedAt = groupEntity.deletedAt;
    group.groupAssignments = groupEntity.groupAssignments
      ? groupEntity.groupAssignments.map((groupAssignment) =>
          this.toGroupAssignment(groupAssignment),
        )
      : [];
    return group;
  }
  toGroupEntity(group: Group): GroupEntity {
    const groupEntity = new GroupEntity();
    groupEntity.id = group.id;
    groupEntity.categoryId = group.categoryId;
    groupEntity.name = group.name;
    groupEntity.status = group.status;
    groupEntity.isFull = group.isFull;
    groupEntity.numberOfSeat = group.numberOfSeat;
    groupEntity.driverId = group.driverId;
    groupEntity.driverName = group.driverName;
    groupEntity.driverPhone = group.driverPhone;
    groupEntity.availableSeats = group.availableSeats;
    groupEntity.kidsRoadDifficultyCost = group.kidsRoadDifficultyCost;
    groupEntity.archiveReason = group.archiveReason;
    groupEntity.createdBy = group.createdBy;
    groupEntity.updatedBy = group.updatedBy;
    groupEntity.deletedBy = group.deletedBy;
    groupEntity.createdAt = group.createdAt;
    groupEntity.updatedAt = group.updatedAt;
    groupEntity.deletedAt = group.deletedAt;
    groupEntity.groupAssignments = group.groupAssignments
      ? group.groupAssignments.map((groupAssignment) =>
          this.toGroupAssignmentEntity(groupAssignment),
        )
      : [];
    return groupEntity;
  }
  toGroupAssignment(
    groupAssignmentEntity: GroupAssignmentEntity,
  ): GroupAssignment {
    const groupAssignment = new GroupAssignment();
    groupAssignment.id = groupAssignmentEntity.id;
    groupAssignment.groupId = groupAssignmentEntity.groupId;
    groupAssignment.kidId = groupAssignmentEntity.kidId;
    groupAssignment.parentId = groupAssignmentEntity.parentId;
    groupAssignment.schoolId = groupAssignmentEntity.schoolId;
    groupAssignment.driverId = groupAssignmentEntity.driverId;
    groupAssignment.driverName = groupAssignmentEntity.driverName;
    groupAssignment.driverPhone = groupAssignmentEntity.driverPhone;
    groupAssignment.status = groupAssignmentEntity.status;
    groupAssignment.amount = groupAssignmentEntity.amount;
    groupAssignment.schoolName = groupAssignmentEntity.schoolName;
    groupAssignment.paidAt = groupAssignmentEntity.paidAt;
    groupAssignment.expiresAt = groupAssignmentEntity.expiresAt;
    groupAssignment.archiveReason = groupAssignmentEntity.archiveReason;
    groupAssignment.createdBy = groupAssignmentEntity.createdBy;
    groupAssignment.updatedBy = groupAssignmentEntity.updatedBy;
    groupAssignment.deletedBy = groupAssignmentEntity.deletedBy;
    groupAssignment.createdAt = groupAssignmentEntity.createdAt;
    groupAssignment.updatedAt = groupAssignmentEntity.updatedAt;
    groupAssignment.deletedAt = groupAssignmentEntity.deletedAt;
    return groupAssignment;
  }
  toGroupAssignmentEntity(
    groupAssignment: GroupAssignment,
  ): GroupAssignmentEntity {
    const groupAssignmentEntity = new GroupAssignmentEntity();
    groupAssignmentEntity.id = groupAssignment.id;
    groupAssignmentEntity.groupId = groupAssignment.groupId;
    groupAssignmentEntity.kidId = groupAssignment.kidId;
    groupAssignmentEntity.parentId = groupAssignment.parentId;
    groupAssignmentEntity.schoolId = groupAssignment.schoolId;
    groupAssignmentEntity.driverId = groupAssignment.driverId;
    groupAssignmentEntity.driverName = groupAssignment.driverName;
    groupAssignmentEntity.driverPhone = groupAssignment.driverPhone;
    groupAssignmentEntity.status = groupAssignment.status;
    groupAssignmentEntity.schoolName = groupAssignment.schoolName;
    groupAssignmentEntity.amount = groupAssignment.amount;
    groupAssignmentEntity.paidAt = groupAssignment.paidAt;
    groupAssignmentEntity.expiresAt = groupAssignment.expiresAt;
    groupAssignmentEntity.archiveReason = groupAssignment.archiveReason;
    groupAssignmentEntity.createdBy = groupAssignment.createdBy;
    groupAssignmentEntity.updatedBy = groupAssignment.updatedBy;
    groupAssignmentEntity.deletedBy = groupAssignment.deletedBy;
    groupAssignmentEntity.createdAt = groupAssignment.createdAt;
    groupAssignmentEntity.updatedAt = groupAssignment.updatedAt;
    groupAssignmentEntity.deletedAt = groupAssignment.deletedAt;
    return groupAssignmentEntity;
  }
}
