import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { SchoolEntity } from '@customer/persistence/schools/school.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { GroupAssignmentStatus } from '@libs/common/enums';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { GroupEntity } from './group.entity';

@Entity('group_assignments')
export class GroupAssignmentEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'group_id', type: 'uuid' })
  groupId: string;
  @Column({ name: 'kid_id', type: 'uuid' })
  kidId: string;
  @Column({ name: 'parent_id', type: 'uuid' })
  parentId: string;
  @Column({ name: 'school_id', type: 'uuid' })
  schoolId: string;
  @Column({ name: 'school_name', nullable: true })
  schoolName: string;
  @Column({ name: 'driver_id', type: 'uuid', nullable: true })
  driverId: string;
  @Column({ name: 'driver_name', nullable: true })
  driverName: string;
  @Column({ name: 'driver_phone', nullable: true })
  driverPhone: string;
  @Column({ nullable: true, type: 'double precision' })
  amount: number;
  @Column({ default: GroupAssignmentStatus.Inactive })
  status: string;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'paid_at',
    nullable: true,
  })
  paidAt: Date;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'expires_at',
    nullable: true,
  })
  expiresAt: Date;
  @ManyToOne(() => ParentEntity, (parent) => parent.groupAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'parent_id' })
  parent: ParentEntity;
  @OneToOne(() => KidEntity, (kid) => kid.groupAssignment, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'kid_id' })
  kid: KidEntity;
  @ManyToOne(() => DriverEntity, (driver) => driver.groupAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @ManyToOne(() => GroupEntity, (group) => group.groupAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'group_id' })
  group: GroupEntity;
  @ManyToOne(() => SchoolEntity, (school) => school.groupAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'school_id' })
  school: SchoolEntity;
}
