import { DepositedBy } from '@assignment/domains/payments/deposited-by';
import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { PaymentStatus } from '@libs/common/enums';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('payments')
export class PaymentEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'parent_id', type: 'uuid' })
  parentId: string;
  @Column({ name: 'is_paid', default: false })
  isPaid: boolean;
  @Column({ nullable: true, type: 'double precision' })
  amount: number;
  @Column({ name: 'company_earning', default: 0, type: 'double precision' })
  companyEarning: number;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'paid_at',
  })
  paidAt: Date;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'expires_at',
  })
  expiresAt: Date;
  @Column({ name: 'deposited_by', type: 'jsonb', nullable: true })
  depositedBy?: DepositedBy;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'trade_date',
    nullable: true,
  })
  tradeDate?: Date;
  @Column({ name: 'trade_number', nullable: true })
  tradeNumber?: string;
  @Column({ name: 'transaction_number', nullable: true })
  transactionNumber?: string;
  @Column({ default: PaymentStatus.Pending })
  status: string;
  @Column({ nullable: true })
  method: string;
  @Column({ nullable: true })
  reason: string;
  @ManyToOne(() => ParentEntity, (parent) => parent.payments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'parent_id' })
  parent: ParentEntity;
}
