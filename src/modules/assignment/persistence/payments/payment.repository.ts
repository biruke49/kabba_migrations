import { Payment } from '@assignment/domains/payments/payment';
import { IPaymentRepository } from '@assignment/domains/payments/payment.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentEntity } from './payment.entity';

@Injectable()
export class PaymentRepository implements IPaymentRepository {
  constructor(
    @InjectRepository(PaymentEntity)
    private paymentRepository: Repository<PaymentEntity>,
  ) {}

  async insert(payment: Payment): Promise<Payment> {
    const paymentEntity = this.toPaymentEntity(payment);
    const result = await this.paymentRepository.save(paymentEntity);
    return result ? this.toPayment(result) : null;
  }
  async update(payment: Payment): Promise<Payment> {
    const paymentEntity = this.toPaymentEntity(payment);
    const result = await this.paymentRepository.save(paymentEntity);
    return result ? this.toPayment(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.paymentRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Payment[]> {
    const payments = await this.paymentRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!payments.length) {
      return null;
    }
    return payments.map((user) => this.toPayment(user));
  }
  async getById(id: string, withDeleted = false): Promise<Payment> {
    const payment = await this.paymentRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!payment[0]) {
      return null;
    }
    return this.toPayment(payment[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.paymentRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.paymentRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }

  toPayment(paymentEntity: PaymentEntity): Payment {
    const payment = new Payment();
    payment.id = paymentEntity.id;
    payment.parentId = paymentEntity.parentId;
    payment.amount = paymentEntity.amount;
    payment.companyEarning = paymentEntity.companyEarning;
    payment.isPaid = paymentEntity.isPaid;
    payment.paidAt = paymentEntity.paidAt;
    payment.expiresAt = paymentEntity.expiresAt;
    payment.tradeDate = paymentEntity.tradeDate;
    payment.tradeNumber = paymentEntity.tradeNumber;
    payment.transactionNumber = paymentEntity.transactionNumber;
    payment.method = paymentEntity.method;
    payment.status = paymentEntity.status;
    payment.reason = paymentEntity.reason;
    payment.archiveReason = paymentEntity.archiveReason;
    payment.createdBy = paymentEntity.createdBy;
    payment.updatedBy = paymentEntity.updatedBy;
    payment.deletedBy = paymentEntity.deletedBy;
    payment.createdAt = paymentEntity.createdAt;
    payment.updatedAt = paymentEntity.updatedAt;
    payment.deletedAt = paymentEntity.deletedAt;
    return payment;
  }
  toPaymentEntity(payment: Payment): PaymentEntity {
    const paymentEntity = new PaymentEntity();
    paymentEntity.id = payment.id;
    paymentEntity.parentId = payment.parentId;
    paymentEntity.amount = payment.amount;
    paymentEntity.companyEarning = payment.companyEarning;
    paymentEntity.isPaid = payment.isPaid;
    paymentEntity.paidAt = payment.paidAt;
    paymentEntity.expiresAt = payment.expiresAt;
    paymentEntity.depositedBy = payment.depositedBy;
    paymentEntity.tradeDate = payment.tradeDate;
    paymentEntity.tradeNumber = payment.tradeNumber;
    paymentEntity.transactionNumber = payment.transactionNumber;
    paymentEntity.method = payment.method;
    paymentEntity.status = payment.status;
    paymentEntity.reason = payment.reason;
    paymentEntity.archiveReason = payment.archiveReason;
    paymentEntity.createdBy = payment.createdBy;
    paymentEntity.updatedBy = payment.updatedBy;
    paymentEntity.deletedBy = payment.deletedBy;
    paymentEntity.createdAt = payment.createdAt;
    paymentEntity.updatedAt = payment.updatedAt;
    paymentEntity.deletedAt = payment.deletedAt;
    return paymentEntity;
  }
}
