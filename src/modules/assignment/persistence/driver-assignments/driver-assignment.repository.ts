import { DriverAssignment } from '@assignment/domains/driver-assignments/driver-assignment';
import { IDriverAssignmentRepository } from '@assignment/domains/driver-assignments/driver-assignment.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DriverAssignmentEntity } from './driver-assignment.entity';

@Injectable()
export class DriverAssignmentRepository implements IDriverAssignmentRepository {
  constructor(
    @InjectRepository(DriverAssignmentEntity)
    private driverAssignemntRepository: Repository<DriverAssignmentEntity>,
  ) {}
  async insert(driverAssignment: DriverAssignment): Promise<DriverAssignment> {
    const driverAssignmentEntity =
      this.toDriverAssignmentEntity(driverAssignment);
    const result = await this.driverAssignemntRepository.save(
      driverAssignmentEntity,
    );
    return result ? this.toDriverAssignment(result) : null;
  }
  async update(driverAssignment: DriverAssignment): Promise<DriverAssignment> {
    const driverAssignmentEntity =
      this.toDriverAssignmentEntity(driverAssignment);
    const result = await this.driverAssignemntRepository.save(
      driverAssignmentEntity,
    );
    return result ? this.toDriverAssignment(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.driverAssignemntRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<DriverAssignment[]> {
    const driverAssignments = await this.driverAssignemntRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driverAssignments.length) {
      return null;
    }
    return driverAssignments.map((user) => this.toDriverAssignment(user));
  }
  async getById(id: string, withDeleted = false): Promise<DriverAssignment> {
    const driverAssignment = await this.driverAssignemntRepository.find({
      where: { id: id },
      relations: ['group'],
      withDeleted: withDeleted,
    });
    if (!driverAssignment[0]) {
      return null;
    }
    return this.toDriverAssignment(driverAssignment[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.driverAssignemntRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.driverAssignemntRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }

  async getByDriverIdAndGroupId(
    driverId: string,
    groupId: string,
    withDeleted = false,
  ): Promise<DriverAssignment> {
    const driverAssignment = await this.driverAssignemntRepository.find({
      where: { driverId: driverId, groupId: groupId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driverAssignment[0]) {
      return null;
    }
    return this.toDriverAssignment(driverAssignment[0]);
  }
  async checkIfDriverIsAssigned(
    driverId: string,
    withDeleted = false,
  ): Promise<boolean> {
    const driverAssignment = await this.driverAssignemntRepository.find({
      where: { driverId: driverId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driverAssignment[0]) {
      return false;
    }
    return true;
  }
  async getByDriverIdAndJobId(
    driverId: string,
    jobId: string,
    withDeleted = false,
  ): Promise<DriverAssignment> {
    const driverAssignment = await this.driverAssignemntRepository.find({
      where: { driverId: driverId, jobId: jobId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driverAssignment[0]) {
      return null;
    }
    return this.toDriverAssignment(driverAssignment[0]);
  }
  toDriverAssignment(
    driverAssignmentEntity: DriverAssignmentEntity,
  ): DriverAssignment {
    const driverAssignment = new DriverAssignment();
    driverAssignment.id = driverAssignmentEntity.id;
    driverAssignment.driverId = driverAssignmentEntity.driverId;
    driverAssignment.driverName = driverAssignmentEntity.driverName;
    driverAssignment.driverPhone = driverAssignmentEntity.driverPhone;
    driverAssignment.groupId = driverAssignmentEntity.groupId;
    driverAssignment.jobId = driverAssignmentEntity.jobId;
    driverAssignment.status = driverAssignmentEntity.status;
    driverAssignment.archiveReason = driverAssignmentEntity.archiveReason;
    driverAssignment.createdBy = driverAssignmentEntity.createdBy;
    driverAssignment.updatedBy = driverAssignmentEntity.updatedBy;
    driverAssignment.deletedBy = driverAssignmentEntity.deletedBy;
    driverAssignment.createdAt = driverAssignmentEntity.createdAt;
    driverAssignment.updatedAt = driverAssignmentEntity.updatedAt;
    driverAssignment.deletedAt = driverAssignmentEntity.deletedAt;
    return driverAssignment;
  }
  toDriverAssignmentEntity(
    driverAssignment: DriverAssignment,
  ): DriverAssignmentEntity {
    const driverAssignmentEntity = new DriverAssignmentEntity();
    driverAssignmentEntity.id = driverAssignment.id;
    driverAssignmentEntity.driverId = driverAssignment.driverId;
    driverAssignmentEntity.driverName = driverAssignment.driverName;
    driverAssignmentEntity.driverPhone = driverAssignment.driverPhone;
    driverAssignmentEntity.groupId = driverAssignment.groupId;
    driverAssignmentEntity.jobId = driverAssignment.jobId;
    driverAssignmentEntity.status = driverAssignment.status;
    driverAssignmentEntity.archiveReason = driverAssignment.archiveReason;
    driverAssignmentEntity.createdBy = driverAssignment.createdBy;
    driverAssignmentEntity.updatedBy = driverAssignment.updatedBy;
    driverAssignmentEntity.deletedBy = driverAssignment.deletedBy;
    driverAssignmentEntity.createdAt = driverAssignment.createdAt;
    driverAssignmentEntity.updatedAt = driverAssignment.updatedAt;
    driverAssignmentEntity.deletedAt = driverAssignment.deletedAt;
    return driverAssignmentEntity;
  }
}
