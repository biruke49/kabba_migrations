import { CommonEntity } from '@libs/common/common.entity';
import { DriverAssignmentStatus } from '@libs/common/enums';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { GroupEntity } from '../groups/group.entity';
import { JobEntity } from '@job/persistence/jobs/job.entity';

@Entity('driver_assignments')
export class DriverAssignmentEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'driver_id', type: 'uuid' })
  driverId: string;
  @Column({ name: 'driver_name', nullable: true })
  driverName: string;
  @Column({ name: 'driver_phone', nullable: true })
  driverPhone: string;
  @Column({ name: 'group_id', type: 'uuid' })
  groupId: string;
  @Column({ name: 'job_id', type: 'uuid', nullable: true })
  jobId: string;
  @Column({ default: DriverAssignmentStatus.Active })
  status: string;
  @ManyToOne(() => GroupEntity, (group) => group.driverAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'group_id' })
  group: GroupEntity;
  @ManyToOne(() => DriverEntity, (driver) => driver.driverAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @ManyToOne(() => JobEntity, (driver) => driver.driverAssignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'job_id' })
  job: JobEntity;
}
