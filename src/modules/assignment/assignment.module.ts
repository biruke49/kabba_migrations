import { Module } from '@nestjs/common';
import { DriverAssignmentController } from './controllers/driver-assignment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountModule } from '@account/account.module';
import { FileManagerService } from '@libs/common/file-manager';
import { GroupController } from './controllers/group.controller';
import { GroupEntity } from './persistence/groups/group.entity';
import { GroupAssignmentEntity } from './persistence/groups/group-assignment.entity';
import { GroupRepository } from './persistence/groups/group.repository';
import { GroupCommands } from './usecases/groups/group.usecase.commands';
import { GroupQuery } from './usecases/groups/group.usecase.queries';
import { DriverAssignmentRepository } from './persistence/driver-assignments/driver-assignment.repository';
import { DriverAssignmentCommands } from './usecases/driver-assignments/driver-assignment.usecase.commands';
import { DriverAssignmentQuery } from './usecases/driver-assignments/driver-assignment.usecase.queries';
import { DriverAssignmentEntity } from './persistence/driver-assignments/driver-assignment.entity';
import { PaymentEntity } from './persistence/payments/payment.entity';
import { PaymentRepository } from './persistence/payments/payment.repository';
import { PaymentCommands } from './usecases/payments/payment.usecase.commads';
import { PaymentQuery } from './usecases/payments/payment.usecase.queries';
import { PaymentController } from './controllers/payment.controller';
import { PaymentService } from '@libs/payment/chapa/services/payment.service';
import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { SchoolEntity } from '@customer/persistence/schools/school.entity';
import { CustomerModule } from '@customer/customer.module';
import { ParentQuery } from '@customer/usecases/parents/parent.usecase.queries';
import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { ParentCommands } from '@customer/usecases/parents/parent.usecase.commands';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { SchoolQuery } from '@customer/usecases/schools/school.usecase.queries';
import { CategoryRepository } from '@classification/persistence/categories/category.repository';
import { ConfigurationEntity } from '@configurations/persistence/configuration/configuration.entity';
import { ConfigurationQuery } from '@configurations/usecases/configuration/configuration.usecase.queries';
import { ChatQuery } from '@chat/usecases/chat.usecase.queries';
import { ChatEntity } from '@chat/persistence/chat.entity';
import { JobEntity } from '@job/persistence/jobs/job.entity';
import { JobRepository } from '@job/persistence/jobs/job.repository';
import { CandidateEntity } from '@job/persistence/jobs/candidate.entity';
import { ConfigurationsModule } from '@configurations/configurations.module';
import { ConfigurationRepository } from '@configurations/persistence/configuration/configuration.repository';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import { MyGateway } from '@chat/gateway';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { AccountEntity } from '@account/persistence/accounts/account.entity';
import { AppService } from 'app.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GroupEntity,
      GroupAssignmentEntity,
      DriverAssignmentEntity,
      KidEntity,
      ParentEntity,
      SchoolEntity,
      PaymentEntity,
      ParentPreferenceEntity,
      CategoryEntity,
      ConfigurationEntity,
      ChatEntity,
      JobEntity,
      CandidateEntity,
      ConfigurationEntity,
      AccountEntity,
      DriverEntity,
    ]),
    AccountModule,
    CustomerModule,
  ],
  providers: [
    GroupRepository,
    GroupCommands,
    GroupQuery,
    DriverAssignmentRepository,
    DriverAssignmentCommands,
    DriverAssignmentQuery,
    FileManagerService,
    ParentQuery,
    CategoryRepository,
    ConfigurationQuery,
    SchoolQuery,
    ParentCommands,
    PaymentRepository,
    PaymentCommands,
    PaymentQuery,
    PaymentService,
    ChatQuery,
    JobRepository,
    ConfigurationRepository,
    AccountRepository,
    DriverRepository,
    ParentRepository,
    MyGateway,
    AppService,
  ],
  controllers: [DriverAssignmentController, GroupController, PaymentController],
  exports: [GroupRepository, GroupCommands],
})
export class AssignmentModule {}
