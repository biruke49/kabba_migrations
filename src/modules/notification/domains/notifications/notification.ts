import { Account } from "@account/domains/accounts/account";

export class Notification {
  id: string;
  title: string;
  body: string;
  receiver: string;
  type: string;
  status: string;
  isSeen: boolean;
  method: string;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  accountReceiver: Account;
}
