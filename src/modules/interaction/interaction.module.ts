import { FavoritesController } from './controllers/favorite.controller';
import { FavoriteQueries } from '@interaction/usecases/favorites/favorite.usecase.queries';
import { FavoriteCommands } from './usecases/favorites/favorite.usecase.commands';
import { FavoriteRepository } from './persistence/favorites/favorite.repository';
import { ReviewCommands } from './usecases/reviews/review.usecase.commands';
import { FeedbackQuery } from './usecases/feedbacks/feedback.usecase.queries';
import { FeedbackCommands } from './usecases/feedbacks/feedback.usecase.commands';
import { FeedbackRepository } from './persistence/feedbacks/feedback.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { FeedbackEntity } from './persistence/feedbacks/feedback.entity';
import { FeedbacksController } from './controllers/feedback.controller';
import { ReviewEntity } from './persistence/reviews/review.entity';
import { ReviewRepository } from './persistence/reviews/review.repository';
import { ReviewQuery } from './usecases/reviews/review.usecase.queries';
import { ReviewsController } from './controllers/review.controller';
import { FavoriteEntity } from './persistence/favorites/favorite.entity';
import { PreferencesController } from './controllers/preference.controller';
import { PreferenceEntity } from './persistence/preferences/preference.entity';
import { PreferenceCommands } from './usecases/preferences/preference.usecase.commands';
import { PreferenceQueries } from './usecases/preferences/preference.usecase.queries';
import { PreferenceRepository } from './persistence/preferences/preference.repository';
import { ParentPreferencesController } from './controllers/parent-preference.controller';
import { ParentPreferenceEntity } from './persistence/parent-preference/parent-preference.entity';
import { ParentPreferenceCommands } from './usecases/parent-preference/parent-preference.usecase.commands';
import { ParentPreferenceQueries } from './usecases/parent-preference/parent-preference.usecase.queries';
import { ParentPreferenceRepository } from './persistence/parent-preference/parent-preference.repository';
@Module({
  controllers: [
    FeedbacksController,
    ReviewsController,
    FavoritesController,
    PreferencesController,
    ParentPreferencesController,
  ],
  imports: [
    TypeOrmModule.forFeature([
      FeedbackEntity,
      ReviewEntity,
      FavoriteEntity,
      PreferenceEntity,
      ParentPreferenceEntity,
    ]),
  ],
  providers: [
    FeedbackRepository,
    FeedbackCommands,
    FeedbackQuery,
    ReviewRepository,
    ReviewCommands,
    ReviewQuery,
    FavoriteRepository,
    FavoriteCommands,
    FavoriteQueries,
    PreferenceCommands,
    PreferenceQueries,
    PreferenceRepository,
    ParentPreferenceCommands,
    ParentPreferenceQueries,
    ParentPreferenceRepository,
  ],
})
export class InteractionModule {}
