import { Passenger } from '@customer/domains/passengers/passenger';
import { Driver } from '@provider/domains/drivers/driver';
export class Review {
  id: string;
  score: number;
  driverId: string;
  passengerId: string;
  description: string;
  archiveReason: string;
  passenger?: Passenger;
  driver?: Driver;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
