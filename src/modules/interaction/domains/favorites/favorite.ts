import { Passenger } from '@customer/domains/passengers/passenger';
import { Route } from '@router/domains/routes/route';

export class Favorite {
  id: string;
  routeId: string;
  routeName: string;
  passengerId: string;
  createdAt: Date;
  passenger: Passenger;
  archiveReason: string;
  route: Route;
}
