import { Favorite } from './favorite';
export interface IFavoriteRepository {
  insert(user: Favorite): Promise<Favorite>;
  update(user: Favorite): Promise<Favorite>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Favorite[]>;
  getById(id: string, withDeleted: boolean): Promise<Favorite>;
  getByPassengerIdAndRouteId(
    passengerId: string,
    routeId: string,
    withDeleted: boolean,
  ): Promise<Favorite>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
