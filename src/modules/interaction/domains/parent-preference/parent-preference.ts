import { Parent } from '@customer/domains/parents/parent';

export class ParentPreference {
  id: string;
  parentId: string;
  days: string[];
  driverGender: string;
  createdAt: Date;
  parent: Parent;
}
