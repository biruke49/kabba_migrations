import { ParentPreference } from './parent-preference';

export interface IParentPreferenceRepository {
  insert(user: ParentPreference): Promise<ParentPreference>;
  update(user: ParentPreference): Promise<ParentPreference>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<ParentPreference[]>;
  getById(id: string, withDeleted: boolean): Promise<ParentPreference>;
  getByParentId(
    parentId: string,
    withDeleted: boolean,
  ): Promise<ParentPreference>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
