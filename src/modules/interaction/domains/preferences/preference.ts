import { Driver } from '@provider/domains/drivers/driver';
import { Route } from '@router/domains/routes/route';

export class Preference {
  id: string;
  routeId: string;
  routeName: string;
  driverId: string;
  createdAt: Date;
  driver: Driver;
  route: Route;
}
