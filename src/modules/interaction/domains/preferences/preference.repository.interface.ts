import { Preference } from './preference';
export interface IPreferenceRepository {
  insert(user: Preference): Promise<Preference>;
  update(user: Preference): Promise<Preference>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Preference[]>;
  getById(id: string, withDeleted: boolean): Promise<Preference>;
  getByDriverIdAndRouteId(
    driverId: string,
    routeId: string,
    withDeleted: boolean,
  ): Promise<Preference>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
