import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Entity('reviews')
export class ReviewEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ type: 'double precision' })
  score: number;
  @Column({ name: 'driver_id' })
  driverId: string;
  @Column({ name: 'parent_id', nullable: true })
  parentId: string;
  @Column({ name: 'passenger_id', nullable: true })
  passengerId: string;
  @Column({ nullable: true })
  description: string;
  @ManyToOne(() => DriverEntity, (driver) => driver.reviews, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @ManyToOne(() => PassengerEntity, (passenger) => passenger.reviews, {
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'passenger_id' })
  passenger: PassengerEntity;
  @ManyToOne(() => ParentEntity, (parent) => parent.reviews, {
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'parent_id' })
  parent: ParentEntity;
}
