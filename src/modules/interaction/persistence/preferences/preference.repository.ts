import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Preference } from '@interaction/domains/preferences/preference';
import { IPreferenceRepository } from '@interaction/domains/preferences/preference.repository.interface';
@Injectable()
export class PreferenceRepository implements IPreferenceRepository {
  constructor(
    @InjectRepository(PreferenceEntity)
    private preferenceRepository: Repository<PreferenceEntity>,
  ) {}
  async insert(preference: Preference): Promise<Preference> {
    const preferenceEntity = this.toPreferenceEntity(preference);
    const result = await this.preferenceRepository.save(preferenceEntity);
    return result ? this.toPreference(result) : null;
  }
  async update(preference: Preference): Promise<Preference> {
    const preferenceEntity = this.toPreferenceEntity(preference);
    const result = await this.preferenceRepository.save(preferenceEntity);
    return result ? this.toPreference(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Preference[]> {
    const preferences = await this.preferenceRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preferences.length) {
      return null;
    }
    return preferences.map((user) => this.toPreference(user));
  }
  async getById(id: string, withDeleted = false): Promise<Preference> {
    const preference = await this.preferenceRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preference[0]) {
      return null;
    }
    return this.toPreference(preference[0]);
  }
  async getByDriverIdAndRouteId(
    driverId: string,
    routeId: string,
    withDeleted = false,
  ): Promise<Preference> {
    const preference = await this.preferenceRepository.find({
      where: { driverId: driverId, routeId: routeId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preference[0]) {
      return null;
    }
    return this.toPreference(preference[0]);
  }
  async getByRouteId(
    routeId: string,
    withDeleted = false,
  ): Promise<Preference> {
    const preference = await this.preferenceRepository.find({
      where: { routeId: routeId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preference[0]) {
      return null;
    }
    return this.toPreference(preference[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toPreference(preferenceEntity: PreferenceEntity): Preference {
    const preference = new Preference();
    preference.id = preferenceEntity.id;
    preference.routeId = preferenceEntity.routeId;
    preference.routeName = preferenceEntity.routeName;
    preference.driverId = preferenceEntity.driverId;
    preference.createdAt = preferenceEntity.createdAt;
    return preference;
  }
  toPreferenceEntity(preference: Preference): PreferenceEntity {
    const preferenceEntity = new PreferenceEntity();
    preferenceEntity.id = preference.id;
    preferenceEntity.routeId = preference.routeId;
    preferenceEntity.routeName = preference.routeName;
    preferenceEntity.driverId = preference.driverId;
    preferenceEntity.createdAt = preference.createdAt;
    return preferenceEntity;
  }
}
