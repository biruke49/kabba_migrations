import { CommonEntity } from '@libs/common/common.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { RouteEntity } from '@router/persistence/routes/route.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Entity('preferences')
export class PreferenceEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'route_id', type: 'uuid' })
  routeId: string;
  @Column({ name: 'route_name' })
  routeName: string;
  @Column({ name: 'driver_id', type: 'uuid' })
  driverId: string;
  @ManyToOne(() => RouteEntity, (route) => route.preferences, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'route_id' })
  route: RouteEntity;
  @ManyToOne(() => DriverEntity, (driver) => driver.preferences, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
}
