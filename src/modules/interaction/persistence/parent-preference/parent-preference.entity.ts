import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { DateOfWeek, Gender } from '@libs/common/enums';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('parent_preferences')
export class ParentPreferenceEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'parent_id', type: 'uuid' })
  parentId: string;
  @Column({ type: 'text', array: true, default: [] })
  days: string[];
  @Column({ name: 'driver_gender' })
  driverGender: string;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'created_at',
  })
  createdAt: Date;
  @ManyToOne(() => ParentEntity, (parent) => parent.parentPreferences, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'parent_id' })
  parent: ParentEntity;
}
