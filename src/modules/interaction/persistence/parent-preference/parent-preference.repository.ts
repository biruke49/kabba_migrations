import { ParentPreference } from '@interaction/domains/parent-preference/parent-preference';
import { IParentPreferenceRepository } from '@interaction/domains/parent-preference/parent-preference.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ParentPreferenceEntity } from './parent-preference.entity';
@Injectable()
export class ParentPreferenceRepository implements IParentPreferenceRepository {
  constructor(
    @InjectRepository(ParentPreferenceEntity)
    private preferenceRepository: Repository<ParentPreferenceEntity>,
  ) {}
  async insert(preference: ParentPreference): Promise<ParentPreference> {
    const preferenceEntity = this.toParentPreferenceEntity(preference);
    const result = await this.preferenceRepository.save(preferenceEntity);
    return result ? this.toParentPreference(result) : null;
  }
  async update(preference: ParentPreference): Promise<ParentPreference> {
    const preferenceEntity = this.toParentPreferenceEntity(preference);
    const result = await this.preferenceRepository.save(preferenceEntity);
    return result ? this.toParentPreference(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<ParentPreference[]> {
    const preferences = await this.preferenceRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preferences.length) {
      return null;
    }
    return preferences.map((user) => this.toParentPreference(user));
  }
  async getById(id: string, withDeleted = false): Promise<ParentPreference> {
    const preference = await this.preferenceRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preference[0]) {
      return null;
    }
    return this.toParentPreference(preference[0]);
  }
  async getByParentId(
    parentId: string,
    withDeleted = false,
  ): Promise<ParentPreference> {
    const preference = await this.preferenceRepository.find({
      where: { parentId: parentId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preference[0]) {
      return null;
    }
    return this.toParentPreference(preference[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toParentPreference(
    preferenceEntity: ParentPreferenceEntity,
  ): ParentPreference {
    const preference = new ParentPreference();
    preference.id = preferenceEntity.id;
    preference.parentId = preferenceEntity.parentId;
    preference.driverGender = preferenceEntity.driverGender;
    preference.days = preferenceEntity.days;
    preference.createdAt = preferenceEntity.createdAt;
    return preference;
  }
  toParentPreferenceEntity(
    preference: ParentPreference,
  ): ParentPreferenceEntity {
    const preferenceEntity = new ParentPreferenceEntity();
    preferenceEntity.id = preference.id;
    preferenceEntity.parentId = preference.parentId;
    preferenceEntity.driverGender = preference.driverGender;
    preferenceEntity.days = preference.days;
    preferenceEntity.createdAt = preference.createdAt;
    return preferenceEntity;
  }
}
