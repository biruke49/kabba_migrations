import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { RouteEntity } from '@router/persistence/routes/route.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Entity('favorites')
export class FavoriteEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'route_id', type: 'uuid' })
  routeId: string;
  @Column({ name: 'route_name' })
  routeName: string;
  @Column({ name: 'passenger_id', type: 'uuid' })
  passengerId: string;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'created_at',
  })
  createdAt: Date;
  @ManyToOne(() => RouteEntity, (route) => route.favorites, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'route_id' })
  route: RouteEntity;
  @ManyToOne(() => PassengerEntity, (passenger) => passenger.favorites, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'passenger_id' })
  passenger: PassengerEntity;
}
