import { FavoriteEntity } from '@interaction/persistence/favorites/favorite.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Favorite } from '@interaction/domains/favorites/favorite';
import { IFavoriteRepository } from '@interaction/domains/favorites/favorite.repository.interface';
@Injectable()
export class FavoriteRepository implements IFavoriteRepository {
  constructor(
    @InjectRepository(FavoriteEntity)
    private favoriteRepository: Repository<FavoriteEntity>,
  ) {}
  async insert(favorite: Favorite): Promise<Favorite> {
    const favoriteEntity = this.toFavoriteEntity(favorite);
    const result = await this.favoriteRepository.save(favoriteEntity);
    return result ? this.toFavorite(result) : null;
  }
  async update(favorite: Favorite): Promise<Favorite> {
    const favoriteEntity = this.toFavoriteEntity(favorite);
    const result = await this.favoriteRepository.save(favoriteEntity);
    return result ? this.toFavorite(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.favoriteRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Favorite[]> {
    const favorites = await this.favoriteRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!favorites.length) {
      return null;
    }
    return favorites.map((user) => this.toFavorite(user));
  }
  async getById(id: string, withDeleted = false): Promise<Favorite> {
    const favorite = await this.favoriteRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!favorite[0]) {
      return null;
    }
    return this.toFavorite(favorite[0]);
  }
  async getByPassengerIdAndRouteId(
    passengerId: string,
    routeId: string,
    withDeleted = false,
  ): Promise<Favorite> {
    const favorite = await this.favoriteRepository.find({
      where: { passengerId: passengerId, routeId: routeId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!favorite[0]) {
      return null;
    }
    return this.toFavorite(favorite[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.favoriteRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.favoriteRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toFavorite(favoriteEntity: FavoriteEntity): Favorite {
    const favorite = new Favorite();
    favorite.id = favoriteEntity.id;
    favorite.routeId = favoriteEntity.routeId;
    favorite.routeName = favoriteEntity.routeName;
    favorite.passengerId = favoriteEntity.passengerId;
    favorite.createdAt = favoriteEntity.createdAt;
    return favorite;
  }
  toFavoriteEntity(favorite: Favorite): FavoriteEntity {
    const favoriteEntity = new FavoriteEntity();
    favoriteEntity.id = favorite.id;
    favoriteEntity.routeId = favorite.routeId;
    favoriteEntity.routeName = favorite.routeName;
    favoriteEntity.passengerId = favorite.passengerId;
    favoriteEntity.createdAt = favorite.createdAt;
    return favoriteEntity;
  }
}
