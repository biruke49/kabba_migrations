import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { RolesGuard } from '@account/guards/role.quards';
import { CreateParentPreferenceCommand } from '@interaction/usecases/parent-preference/parent-preference.commands';
import { ParentPreferenceResponse } from '@interaction/usecases/parent-preference/parent-preference.response';
import { ParentPreferenceCommands } from '@interaction/usecases/parent-preference/parent-preference.usecase.commands';
import { ParentPreferenceQueries } from '@interaction/usecases/parent-preference/parent-preference.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('parent-preferences')
@ApiTags('parent-preferences')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class ParentPreferencesController {
  constructor(
    private command: ParentPreferenceCommands,
    private parentparentPreferenceQuery: ParentPreferenceQueries,
  ) {}
  @Get('get-parent-preference/:id')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiOkResponse({ type: ParentPreferenceResponse })
  async getParentPreference(@Param('id') id: string) {
    return this.parentparentPreferenceQuery.getParentPreference(id);
  }
  @Get('get-parent-preferences')
  @AllowAnonymous()
  @ApiPaginatedResponse(ParentPreferenceResponse)
  async getParentPreferences(@Query() query: CollectionQuery) {
    return this.parentparentPreferenceQuery.getParentPreferences(query);
  }
  @Get('get-my-parent-preferences')
  @UseGuards(RolesGuard('parent'))
  @ApiPaginatedResponse(ParentPreferenceResponse)
  async getMyParentPreferences(
    @CurrentUser() parent: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.parentparentPreferenceQuery.getParentPreferenceByParentId(
      parent.id,
      query,
    );
  }
  @Get('get-parent-preferences/:parentId')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiPaginatedResponse(ParentPreferenceResponse)
  async getParentPreferenceByParentId(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.parentparentPreferenceQuery.getParentPreferenceByParentId(
      parentId,
      query,
    );
  }
  @Post('create-parent-preference')
  @UseGuards(RolesGuard('parent'))
  @ApiOkResponse({ type: ParentPreferenceResponse })
  async createParentPreference(
    @CurrentUser() user: UserInfo,
    @Body() createPreferenceCommand: CreateParentPreferenceCommand,
  ) {
    createPreferenceCommand.parentId = user.id;
    return this.command.createParentPreference(createPreferenceCommand);
  }
  @Delete('delete-parent-preference/:id')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiOkResponse({ type: Boolean })
  async deleteParentPreference(@Param('id') id: string) {
    return this.command.deleteParentPreference(id);
  }
  @Get('get-archived-parent-preferences')
  @ApiPaginatedResponse(ParentPreferenceResponse)
  async getArchivedParentPreferences(@Query() query: CollectionQuery) {
    return this.parentparentPreferenceQuery.getArchivedParentPreferences(query);
  }
}
