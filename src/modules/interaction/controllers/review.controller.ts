import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { RolesGuard } from '@account/guards/role.quards';
import {
  ArchiveReviewCommand,
  CreateReviewCommand,
} from '@interaction/usecases/reviews/review.commands';
import { ReviewResponse } from '@interaction/usecases/reviews/review.response';
import { ReviewCommands } from '@interaction/usecases/reviews/review.usecase.commands';
import { ReviewQuery } from '@interaction/usecases/reviews/review.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('reviews')
@ApiTags('reviews')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class ReviewsController {
  constructor(
    private commands: ReviewCommands,
    private reviewQueries: ReviewQuery,
  ) {}
  @Get('get-review/:id')
  @ApiOkResponse({ type: ReviewResponse })
  async getReview(@Param('id') id: string) {
    return this.reviewQueries.getReview(id);
  }
  @Get('get-reviews')
  @ApiPaginatedResponse(ReviewResponse)
  async getReviews(@Query() query: CollectionQuery) {
    return this.reviewQueries.getReviews(query);
  }
  @Post('create-review')
  @UseGuards(RolesGuard('parent|passenger'))
  @ApiOkResponse({ type: ReviewResponse })
  async createReview(@Body() createReviewCommand: CreateReviewCommand) {
    return this.commands.createReview(createReviewCommand);
  }
  @Delete('archive-review')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: Boolean })
  async archiveReview(
    @CurrentUser() user: UserInfo,
    @Body() command: ArchiveReviewCommand,
  ) {
    command.currentUser = user;
    return this.commands.archiveReview(command);
  }
  @Delete('delete-review/:id')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: Boolean })
  async deleteReview(@Param('id') id: string) {
    return this.commands.deleteReview(id);
  }
  @Post('restore-review/:id')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: ReviewResponse })
  async restoreReview(@Param('id') id: string) {
    return this.commands.restoreReview(id);
  }
  @Get('get-archived-reviews')
  @ApiPaginatedResponse(ReviewResponse)
  async getArchivedReviews(@Query() query: CollectionQuery) {
    return this.reviewQueries.getArchivedReviews(query);
  }
}
