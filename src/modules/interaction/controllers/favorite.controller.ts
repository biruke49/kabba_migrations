import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { RolesGuard } from '@account/guards/role.quards';
import { CreateFavoriteCommand } from '@interaction/usecases/favorites/favorite.commands';
import { FavoriteResponse } from '@interaction/usecases/favorites/favorite.response';
import { FavoriteCommands } from '@interaction/usecases/favorites/favorite.usecase.commands';
import { FavoriteQueries } from '@interaction/usecases/favorites/favorite.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('favorites')
@ApiTags('favorites')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class FavoritesController {
  constructor(
    private command: FavoriteCommands,
    private favoriteQuery: FavoriteQueries,
  ) {}
  @Get('get-favorite/:id')
  @ApiOkResponse({ type: FavoriteResponse })
  async getFavorite(@Param('id') id: string) {
    return this.favoriteQuery.getFavorite(id);
  }
  @Get('get-favorites')
  @ApiPaginatedResponse(FavoriteResponse)
  async getFavorites(@Query() query: CollectionQuery) {
    return this.favoriteQuery.getFavorites(query);
  }
  @Get('get-my-favorites')
  @UseGuards(RolesGuard('passenger'))
  @ApiPaginatedResponse(FavoriteResponse)
  async getMyFavorites(
    @CurrentUser() passenger: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.favoriteQuery.getPassengerFavoriteRoutes(passenger.id, query);
  }
  @Get('get-passenger-favorites/:passengerId')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiPaginatedResponse(FavoriteResponse)
  async getPassengerFavorites(
    @Param('passengerId') passengerId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.favoriteQuery.getPassengerFavoriteRoutes(passengerId, query);
  }
  @Post('create-favorite')
  @UseGuards(RolesGuard('passenger'))
  @ApiOkResponse({ type: FavoriteResponse })
  async createFavorite(
    @CurrentUser() user: UserInfo,
    @Body() createFavoriteCommand: CreateFavoriteCommand,
  ) {
    createFavoriteCommand.passengerId = user.id;
    return this.command.createFavorite(createFavoriteCommand);
  }
  @Delete('delete-favorite/:id')
  @ApiOkResponse({ type: Boolean })
  async deleteFavorite(@Param('id') id: string) {
    return this.command.deleteFavorite(id);
  }
  @Get('get-archived-favorites')
  @ApiPaginatedResponse(FavoriteResponse)
  async getArchivedFavorites(@Query() query: CollectionQuery) {
    return this.favoriteQuery.getArchivedFavorites(query);
  }
}
