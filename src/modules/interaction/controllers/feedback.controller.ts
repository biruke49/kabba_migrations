import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import {
  ArchiveFeedbackCommand,
  CreateAnonymousFeedbackCommand,
  CreateFeedbackCommand,
} from '@interaction/usecases/feedbacks/feedback.commands';
import { FeedbackResponse } from '@interaction/usecases/feedbacks/feedback.response';
import { FeedbackCommands } from '@interaction/usecases/feedbacks/feedback.usecase.commands';
import { FeedbackQuery } from '@interaction/usecases/feedbacks/feedback.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('feedbacks')
@ApiTags('feedbacks')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class FeedbacksController {
  constructor(
    private command: FeedbackCommands,
    private feedbackQuery: FeedbackQuery,
  ) {}
  @Get('get-feedback/:id')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: FeedbackResponse })
  async getFeedback(@Param('id') id: string) {
    return this.feedbackQuery.getFeedback(id);
  }
  @Get('get-feedbacks')
  @UseGuards(RolesGuard('admin'))
  @ApiPaginatedResponse(FeedbackResponse)
  async getFeedbacks(@Query() query: CollectionQuery) {
    return this.feedbackQuery.getFeedbacks(query);
  }
  @Post('create-feedback')
  @UseGuards(RolesGuard('passenger|parent'))
  @ApiOkResponse({ type: FeedbackResponse })
  async createFeedback(
    @CurrentUser() user: UserInfo,
    @Body() createFeedbackCommand: CreateFeedbackCommand,
  ) {
    createFeedbackCommand.name = user.name;
    createFeedbackCommand.email = user.email;
    createFeedbackCommand.phoneNumber = user.phoneNumber;
    return this.command.createFeedback(createFeedbackCommand);
  }
  @Post('create-anonymous-feedback')
  @AllowAnonymous()
  @ApiOkResponse({ type: FeedbackResponse })
  async createAnonymousFeedback(
    @Body() createFeedbackCommand: CreateAnonymousFeedbackCommand,
  ) {
    return this.command.createFeedback(createFeedbackCommand);
  }
  @Delete('archive-feedback')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: FeedbackResponse })
  async archiveFeedback(
    @CurrentUser() user: UserInfo,
    @Body() command: ArchiveFeedbackCommand,
  ) {
    command.currentUser = user;
    return this.command.archiveFeedback(command);
  }
  @Delete('delete-feedback/:id')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: Boolean })
  async deleteFeedback(@Param('id') id: string) {
    return this.command.deleteFeedback(id);
  }
  @Post('restore-feedback/:id')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: FeedbackResponse })
  async restoreFeedback(@Param('id') id: string) {
    return this.command.restoreFeedback(id);
  }
  @Get('get-archived-feedbacks')
  @UseGuards(RolesGuard('admin'))
  @ApiPaginatedResponse(FeedbackResponse)
  async getArchivedFeedbacks(@Query() query: CollectionQuery) {
    return this.feedbackQuery.getArchivedFeedbacks(query);
  }
}
