import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { RolesGuard } from '@account/guards/role.quards';
import { CreatePreferenceCommand } from '@interaction/usecases/preferences/preference.commands';
import { PreferenceResponse } from '@interaction/usecases/preferences/preference.response';
import { PreferenceCommands } from '@interaction/usecases/preferences/preference.usecase.commands';
import { PreferenceQueries } from '@interaction/usecases/preferences/preference.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('preferences')
@ApiTags('preferences')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class PreferencesController {
  constructor(
    private command: PreferenceCommands,
    private preferenceQuery: PreferenceQueries,
  ) {}
  @Get('get-preference/:id')
  @ApiOkResponse({ type: PreferenceResponse })
  async getPreference(@Param('id') id: string) {
    return this.preferenceQuery.getPreference(id);
  }
  @Get('get-preferences')
  @AllowAnonymous()
  @ApiPaginatedResponse(PreferenceResponse)
  async getPreferences(@Query() query: CollectionQuery) {
    return this.preferenceQuery.getPreferences(query);
  }
  @Get('get-my-preferences')
  @UseGuards(RolesGuard('driver'))
  @ApiPaginatedResponse(PreferenceResponse)
  async getMyPreferences(
    @CurrentUser() driver: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.preferenceQuery.getDriverPreferenceRoutes(driver.id, query);
  }
  @Get('get-driver-preferences/:driverId')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiPaginatedResponse(PreferenceResponse)
  async getDriverPreferences(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.preferenceQuery.getDriverPreferenceRoutes(driverId, query);
  }
  @Post('create-preference')
  @UseGuards(RolesGuard('driver'))
  @ApiOkResponse({ type: PreferenceResponse })
  async createPreference(
    @CurrentUser() user: UserInfo,
    @Body() createPreferenceCommand: CreatePreferenceCommand,
  ) {
    createPreferenceCommand.driverId = user.id;
    return this.command.createPreference(createPreferenceCommand);
  }
  @Delete('delete-preference/:id')
  @UseGuards(RolesGuard('admin|driver|operator'))
  @ApiOkResponse({ type: Boolean })
  async deletePreference(@Param('id') id: string) {
    return this.command.deletePreference(id);
  }
  @Get('get-archived-preferences')
  @ApiPaginatedResponse(PreferenceResponse)
  async getArchivedPreferences(@Query() query: CollectionQuery) {
    return this.preferenceQuery.getArchivedPreferences(query);
  }
}
