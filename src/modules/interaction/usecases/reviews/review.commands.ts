import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsUUID } from 'class-validator';
import { Review } from '@interaction/domains/reviews/review';
import { UserInfo } from '@account/dtos/user-info.dto';

export class CreateReviewCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  score: number;
  @ApiProperty({
    example: '1ab63a0f-7e18-46d9-af97-ff3dbc88c151',
  })
  @IsUUID()
  driverId: string;
  @ApiProperty({
    example: '1ab63a0f-7e18-46d9-af97-ff3dbc88c150',
  })
  @IsNotEmpty()
  @IsUUID()
  passengerId: string;
  @ApiProperty()
  description: string;
  static fromCommand(command: CreateReviewCommand): Review {
    const reviewDomain = new Review();
    reviewDomain.score = command.score;
    reviewDomain.driverId = command.driverId;
    reviewDomain.passengerId = command.passengerId;
    reviewDomain.description = command.description;
    return reviewDomain;
  }
}
export class ArchiveReviewCommand {
  @ApiProperty()
  id: string;
  @ApiProperty()
  reason: string;
  currentUser: UserInfo;
}
