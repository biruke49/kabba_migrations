import { PassengerResponse } from '@customer/usecases/passengers/passenger.response';
import { Review } from '@interaction/domains/reviews/review';
import { ReviewEntity } from '@interaction/persistence/reviews/review.entity';
import { ApiProperty } from '@nestjs/swagger';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';

export class ReviewResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  score: number;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  passengerId: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty()
  passenger: PassengerResponse;
  @ApiProperty()
  driver: DriverResponse;
  static fromEntity(reviewEntity: ReviewEntity): ReviewResponse {
    const reviewResponse = new ReviewResponse();
    reviewResponse.id = reviewEntity.id;
    reviewResponse.score = reviewEntity.score;
    reviewResponse.driverId = reviewEntity.driverId;
    reviewResponse.passengerId = reviewEntity.passengerId;
    reviewResponse.description = reviewEntity.description;
    if (reviewEntity.passenger) {
      reviewResponse.passenger = PassengerResponse.fromEntity(
        reviewEntity.passenger,
      );
    }
    if (reviewEntity.driver) {
      reviewResponse.driver = DriverResponse.fromEntity(reviewEntity.driver);
    }
    reviewResponse.archiveReason = reviewEntity.archiveReason;
    reviewResponse.createdBy = reviewEntity.createdBy;
    reviewResponse.updatedBy = reviewEntity.updatedBy;
    reviewResponse.deletedBy = reviewEntity.deletedBy;
    reviewResponse.createdAt = reviewEntity.createdAt;
    reviewResponse.updatedAt = reviewEntity.updatedAt;
    reviewResponse.deletedAt = reviewEntity.deletedAt;
    return reviewResponse;
  }
  static fromDomain(review: Review): ReviewResponse {
    const reviewResponse = new ReviewResponse();
    reviewResponse.id = review.id;
    reviewResponse.score = review.score;
    reviewResponse.driverId = review.driverId;
    reviewResponse.passengerId = review.passengerId;
    reviewResponse.description = review.description;
    if (review.passenger) {
      reviewResponse.passenger = PassengerResponse.fromDomain(review.passenger);
    }
    if (review.driver) {
      reviewResponse.driver = DriverResponse.fromDomain(review.driver);
    }
    reviewResponse.archiveReason = review.archiveReason;
    reviewResponse.createdBy = review.createdBy;
    reviewResponse.updatedBy = review.updatedBy;
    reviewResponse.deletedBy = review.deletedBy;
    reviewResponse.createdAt = review.createdAt;
    reviewResponse.updatedAt = review.updatedAt;
    reviewResponse.deletedAt = review.deletedAt;
    return reviewResponse;
  }
}
