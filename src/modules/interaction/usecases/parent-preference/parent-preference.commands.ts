import { ParentPreference } from '@interaction/domains/parent-preference/parent-preference';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateParentPreferenceCommand {
  @ApiProperty()
  @IsNotEmpty()
  days: string[];
  @ApiProperty()
  @IsNotEmpty()
  driverGender: string;
  parentId: string;
  static fromCommand(command: CreateParentPreferenceCommand): ParentPreference {
    const preferenceDomain = new ParentPreference();
    preferenceDomain.days = command.days;
    preferenceDomain.driverGender = command.driverGender;
    preferenceDomain.parentId = command.parentId;
    return preferenceDomain;
  }
}
