import { ParentPreferenceRepository } from '@interaction/persistence/parent-preference/parent-preference.repository';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateParentPreferenceCommand } from './parent-preference.commands';
import { ParentPreferenceResponse } from './parent-preference.response';

@Injectable()
export class ParentPreferenceCommands {
  constructor(private parentPreferenceRepository: ParentPreferenceRepository) {}
  async createParentPreference(
    command: CreateParentPreferenceCommand,
  ): Promise<ParentPreferenceResponse> {
    const existingDriverPreference =
      await this.parentPreferenceRepository.getByParentId(command.parentId);
    if (existingDriverPreference) {
      this.deleteParentPreference(existingDriverPreference.id);
    }
    const preferenceDomain = CreateParentPreferenceCommand.fromCommand(command);
    const preference = await this.parentPreferenceRepository.insert(
      preferenceDomain,
    );
    return ParentPreferenceResponse.fromDomain(preference);
  }
  async deleteParentPreference(id: string): Promise<boolean> {
    const preferenceDomain = await this.parentPreferenceRepository.getById(
      id,
      true,
    );
    if (!preferenceDomain) {
      throw new NotFoundException(`Parent Preference not found with id ${id}`);
    }
    return await this.parentPreferenceRepository.delete(id);
  }
}
