import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ParentPreferenceResponse } from './parent-preference.response';

@Injectable()
export class ParentPreferenceQueries {
  constructor(
    @InjectRepository(ParentPreferenceEntity)
    private preferenceRepository: Repository<ParentPreferenceEntity>,
  ) {}
  async getParentPreference(id: string): Promise<ParentPreferenceResponse> {
    const preference = await this.preferenceRepository.find({
      where: { id: id },
      relations: [],
    });
    if (!preference[0]) {
      throw new NotFoundException(`Preference not found with id ${id}`);
    }
    return ParentPreferenceResponse.fromEntity(preference[0]);
  }
  async getParentPreferences(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ParentPreferenceResponse>> {
    const dataQuery = QueryConstructor.constructQuery<ParentPreferenceEntity>(
      this.preferenceRepository,
      query,
    );
    const d = new DataResponseFormat<ParentPreferenceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        ParentPreferenceResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getParentPreferenceByParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ParentPreferenceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'parentId',
        value: parentId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<ParentPreferenceEntity>(
      this.preferenceRepository,
      query,
    );
    const d = new DataResponseFormat<ParentPreferenceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        ParentPreferenceResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getArchivedParentPreferences(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ParentPreferenceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<ParentPreferenceEntity>(
      this.preferenceRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<ParentPreferenceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        ParentPreferenceResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
}
