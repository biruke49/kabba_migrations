import { ParentResponse } from '@customer/usecases/parents/parent.response';
import { ParentPreference } from '@interaction/domains/parent-preference/parent-preference';
import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { ApiProperty } from '@nestjs/swagger';

export class ParentPreferenceResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  parentId: string;
  @ApiProperty()
  driverGender: string;
  @ApiProperty()
  days: string[];
  @ApiProperty()
  createdAt: Date;
  parent: ParentResponse;
  static fromEntity(
    preferenceEntity: ParentPreferenceEntity,
  ): ParentPreferenceResponse {
    const preferenceResponse = new ParentPreferenceResponse();
    preferenceResponse.id = preferenceEntity.id;
    preferenceResponse.parentId = preferenceEntity.parentId;
    preferenceResponse.driverGender = preferenceEntity.driverGender;
    preferenceResponse.days = preferenceEntity.days;
    preferenceResponse.createdAt = preferenceEntity.createdAt;
    if (preferenceEntity.parent) {
      preferenceResponse.parent = ParentResponse.fromEntity(
        preferenceEntity.parent,
      );
    }
    return preferenceResponse;
  }
  static fromDomain(preference: ParentPreference): ParentPreferenceResponse {
    const preferenceResponse = new ParentPreferenceResponse();
    preferenceResponse.id = preference.id;
    preferenceResponse.parentId = preference.parentId;
    preferenceResponse.driverGender = preference.driverGender;
    preferenceResponse.days = preference.days;
    preferenceResponse.createdAt = preference.createdAt;
    if (preference.parent) {
      preferenceResponse.parent = ParentResponse.fromDomain(preference.parent);
    }
    return preferenceResponse;
  }
}
