import { EventEmitter2 } from '@nestjs/event-emitter';
import {
  ArchiveFeedbackCommand,
  CreateAnonymousFeedbackCommand,
} from './feedback.commands';
import { FeedbackRepository } from '@interaction/persistence/feedbacks/feedback.repository';
import { FeedbackResponse } from './feedback.response';
import { Injectable, NotFoundException } from '@nestjs/common';
import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
@Injectable()
export class FeedbackCommands {
  constructor(private feedbackRepository: FeedbackRepository) {}
  async createFeedback(
    command: CreateAnonymousFeedbackCommand,
  ): Promise<FeedbackResponse> {
    const feedbackDomain = CreateAnonymousFeedbackCommand.fromCommand(command);
    const feedback = await this.feedbackRepository.insert(feedbackDomain);
    return FeedbackResponse.fromDomain(feedback);
  }

  async archiveFeedback(
    command: ArchiveFeedbackCommand,
  ): Promise<FeedbackResponse> {
    const feedbackDomain = await this.feedbackRepository.getById(command.id);
    if (!feedbackDomain) {
      throw new NotFoundException(`Feedback not found with id ${command.id}`);
    }
    feedbackDomain.deletedAt = new Date();
    feedbackDomain.deletedBy = command.currentUser.id;
    feedbackDomain.archiveReason = command.reason;
    const result = await this.feedbackRepository.update(feedbackDomain);
    return FeedbackResponse.fromDomain(result);
  }
  async restoreFeedback(id: string): Promise<FeedbackResponse> {
    const feedbackDomain = await this.feedbackRepository.getById(id, true);
    if (!feedbackDomain) {
      throw new NotFoundException(`Feedback not found with id ${id}`);
    }
    const r = await this.feedbackRepository.restore(id);
    if (r) {
      feedbackDomain.deletedAt = null;
    }
    return FeedbackResponse.fromDomain(feedbackDomain);
  }
  async deleteFeedback(id: string): Promise<boolean> {
    const feedbackDomain = await this.feedbackRepository.getById(id, true);
    if (!feedbackDomain) {
      throw new NotFoundException(`Feedback not found with id ${id}`);
    }
    return await this.feedbackRepository.delete(id);
  }
}
