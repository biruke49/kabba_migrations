import { ApiProperty } from '@nestjs/swagger';
import { Favorite } from '@interaction/domains/favorites/favorite';
import { IsNotEmpty } from 'class-validator';

export class CreateFavoriteCommand {
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeName: string;
  passengerId: string;
  static fromCommand(command: CreateFavoriteCommand): Favorite {
    const favoriteDomain = new Favorite();
    favoriteDomain.routeId = command.routeId;
    favoriteDomain.routeName = command.routeName;
    favoriteDomain.passengerId = command.passengerId;
    return favoriteDomain;
  }
}
