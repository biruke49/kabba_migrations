import { FavoriteRepository } from '@interaction/persistence/favorites/favorite.repository';
import { FavoriteResponse } from './favorite.response';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateFavoriteCommand } from './favorite.commands';
@Injectable()
export class FavoriteCommands {
  constructor(private favoriteRepository: FavoriteRepository) {}
  async createFavorite(
    command: CreateFavoriteCommand,
  ): Promise<FavoriteResponse> {
    const existingPassengerFavorite =
      await this.favoriteRepository.getByPassengerIdAndRouteId(
        command.passengerId,
        command.routeId,
      );
    if (existingPassengerFavorite) {
      return FavoriteResponse.fromDomain(existingPassengerFavorite);
    }
    const favoriteDomain = CreateFavoriteCommand.fromCommand(command);
    const favorite = await this.favoriteRepository.insert(favoriteDomain);
    return FavoriteResponse.fromDomain(favorite);
  }
  async deleteFavorite(id: string): Promise<boolean> {
    const favoriteDomain = await this.favoriteRepository.getById(id, true);
    if (!favoriteDomain) {
      throw new NotFoundException(`Favorite not found with id ${id}`);
    }
    return await this.favoriteRepository.delete(id);
  }
}
