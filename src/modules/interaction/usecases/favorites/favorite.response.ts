import { PassengerResponse } from '@customer/usecases/passengers/passenger.response';
import { Favorite } from '@interaction/domains/favorites/favorite';
import { FavoriteEntity } from '@interaction/persistence/favorites/favorite.entity';
import { ApiProperty } from '@nestjs/swagger';
import { RouteResponse } from '@router/usecases/routes/route.response';

export class FavoriteResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  routeName: string;
  @ApiProperty()
  passengerId: string;
  @ApiProperty()
  createdAt: Date;
  passenger: PassengerResponse;
  route: RouteResponse;
  static fromEntity(favoriteEntity: FavoriteEntity): FavoriteResponse {
    const favoriteResponse = new FavoriteResponse();
    favoriteResponse.id = favoriteEntity.id;
    favoriteResponse.passengerId = favoriteEntity.passengerId;
    favoriteResponse.routeId = favoriteEntity.routeId;
    favoriteResponse.routeName = favoriteEntity.routeName;
    favoriteResponse.createdAt = favoriteEntity.createdAt;
    if (favoriteEntity.passenger) {
      favoriteResponse.passenger = PassengerResponse.fromEntity(
        favoriteEntity.passenger,
      );
    }
    if (favoriteEntity.route) {
      favoriteResponse.route = RouteResponse.fromEntity(favoriteEntity.route);
    }
    return favoriteResponse;
  }
  static fromDomain(favorite: Favorite): FavoriteResponse {
    const favoriteResponse = new FavoriteResponse();
    favoriteResponse.id = favorite.id;
    favoriteResponse.passengerId = favorite.passengerId;
    favoriteResponse.routeId = favorite.routeId;
    favoriteResponse.routeName = favorite.routeName;
    favoriteResponse.createdAt = favorite.createdAt;
    if (favorite.passenger) {
      favoriteResponse.passenger = PassengerResponse.fromDomain(
        favorite.passenger,
      );
    }
    if (favorite.route) {
      favoriteResponse.route = RouteResponse.fromDomain(favorite.route);
    }
    return favoriteResponse;
  }
}
