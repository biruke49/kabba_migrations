import {
  CollectionQuery,
  Filter,
} from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FavoriteEntity } from '@interaction/persistence/favorites/favorite.entity';
import { Repository } from 'typeorm';
import { FavoriteResponse } from './favorite.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
@Injectable()
export class FavoriteQueries {
  constructor(
    @InjectRepository(FavoriteEntity)
    private favoriteRepository: Repository<FavoriteEntity>,
  ) {}
  async getFavorite(id: string): Promise<FavoriteResponse> {
    const favorite = await this.favoriteRepository.find({
      where: { id: id },
      relations: [],
    });
    if (!favorite[0]) {
      throw new NotFoundException(`Favorite not found with id ${id}`);
    }
    return FavoriteResponse.fromEntity(favorite[0]);
  }
  async getFavorites(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<FavoriteResponse>> {
    const dataQuery = QueryConstructor.constructQuery<FavoriteEntity>(
      this.favoriteRepository,
      query,
    );
    const d = new DataResponseFormat<FavoriteResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => FavoriteResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getPassengerFavoriteRoutes(
    passengerId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<FavoriteResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'passengerId',
        value: passengerId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<FavoriteEntity>(
      this.favoriteRepository,
      query,
    );
    const d = new DataResponseFormat<FavoriteResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => FavoriteResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedFavorites(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<FavoriteResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<FavoriteEntity>(
      this.favoriteRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<FavoriteResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => FavoriteResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
