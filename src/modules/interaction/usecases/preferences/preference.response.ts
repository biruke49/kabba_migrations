import { Preference } from '@interaction/domains/preferences/preference';
import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { ApiProperty } from '@nestjs/swagger';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { RouteResponse } from '@router/usecases/routes/route.response';

export class PreferenceResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  routeName: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  createdAt: Date;
  driver: DriverResponse;
  route: RouteResponse;
  static fromEntity(preferenceEntity: PreferenceEntity): PreferenceResponse {
    const preferenceResponse = new PreferenceResponse();
    preferenceResponse.id = preferenceEntity.id;
    preferenceResponse.driverId = preferenceEntity.driverId;
    preferenceResponse.routeId = preferenceEntity.routeId;
    preferenceResponse.routeName = preferenceEntity.routeName;
    preferenceResponse.createdAt = preferenceEntity.createdAt;
    if (preferenceEntity.driver) {
      preferenceResponse.driver = DriverResponse.fromEntity(
        preferenceEntity.driver,
      );
    }
    if (preferenceEntity.route) {
      preferenceResponse.route = RouteResponse.fromEntity(
        preferenceEntity.route,
      );
    }
    return preferenceResponse;
  }
  static fromDomain(preference: Preference): PreferenceResponse {
    const preferenceResponse = new PreferenceResponse();
    preferenceResponse.id = preference.id;
    preferenceResponse.driverId = preference.driverId;
    preferenceResponse.routeId = preference.routeId;
    preferenceResponse.routeName = preference.routeName;
    preferenceResponse.createdAt = preference.createdAt;
    if (preference.driver) {
      preferenceResponse.driver = DriverResponse.fromDomain(preference.driver);
    }
    if (preference.route) {
      preferenceResponse.route = RouteResponse.fromDomain(preference.route);
    }
    return preferenceResponse;
  }
}
