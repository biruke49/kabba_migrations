import { ApiProperty } from '@nestjs/swagger';
import { Preference } from '@interaction/domains/preferences/preference';
import { IsNotEmpty } from 'class-validator';

export class CreatePreferenceCommand {
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeName: string;
  driverId: string;
  static fromCommand(command: CreatePreferenceCommand): Preference {
    const preferenceDomain = new Preference();
    preferenceDomain.routeId = command.routeId;
    preferenceDomain.routeName = command.routeName;
    preferenceDomain.driverId = command.driverId;
    return preferenceDomain;
  }
}
