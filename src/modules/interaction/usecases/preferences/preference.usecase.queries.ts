import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { Repository } from 'typeorm';
import { PreferenceResponse } from './preference.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
@Injectable()
export class PreferenceQueries {
  constructor(
    @InjectRepository(PreferenceEntity)
    private preferenceRepository: Repository<PreferenceEntity>,
  ) {}
  async getPreference(id: string): Promise<PreferenceResponse> {
    const preference = await this.preferenceRepository.find({
      where: { id: id },
      relations: [],
    });
    if (!preference[0]) {
      throw new NotFoundException(`Preference not found with id ${id}`);
    }
    return PreferenceResponse.fromEntity(preference[0]);
  }
  async getPreferences(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PreferenceResponse>> {
    const dataQuery = QueryConstructor.constructQuery<PreferenceEntity>(
      this.preferenceRepository,
      query,
    );
    const d = new DataResponseFormat<PreferenceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PreferenceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getDriverPreferenceRoutes(
    driverId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PreferenceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        value: driverId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<PreferenceEntity>(
      this.preferenceRepository,
      query,
    );
    const d = new DataResponseFormat<PreferenceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PreferenceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getPreferencesByRoute(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PreferenceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'routeId',
        value: routeId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<PreferenceEntity>(
      this.preferenceRepository,
      query,
    );
    const d = new DataResponseFormat<PreferenceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PreferenceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedPreferences(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PreferenceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<PreferenceEntity>(
      this.preferenceRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<PreferenceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PreferenceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
