import { PreferenceRepository } from '@interaction/persistence/preferences/preference.repository';
import { PreferenceResponse } from './preference.response';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePreferenceCommand } from './preference.commands';
@Injectable()
export class PreferenceCommands {
  constructor(private preferenceRepository: PreferenceRepository) {}
  async createPreference(
    command: CreatePreferenceCommand,
  ): Promise<PreferenceResponse> {
    const existingDriverPreference =
      await this.preferenceRepository.getByDriverIdAndRouteId(
        command.driverId,
        command.routeId,
      );
    if (existingDriverPreference) {
      return PreferenceResponse.fromDomain(existingDriverPreference);
    }
    const preferenceDomain = CreatePreferenceCommand.fromCommand(command);
    const preference = await this.preferenceRepository.insert(preferenceDomain);
    return PreferenceResponse.fromDomain(preference);
  }
  async deletePreference(id: string): Promise<boolean> {
    const preferenceDomain = await this.preferenceRepository.getById(id, true);
    if (!preferenceDomain) {
      throw new NotFoundException(`Preference not found with id ${id}`);
    }
    return await this.preferenceRepository.delete(id);
  }
}
