import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ArchiveBusStopCommand,
  CreateBusStopCommand,
  UpdateBusStopCommand,
} from '@router/usecases/cities/bus-stop.commands';
import { BusStopResponse } from '@router/usecases/cities/bus-stop.response';
import { BusStopCommands } from '@router/usecases/cities/bus-stop.usecase.commands';
import { BusStopQueries } from '@router/usecases/cities/bus-stop.usecase.queries';
import {
  ArchiveCityCommand,
  CreateCityCommand,
  UpdateCityCommand,
} from '@router/usecases/cities/city.commands';
import { CityResponse } from '@router/usecases/cities/city.response';
import { CityCommands } from '@router/usecases/cities/city.usecase.commands';
import { CityQueries } from '@router/usecases/cities/city.usecase.queries';

@Controller('cities')
@ApiTags('cities')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class CitiesController {
  constructor(
    private readonly command: CityCommands,
    private readonly cityQueries: CityQueries,
    private readonly busStopCommand: BusStopCommands,
    private readonly busStopQueries: BusStopQueries,
  ) {}
  @Get('get-city/:id')
  @ApiOkResponse({ type: CityResponse })
  async getCity(@Param('id') id: string) {
    return this.cityQueries.getCity(id);
  }
  @Get('get-archived-city/:id')
  @ApiOkResponse({ type: CityResponse })
  async getArchivedCity(@Param('id') id: string) {
    return this.cityQueries.getCity(id, true);
  }
  @Get('get-cities')
  @ApiPaginatedResponse(CityResponse)
  async getCities(@Query() query: CollectionQuery) {
    return this.cityQueries.getCities(query);
  }
  @Post('create-city')
  @UseGuards(PermissionsGuard('manage-cities'))
  @ApiOkResponse({ type: CityResponse })
  async createCity(
    @CurrentUser() user: UserInfo,
    @Body() createCityCommand: CreateCityCommand,
  ) {
    createCityCommand.currentUser = user;
    return this.command.createCity(createCityCommand);
  }
  @Put('update-city')
  @UseGuards(PermissionsGuard('manage-cities'))
  @ApiOkResponse({ type: CityResponse })
  async updateCity(
    @CurrentUser() user: UserInfo,
    @Body() updateCityCommand: UpdateCityCommand,
  ) {
    updateCityCommand.currentUser = user;
    return this.command.updateCity(updateCityCommand);
  }
  @Delete('archive-city')
  @UseGuards(PermissionsGuard('manage-cities'))
  @ApiOkResponse({ type: Boolean })
  async archiveCity(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveCityCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveCity(archiveCommand);
  }
  @Delete('delete-city/:id')
  @UseGuards(PermissionsGuard('manage-cities'))
  @ApiOkResponse({ type: Boolean })
  async deleteCity(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteCity(id, user);
  }
  @Post('restore-city/:id')
  @UseGuards(PermissionsGuard('manage-cities'))
  @ApiOkResponse({ type: CityResponse })
  async restoreCity(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreCity(id, user);
  }
  @Get('get-archived-cities')
  @ApiPaginatedResponse(CityResponse)
  async getArchivedCities(@Query() query: CollectionQuery) {
    return this.cityQueries.getArchivedCities(query);
  }

  //bus stop
  @Get('get-city-bus-stop/:id')
  @ApiOkResponse({ type: BusStopResponse })
  async getCityBusStop(@Param('id') id: string) {
    return this.busStopQueries.getBusStop(id);
  }
  @Get('get-archived-bus-stop/:id')
  @ApiOkResponse({ type: BusStopResponse })
  async getArchivedCityBusStop(@Param('id') id: string) {
    return this.busStopQueries.getBusStop(id, true);
  }
  @Get('get-city-bus-stops')
  @ApiPaginatedResponse(BusStopResponse)
  async getCityBusStops(@Query() query: CollectionQuery) {
    return this.busStopQueries.getBusStops(query);
  }
  @Get('get-bus-stops-by-city-id/:cityId')
  @ApiPaginatedResponse(BusStopResponse)
  async getCityBusStopsByCityId(
    @Param('cityId') cityId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.busStopQueries.getBusStopsByCityId(cityId, query);
  }
  @Post('create-city-bus-stop')
  @UseGuards(PermissionsGuard('manage-bus-stops'))
  @ApiOkResponse({ type: BusStopResponse })
  async createCityBusStop(
    @CurrentUser() user: UserInfo,
    @Body() command: CreateBusStopCommand,
  ) {
    command.currentUser = user;
    return this.busStopCommand.createBusStop(command);
  }
  @Put('update-city-bus-stop')
  @UseGuards(PermissionsGuard('manage-bus-stops'))
  @ApiOkResponse({ type: BusStopResponse })
  async updateCityBusStop(
    @CurrentUser() user: UserInfo,
    @Body() updateCommand: UpdateBusStopCommand,
  ) {
    updateCommand.currentUser = user;
    return this.busStopCommand.updateBusStop(updateCommand);
  }
  @Delete('archive-city-bus-stop')
  @UseGuards(PermissionsGuard('manage-bus-stops'))
  @ApiOkResponse({ type: Boolean })
  async archiveCityBusStop(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveBusStopCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.busStopCommand.archiveBusStop(archiveCommand);
  }
  @Delete('delete-city-bus-stop/:id')
  @UseGuards(PermissionsGuard('manage-bus-stops'))
  @ApiOkResponse({ type: Boolean })
  async deleteCityBusStop(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.busStopCommand.deleteBusStop(id, user);
  }
  @Post('restore-city-bus-stop/:id')
  @UseGuards(PermissionsGuard('manage-bus-stops'))
  @ApiOkResponse({ type: BusStopResponse })
  async restoreCityBusStop(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.busStopCommand.restoreBusStop(id, user);
  }
  @ApiPaginatedResponse(CityResponse)
  async getArchivedCityBusStops(@Query() query: CollectionQuery) {
    return this.busStopQueries.getArchivedBusStops(query);
  }
}
