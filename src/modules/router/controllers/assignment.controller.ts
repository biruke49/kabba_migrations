import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  StreamableFile,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import {
  ArchiveAssignmentCommand,
  CreateAssignmentCommand,
  CreateBulkAssignmentCommand2,
  PayForMultipleDriverAssignmentsCommand,
  UpdateAssignmentCommand,
} from '@router/usecases/assignments/assignment.commands';
import {
  AssignmentResponse,
  CountAssignmentByRoute,
  CountByStatusResponse,
} from '@router/usecases/assignments/assignment.response';
import { AssignmentCommands } from '@router/usecases/assignments/assignment.usecase.commands';
import { AssignmentQueries } from '@router/usecases/assignments/assignment.usecase.queries';

import {
  CancelAssignmentCommand,
  PayForDriverAssignmentsCommand,
} from '../usecases/assignments/assignment.commands';
import { TransactionTotalResponse } from '@libs/common/corporate-transaction-spending';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { Response } from 'express';
import * as XLSX from 'xlsx';
@Controller('assignments')
@ApiTags('assignments')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class AssignmentsController {
  constructor(
    private readonly commands: AssignmentCommands,
    private readonly assignmentQueries: AssignmentQueries,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-assignment/:id')
  @ApiOkResponse({ type: AssignmentResponse })
  async getAssignment(@Param('id') id: string) {
    return this.assignmentQueries.getAssignment(id);
  }
  @Get('get-archived-assignment/:id')
  @ApiOkResponse({ type: AssignmentResponse })
  async getArchivedAssignment(@Param('id') id: string) {
    return this.assignmentQueries.getAssignment(id, true);
  }
  @Get('get-assignments')
  @ApiPaginatedResponse(AssignmentResponse)
  async getAssignments(@Query() query: CollectionQuery) {
    return this.assignmentQueries.getAssignments(query);
  }
  @Get('group-assignments-by-routes')
  @ApiPaginatedResponse(CountAssignmentByRoute)
  async groupAssignmentsByRoutes(@Query() query: CollectionQuery) {
    return this.assignmentQueries.groupAssignmentsByRoutes(query);
  }
  @Get('get-company-earning')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getCompanyEarning(@Query() query: CollectionQuery) {
    return this.assignmentQueries.getCompanyEarning(query);
  }
  @Get('group-earning-by-created-date/:format')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupEarningsByCreatedDate(
    @Query() query: CollectionQuery,
    @Param('format') format: string,
  ) {
    return this.assignmentQueries.groupEarningsByCreatedDate(query, format);
  }
  // @Get('group-earning-by-created-date/:format')
  // @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  // async groupBookingsByCreatedDate(
  //   @Query() query: CollectionQuery,
  //   @Param('format') format: string,
  // ) {
  //   return this.assignmentQueries.groupEarningsByCreatedDateFormat(
  //     query,
  //     format,
  //   );
  // }
  @Get('group-earning-by-category')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupEarningsByCategory(@Query() query: CollectionQuery) {
    return this.assignmentQueries.groupEarningsByCategory(query);
  }
  @Get('group-assignments-by-status')
  @ApiOkResponse({ type: CountByStatusResponse, isArray: true })
  async groupAssignmentsByStatus(@Query() query: CollectionQuery) {
    return this.assignmentQueries.groupAssignmentsByStatus(query);
  }
  @Get('group-assignments-status-by-routes')
  @ApiOkResponse({ type: CountByStatusResponse, isArray: true })
  async groupAssignmentsStatusByRoutes(@Query() query: CollectionQuery) {
    return this.assignmentQueries.groupAssignmentsStatusByRoutes(query);
  }
  // @Get('get-my-earning')
  // @ApiOkResponse({ type: TransactionTotalResponse })
  // async getMyEarning(
  //   @CurrentUser() profile: UserInfo,
  //   @Query() query: CollectionQuery,
  // ) {
  //   return this.assignmentQueries.getDriverEarning(profile.id, query);
  // }
  // @Get('get-my-weekly-earning')
  // @ApiOkResponse({ type: CountWeeklyBookingByCreatedAtResponse })
  // async getMyWeeklyEarning(
  //   @CurrentUser() profile: UserInfo,
  //   @Query() query: CollectionQuery,
  // ) {
  //   return this.assignmentQueries.getDriverWeeklyEarning(profile.id, query);
  // }
  // @Get('get-driver-weekly-earning/:driverId')
  // @ApiOkResponse({ type: CountWeeklyBookingByCreatedAtResponse, isArray: true })
  // async getDriverWeeklyEarning(
  //   @Param('driverId') driverId: string,
  //   @Query() query: CollectionQuery,
  // ) {
  //   return this.assignmentQueries.getDriverWeeklyEarning(driverId, query);
  // }
  @Get('get-route-assignments/:routeId')
  @ApiPaginatedResponse(AssignmentResponse)
  async getRouteAssignments(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getRouteAssignments(routeId, query);
  }
  @Get('get-my-assignments')
  @UseGuards(RolesGuard('driver'))
  @ApiPaginatedResponse(AssignmentResponse)
  async getMyAssignments(
    @CurrentUser() user: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getDriverAssignments(user.id, query);
  }
  @Get('get-my-active-assignments')
  @UseGuards(RolesGuard('driver'))
  @ApiPaginatedResponse(AssignmentResponse)
  async getMyActiveAssignments(
    @CurrentUser() user: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    if (!query.filter) {
      query.filter = [];
    }
    const currentDate = new Date();
    const date =
      currentDate.getDate() >= 10
        ? currentDate.getDate()
        : '0' + currentDate.getDate().toString();
    const month =
      1 + currentDate.getMonth() >= 10
        ? currentDate.getMonth() + 1
        : '0' + (currentDate.getMonth() + 1).toString();
    query.filter.push(
      [
        {
          field: 'assignmentDate',
          value: `${currentDate.getFullYear()}-${month}-${date}`,
          operator: FilterOperators.GreaterThanOrEqualTo,
        },
      ],
      // [{
      //   field: 'status',
      //   value: Status.,
      //   operator: FilterOperators.EqualTo
      // }]
    );
    return this.assignmentQueries.getDriverAssignments(user.id, query);
  }
  @Get('get-driver-assignments/:driverId')
  @ApiPaginatedResponse(AssignmentResponse)
  async getDriverAssignments(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getDriverAssignments(driverId, query);
  }
  @Post('create-assignment')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: AssignmentResponse })
  async createAssignment(
    @CurrentUser() user: UserInfo,
    @Body() createAssignmentCommand: CreateAssignmentCommand,
  ) {
    createAssignmentCommand.currentUser = user;
    return this.commands.createAssignment(createAssignmentCommand);
  }
  @Post('create-bulk-assignment')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: AssignmentResponse, isArray: true })
  async createBulkAssignment(
    @CurrentUser() user: UserInfo,
    @Body() createBulkAssignmentCommand: CreateBulkAssignmentCommand2,
  ) {
    createBulkAssignmentCommand.currentUser = user;
    return this.commands.createBulkAssignment2(createBulkAssignmentCommand);
  }
  @Put('update-assignment')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: AssignmentResponse })
  async updateAssignment(
    @CurrentUser() user: UserInfo,
    @Body() updateAssignmentCommand: UpdateAssignmentCommand,
  ) {
    updateAssignmentCommand.currentUser = user;
    return this.commands.updateAssignment(updateAssignmentCommand);
  }
  @Delete('archive-assignment')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: Boolean })
  async archiveAssignment(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveAssignmentCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.commands.archiveAssignment(archiveCommand);
  }
  @Delete('delete-assignment/:id')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: Boolean })
  async deleteAssignment(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.commands.deleteAssignment(id, user);
  }
  @Post('restore-assignment/:id')
  @UseGuards(PermissionsGuard('assignments/manage-assignments'))
  @ApiOkResponse({ type: AssignmentResponse })
  async restoreAssignment(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.commands.restoreAssignment(id, user);
  }
  @Get('get-archived-assignments')
  @ApiPaginatedResponse(AssignmentResponse)
  async getArchivedAssignments(@Query() query: CollectionQuery) {
    return this.assignmentQueries.getArchivedAssignments(query);
  }
  @Post('start-assignment/:id')
  @UseGuards(RolesGuard('driver|admin|operator'))
  @ApiOkResponse({ type: AssignmentResponse })
  async startAssignment(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.commands.startAssignment(id, user);
  }
  @Post('complete-assignment/:id')
  @UseGuards(RolesGuard('driver|admin|operator'))
  @ApiOkResponse({ type: AssignmentResponse })
  async completeAssignment(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.commands.completeAssignment(id, user);
  }
  @Post('cancel-assignment')
  @UseGuards(RolesGuard('driver|admin|operator'))
  @ApiOkResponse({ type: AssignmentResponse })
  async cancelAssignment(
    @CurrentUser() user: UserInfo,
    @Body() command: CancelAssignmentCommand,
  ) {
    command.currentUser = user;
    command.cancelledReason.cancelledAt = new Date();
    return this.commands.cancelAssignment(command);
  }
  @Post('pay-for-driver-assignment')
  @UseGuards(PermissionsGuard('assignments/pay-for-driver-assignment'))
  async payForDriverAssignment(
    @CurrentUser() user: UserInfo,
    @Body() command: PayForDriverAssignmentsCommand,
  ) {
    command.currentUser = user;
    return this.commands.payForDriverAssignments(command);
  }
  @Post('pay-for-multiple-driver-assignment')
  @UseGuards(PermissionsGuard('assignments/pay-for-driver-assignment'))
  async payForMultipleDriverAssignments(
    @CurrentUser() user: UserInfo,
    @Body() command: PayForMultipleDriverAssignmentsCommand,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    command.currentUser = user;
    const exportData = await this.commands.payForMultipleDriverAssignments(
      command,
    );
    const currentDate = new Date();
    const fileName = `driver-payment-${currentDate.getFullYear()}-${currentDate.getMonth()}-${currentDate.getDate()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const rows = exportData.map((payment) => {
      return {
        Name: payment.driverName,
        'Account Number': payment.accountNumber,
        Total: payment.totalPayment,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(
      workBook,
      workSheet,
      `Driver-payment-${currentDate.getFullYear()}-${currentDate.getMonth()}-${currentDate.getDate()}`,
    );
    /* fix headers */
    XLSX.utils.sheet_add_aoa(workSheet, [['Name', 'Account Number', 'Total']], {
      origin: 'A1',
    });

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
  @Get('get-drivers-in-route/:routeId')
  @ApiOkResponse({ type: DriverResponse })
  async getDriversInRoute(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getDriversInRoute(routeId, query);
  }
  @Get('get-route-assignment-dates/:routeId')
  @ApiOkResponse({ type: AssignmentResponse })
  async getRouteAssignmentDates(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getRouteAssignmentDates(routeId, query);
  }
  @Get('get-route-assignment-times/:routeId')
  @ApiOkResponse({ type: AssignmentResponse })
  async getRouteAssignmentTimes(
    @Param('routeId') routeId: string,
    @Query('assignmentDate') assignmentDate: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getRouteAssignmentTimes(
      routeId,
      assignmentDate,
      query,
    );
  }
  @Get('get-available-assignment-vehicle-categories/:routeId')
  @ApiOkResponse({ type: AssignmentResponse })
  async getAvailableAssignmentVehicleCategories(
    @Query('pickupTime') pickupTime: string,
    @Query('assignmentDate') assignmentDate: string,
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getAvailableAssignmentVehicleCategories(
      routeId,
      assignmentDate,
      pickupTime,
      query,
    );
  }
  @Get('get-available-assignment-vehicle-categories-by-time')
  @ApiOkResponse({ type: AssignmentResponse })
  async getAvailableAssignmentVehicleCategoriesByTime(
    @Query('pickupTime') pickupTime: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getAvailableAssignmentVehicleCategoriesByTime(
      pickupTime,
      query,
    );
  }
  @Get('get-driver-assignment-dates/:driverId')
  @ApiOkResponse({ type: AssignmentResponse })
  async getDriverAssignmentDates(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getDriverAssignmentDates(driverId, query);
  }
  @Get('get-my-assignment-dates')
  @UseGuards(RolesGuard('driver'))
  @ApiOkResponse({ type: AssignmentResponse })
  async getMyAssignmentDates(
    @CurrentUser() driver: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.assignmentQueries.getDriverAssignmentDates(driver.id, query);
  }
  @Get('check-uncompleted-assignments')
  @AllowAnonymous()
  @ApiOkResponse({ type: AssignmentResponse })
  async getUncompletedAssignments(@Query() query: CollectionQuery) {
    await this.assignmentQueries.getUncompletedAssignments(query);
  }
}
