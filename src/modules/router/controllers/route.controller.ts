import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  RestoreRoutePriceDriverCommissionCommand,
  RestoreRoutePriceFeeCommand,
  UpdateRoutePriceDriverCommissionCommand,
  UpdateRoutePriceFeeCommand,
} from '@router/usecases/routes/route-price.commands';
import { RoutePriceResponse } from '@router/usecases/routes/route-price.response';
import {
  ArchiveRouteCommand,
  CreateRouteCommand,
  GetNearestRouteQuery,
  UpdateRouteCommand,
} from '@router/usecases/routes/route.commands';
import { PublicRouteResponse, RouteResponse } from '@router/usecases/routes/route.response';
import { RouteCommands } from '@router/usecases/routes/route.usecase.commands';
import { RouteQueries } from '@router/usecases/routes/route.usecase.queries';
import {
  ArchiveStationCommand,
  AssignStationToRoute,
  CreateStationCommand,
  DeleteStationCommand,
  UpdateStationCommand,
} from '@router/usecases/routes/station.commands';
import { StationResponse } from '@router/usecases/routes/station.response';

@Controller('routes')
@ApiTags('routes')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class RoutesController {
  constructor(
    private readonly command: RouteCommands,
    private readonly routeQueries: RouteQueries,
  ) {}
  @Get('get-route/:id')
  @ApiOkResponse({ type: RouteResponse })
  async getRoute(@Param('id') id: string) {
    return this.routeQueries.getRoute(id);
  }
  @Get('get-archived-route/:id')
  @ApiOkResponse({ type: RouteResponse })
  async getArchivedRoute(@Param('id') id: string) {
    return this.routeQueries.getRoute(id, true);
  }
  @Get('get-routes')
  @ApiPaginatedResponse(RouteResponse)
  async getRoutes(@Query() query: CollectionQuery) {
    return this.routeQueries.getRoutes(query);
  }
  @Get('explore-routes')
  @ApiPaginatedResponse(PublicRouteResponse)
  @AllowAnonymous()
  async exploreRoutes(@Query() query: CollectionQuery) {
    return this.routeQueries.exploreRoutes(query);
  }
  @Get('get-routes-by-city/:cityId')
  @ApiPaginatedResponse(RouteResponse)
  async getRoutesByCity(
    @Param('cityId') cityId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.routeQueries.getRoutesByCity(cityId, query);
  }
  @Post('create-route')
  @UseGuards(PermissionsGuard('manage-routes'))
  @ApiOkResponse({ type: RouteResponse })
  async createRoute(
    @CurrentUser() user: UserInfo,
    @Body() createRouteCommand: CreateRouteCommand,
  ) {
    createRouteCommand.currentUser = user;
    return this.command.createRoute(createRouteCommand);
  }
  @Put('update-route')
  @UseGuards(PermissionsGuard('manage-routes'))
  @ApiOkResponse({ type: RouteResponse })
  async updateRoute(
    @CurrentUser() user: UserInfo,
    @Body() updateRouteCommand: UpdateRouteCommand,
  ) {
    updateRouteCommand.currentUser = user;
    return this.command.updateRoute(updateRouteCommand);
  }
  @Put('update-route-view/:routeId')
  @AllowAnonymous()
  @ApiOkResponse({ type: RouteResponse })
  async updateRouteView(@Param('routeId') routeId: string) {
    return this.command.updateRouteView(routeId);
  }
  @Post('activate-or-block-route/:id')
  @ApiOkResponse({ type: RouteResponse })
  async activateOrBlockSchool(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockRoute(id, user);
  }
  @Delete('archive-route')
  @UseGuards(PermissionsGuard('manage-routes'))
  @ApiOkResponse({ type: RouteResponse })
  async archiveRoute(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveRouteCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveRoute(archiveCommand);
  }
  @Delete('delete-route/:id')
  @UseGuards(PermissionsGuard('manage-routes'))
  @ApiOkResponse({ type: Boolean })
  async deleteRoute(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteRoute(id, user);
  }
  @Post('restore-route/:id')
  @UseGuards(PermissionsGuard('manage-routes'))
  @ApiOkResponse({ type: RouteResponse })
  async restoreRoute(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreRoute(id, user);
  }
  @Get('get-archived-routes')
  @ApiPaginatedResponse(RouteResponse)
  async getArchivedRoutes(@Query() query: CollectionQuery) {
    return this.routeQueries.getArchivedRoutes(query);
  }

  @Post('add-route-station')
  @UseGuards(PermissionsGuard('manage-routes|manage-route-station'))
  @ApiOkResponse({ type: StationResponse })
  async addRouteStation(
    @CurrentUser() user: UserInfo,
    @Body() createStationCommand: CreateStationCommand,
  ) {
    createStationCommand.currentUser = user;
    return this.command.addStation(createStationCommand);
  }
  @Post('add-route-stations')
  @UseGuards(PermissionsGuard('manage-routes|manage-route-station'))
  @ApiOkResponse({ type: StationResponse, isArray: true })
  async addRouteStations(
    @CurrentUser() user: UserInfo,
    @Body() createStationCommand: AssignStationToRoute,
  ) {
    createStationCommand.currentUser = user;
    return this.command.addStations(createStationCommand);
  }
  @Put('update-route-station')
  @UseGuards(PermissionsGuard('manage-routes|manage-route-station'))
  @ApiOkResponse({ type: StationResponse })
  async updateRouteStation(
    @CurrentUser() user: UserInfo,
    @Body() updateStationCommand: UpdateStationCommand,
  ) {
    updateStationCommand.currentUser = user;
    return this.command.updateStation(updateStationCommand);
  }
  @Delete('remove-route-station')
  @UseGuards(PermissionsGuard('manage-routes|manage-route-station'))
  @ApiOkResponse({ type: Boolean })
  async removeRouteStation(
    @CurrentUser() user: UserInfo,
    @Body() removeStationCommand: DeleteStationCommand,
  ) {
    removeStationCommand.currentUser = user;
    return this.command.deleteStation(removeStationCommand);
  }
  @Delete('archive-route-station')
  @UseGuards(PermissionsGuard('manage-routes|manage-route-station'))
  @ApiOkResponse({ type: Boolean })
  async archiveRouteStation(
    @CurrentUser() user: UserInfo,
    @Body() archiveStationCommand: ArchiveStationCommand,
  ) {
    archiveStationCommand.currentUser = user;
    return this.command.archiveStation(archiveStationCommand);
  }
  @Post('restore-route-station')
  @UseGuards(PermissionsGuard('manage-routes|manage-route-station'))
  @ApiOkResponse({ type: StationResponse })
  async restoreRouteStation(
    @CurrentUser() user: UserInfo,
    @Body() restoreStationCommand: DeleteStationCommand,
  ) {
    restoreStationCommand.currentUser = user;
    return this.command.restoreStation(restoreStationCommand);
  }
  @Get('get-station/:id')
  @ApiOkResponse({ type: StationResponse })
  async getStation(@Param('id') id: string) {
    return this.routeQueries.getStation(id);
  }
  @Get('get-stations')
  @ApiPaginatedResponse(StationResponse)
  async getStations(@Query() query: CollectionQuery) {
    return this.routeQueries.getStations(query);
  }
  @Get('get-archived-stations')
  @ApiPaginatedResponse(StationResponse)
  async getArchivedStations(@Query() query: CollectionQuery) {
    return this.routeQueries.getArchivedStations(query);
  }
  @Get('get-archived-route-stations/:routeId')
  @ApiPaginatedResponse(StationResponse)
  async getArchivedRouteStations(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.routeQueries.getArchivedRouteStations(routeId, query);
  }
  @Get('get-route-stations/:routeId')
  @ApiPaginatedResponse(StationResponse)
  async getRouteStations(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.routeQueries.getRouteStations(routeId, query);
  }

  //Route Price
  @Get('get-route-price/:id')
  @ApiOkResponse({ type: RoutePriceResponse })
  async getRoutePrice(@Param('id') id: string) {
    return this.routeQueries.getRoutePrice(id);
  }
  @Get('get-route-prices')
  @ApiPaginatedResponse(RoutePriceResponse)
  async getAllRoutePrices(@Query() query: CollectionQuery) {
    return this.routeQueries.getAllRoutePrices(query);
  }
  @Get('get-archived-route-prices')
  @ApiPaginatedResponse(RoutePriceResponse)
  async getAllArchivedRoutePrices(@Query() query: CollectionQuery) {
    return this.routeQueries.getAllArchivedRoutePrices(query);
  }
  @Get('get-archived-route-prices/:routeId')
  @ApiPaginatedResponse(RoutePriceResponse)
  async getArchivedRoutePrices(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.routeQueries.getArchivedRoutePrices(routeId, query);
  }
  @Get('get-route-prices/:routeId')
  @ApiPaginatedResponse(RoutePriceResponse)
  async getRoutePrices(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.routeQueries.getRoutePrices(routeId, query);
  }
  @Post('restore-route-price-fee')
  @UseGuards(PermissionsGuard('manage-route-prices'))
  @ApiPaginatedResponse(RoutePriceResponse)
  async restoreRoutePriceFee(
    @CurrentUser() user: UserInfo,
    @Body() command: RestoreRoutePriceFeeCommand,
  ) {
    command.currentUser = user;
    return this.command.restoreRoutePriceFee(command);
  }
  @Post('update-route-price-fee')
  @UseGuards(PermissionsGuard('manage-route-prices'))
  @ApiPaginatedResponse(RoutePriceResponse)
  async updateRoutePriceFee(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateRoutePriceFeeCommand,
  ) {
    command.currentUser = user;
    return this.command.updateRoutePriceFee(command);
  }

  @Post('restore-route-price-driver-commission')
  @UseGuards(PermissionsGuard('manage-route-prices'))
  @ApiPaginatedResponse(RoutePriceResponse)
  async restoreRoutePriceDriverCommission(
    @CurrentUser() user: UserInfo,
    @Body() command: RestoreRoutePriceDriverCommissionCommand,
  ) {
    command.currentUser = user;
    return this.command.restoreRoutePriceDriverCommission(command);
  }

  @Post('update-route-price-driver-commission')
  @UseGuards(PermissionsGuard('manage-route-prices'))
  @ApiPaginatedResponse(RoutePriceResponse)
  async updateRoutePriceDriverCommission(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateRoutePriceDriverCommissionCommand,
  ) {
    command.currentUser = user;
    return this.command.updateRoutePriceDriverCommission(command);
  }
  @Get('get-nearest-routes')
  async getNearestRoute(@Query() query: GetNearestRouteQuery) {
    return this.routeQueries.getNearestRoute(query);
  }
}
