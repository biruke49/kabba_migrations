import { City } from './city';
export interface ICityRepository {
  insert(user: City): Promise<City>;
  update(user: City): Promise<City>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<City[]>;
  getById(id: string, withDeleted: boolean): Promise<City>;
  getByName(name: string, withDeleted: boolean): Promise<City>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
