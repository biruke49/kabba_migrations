import { BusStop } from './bus-stop';
export interface IBusStopRepository {
  insert(user: BusStop): Promise<BusStop>;
  update(user: BusStop): Promise<BusStop>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<BusStop[]>;
  getById(id: string, withDeleted: boolean): Promise<BusStop>;
  getByName(name: string, withDeleted: boolean): Promise<BusStop>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
