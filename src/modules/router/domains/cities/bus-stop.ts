import { City } from './city';

export class BusStop {
  id: string;
  name: string;
  cityId: string;
  description: string;
  isActive: boolean;
  lat: number;
  lng: number;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  archiveReason: string;
  city?: City;
}
