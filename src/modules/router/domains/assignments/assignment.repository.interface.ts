import { Assignment } from './assignment';
export interface IAssignmentRepository {
  insert(assignment: Assignment): Promise<Assignment>;
  insertMany(assignments: Assignment[]): Promise<Assignment[]>;
  update(assignment: Assignment): Promise<Assignment>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Assignment[]>;
  getById(id: string, withDeleted: boolean): Promise<Assignment>;
  getByAssignedDate(date: Date, withDeleted: boolean): Promise<Assignment>;
  getActiveDriverAssignmentsByAssignedDateAndPickupTime(
    driverId: string,
    assignmentDate: Date,
    pickupTime: string,
    status: string,
  ): Promise<Assignment>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
