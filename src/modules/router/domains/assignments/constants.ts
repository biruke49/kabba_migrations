export enum AssignmentStatus {
  ASSIGNED = 'Assigned',
  STARTED = 'Started',
  COMPLETED = 'Completed',
  CANCELLED = 'Cancelled',
}
export enum DriverPaymentStatus {
  UNPAID = 'Unpaid',
  INITIATED = 'Initiated',
  PAID = 'Paid',
  REJECTED = 'Rejected',
}
