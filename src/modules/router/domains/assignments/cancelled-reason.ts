import { ApiProperty } from '@nestjs/swagger';

export class CancellationReason {
  @ApiProperty()
  reason: string;
  @ApiProperty()
  cancelledBy: string;
  @ApiProperty()
  cancelledAt: Date;
}
