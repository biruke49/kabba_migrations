import { BusStop } from '@router/domains/cities/bus-stop';
import { Driver } from '@provider/domains/drivers/driver';
import { Route } from '../routes/route';
import { CancellationReason } from './cancelled-reason';

export class Assignment {
  id: string;
  driverId: string;
  routeId: string;
  vehicleId: string;
  vehiclePlateNumber: string;
  vehicleCategoryId: string;
  vehicleCategoryName: string;
  status: string;
  paymentStatus: string;
  fee: number;
  companyEarning: number;
  assignmentDate: Date;
  remark: string;
  cancellationReason: CancellationReason;
  availableSeats: number;
  vehicleModel: string;
  routeName: string;
  driverName: string;
  driverPhone: string;
  startingAt: Date;
  completedAt: Date;
  paidAt: Date;
  isPaid: boolean;
  pickupTime: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  route?: Route;
  driver?: Driver;
}
