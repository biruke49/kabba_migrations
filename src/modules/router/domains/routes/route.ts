import { City } from '@router/domains/cities/city';
import { BusStop } from '@router/domains/cities/bus-stop';
import { Station } from './station';
import { RoutePrice } from './route-price';
export class Route {
  id: string;
  name: string;
  description: string;
  cityId: string;
  isActive: boolean;
  difficultyLevel: string;
  trafficJamLevel: string;
  numberOfViews: number;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  archiveReason: string;
  city?: City;
  totalDistance: number;
  stations: Station[];
  prices: RoutePrice[];
  async addStation(station: Station) {
    this.stations.push(station);
  }
  async updateStation(station: Station) {
    const existIndex = this.stations.findIndex(
      (element) => element.id == station.id,
    );
    this.stations[existIndex] = station;
  }
  async removeStation(id: string) {
    this.stations = this.stations.filter((element) => element.id != id);
  }
  async updateStations(stations: Station[]) {
    this.stations = stations;
  }
  async addPrice(price: RoutePrice) {
    this.prices.push(price);
  }
  async updatePrice(price: RoutePrice) {
    const existIndex = this.prices.findIndex(
      (element) => element.id == price.id,
    );
    this.prices[existIndex] = price;
  }
  async removePrice(id: string) {
    this.prices = this.prices.filter((element) => element.id != id);
  }
  async updatePrices(prices: RoutePrice[]) {
    this.prices = prices;
  }
}
