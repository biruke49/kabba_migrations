import { Route } from './route';
export interface IRouteRepository {
  insert(route: Route): Promise<Route>;
  update(route: Route): Promise<Route>;
  updateMany(routes: Route[]): Promise<Route[]>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Route[]>;
  getById(id: string, withDeleted: boolean): Promise<Route>;
  getByCityIdAndName(
    cityId: string,
    name: string,
    withDeleted: boolean,
  ): Promise<Route>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
