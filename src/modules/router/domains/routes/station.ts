import { BusStop } from '../cities/bus-stop';
import { Route } from './route';

export class Station {
  id: string;
  routeId: string;
  busStopId: string;
  isPickup: boolean;
  isDestination: boolean;
  position: number;
  nextBusStopId: string;
  distanceToNextBusStop: number;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  busStop?: BusStop;
  route?: Route;
}
