import { Category } from '@classification/domains/categories/category';

export class RoutePrice {
  id: string;
  routeId: string;
  categoryId: string;
  calculatedFee: number;
  fee: number;
  defaultDriverCommission: number;
  driverCommission: number;
  category: Category;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
