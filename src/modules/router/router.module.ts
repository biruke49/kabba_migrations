import { CategoryEntity } from './../classification/persistence/categories/category.entity';
import { RouteEntity } from '@router/persistence/routes/route.entity';
import { RouteQueries } from './usecases/routes/route.usecase.queries';
import { RouteCommands } from './usecases/routes/route.usecase.commands';
import { RouteRepository } from '@router/persistence/routes/route.repository';
import { BusStopQueries } from './usecases/cities/bus-stop.usecase.queries';
import { BusStopRepository } from './persistence/cities/bus-stop.repository';
import { BusStopCommands } from '@router/usecases/cities/bus-stop.usecase.commands';
import { BusStopEntity } from './persistence/cities/bus-stop.entity';
import { CityRepository } from './persistence/cities/city.repository';
import { CityCommands } from './usecases/cities/city.usecase.commands';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { CityEntity } from './persistence/cities/city.entity';
import { CitiesController } from './controllers/city.controller';
import { CityQueries } from './usecases/cities/city.usecase.queries';
import { RoutesController } from './controllers/route.controller';
import { StationEntity } from './persistence/routes/station.entity';
import { RoutePriceEntity } from './persistence/routes/route-price.entity';
import { AssignmentEntity } from './persistence/assignments/assignment.entity';
import { AssignmentRepository } from './persistence/assignments/assignment.repository';
import { AssignmentCommands } from './usecases/assignments/assignment.usecase.commands';
import { AssignmentQueries } from './usecases/assignments/assignment.usecase.queries';
import { AssignmentsController } from './controllers/assignment.controller';
import { ProviderModule } from '@provider/provider.module';
import { DriverFeeCommands } from '@provider/usecases/driver-fees/driver-fee.usecase.commands';
import { DriverFeeRepository } from '@provider/persistence/driver-fees/driver-fee.repository';
import { DriverFeeEntity } from '@provider/persistence/driver-fees/driver-fee.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { PreferenceRepository } from '@interaction/persistence/preferences/preference.repository';
import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { PreferenceQueries } from '@interaction/usecases/preferences/preference.usecase.queries';
import { BookingRepository } from '@order/persistance/bookings/booking.repository';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { AccountEntity } from '@account/persistence/accounts/account.entity';
import { BookingQueries } from '@order/usecases/bookings/booking.usecase.queries';
import { PassengerRepository } from '@customer/persistence/passengers/passenger.repository';
import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { AppService } from 'app.service';
import { FileManagerService } from '@libs/common/file-manager';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CityEntity,
      BusStopEntity,
      RouteEntity,
      StationEntity,
      RoutePriceEntity,
      AssignmentEntity,
      DriverFeeEntity,
      DriverEntity,
      CategoryEntity,
      PreferenceEntity,
      BookingEntity,
      AccountEntity,
      PassengerEntity,
    ]),
    ProviderModule,
  ],
  providers: [
    CityQueries,
    CityCommands,
    CityRepository,
    BusStopCommands,
    BusStopRepository,
    BusStopQueries,
    RouteRepository,
    RouteCommands,
    RouteQueries,
    AssignmentRepository,
    AssignmentCommands,
    AssignmentQueries,
    DriverFeeCommands,
    DriverFeeRepository,
    DriverRepository,
    PreferenceRepository,
    PreferenceQueries,
    BookingRepository,
    AccountRepository,
    BookingQueries,
    PassengerRepository,
    AppService,
    FileManagerService,
  ],
  controllers: [CitiesController, RoutesController, AssignmentsController],
  exports: [AssignmentRepository],
})
export class RouterModule {}
