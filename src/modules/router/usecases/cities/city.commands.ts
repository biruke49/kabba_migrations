import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { City } from '@router/domains/cities/city';
export class CreateCityCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  isActive: boolean;
  currentUser: UserInfo;
  static fromCommand(command: CreateCityCommand): City {
    const cityDomain = new City();
    cityDomain.name = command.name;
    cityDomain.isActive = command.isActive;
    return cityDomain;
  }
}
export class UpdateCityCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  isActive: boolean;
  currentUser: UserInfo;
}
export class ArchiveCityCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
