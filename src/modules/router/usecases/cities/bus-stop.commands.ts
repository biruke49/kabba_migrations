import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPositive, IsUUID } from 'class-validator';
import { BusStop } from '@router/domains/cities/bus-stop';
export class CreateBusStopCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  cityId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  lng: number;
  @ApiProperty()
  isActive: boolean;
  currentUser: UserInfo;
  static fromCommand(command: CreateBusStopCommand): BusStop {
    const cityDomain = new BusStop();
    cityDomain.name = command.name;
    cityDomain.cityId = command.cityId;
    cityDomain.description = command.description;
    cityDomain.lat = command.lat;
    cityDomain.lng = command.lng;
    cityDomain.isActive = command.isActive;
    return cityDomain;
  }
}
export class UpdateBusStopCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  cityId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  lng: number;
  @ApiProperty()
  isActive: boolean;
  currentUser: UserInfo;
}

export class ArchiveBusStopCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
