import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BusStopEntity } from '@router/persistence/cities/bus-stop.entity';
import { Repository } from 'typeorm';
import { BusStopResponse } from './bus-stop.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
@Injectable()
export class BusStopQueries {
  constructor(
    @InjectRepository(BusStopEntity)
    private busStopRepository: Repository<BusStopEntity>,
  ) {}
  async getBusStop(id: string, withDeleted = false): Promise<BusStopResponse> {
    const busStop = await this.busStopRepository.find({
      where: { id: id },
      relations: ['city'],
      withDeleted: withDeleted,
    });
    if (!busStop[0]) {
      throw new NotFoundException(`BusStop not found with id ${id}`);
    }
    return BusStopResponse.fromEntity(busStop[0]);
  }
  async getSingleBusStop(
    id: string,
    withDeleted = false,
  ): Promise<BusStopResponse> {
    const busStop = await this.busStopRepository.find({
      where: { id: id },
      withDeleted: withDeleted,
    });
    if (!busStop[0]) {
      throw new NotFoundException(`BusStop not found with id ${id}`);
    }
    return BusStopResponse.fromEntity(busStop[0]);
  }
  async getBusStops(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BusStopResponse>> {
    const dataQuery = QueryConstructor.constructQuery<BusStopEntity>(
      this.busStopRepository,
      query,
    );
    const d = new DataResponseFormat<BusStopResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BusStopResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getBusStopsByCityId(
    cityId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BusStopResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'cityId',
        value: cityId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BusStopEntity>(
      this.busStopRepository,
      query,
    );
    const d = new DataResponseFormat<BusStopResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BusStopResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedBusStops(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BusStopResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BusStopEntity>(
      this.busStopRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<BusStopResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BusStopResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
