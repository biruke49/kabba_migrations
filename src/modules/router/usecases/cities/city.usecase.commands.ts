import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import {
  ArchiveCityCommand,
  CreateCityCommand,
  UpdateCityCommand,
} from './city.commands';
import { CityRepository } from '@router/persistence/cities/city.repository';
import { CityResponse } from './city.response';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { UserInfo } from '@account/dtos/user-info.dto';
import { BusStopRepository } from '@router/persistence/cities/bus-stop.repository';
@Injectable()
export class CityCommands {
  constructor(
    private cityRepository: CityRepository,
    private busStopRepository: BusStopRepository,
    private readonly eventEmitter: EventEmitter2,
  ) {}
  async createCity(command: CreateCityCommand): Promise<CityResponse> {
    const c = await this.cityRepository.getByName(command.name);
    if (c) {
      throw new BadRequestException(`City with this name already exist`);
    }
    const cityDomain = CreateCityCommand.fromCommand(command);
    cityDomain.createdBy = command.currentUser.id;
    cityDomain.updatedBy = command.currentUser.id;
    const city = await this.cityRepository.insert(cityDomain);
    if (city) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: city.id,
        modelName: MODELS.CITY,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
    }
    return CityResponse.fromDomain(city);
  }
  async updateCity(command: UpdateCityCommand): Promise<CityResponse> {
    const cityDomain = await this.cityRepository.getById(command.id);
    if (!cityDomain) {
      throw new NotFoundException(`City not found with id ${command.id}`);
    }
    const oldPayload = cityDomain;

    if (
      cityDomain.name.toLowerCase() != command.name.toLowerCase() &&
      (await this.cityRepository.getByName(command.name))
    ) {
      throw new BadRequestException(`City with this name already exist`);
    }
    cityDomain.name = command.name;
    cityDomain.isActive = command.isActive;
    cityDomain.updatedBy = command.currentUser.id;
    const city = await this.cityRepository.update(cityDomain);
    if (city) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: city.id,
        modelName: MODELS.CITY,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: city,
        oldPayload: oldPayload,
      });
    }
    return CityResponse.fromDomain(city);
  }
  async archiveCity(command: ArchiveCityCommand): Promise<CityResponse> {
    const cityDomain = await this.cityRepository.getById(command.id);
    if (!cityDomain) {
      throw new NotFoundException(`City not found with id ${command.id}`);
    }
    cityDomain.archiveReason = command.reason;
    cityDomain.deletedAt = new Date();
    cityDomain.deletedBy = command.currentUser.id;
    const result = await this.cityRepository.update(cityDomain);
    if (result) {
      const busStops = await this.busStopRepository.getAllByCityId(
        command.id,
        false,
      );
      if (busStops && busStops.length > 0) {
        for (const bus of busStops) {
          bus.archiveReason = command.reason;
          bus.deletedAt = new Date();
          bus.deletedBy = command.currentUser.id;
          const resultt = await this.busStopRepository.update(bus);
        }
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.CITY,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
        reason: command.reason,
      });
    }
    return CityResponse.fromDomain(result);
  }
  async restoreCity(id: string, currentUser: UserInfo): Promise<CityResponse> {
    const cityDomain = await this.cityRepository.getById(id, true);
    if (!cityDomain) {
      throw new NotFoundException(`City not found with id ${id}`);
    }
    const r = await this.cityRepository.restore(id);
    if (r) {
      cityDomain.deletedAt = null;
      cityDomain.deletedBy = null;
      cityDomain.archiveReason = null;
      const result = await this.cityRepository.update(cityDomain);
      const busStops = await this.busStopRepository.getAllByCityId(id, true);
      if (busStops && busStops.length > 0) {
        for (const bus of busStops) {
          bus.archiveReason = null;
          bus.deletedAt = null;
          bus.deletedBy = null;
          const resultt = await this.busStopRepository.update(bus);
        }
      }
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.CITY,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    return CityResponse.fromDomain(cityDomain);
  }
  async deleteCity(id: string, currentUser: UserInfo): Promise<boolean> {
    const cityDomain = await this.cityRepository.getById(id, true);
    if (!cityDomain) {
      throw new NotFoundException(`City not found with id ${id}`);
    }
    const busStops = await this.busStopRepository.getByCityId(id);
    if (busStops) {
      throw new NotFoundException(
        `City can not be deleted because it has bus stops added`,
      );
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.CITY,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    return await this.cityRepository.delete(id);
  }
}
