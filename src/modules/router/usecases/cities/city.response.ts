import { ApiProperty } from '@nestjs/swagger';
import { City } from '@router/domains/cities/city';
import { CityEntity } from '@router/persistence/cities/city.entity';

export class CityResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty()
  archiveReason: string;
  static fromEntity(cityEntity: CityEntity): CityResponse {
    const cityResponse = new CityResponse();
    cityResponse.id = cityEntity.id;
    cityResponse.name = cityEntity.name;
    cityResponse.isActive = cityEntity.isActive;
    cityResponse.createdBy = cityEntity.createdBy;
    cityResponse.updatedBy = cityEntity.updatedBy;
    cityResponse.deletedBy = cityEntity.deletedBy;
    cityResponse.createdAt = cityEntity.createdAt;
    cityResponse.updatedAt = cityEntity.updatedAt;
    cityResponse.deletedAt = cityEntity.deletedAt;
    cityResponse.archiveReason = cityEntity.archiveReason;
    return cityResponse;
  }
  static fromDomain(city: City): CityResponse {
    const cityResponse = new CityResponse();
    cityResponse.id = city.id;
    cityResponse.name = city.name;
    cityResponse.isActive = city.isActive;
    cityResponse.createdBy = city.createdBy;
    cityResponse.updatedBy = city.updatedBy;
    cityResponse.deletedBy = city.deletedBy;
    cityResponse.createdAt = city.createdAt;
    cityResponse.updatedAt = city.updatedAt;
    cityResponse.deletedAt = city.deletedAt;
    cityResponse.archiveReason = city.archiveReason;
    return cityResponse;
  }
}

export class PublicCityResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  static fromEntity(cityEntity: CityEntity): PublicCityResponse {
    const cityResponse = new PublicCityResponse();
    cityResponse.id = cityEntity.id;
    cityResponse.name = cityEntity.name;
    return cityResponse;
  }
  static fromDomain(city: City): PublicCityResponse {
    const cityResponse = new PublicCityResponse();
    cityResponse.id = city.id;
    cityResponse.name = city.name;
    return cityResponse;
  }
}
