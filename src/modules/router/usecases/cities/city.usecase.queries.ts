import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CityEntity } from '@router/persistence/cities/city.entity';
import { Repository } from 'typeorm';
import { CityResponse } from './city.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
@Injectable()
export class CityQueries {
  constructor(
    @InjectRepository(CityEntity)
    private cityRepository: Repository<CityEntity>,
  ) {}
  async getCity(id: string, withDeleted = false): Promise<CityResponse> {
    const city = await this.cityRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!city[0]) {
      throw new NotFoundException(`City not found with id ${id}`);
    }
    return CityResponse.fromEntity(city[0]);
  }
  async getCities(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CityResponse>> {
    const dataQuery = QueryConstructor.constructQuery<CityEntity>(
      this.cityRepository,
      query,
    );
    const d = new DataResponseFormat<CityResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CityResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedCities(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CityResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CityEntity>(
      this.cityRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<CityResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CityResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
