import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import {
  ArchiveBusStopCommand,
  CreateBusStopCommand,
  UpdateBusStopCommand,
} from './bus-stop.commands';
import { BusStopRepository } from '@router/persistence/cities/bus-stop.repository';
import { BusStopResponse } from './bus-stop.response';
import { Injectable, NotFoundException } from '@nestjs/common';
import { UserInfo } from '@account/dtos/user-info.dto';
import { CityRepository } from '@router/persistence/cities/city.repository';
import { RouteRepository } from '@router/persistence/routes/route.repository';
@Injectable()
export class BusStopCommands {
  constructor(
    private busStopRepository: BusStopRepository,
    private cityRepository: CityRepository,
    private routeRepository: RouteRepository,
    private readonly eventEmitter: EventEmitter2,
  ) {}
  async createBusStop(command: CreateBusStopCommand): Promise<BusStopResponse> {
    const city = await this.cityRepository.getById(command.cityId);
    if (!city) {
      throw new NotFoundException(`City not found with id ${command.cityId}`);
    }
    const busStopDomain = CreateBusStopCommand.fromCommand(command);
    busStopDomain.createdBy = command.currentUser.id;
    busStopDomain.updatedBy = command.currentUser.id;
    const busStop = await this.busStopRepository.insert(busStopDomain);
    if (busStop) {
      busStop.city = city;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: busStop.id,
        modelName: MODELS.BUSSTOP,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
    }
    return BusStopResponse.fromDomain(busStop);
  }
  async updateBusStop(command: UpdateBusStopCommand): Promise<BusStopResponse> {
    const busStopDomain = await this.busStopRepository.getById(command.id);
    if (!busStopDomain) {
      throw new NotFoundException(`BusStop not found with id ${command.id}`);
    }
    const city = await this.cityRepository.getById(command.cityId);
    if (!city) {
      throw new NotFoundException(`City not found with id ${command.cityId}`);
    }
    const oldPayload = busStopDomain;
    busStopDomain.name = command.name;
    busStopDomain.description = command.description;
    busStopDomain.lat = command.lat;
    busStopDomain.lng = command.lng;
    busStopDomain.cityId = command.cityId;
    busStopDomain.isActive = command.isActive;
    busStopDomain.updatedBy = command.currentUser.id;
    const busStop = await this.busStopRepository.update(busStopDomain);
    if (busStop) {
      busStop.city = city;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: busStop.id,
        modelName: MODELS.BUSSTOP,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: busStop,
        oldPayload: oldPayload,
      });
    }
    return BusStopResponse.fromDomain(busStop);
  }
  async archiveBusStop(
    command: ArchiveBusStopCommand,
  ): Promise<BusStopResponse> {
    const busStopDomain = await this.busStopRepository.getById(command.id);
    if (!busStopDomain) {
      throw new NotFoundException(`BusStop not found with id ${command.id}`);
    }
    const stationDomain = await this.routeRepository.getByBusStopId(command.id);
    if (stationDomain) {
      throw new NotFoundException(
        `Bus Stop has already been added as a station`,
      );
    }
    busStopDomain.archiveReason = command.reason;
    busStopDomain.deletedAt = new Date();
    busStopDomain.deletedBy = command.currentUser.id;
    const result = await this.busStopRepository.update(busStopDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.BUSSTOP,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
        reason: command.reason,
      });
    }
    return BusStopResponse.fromDomain(result);
  }
  async restoreBusStop(
    id: string,
    currentUser: UserInfo,
  ): Promise<BusStopResponse> {
    const busStopDomain = await this.busStopRepository.getById(id, true);
    if (!busStopDomain) {
      throw new NotFoundException(`BusStop not found with id ${id}`);
    }
    const r = await this.busStopRepository.restore(id);
    if (r) {
      busStopDomain.deletedAt = null;
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.BUSSTOP,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    return BusStopResponse.fromDomain(busStopDomain);
  }
  async deleteBusStop(id: string, currentUser: UserInfo): Promise<boolean> {
    const busStopDomain = await this.busStopRepository.getById(id, true);
    if (!busStopDomain) {
      throw new NotFoundException(`Bus Stop not found with id ${id}`);
    }
    const stationDomain = await this.routeRepository.getByBusStopId(id);
    if (stationDomain) {
      throw new NotFoundException(
        `Bus Stop has already been added as a station`,
      );
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.BUSSTOP,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    return await this.busStopRepository.delete(id);
  }
}
