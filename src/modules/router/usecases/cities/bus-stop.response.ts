import { CityResponse, PublicCityResponse } from './city.response';
import { ApiProperty } from '@nestjs/swagger';
import { BusStop } from '@router/domains/cities/bus-stop';
import { BusStopEntity } from '@router/persistence/cities/bus-stop.entity';

export class BusStopResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  cityId: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  lng: number;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty()
  city?: CityResponse;
  @ApiProperty()
  archiveReason: string;
  static fromEntity(busStopEntity: BusStopEntity): BusStopResponse {
    const busStopResponse = new BusStopResponse();
    busStopResponse.id = busStopEntity.id;
    busStopResponse.name = busStopEntity.name;
    busStopResponse.cityId = busStopEntity.cityId;
    busStopResponse.description = busStopEntity.description;
    busStopResponse.lat = busStopEntity.lat;
    busStopResponse.lng = busStopEntity.lng;
    busStopResponse.isActive = busStopEntity.isActive;
    busStopResponse.createdBy = busStopEntity.createdBy;
    busStopResponse.updatedBy = busStopEntity.updatedBy;
    busStopResponse.deletedBy = busStopEntity.deletedBy;
    busStopResponse.createdAt = busStopEntity.createdAt;
    busStopResponse.updatedAt = busStopEntity.updatedAt;
    busStopResponse.deletedAt = busStopEntity.deletedAt;
    if (busStopEntity.city) {
      busStopResponse.city = CityResponse.fromEntity(busStopEntity.city);
    }
    busStopResponse.archiveReason = busStopEntity.archiveReason;
    return busStopResponse;
  }
  static fromDomain(busStop: BusStop): BusStopResponse {
    const busStopResponse = new BusStopResponse();
    busStopResponse.id = busStop.id;
    busStopResponse.name = busStop.name;
    busStopResponse.cityId = busStop.cityId;
    busStopResponse.description = busStop.description;
    busStopResponse.lat = busStop.lat;
    busStopResponse.lng = busStop.lng;
    busStopResponse.isActive = busStop.isActive;
    busStopResponse.createdBy = busStop.createdBy;
    busStopResponse.updatedBy = busStop.updatedBy;
    busStopResponse.deletedBy = busStop.deletedBy;
    busStopResponse.createdAt = busStop.createdAt;
    busStopResponse.updatedAt = busStop.updatedAt;
    busStopResponse.deletedAt = busStop.deletedAt;
    busStopResponse.archiveReason = busStop.archiveReason;
    if (busStop.city) {
      busStopResponse.city = CityResponse.fromDomain(busStop.city);
    }
    return busStopResponse;
  }
}

export class PublicBusStopResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  cityId: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  lng: number;
  @ApiProperty()
  city?: PublicCityResponse;
  static fromEntity(busStopEntity: BusStopEntity): PublicBusStopResponse {
    const busStopResponse = new PublicBusStopResponse();
    busStopResponse.id = busStopEntity.id;
    busStopResponse.name = busStopEntity.name;
    busStopResponse.cityId = busStopEntity.cityId;
    busStopResponse.description = busStopEntity.description;
    busStopResponse.lat = busStopEntity.lat;
    busStopResponse.lng = busStopEntity.lng;
    if (busStopEntity.city) {
      busStopResponse.city = PublicCityResponse.fromEntity(busStopEntity.city);
    }
    return busStopResponse;
  }
  static fromDomain(busStop: BusStop): PublicBusStopResponse {
    const busStopResponse = new PublicBusStopResponse();
    busStopResponse.id = busStop.id;
    busStopResponse.name = busStop.name;
    busStopResponse.cityId = busStop.cityId;
    busStopResponse.description = busStop.description;
    busStopResponse.lat = busStop.lat;
    busStopResponse.lng = busStop.lng;
    if (busStop.city) {
      busStopResponse.city = PublicCityResponse.fromDomain(busStop.city);
    }
    return busStopResponse;
  }
}
