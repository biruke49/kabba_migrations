import { AssignmentStatus } from '@router/domains/assignments/constants';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { Repository } from 'typeorm';
import {
  AssignmentResponse,
  CountAssignmentByRoute,
  CountByStatusAndRouteResponse,
  CountByStatusResponse,
  CountEarningsByCategoryResponse,
} from './assignment.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import {
  CountByCreatedAtResponse,
  CountEarningsByCreatedAtResponse,
  CountWeeklyBookingByCreatedAtResponse,
} from '@order/usecases/bookings/booking.response';
import { Util } from '@libs/common/util';
import { AssignmentCommands } from './assignment.usecase.commands';
import { EventEmitter2 } from '@nestjs/event-emitter';
@Injectable()
export class AssignmentQueries {
  constructor(
    @InjectRepository(AssignmentEntity)
    private assignmentRepository: Repository<AssignmentEntity>,
    @InjectRepository(DriverEntity)
    private driverRepository: Repository<DriverEntity>,
    private readonly eventEmitter: EventEmitter2,
  ) {}
  async getAssignment(
    id: string,
    withDeleted = false,
  ): Promise<AssignmentResponse> {
    const assignment = await this.assignmentRepository.find({
      where: { id: id },
      relations: [
        'driver',
        'route',
        'route.stations',
        'route.stations.busStop',
      ],
      withDeleted: withDeleted,
    });
    if (!assignment[0]) {
      throw new NotFoundException(`Assignment not found with id ${id}`);
    }
    return AssignmentResponse.fromEntity(assignment[0]);
  }
  async getAssignments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<AssignmentResponse>> {
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const d = new DataResponseFormat<AssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => AssignmentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getCompanyEarning(query: CollectionQuery): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'status',
        operator: FilterOperators.EqualTo,
        value: AssignmentStatus.COMPLETED,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const data = await dataQuery
      .select('SUM(company_earning)', 'total')
      .getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async groupEarningsByCreatedDate(
    query: CollectionQuery,
    format: string,
  ): Promise<CountEarningsByCreatedAtResponse[]> {
    let formatQ = '';
    if (format === 'year') {
      formatQ = 'YYYY';
    } else if (format === 'month') {
      formatQ = 'YYYY-MM';
    } else if (format === 'date') {
      formatQ = 'YYYY-MM-dd';
    }
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'assignments.status',
        value: AssignmentStatus.COMPLETED,
        operator: FilterOperators.EqualTo,
      },
    ]);
    query.select = [];
    query.select.push(
      `to_char(assignments.assignment_date,'${formatQ}') as created_date`,
      'SUM(assignments.company_earning)',
    );
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date', 'DESC')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountEarningsByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.total = d.sum;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupEarningsByCategory(
    query: CollectionQuery,
  ): Promise<CountEarningsByCategoryResponse[]> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'status',
        operator: FilterOperators.EqualTo,
        value: AssignmentStatus.COMPLETED,
      },
    ]);
    query.select = [];
    query.select.push(
      'category.id as categoryid',
      'category.name as categoryname',
      'SUM(assignments.company_earning)',
    );
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const data = await dataQuery
      .leftJoin('assignments.driver', 'driver')
      .leftJoin('driver.vehicle', 'vehicle')
      .leftJoin('vehicle.category', 'category')
      .groupBy(`categoryid`)
      .addGroupBy(`categoryname`)
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountEarningsByCategoryResponse();
      countResponse.categoryId = d.categoryid;
      countResponse.categoryName = d.categoryname;
      countResponse.total = d.sum;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupAssignmentsByStatus(
    query: CollectionQuery,
  ): Promise<CountByStatusResponse[]> {
    query.select = [];
    query.select.push(`assignments.status as status`, 'COUNT(assignments.id)');
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const data = await dataQuery.groupBy(`status`).getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByStatusResponse();
      countResponse.status = d.status;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupAssignmentsStatusByRoutes(
    query: CollectionQuery,
  ): Promise<CountByStatusResponse[]> {
    query.select = [];
    query.select.push(
      `assignments.status as status`,
      'assignments.route_id as routeid',
      'assignments.route_name as routename',
      'COUNT(assignments.id)',
    );
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`status`)
      .addGroupBy('routeid')
      .addGroupBy('routename')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByStatusAndRouteResponse();
      countResponse.status = d.status;
      countResponse.routeId = d.routeid;
      countResponse.routeName = d.routename;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  // async getDriverEarning(
  //   driverId: string,
  //   query: CollectionQuery,
  // ): Promise<any> {
  //   const today = new Date('Y-m-d');
  //   // const lastMonday = Util.getTheLastMonday(today);
  //   if (!query.filter) {
  //     query.filter = [];
  //   }
  //   query.filter.push([
  //     {
  //       field: 'driverId',
  //       value: driverId,
  //       operator: FilterOperators.EqualTo,
  //     },
  //   ]);
  //   const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
  //     this.assignmentRepository,
  //     query,
  //   );
  //   const data = await dataQuery.select('SUM(fee)', 'total').getRawOne();
  //   const total = data.total != null ? data.total : 0;
  //   return { total: total };
  // }
  async groupAssignmentsByRoutes(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CountAssignmentByRoute>> {
    query.select = [];
    query.select.push(
      `route.name as route_name`,
      `assignments.route_id as route_id`,
      `assignments.pickupTime as pickup_time`,
      'COUNT(assignments.id)',
      'SUM(assignments.availableSeats)',
    );
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );

    await dataQuery
      .groupBy(`route_id`)
      .addGroupBy('route.name')
      .addGroupBy('assignments.pickupTime')
      .leftJoin('assignments.route', 'route')
      .orderBy('count', 'DESC');
    if (query.top) {
      dataQuery.limit(query.top);
    }
    if (query.skip) {
      dataQuery.offset(query.skip);
    }
    const d = new DataResponseFormat<CountAssignmentByRoute>();
    d.count = await dataQuery.getCount();
    const result = await dataQuery.getRawMany();
    d.data = result.map((entity) => {
      const r: CountAssignmentByRoute = {
        routeId: entity.route_id,
        routeName: entity.route_name,
        pickupTime: entity.pickup_time,
        availableSeats: entity.sum,
        count: entity.count,
      };
      return r;
    });
    return d;
  }
  async getRouteAssignments(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<AssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'routeId',
        operator: FilterOperators.EqualTo,
        value: routeId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const d = new DataResponseFormat<AssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => AssignmentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedAssignments(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<AssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<AssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => AssignmentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getDriverAssignments(
    driverId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<AssignmentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        operator: FilterOperators.EqualTo,
        value: driverId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    const d = new DataResponseFormat<AssignmentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => AssignmentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getDriversInRoute(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'vehicleId',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverEntity>(
      this.driverRepository,
      query,
    );
    dataQuery
      .innerJoin('drivers.assignments', 'assignments')
      .andWhere('assignments.route_id=:routeId', { routeId: routeId })
      .distinct(true);
    const d = new DataResponseFormat<DriverResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => DriverResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getRouteAssignmentDates(
    routeId: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'routeId',
        operator: FilterOperators.EqualTo,
        value: routeId,
      },
    ]);
    if (!query.select) {
      query.select = [];
    }
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    dataQuery.select([
      `DISTINCT(to_char(assignment_date,'YYYY-MM-dd') ) as assignment_date`,
    ]);
    dataQuery.orderBy('assignment_date', 'ASC');
    const result = await dataQuery.getRawMany();
    return result.map((r) => {
      return { assignmentDate: r.assignment_date };
    });
  }
  async getRouteAssignmentTimes(
    routeId: string,
    assignmentDate: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'routeId',
          operator: FilterOperators.EqualTo,
          value: routeId,
        },
      ],
      [
        {
          field: 'assignmentDate',
          operator: FilterOperators.EqualTo,
          value: assignmentDate,
        },
      ],
    );
    if (!query.select) {
      query.select = [];
    }
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    dataQuery.select([`DISTINCT(pickup_time ) as pickup_time`]);
    dataQuery.orderBy('pickup_time', 'ASC');
    const result = await dataQuery.getRawMany();
    return result.map((r) => {
      return { pickupTime: r.pickup_time };
    });
  }

  async getAvailableAssignmentVehicleCategories(
    routeId: string,
    assignmentDate: string,
    pickupTime: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'routeId',
          operator: FilterOperators.EqualTo,
          value: routeId,
        },
      ],
      [
        {
          field: 'assignmentDate',
          operator: FilterOperators.EqualTo,
          value: assignmentDate,
        },
      ],
      [
        {
          field: 'pickupTime',
          operator: FilterOperators.EqualTo,
          value: pickupTime,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.NotIn,
          value: [
            AssignmentStatus.COMPLETED,
            AssignmentStatus.CANCELLED,
            AssignmentStatus.STARTED,
          ],
        },
      ],
      [
        {
          field: 'availableSeats',
          operator: FilterOperators.GreaterThan,
          value: 0,
        },
      ],
      [
        {
          field: 'vehicle_category_id',
          operator: FilterOperators.NotNull,
        },
      ],
    );
    if (!query.select) {
      query.select = [];
    }
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    dataQuery.select([
      `DISTINCT(vehicle_category_id ) as vehicle_category_id`,
      'vehicle_category_name',
    ]);
    const result = await dataQuery.getRawMany();
    return result.map((r) => {
      return {
        categoryId: r.vehicle_category_id,
        categoryName: r.vehicle_category_name,
      };
    });
  }
  async getAvailableAssignmentVehicleCategoriesByTime(
    pickupTime: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'pickupTime',
          operator: FilterOperators.EqualTo,
          value: pickupTime,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.NotIn,
          value: [
            AssignmentStatus.COMPLETED,
            AssignmentStatus.CANCELLED,
            AssignmentStatus.STARTED,
          ],
        },
      ],
      [
        {
          field: 'availableSeats',
          operator: FilterOperators.GreaterThan,
          value: 0,
        },
      ],
      [
        {
          field: 'vehicle_category_id',
          operator: FilterOperators.NotNull,
        },
      ],
    );
    if (!query.select) {
      query.select = [];
    }
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    dataQuery.select([
      `DISTINCT(vehicle_category_id) as vehicle_category_id`,
      'vehicle_category_name',
    ]);
    const result = await dataQuery.getRawMany();
    return result.map((r) => {
      return {
        categoryId: r.vehicle_category_id,
        categoryName: r.vehicle_category_name,
      };
    });
  }
  async getDriverAssignmentDates(
    driverId: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        operator: FilterOperators.EqualTo,
        value: driverId,
      },
    ]);
    if (!query.select) {
      query.select = [];
    }
    const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
      this.assignmentRepository,
      query,
    );
    dataQuery.select([
      `DISTINCT(to_char(assignment_date,'YYYY-MM-dd') ) as assignment_date`,
    ]);
    dataQuery.orderBy('assignment_date', 'ASC');
    const result = await dataQuery.getRawMany();
    return result.map((r) => {
      return { assignmentDate: r.assignment_date };
    });
  }
  async getUncompletedAssignments(query: CollectionQuery) {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Month is zero-based, so we add 1
    const day = String(currentDate.getDate()).padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;

    if (!query.filter) {
      query.filter = [];
    }
    query.filter = [
      [
        {
          field: 'status',
          operator: FilterOperators.EqualTo,
          value: AssignmentStatus.STARTED,
        },
      ],
      [
        {
          field: 'assignmentDate',
          operator: FilterOperators.LessThan,
          value: formattedDate,
        },
      ],
    ];
    const assignments = await this.getAssignments(query);
    if (assignments) {
      for (const ass of assignments.data) {
        this.eventEmitter.emit('complete.expired.assignments', ass.id);
      }
    }
  }
}
