import { Util } from '@libs/common/util';
import { RouteRepository } from '@router/persistence/routes/route.repository';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import {
  ArchiveAssignmentCommand,
  CancelAssignmentCommand,
  ChangePaymentStatusCommand,
  CreateAssignmentCommand,
  CreateBulkAssignmentCommand,
  CreateBulkAssignmentCommand2,
  PayForDriverAssignmentsCommand,
  PayForMultipleDriverAssignmentsCommand,
  UpdateAssignmentCommand,
} from './assignment.commands';
import { AssignmentRepository } from '@router/persistence/assignments/assignment.repository';
import { AssignmentResponse } from './assignment.response';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { UserInfo } from '@account/dtos/user-info.dto';
import {
  AssignmentStatus,
  DriverPaymentStatus,
} from '@router/domains/assignments/constants';
import { AssignmentQueries } from './assignment.usecase.queries';
import { DriverFeeCommands } from '@provider/usecases/driver-fees/driver-fee.usecase.commands';
import {
  ChangeDriverFeeStatusCommand,
  CreateDriverFeeCommand,
} from '@provider/usecases/driver-fees/driver-fee.commands';
import { Assignment } from '@router/domains/assignments/assignment';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BookingRepository } from '@order/persistance/bookings/booking.repository';
import { BookingStatus } from '@order/domains/bookings/constants';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { BookingQueries } from '@order/usecases/bookings/booking.usecase.queries';
import { AppService } from 'app.service';
import { DriverPaymentExcelExport } from '@provider/usecases/driver-fees/driver-fee.response';
@Injectable()
export class AssignmentCommands {
  constructor(
    private assignmentRepository: AssignmentRepository,
    private bookingRepository: BookingRepository,
    private bookingQuery: BookingQueries,
    private readonly eventEmitter: EventEmitter2,
    private readonly assignmentQueries: AssignmentQueries,
    private readonly driverFeeCommands: DriverFeeCommands,
    private readonly driverRepository: DriverRepository,
    private readonly routeRepository: RouteRepository,
    @InjectRepository(CategoryEntity)
    private categoryRepository: Repository<CategoryEntity>,
  ) {}
  async createAssignment(
    command: CreateAssignmentCommand,
  ): Promise<AssignmentResponse> {
    const driver = await this.driverRepository.getById(command.driverId);
    if (!driver) {
      throw new NotFoundException(
        `Driver not found with id ${command.driverId}`,
      );
    }
    if (!driver.vehicle) {
      throw new NotFoundException(
        `This Driver did't assigned the vehicle yet `,
      );
    }
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'status',
          operator: FilterOperators.In,
          value: [AssignmentStatus.ASSIGNED, AssignmentStatus.STARTED],
        },
      ],
      [
        {
          field: 'driverId',
          operator: FilterOperators.EqualTo,
          value: command.driverId,
        },
      ],
      [
        {
          field: 'assignmentDate',
          operator: FilterOperators.NotEqualTo,
          value: command.assignmentDate,
        },
      ],
      [
        {
          field: 'pickupTime',
          operator: FilterOperators.EqualTo,
          value: command.pickupTime,
        },
      ],
    ];
    query.top = 1;
    const assignments = await this.assignmentQueries.getAssignments(query);
    if (assignments.data.length > 0) {
      throw new BadRequestException(
        `Driver already assigned on this date and time`,
      );
    }

    const route = await this.routeRepository.getById(command.routeId);
    const category = await this.categoryRepository.findOneBy({
      id: driver.vehicle.categoryId,
    });
    if (!route) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId} `,
      );
    }
    const routePrice = route.prices.find(
      (price) => price.categoryId === driver.vehicle.categoryId,
    );
    const driverFee =
      (category.capacity * routePrice.fee * routePrice.driverCommission) / 100;
    const companyEarning = category.capacity * routePrice.fee - driverFee;
    command.fee = driverFee;
    command.companyEarning = companyEarning;
    const assignmentDomain = CreateAssignmentCommand.fromCommand(command);
    assignmentDomain.createdBy = command.currentUser.id;
    assignmentDomain.updatedBy = command.currentUser.id;
    assignmentDomain.vehicleId = driver.vehicle.id;
    assignmentDomain.vehiclePlateNumber = driver.vehicle.plateNumber;
    assignmentDomain.vehicleCategoryId = driver.vehicle.categoryId;
    assignmentDomain.vehicleCategoryName = category.name;
    assignmentDomain.availableSeats = category.capacity;
    const assignment = await this.assignmentRepository.insert(assignmentDomain);
    assignment.driver = driver;
    if (assignment) {
      //Send Notification
      const notificationData = {
        title: 'Route Assignment',
        body: `Route '${route.name}' has been assigned to you`,
        receiver: driver.id,
      };
      this.eventEmitter.emit('create.notification', notificationData);
      this.eventEmitter.emit('activity-logger.store', {
        modelId: assignment.id,
        modelName: MODELS.ASSIGNMENT,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
    }
    return AssignmentResponse.fromDomain(assignment);
  }
  async createBulkAssignment(
    bulkAssignmentCommand: CreateBulkAssignmentCommand,
  ): Promise<AssignmentResponse[]> {
    const assignmentResponses = [];
    const route = await this.routeRepository.getById(
      bulkAssignmentCommand.routeId,
    );
    if (!route) {
      throw new NotFoundException(
        `Route does not found with id ${bulkAssignmentCommand.routeId} `,
      );
    }
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'status',
          operator: FilterOperators.In,
          value: [AssignmentStatus.ASSIGNED, AssignmentStatus.STARTED],
        },
      ],
      [
        {
          field: 'driverId',
          operator: FilterOperators.In,
          value: bulkAssignmentCommand.drivers,
        },
      ],
      [
        {
          field: 'assignmentDate',
          operator: FilterOperators.EqualTo,
          value: bulkAssignmentCommand.assignmentDate,
        },
      ],
      [
        {
          field: 'pickupTime',
          operator: FilterOperators.EqualTo,
          value: bulkAssignmentCommand.pickupTime,
        },
      ],
    ];
    query.top = 1;
    const assignments = await this.assignmentQueries.getAssignments(query);
    if (assignments.data.length > 0) {
      throw new BadRequestException(
        `One of Drivers already assigned on this date and time`,
      );
    }

    for (const driverId of bulkAssignmentCommand.drivers) {
      const driver = await this.driverRepository.getById(driverId);
      if (!driver.vehicle || driver.enabled === false) {
        throw new BadRequestException(
          `Driver doesn't have a vehicle or vehicle has been archived/deactivated`,
        );
      }
      if (driver && driver.vehicle) {
        const category = await this.categoryRepository.findOneBy({
          id: driver.vehicle.categoryId,
        });
        const routePrice = route.prices.find(
          (price) => price.categoryId === category.id,
        );
        const driverFee =
          (category.capacity * routePrice.fee * routePrice.driverCommission) /
          100;
        const companyEarning = category.capacity * routePrice.fee - driverFee;
        const command: CreateAssignmentCommand = {
          driverId: driverId,
          routeId: bulkAssignmentCommand.routeId,
          remark: bulkAssignmentCommand.remark,
          pickupTime: bulkAssignmentCommand.pickupTime,
          assignmentDate: bulkAssignmentCommand.assignmentDate,
          fee: driverFee,
          companyEarning: companyEarning,
          currentUser: bulkAssignmentCommand.currentUser,
          driverName: driver.name,
          driverPhone: driver.phoneNumber,
          routeName: route.name,
          vehicleModel: driver.vehicle.model,
        };
        const assignmentDomain = CreateAssignmentCommand.fromCommand(command);
        assignmentDomain.createdBy = bulkAssignmentCommand.currentUser.id;
        assignmentDomain.updatedBy = bulkAssignmentCommand.currentUser.id;
        assignmentDomain.vehicleId = driver.vehicle.id;
        assignmentDomain.vehiclePlateNumber = driver.vehicle.plateNumber;
        assignmentDomain.vehicleCategoryId = driver.vehicle.categoryId;
        assignmentDomain.vehicleCategoryName = category.name;
        assignmentDomain.availableSeats = category.capacity;
        const assignment = await this.assignmentRepository.insert(
          assignmentDomain,
        );
        if (assignment) {
          //Send Notification
          const notificationData = {
            title: 'Route Assignment',
            body: `Route '${route.name}' has been assigned to you`,
            receiver: driver.id,
          };
          this.eventEmitter.emit('create.notification', notificationData);
          assignment.driver = driver;
          this.eventEmitter.emit('activity-logger.store', {
            modelId: assignment.id,
            modelName: MODELS.ASSIGNMENT,
            action: ACTIONS.CREATE,
            user: bulkAssignmentCommand.currentUser,
            userId: bulkAssignmentCommand.currentUser.id,
          });
          assignmentResponses.push(AssignmentResponse.fromDomain(assignment));
        }
      }
    }
    return assignmentResponses;
  }
  async updateAssignment(
    command: UpdateAssignmentCommand,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(
      command.id,
    );
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${command.id}`);
    }
    const driver = await this.driverRepository.getById(command.driverId);
    if (!driver) {
      throw new NotFoundException(
        `Driver not found with id ${command.driverId}`,
      );
    }
    if (!driver.vehicle) {
      throw new NotFoundException(
        `This Driver didn't assigned the vehicle yet `,
      );
    }
    const route = await this.routeRepository.getById(command.routeId);
    const category = await this.categoryRepository.findOneBy({
      id: driver.vehicle.categoryId,
    });
    if (!route) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId} `,
      );
    }
    const routePrice = route.prices.find(
      (price) => price.categoryId === driver.vehicle.categoryId,
    );
    const oldDriver = assignmentDomain.driverId;
    const driverFee =
      (category.capacity * routePrice.fee * routePrice.driverCommission) / 100;
    const companyEarning = category.capacity * routePrice.fee - driverFee;
    command.fee = driverFee;
    command.companyEarning = companyEarning;
    const oldPayload = { ...assignmentDomain };
    assignmentDomain.driverId = command.driverId;
    assignmentDomain.routeId = command.routeId;
    assignmentDomain.assignmentDate = command.assignmentDate;
    assignmentDomain.remark = command.remark;
    assignmentDomain.fee = command.fee ? command.fee : 0;
    assignmentDomain.companyEarning = command.companyEarning
      ? command.companyEarning
      : 0;
    assignmentDomain.pickupTime = command.pickupTime;
    assignmentDomain.vehicleModel = driver.vehicle.model;
    assignmentDomain.driverName = driver.name;
    assignmentDomain.driverPhone = driver.phoneNumber;
    assignmentDomain.routeName = route.name;
    assignmentDomain.updatedBy = command.currentUser.id;
    const assignment = await this.assignmentRepository.update(assignmentDomain);
    if (assignment) {
      if (oldDriver != command.driverId) {
        //Send Notification to new driver
        const notificationData = {
          title: 'Route Assignment',
          body: `Route '${route.name}' has been assigned to you`,
          receiver: driver.id,
        };
        this.eventEmitter.emit('create.notification', notificationData);
        //Send Notification to old driver
        const notificationOldDriverData = {
          title: 'Route Assignment',
          body: `Route '${route.name}' has been re-assigned to a new driver`,
          receiver: oldDriver,
        };
        this.eventEmitter.emit('create.notification', notificationOldDriverData);
        //Send Notification to Passengers
        const bookings = await this.bookingQuery.getActiveAssignmentBookings(
          command.id,
          new CollectionQuery(),
        );
        for (const passenger of bookings.data) {
          //Send Notification to passengers
          const notificationPassengerData = {
            title: 'Route Assignment',
            body: `Route '${route.name}' has been reassigned to a new driver`,
            receiver: passenger.passengerId,
          };
          this.eventEmitter.emit(
            'create.notification',
            notificationPassengerData,
          );
        }
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: assignment.id,
        modelName: MODELS.ASSIGNMENT,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: { ...assignment },
        oldPayload: { ...oldPayload },
      });
    }
    assignment.driver = driver;
    return AssignmentResponse.fromDomain(assignment);
  }
  async archiveAssignment(command: ArchiveAssignmentCommand): Promise<boolean> {
    const assignmentDomain = await this.assignmentRepository.getById(
      command.id,
    );
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${command.id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.ASSIGNMENT,
      action: ACTIONS.ARCHIVE,
      userId: command.currentUser.id,
      user: command.currentUser,
      reason: command.reason,
    });
    return await this.assignmentRepository.archive(command.id);
  }
  async restoreAssignment(
    id: string,
    currentUser: UserInfo,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(id, true);
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${id}`);
    }
    const r = await this.assignmentRepository.restore(id);
    if (r) {
      assignmentDomain.deletedAt = null;
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.ASSIGNMENT,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    return AssignmentResponse.fromDomain(assignmentDomain);
  }
  async deleteAssignment(id: string, currentUser: UserInfo): Promise<boolean> {
    const assignmentDomain = await this.assignmentRepository.getById(id, true);
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.ASSIGNMENT,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    return await this.assignmentRepository.delete(id);
  }
  async completeAssignment(
    id: string,
    currentUser: UserInfo,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(id);
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${id}`);
    }
    if (assignmentDomain.status !== AssignmentStatus.STARTED) {
      throw new BadRequestException(
        `This assignment is either cancelled or still assigned`,
      );
    }
    assignmentDomain.status = AssignmentStatus.COMPLETED;
    assignmentDomain.completedAt = new Date();
    const result = await this.assignmentRepository.update(assignmentDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.ASSIGNMENT,
        action: ACTIONS.COMPLETED,
        userId: currentUser.id,
        user: { ...currentUser },
      });
      this.eventEmitter.emit('complete.bookings', id);
    }
    return AssignmentResponse.fromDomain(result);
  }
  @OnEvent('complete.expired.assignments')
  async completeAssignmentWhenDateHasPassed(
    id: string,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(id);
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${id}`);
    }
    if (assignmentDomain.status !== AssignmentStatus.STARTED) {
      throw new BadRequestException(
        `This assignment is either cancelled or still assigned`,
      );
    }
    assignmentDomain.status = AssignmentStatus.COMPLETED;
    assignmentDomain.completedAt = new Date();
    const result = await this.assignmentRepository.update(assignmentDomain);
    if (result) {
      this.eventEmitter.emit('complete.bookings', id);
    }
    return AssignmentResponse.fromDomain(result);
  }
  async startAssignment(
    id: string,
    currentUser: UserInfo,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(id);
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${id}`);
    }
    if (assignmentDomain.status !== AssignmentStatus.ASSIGNED) {
      throw new BadRequestException(
        `This assignment is either cancelled or completed or not paid`,
      );
    }
    const currentDate = new Date();
    const assDate = new Date(assignmentDomain.assignmentDate.toString());
    const t = assignmentDomain.pickupTime.split(':');
    assDate.setHours(+t[0], +t[1], 0o00);
    assDate.setMinutes(assDate.getMinutes() - 30);
    if (Util.compareDate(assDate, currentDate) > 0) {
      throw new BadRequestException(
        `You can only start the trip at least 30 minutes before the pickup time`,
      );
    }
    assignmentDomain.status = AssignmentStatus.STARTED;
    assignmentDomain.startingAt = new Date();
    const result = await this.assignmentRepository.update(assignmentDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.ASSIGNMENT,
        action: ACTIONS.STARTED,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return AssignmentResponse.fromDomain(result);
  }
  async cancelAssignment(
    command: CancelAssignmentCommand,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(
      command.id,
    );
    if (!assignmentDomain) {
      throw new NotFoundException(`Assignment not found with id ${command.id}`);
    }
    if (assignmentDomain.isPaid) {
      throw new BadRequestException(`You can't Cancel paid assignments`);
    }
    const bookingArrivedDomain = await this.bookingRepository.getByAssignmentId(
      command.id,
      BookingStatus.ARRIVED,
    );
    const bookingBoardedDomain = await this.bookingRepository.getByAssignmentId(
      command.id,
      BookingStatus.BOARDED,
    );
    const bookingCreatedDomain = await this.bookingRepository.getByAssignmentId(
      command.id,
      BookingStatus.CREATED,
    );
    if (bookingCreatedDomain && bookingCreatedDomain.length > 0) {
      throw new BadRequestException(
        `You can't cancel assignments with active bookings. Try reassigning a driver`,
      );
    }
    if (
      (bookingBoardedDomain && bookingBoardedDomain.length > 0) ||
      (bookingArrivedDomain && bookingArrivedDomain.length > 0)
    ) {
      throw new BadRequestException(
        `You can't cancel assignments with active bookings`,
      );
    }
    const driver = await this.driverRepository.getById(
      assignmentDomain.driverId,
    );
    assignmentDomain.status = AssignmentStatus.CANCELLED;
    assignmentDomain.cancellationReason = command.cancelledReason;
    const result = await this.assignmentRepository.update(assignmentDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.ASSIGNMENT,
        action: ACTIONS.CANCELLED,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    result.driver = driver;
    return AssignmentResponse.fromDomain(result);
  }
  @OnEvent('decrease.assignment.seats')
  async decreaseAssignmentAvailableSeat(
    assignmentId: string,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(
      assignmentId,
    );
    if (assignmentDomain) {
      assignmentDomain.availableSeats -= 1;
      const result = await this.assignmentRepository.update(assignmentDomain);
      return AssignmentResponse.fromDomain(result);
    }
  }
  @OnEvent('increase.assignment.seats')
  async increaseAssignmentAvailableSeat(
    assignmentId: string,
  ): Promise<AssignmentResponse> {
    const assignmentDomain = await this.assignmentRepository.getById(
      assignmentId,
    );
    if (assignmentDomain) {
      assignmentDomain.availableSeats -= 1;
      const result = await this.assignmentRepository.update(assignmentDomain);
      return AssignmentResponse.fromDomain(result);
    }
  }
  async payForDriverAssignments(
    command: PayForDriverAssignmentsCommand,
  ): Promise<AssignmentResponse[]> {
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'id',
          operator: FilterOperators.In,
          value: command.assignmentIds,
        },
      ],
      [
        {
          field: 'driverId',
          operator: FilterOperators.EqualTo,
          value: command.driverId,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.NotEqualTo,
          value: AssignmentStatus.CANCELLED,
        },
      ],
      [
        {
          field: 'isPaid',
          operator: FilterOperators.NotEqualTo,
          value: true,
        },
      ],
    ];
    const assignments = await this.assignmentQueries.getAssignments(query);
    if (assignments.data.length === 0) {
      return [];
    }
    let totalPayment = 0;
    const assignmentIds = [];
    assignments.data.forEach((assignment) => {
      assignmentIds.push(assignment.id);
      totalPayment += assignment.fee;
    });
    const createPaymentCommand: CreateDriverFeeCommand = {
      driverId: command.driverId,
      // fromDate: command.fromDate,
      // toDate: command.toDate,
      fee: totalPayment,
      paymentMethod: command.paymentMethod,
      paymentStatus: DriverPaymentStatus.INITIATED,
      currentUser: command.currentUser,
      assignmentIds,
    };
    const paymentResponse = await this.driverFeeCommands.createDriverFee(
      createPaymentCommand,
    );
    const responses = [];
    if (paymentResponse) {
      for (const assignment of assignments.data) {
        assignment.isPaid = false;
        assignment.paymentStatus = DriverPaymentStatus.INITIATED;
        const result = await this.assignmentRepository.update(
          assignment as Assignment,
        );
        responses.push(AssignmentResponse.fromDomain(result));
      }
    }
    return responses;
  }
  async payForMultipleDriverAssignments(
    command: PayForMultipleDriverAssignmentsCommand,
  ): Promise<DriverPaymentExcelExport[]> /*Promise<AssignmentResponse[]>*/ {
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'id',
          operator: FilterOperators.In,
          value: command.assignmentIds,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.NotEqualTo,
          value: AssignmentStatus.CANCELLED,
        },
      ],
      [
        {
          field: 'isPaid',
          operator: FilterOperators.NotEqualTo,
          value: true,
        },
      ],
    ];
    query.includes = ['driver', 'driver.bankAccounts'];
    const assignments = await this.assignmentQueries.getAssignments(query);
    if (assignments.data.length === 0) {
      return [];
    }
    const driverIds = [];
    const exportData: DriverPaymentExcelExport[] = [];
    for (const ass of assignments.data) {
      if (!(ass.driverId in driverIds)) {
        driverIds.push(ass.driverId);
      }
    }
    for (const driverId of driverIds) {
      const driverAssignments = assignments.data.filter(
        (d) => d.driverId === driverId,
      );
      let totalPayment = 0;
      let accountNumber = null;
      let driverName = null;
      const assignmentIds = [];
      driverAssignments.forEach((assignment) => {
        assignmentIds.push(assignment.id);
        totalPayment += assignment.fee;
        if (accountNumber === null) {
          accountNumber = assignment.driver.bankAccounts.find(
            (bankAccount) => bankAccount.bankName === command.bankName,
          )?.accountNumber;
        }
        if (driverName === null) {
          driverName = assignment.driver.name;
        }
      });
      exportData.push({
        driverName,
        accountNumber,
        totalPayment,
      });
      const createPaymentCommand: CreateDriverFeeCommand = {
        driverId: driverId,
        // fromDate: command.fromDate,
        // toDate: command.toDate,
        bankName: command.bankName,
        accountNumber: accountNumber,
        fee: totalPayment,
        paymentStatus: DriverPaymentStatus.INITIATED,
        paymentMethod: command.paymentMethod,
        currentUser: command.currentUser,
        assignmentIds,
      };
      const paymentResponse = await this.driverFeeCommands.createDriverFee(
        createPaymentCommand,
      );
    }
    //const responses = [];
    // if (paymentResponse) {
    for (const assignment of assignments.data) {
      assignment.paymentStatus = DriverPaymentStatus.INITIATED;
      // assignment.isPaid = true;
      // assignment.paidAt = new Date();
      const result = await this.assignmentRepository.update(
        assignment as Assignment,
      );
      // responses.push(AssignmentResponse.fromDomain(result));
    }
    // }
    // return responses;
    return exportData;
  }
  @OnEvent('change.driver.fee.assignment.status')
  async changeAssignmentPaymentStatus(
    command: ChangePaymentStatusCommand,
  ): Promise<void> {
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'id',
          operator: FilterOperators.In,
          value: command.assignmentIds,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.NotEqualTo,
          value: AssignmentStatus.CANCELLED,
        },
      ],
      [
        {
          field: 'isPaid',
          operator: FilterOperators.NotEqualTo,
          value: true,
        },
      ],
    ];
    const assignments = await this.assignmentQueries.getAssignments(query);
    const responses = [];
    for (const assignment of assignments.data) {
      assignment.paymentStatus = command.status;
      if (command.status === DriverPaymentStatus.PAID) {
        assignment.isPaid = true;
        assignment.paidAt = new Date();
      }
      const result = await this.assignmentRepository.update(
        assignment as Assignment,
      );
    }
  }
  async createBulkAssignment2(
    bulkAssignmentCommand: CreateBulkAssignmentCommand2,
  ): Promise<AssignmentResponse[]> {
    const assignmentResponses = [];
    const route = await this.routeRepository.getById(
      bulkAssignmentCommand.routeId,
    );
    if (!route) {
      throw new NotFoundException(
        `Route does not found with id ${bulkAssignmentCommand.routeId} `,
      );
    }
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'status',
          operator: FilterOperators.In,
          value: [AssignmentStatus.ASSIGNED, AssignmentStatus.STARTED],
        },
      ],
      [
        {
          field: 'driverId',
          operator: FilterOperators.In,
          value: bulkAssignmentCommand.drivers,
        },
      ],
      [
        {
          field: 'assignmentDate',
          operator: FilterOperators.In,
          value: bulkAssignmentCommand.assignmentDate,
        },
      ],
      [
        {
          field: 'pickupTime',
          operator: FilterOperators.EqualTo,
          value: bulkAssignmentCommand.pickupTime,
        },
      ],
    ];
    query.top = 1;
    const assignments = await this.assignmentQueries.getAssignments(query);
    if (assignments.data.length > 0) {
      throw new BadRequestException(
        `One of Drivers already assigned on this date and time`,
      );
    }

    for (const driverId of bulkAssignmentCommand.drivers) {
      const driver = await this.driverRepository.getById(driverId);
      if (!driver.vehicle || driver.enabled === false) {
        throw new BadRequestException(
          `Driver doesn't have a vehicle or vehicle has been archived/deactivated`,
        );
      }
      if (driver && driver.vehicle) {
        const category = await this.categoryRepository.findOneBy({
          id: driver.vehicle.categoryId,
        });
        const routePrice = route.prices.find(
          (price) => price.categoryId === category.id,
        );
        const driverFee =
          (category.capacity * routePrice.fee * routePrice.driverCommission) /
          100;
        const companyEarning = category.capacity * routePrice.fee - driverFee;
        for (const d of bulkAssignmentCommand.assignmentDate) {
          const command: CreateAssignmentCommand = {
            driverId: driverId,
            routeId: bulkAssignmentCommand.routeId,
            remark: bulkAssignmentCommand.remark,
            pickupTime: bulkAssignmentCommand.pickupTime,
            assignmentDate: d,
            fee: driverFee,
            companyEarning: companyEarning,
            currentUser: bulkAssignmentCommand.currentUser,
            driverName: driver.name,
            driverPhone: driver.phoneNumber,
            routeName: route.name,
            vehicleModel: driver.vehicle.model,
          };
          const assignmentDomain = CreateAssignmentCommand.fromCommand(command);
          assignmentDomain.createdBy = bulkAssignmentCommand.currentUser.id;
          assignmentDomain.updatedBy = bulkAssignmentCommand.currentUser.id;
          assignmentDomain.vehicleId = driver.vehicle.id;
          assignmentDomain.vehiclePlateNumber = driver.vehicle.plateNumber;
          assignmentDomain.vehicleCategoryId = driver.vehicle.categoryId;
          assignmentDomain.vehicleCategoryName = category.name;
          assignmentDomain.availableSeats = category.capacity;
          const assignment = await this.assignmentRepository.insert(
            assignmentDomain,
          );
          if (assignment) {
            //Send Notification
            const notificationData = {
              title: 'Route Assignment',
              body: `Route '${route.name}' has been assigned to you`,
              receiver: driver.id,
            };
            this.eventEmitter.emit('create.notification', notificationData);
            assignment.driver = driver;
            this.eventEmitter.emit('activity-logger.store', {
              modelId: assignment.id,
              modelName: MODELS.ASSIGNMENT,
              action: ACTIONS.CREATE,
              user: bulkAssignmentCommand.currentUser,
              userId: bulkAssignmentCommand.currentUser.id,
            });
            assignmentResponses.push(AssignmentResponse.fromDomain(assignment));
          }
        }
      }
    }
    return assignmentResponses;
  }
}
