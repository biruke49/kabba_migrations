import { ApiProperty } from '@nestjs/swagger';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { Assignment } from '@router/domains/assignments/assignment';
import { CancellationReason } from '@router/domains/assignments/cancelled-reason';
import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { BusStopResponse } from '../cities/bus-stop.response';
import { RouteResponse } from '../routes/route.response';

export class AssignmentResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  vehicleId: string;
  @ApiProperty()
  vehiclePlateNumber: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  paymentStatus: string;
  @ApiProperty()
  fee: number;
  @ApiProperty()
  companyEarning: number;
  @ApiProperty()
  assignmentDate: Date;
  @ApiProperty()
  remark: string;
  @ApiProperty()
  cancellationReason: CancellationReason;
  @ApiProperty()
  availableSeats: number;
  @ApiProperty()
  vehicleCategoryId: string;
  @ApiProperty()
  vehicleCategoryName: string;
  @ApiProperty()
  startingAt: Date;
  @ApiProperty()
  completedAt: Date;
  @ApiProperty()
  paidAt: Date;
  @ApiProperty()
  isPaid: boolean;
  @ApiProperty()
  pickupTime: string;
  @ApiProperty()
  vehicleModel: string;
  @ApiProperty()
  routeName: string;
  @ApiProperty()
  driverName: string;
  @ApiProperty()
  driverPhone: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  route?: RouteResponse;
  driver?: DriverResponse;
  pickup?: BusStopResponse;
  destination?: BusStopResponse;
  static fromEntity(assignmentEntity: AssignmentEntity): AssignmentResponse {
    const assignmentResponse = new AssignmentResponse();
    assignmentResponse.id = assignmentEntity.id;
    assignmentResponse.routeId = assignmentEntity.routeId;
    assignmentResponse.driverId = assignmentEntity.driverId;
    assignmentResponse.vehicleId = assignmentEntity.vehicleId;
    assignmentResponse.vehicleId = assignmentEntity.vehicleId;
    assignmentResponse.vehiclePlateNumber = assignmentEntity.vehiclePlateNumber;
    assignmentResponse.vehicleCategoryId = assignmentEntity.vehicleCategoryId;
    assignmentResponse.vehicleCategoryName =
      assignmentEntity.vehicleCategoryName;
    assignmentResponse.assignmentDate = assignmentEntity.assignmentDate;
    assignmentResponse.fee = assignmentEntity.fee;
    assignmentResponse.companyEarning = assignmentEntity.companyEarning;
    assignmentResponse.cancellationReason = assignmentEntity.cancellationReason;
    assignmentResponse.remark = assignmentEntity.remark;
    assignmentResponse.availableSeats = assignmentEntity.availableSeats;
    assignmentResponse.pickupTime = assignmentEntity.pickupTime;
    assignmentResponse.startingAt = assignmentEntity.startingAt;
    assignmentResponse.completedAt = assignmentEntity.completedAt;
    assignmentResponse.status = assignmentEntity.status;
    assignmentResponse.paymentStatus = assignmentEntity.paymentStatus;
    assignmentResponse.paidAt = assignmentEntity.paidAt;
    assignmentResponse.isPaid = assignmentEntity.isPaid;
    assignmentResponse.vehicleModel = assignmentEntity.vehicleModel;
    assignmentResponse.routeName = assignmentEntity.routeName;
    assignmentResponse.driverName = assignmentEntity.driverName;
    assignmentResponse.driverPhone = assignmentEntity.driverPhone;
    assignmentResponse.createdBy = assignmentEntity.createdBy;
    assignmentResponse.updatedBy = assignmentEntity.updatedBy;
    assignmentResponse.deletedBy = assignmentEntity.deletedBy;
    assignmentResponse.createdAt = assignmentEntity.createdAt;
    assignmentResponse.updatedAt = assignmentEntity.updatedAt;
    assignmentResponse.deletedAt = assignmentEntity.deletedAt;
    if (assignmentEntity.route) {
      assignmentResponse.route = RouteResponse.fromEntity(
        assignmentEntity.route,
      );
    }
    if (assignmentEntity.driver) {
      assignmentResponse.driver = DriverResponse.fromEntity(
        assignmentEntity.driver,
      );
    }
    return assignmentResponse;
  }
  static fromDomain(assignment: Assignment): AssignmentResponse {
    const assignmentResponse = new AssignmentResponse();
    assignmentResponse.id = assignment.id;
    assignmentResponse.routeId = assignment.routeId;
    assignmentResponse.driverId = assignment.driverId;
    assignmentResponse.vehicleId = assignment.vehicleId;
    assignmentResponse.vehicleId = assignment.vehicleId;
    assignmentResponse.vehiclePlateNumber = assignment.vehiclePlateNumber;
    assignmentResponse.vehicleCategoryId = assignment.vehicleCategoryId;
    assignmentResponse.vehicleCategoryName = assignment.vehicleCategoryName;
    assignmentResponse.assignmentDate = assignment.assignmentDate;
    assignmentResponse.fee = assignment.fee;
    assignmentResponse.companyEarning = assignment.companyEarning;
    assignmentResponse.cancellationReason = assignment.cancellationReason;
    assignmentResponse.remark = assignment.remark;
    assignmentResponse.availableSeats = assignment.availableSeats;
    assignmentResponse.pickupTime = assignment.pickupTime;
    assignmentResponse.startingAt = assignment.startingAt;
    assignmentResponse.completedAt = assignment.completedAt;
    assignmentResponse.paidAt = assignment.paidAt;
    assignmentResponse.isPaid = assignment.isPaid;
    assignmentResponse.status = assignment.status;
    assignmentResponse.paymentStatus = assignment.paymentStatus;
    assignmentResponse.vehicleModel = assignment.vehicleModel;
    assignmentResponse.routeName = assignment.routeName;
    assignmentResponse.driverName = assignment.driverName;
    assignmentResponse.driverPhone = assignment.driverPhone;
    assignmentResponse.createdBy = assignment.createdBy;
    assignmentResponse.updatedBy = assignment.updatedBy;
    assignmentResponse.deletedBy = assignment.deletedBy;
    assignmentResponse.createdAt = assignment.createdAt;
    assignmentResponse.updatedAt = assignment.updatedAt;
    assignmentResponse.deletedAt = assignment.deletedAt;
    if (assignment.route) {
      assignmentResponse.route = RouteResponse.fromDomain(assignment.route);
    }
    if (assignment.driver) {
      assignmentResponse.driver = DriverResponse.fromDomain(assignment.driver);
    }
    return assignmentResponse;
  }
}
export class CountAssignmentByRoute {
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  routeName: string;
  @ApiProperty()
  pickupTime: string;
  @ApiProperty()
  availableSeats: string;
  @ApiProperty()
  count: number;
}
export class CountByStatusResponse {
  @ApiProperty()
  status: string;
  @ApiProperty()
  count: number;
}
export class CountByStatusAndRouteResponse {
  @ApiProperty()
  status: string;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  routeName: string;
  @ApiProperty()
  count: number;
}
export class CountEarningsByCategoryResponse {
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  categoryName: string;
  @ApiProperty()
  total: number;
}
