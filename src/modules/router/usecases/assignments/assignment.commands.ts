import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Assignment } from '@router/domains/assignments/assignment';
import { CancellationReason } from '@router/domains/assignments/cancelled-reason';
import { IsArray, IsNotEmpty } from 'class-validator';

export class CreateAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  assignmentDate: Date;
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  // @ApiProperty()
  fee: number;
  companyEarning: number;
  @ApiProperty()
  remark: string;
  vehicleModel: string;
  routeName: string;
  driverName: string;
  driverPhone: string;
  currentUser?: UserInfo;
  static fromCommand(command: CreateAssignmentCommand): Assignment {
    const assignmentDomain = new Assignment();
    assignmentDomain.driverId = command.driverId;
    assignmentDomain.routeId = command.routeId;
    assignmentDomain.assignmentDate = command.assignmentDate;
    assignmentDomain.remark = command.remark;
    assignmentDomain.fee = command.fee ? command.fee : 0;
    assignmentDomain.companyEarning = command.companyEarning
      ? command.companyEarning
      : 0;
    assignmentDomain.pickupTime = command.pickupTime;
    assignmentDomain.vehicleModel = command.vehicleModel;
    assignmentDomain.routeName = command.routeName;
    assignmentDomain.driverName = command.driverName;
    assignmentDomain.driverPhone = command.driverPhone;
    assignmentDomain.createdBy = command.currentUser.id;
    assignmentDomain.updatedBy = command.currentUser.id;
    return assignmentDomain;
  }
}
export class UpdateAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  @ApiProperty()
  @IsNotEmpty()
  assignmentDate: Date;
  //@ApiProperty()
  fee: number;
  companyEarning: number;
  @ApiProperty()
  remark: string;
  paymentStatus: string;
  vehicleModel: string;
  routeName: string;
  driverName: string;
  driverPhone: string;
  currentUser: UserInfo;
  static fromCommand(command: UpdateAssignmentCommand): Assignment {
    const assignmentDomain = new Assignment();
    assignmentDomain.id = command.id;
    assignmentDomain.driverId = command.driverId;
    assignmentDomain.routeId = command.routeId;
    assignmentDomain.assignmentDate = command.assignmentDate;
    assignmentDomain.remark = command.remark;
    assignmentDomain.paymentStatus = command.paymentStatus;
    assignmentDomain.fee = command.fee ? command.fee : 0;
    assignmentDomain.companyEarning = command.companyEarning
      ? command.companyEarning
      : 0;
    assignmentDomain.pickupTime = command.pickupTime;
    assignmentDomain.vehicleModel = command.vehicleModel;
    assignmentDomain.routeName = command.routeName;
    assignmentDomain.driverName = command.driverName;
    assignmentDomain.driverPhone = command.driverPhone;
    assignmentDomain.createdBy = command.currentUser.id;
    assignmentDomain.updatedBy = command.currentUser.id;
    return assignmentDomain;
  }
}
export class ArchiveAssignmentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class CancelAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  cancelledReason: CancellationReason;
  currentUser: UserInfo;
}
export class PayForDriverAssignmentsCommand {
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  assignmentIds: string[];
  @ApiProperty()
  paymentMethod: string;
  currentUser: UserInfo;
}
export class PayForMultipleDriverAssignmentsCommand {
  @ApiProperty()
  @IsNotEmpty()
  assignmentIds: string[];
  @ApiProperty()
  paymentMethod: string;
  @ApiProperty()
  bankName: string;
  currentUser: UserInfo;
}
export class ChangePaymentStatusCommand {
  @ApiProperty()
  @IsNotEmpty()
  assignmentIds: string[];
  @ApiProperty()
  status: string;
  currentUser: UserInfo;
}
export class CreateBulkAssignmentCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  drivers: string[];
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  assignmentDate: Date;
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  // @ApiProperty()
  // fee: number;
  @ApiProperty()
  remark: string;
  vehicleModel: string;
  routeName: string;
  driverName: string;
  driverPhone: string;
  currentUser: UserInfo;
}
export class CreateBulkAssignmentCommand2 {
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  drivers: string[];
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  assignmentDate: Date[];
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  // @ApiProperty()
  // fee: number;
  @ApiProperty()
  remark: string;
  vehicleModel: string;
  routeName: string;
  driverName: string;
  driverPhone: string;
  currentUser: UserInfo;
}
