import { City } from '@router/domains/cities/city';
import { ApiProperty } from '@nestjs/swagger';
import { RouteEntity } from '@router/persistence/routes/route.entity';
import { Route } from '@router/domains/routes/route';
import { CityResponse, PublicCityResponse } from '../cities/city.response';
import { PublicStationResponse, StationResponse } from './station.response';
import { RoutePriceResponse } from './route-price.response';
export class RouteResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  cityId: string;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  difficultyLevel: string;
  @ApiProperty()
  trafficJamLevel: string;
  @ApiProperty()
  totalDistance: number;
  @ApiProperty()
  numberOfViews: number;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  city?: City;
  stations: StationResponse[];
  prices: RoutePriceResponse[];

  static fromEntity(routeEntity: RouteEntity): RouteResponse {
    const routeResponse = new RouteResponse();
    routeResponse.id = routeEntity.id;
    routeResponse.name = routeEntity.name;
    routeResponse.description = routeEntity.description;
    routeResponse.cityId = routeEntity.cityId;
    routeResponse.difficultyLevel = routeEntity.difficultyLevel;
    routeResponse.trafficJamLevel = routeEntity.trafficJamLevel;
    routeResponse.totalDistance = routeEntity.totalDistance;
    routeResponse.numberOfViews = routeEntity.numberOfViews;
    if (routeEntity.city) {
      routeResponse.city = CityResponse.fromEntity(routeEntity.city);
    }
    if (routeEntity.stations) {
      routeResponse.stations = routeEntity.stations.map((station) => {
        return StationResponse.fromEntity(station);
      });
    }
    if (routeEntity.prices) {
      routeResponse.prices = routeEntity.prices.map((price) => {
        return RoutePriceResponse.fromEntity(price);
      });
    }
    routeResponse.isActive = routeEntity.isActive;
    routeResponse.totalDistance = routeEntity.totalDistance;
    routeResponse.createdBy = routeEntity.createdBy;
    routeResponse.updatedBy = routeEntity.updatedBy;
    routeResponse.deletedBy = routeEntity.deletedBy;
    routeResponse.createdAt = routeEntity.createdAt;
    routeResponse.updatedAt = routeEntity.updatedAt;
    routeResponse.deletedAt = routeEntity.deletedAt;
    routeResponse.archiveReason = routeEntity.archiveReason;
    return routeResponse;
  }
  static fromDomain(route: Route): RouteResponse {
    const routeResponse = new RouteResponse();
    routeResponse.id = route.id;
    routeResponse.name = route.name;
    routeResponse.description = route.description;
    routeResponse.cityId = route.cityId;
    routeResponse.difficultyLevel = route.difficultyLevel;
    routeResponse.trafficJamLevel = route.trafficJamLevel;
    routeResponse.numberOfViews = route.numberOfViews;
    if (route.city) {
      routeResponse.city = CityResponse.fromDomain(route.city);
    }
    if (route.stations) {
      routeResponse.stations = route.stations.map((station) => {
        return StationResponse.fromDomain(station);
      });
    }
    if (route.prices) {
      routeResponse.prices = route.prices.map((price) => {
        return RoutePriceResponse.fromDomain(price);
      });
    }
    routeResponse.isActive = route.isActive;
    routeResponse.createdBy = route.createdBy;
    routeResponse.updatedBy = route.updatedBy;
    routeResponse.deletedBy = route.deletedBy;
    routeResponse.createdAt = route.createdAt;
    routeResponse.updatedAt = route.updatedAt;
    routeResponse.deletedAt = route.deletedAt;
    routeResponse.archiveReason = route.archiveReason;
    return routeResponse;
  }
}

export class PublicRouteResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  cityId: string;
  @ApiProperty()
  difficultyLevel: string;
  @ApiProperty()
  trafficJamLevel: string;
  @ApiProperty()
  totalDistance: number;
  @ApiProperty()
  city?: PublicCityResponse;
  stations: PublicStationResponse[];
  prices: RoutePriceResponse[];

  static fromEntity(routeEntity: RouteEntity): PublicRouteResponse {
    const routeResponse = new PublicRouteResponse();
    routeResponse.id = routeEntity.id;
    routeResponse.name = routeEntity.name;
    routeResponse.description = routeEntity.description;
    routeResponse.cityId = routeEntity.cityId;
    routeResponse.difficultyLevel = routeEntity.difficultyLevel;
    routeResponse.trafficJamLevel = routeEntity.trafficJamLevel;
    routeResponse.totalDistance = routeEntity.totalDistance;
    if (routeEntity.city) {
      routeResponse.city = PublicCityResponse.fromEntity(routeEntity.city);
    }
    if (routeEntity.stations) {
      routeResponse.stations = routeEntity.stations.map((station) => {
        return PublicStationResponse.fromEntity(station);
      });
    }
    return routeResponse;
  }
  static fromDomain(route: Route): PublicRouteResponse {
    const routeResponse = new PublicRouteResponse();
    routeResponse.id = route.id;
    routeResponse.name = route.name;
    routeResponse.description = route.description;
    routeResponse.cityId = route.cityId;
    routeResponse.difficultyLevel = route.difficultyLevel;
    routeResponse.trafficJamLevel = route.trafficJamLevel;
    if (route.city) {
      routeResponse.city = PublicCityResponse.fromDomain(route.city);
    }
    if (route.stations) {
      routeResponse.stations = route.stations.map((station) => {
        return PublicStationResponse.fromDomain(station);
      });
    }
    return routeResponse;
  }
}
