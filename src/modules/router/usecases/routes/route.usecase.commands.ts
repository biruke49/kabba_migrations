import { Route } from '@router/domains/routes/route';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import {
  ArchiveRouteCommand,
  CreateRouteCommand,
  UpdateRouteCommand,
} from './route.commands';
import { RouteRepository } from '@router/persistence/routes/route.repository';
import { RouteResponse } from './route.response';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserInfo } from '@account/dtos/user-info.dto';
import { StationResponse } from './station.response';
import {
  ArchiveStationCommand,
  AssignStationToRoute,
  CreateStationCommand,
  DeleteStationCommand,
  UpdateStationCommand,
} from './station.commands';
import { InjectRepository } from '@nestjs/typeorm';
import { StationEntity } from '@router/persistence/routes/station.entity';
import { Repository } from 'typeorm';
import { RoutePriceEntity } from '@router/persistence/routes/route-price.entity';
import {
  CreateRoutePriceCommand,
  RestoreRoutePriceDriverCommissionCommand,
  RestoreRoutePriceFeeCommand,
  UpdateRoutePriceDriverCommissionCommand,
  UpdateRoutePriceFeeCommand,
} from './route-price.commands';
import { RoutePriceResponse } from './route-price.response';
import { BusStopQueries } from '../cities/bus-stop.usecase.queries';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { RouteLevel } from '@libs/common/enums';
import { Category } from '@classification/domains/categories/category';
import { AssignmentRepository } from '@router/persistence/assignments/assignment.repository';
import { PreferenceRepository } from '@interaction/persistence/preferences/preference.repository';
import { PreferenceQueries } from '@interaction/usecases/preferences/preference.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
@Injectable()
export class RouteCommands {
  constructor(
    private routeRepository: RouteRepository,
    private preferenceRepository: PreferenceRepository,
    private preferenceQuery: PreferenceQueries,
    private assignmentRepository: AssignmentRepository,
    private readonly eventEmitter: EventEmitter2,
    @InjectRepository(StationEntity)
    private stationRepository: Repository<StationEntity>,
    @InjectRepository(RoutePriceEntity)
    private routePriceRepository: Repository<RoutePriceEntity>,
    private readonly busStopQueries: BusStopQueries,
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
  ) {}
  async createRoute(command: CreateRouteCommand): Promise<RouteResponse> {
    const existingRoute = await this.routeRepository.getByCityIdAndName(
      command.cityId,
      command.name,
      true,
    );
    if (existingRoute) {
      throw new BadRequestException(`Route already exists in this city`);
    }
    const routeDomain = CreateRouteCommand.fromCommand(command);
    routeDomain.createdBy = command.currentUser.id;
    routeDomain.updatedBy = command.currentUser.id;
    const route = await this.routeRepository.insert(routeDomain);
    const routeResponse = RouteResponse.fromDomain(route);
    if (route) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: route.id,
        modelName: MODELS.ROUTE,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
      routeResponse.prices = await this.addAutomaticRoutePrices(route);
    }
    return routeResponse;
  }
  async updateRoute(command: UpdateRouteCommand): Promise<RouteResponse> {
    const routeDomain = await this.routeRepository.getById(command.id);
    if (!routeDomain) {
      throw new NotFoundException(`Route not found with id ${command.id}`);
    }

    if (routeDomain.name !== command.name) {
      const existingRoute = await this.routeRepository.getByCityIdAndName(
        command.cityId,
        command.name,
        true,
      );
      if (existingRoute)
        throw new BadRequestException(`Rout already exist in the city`);
    }
    const oldPayload = routeDomain;
    routeDomain.isActive = command.isActive;
    routeDomain.updatedBy = command.currentUser.id;
    routeDomain.cityId = command.cityId;
    routeDomain.description = command.description;
    routeDomain.name = command.name;
    routeDomain.difficultyLevel = command.difficultyLevel;
    routeDomain.trafficJamLevel = command.trafficJamLevel;
    routeDomain.totalDistance = command.totalDistance;
    const route = await this.routeRepository.update(routeDomain);
    const routeResponse = RouteResponse.fromDomain(route);
    if (route) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: route.id,
        modelName: MODELS.ROUTE,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: { ...route },
        oldPayload: { ...oldPayload },
      });
      routeResponse.prices = await this.addAutomaticRoutePrices(route);
    }
    return routeResponse;
  }
  async updateRouteView(routeId: string): Promise<RouteResponse> {
    const routeDomain = await this.routeRepository.getById(routeId);
    if (!routeDomain) {
      throw new NotFoundException(`Route not found with id ${routeId}`);
    }
    routeDomain.numberOfViews += 1;
    const route = await this.routeRepository.update(routeDomain);
    return RouteResponse.fromDomain(route);
  }
  async archiveRoute(command: ArchiveRouteCommand): Promise<RouteResponse> {
    const routeDomain = await this.routeRepository.getById(command.id);
    if (!routeDomain) {
      throw new NotFoundException(`Route not found with id ${command.id}`);
    }
    const assignedAssignmentDomain =
      await this.assignmentRepository.getByAssignedRouteId(command.id);
    if (assignedAssignmentDomain) {
      throw new BadRequestException(`Route has already been assigned`);
    }
    const startedAssignmentDomain =
      await this.assignmentRepository.getByStartedRouteId(command.id);
    if (startedAssignmentDomain) {
      throw new BadRequestException(`Route has a started assignment`);
    }
    const preferenceDomain = await this.preferenceQuery.getPreferencesByRoute(
      command.id,
      new CollectionQuery(),
    );
    if (preferenceDomain) {
      for (const preference of preferenceDomain.data) {
        await this.preferenceRepository.delete(preference.id);
      }
    }
    routeDomain.deletedAt = new Date();
    routeDomain.deletedBy = command.currentUser.id;
    routeDomain.archiveReason = command.reason;
    const result = await this.routeRepository.update(routeDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.ROUTE,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
        reason: command.reason,
      });
    }
    return RouteResponse.fromDomain(result);
  }
  async restoreRoute(
    id: string,
    currentUser: UserInfo,
  ): Promise<RouteResponse> {
    const routeDomain = await this.routeRepository.getById(id, true);
    if (!routeDomain) {
      throw new NotFoundException(`Route not found with id ${id}`);
    }
    const r = await this.routeRepository.restore(id);
    const routeResponse = RouteResponse.fromDomain(routeDomain);
    if (r) {
      routeDomain.deletedAt = null;
      routeResponse.prices = await this.addAutomaticRoutePrices(routeDomain);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.ROUTE,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    routeResponse.deletedAt = null;
    return routeResponse;
  }

  async deleteRoute(id: string, currentUser: UserInfo): Promise<boolean> {
    const routeDomain = await this.routeRepository.getById(id, true);
    if (!routeDomain) {
      throw new NotFoundException(`Route not found with id ${id}`);
    }
    const assignmentDomain = await this.assignmentRepository.getByRouteId(id);
    if (assignmentDomain) {
      throw new BadRequestException(`Route has already been assigned`);
    }
    const preferenceDomain = await this.preferenceRepository.getByRouteId(id);
    if (preferenceDomain) {
      throw new BadRequestException(`Route has been selected as a preference`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.ROUTE,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    return await this.routeRepository.delete(id);
  }
  async activateOrBlockRoute(
    id: string,
    currentUser: UserInfo,
  ): Promise<RouteResponse> {
    const routeDomain = await this.routeRepository.getById(id);
    if (!routeDomain) {
      throw new NotFoundException(`School not found with id ${id}`);
    }
    routeDomain.isActive = !routeDomain.isActive;
    const result = await this.routeRepository.update(routeDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.ROUTE,
        action: routeDomain.isActive ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return RouteResponse.fromDomain(result);
  }

  //Stations
  async addStation(command: CreateStationCommand): Promise<StationResponse> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    const station = CreateStationCommand.fromCommand(command);
    routeDomain.addStation(station);
    const result = await this.routeRepository.update(routeDomain);
    if (!result) return null;

    const stationResponse = StationResponse.fromDomain(
      result.stations[result.stations.length - 1],
    );
    const busStopResponse = await this.busStopQueries.getSingleBusStop(
      command.busStopId,
    );
    stationResponse.busStop = busStopResponse;
    this.eventEmitter.emit('activity-logger.store', {
      modelId: stationResponse.id,
      modelName: MODELS.STATION,
      action: ACTIONS.CREATE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return stationResponse;
  }
  async addStations(command: AssignStationToRoute): Promise<StationResponse[]> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    routeDomain.stations = [];
    let totalDistance = 0;
    for (const c of command.stations) {
      const singleStationCommand = new CreateStationCommand();
      singleStationCommand.routeId = command.routeId;
      singleStationCommand.busStopId = c.busStopId;
      singleStationCommand.currentUser = command.currentUser;
      singleStationCommand.distanceToNextBusStop = c.distanceToNextBusStop;
      singleStationCommand.isDestination = c.isDestination;
      singleStationCommand.isPickup = c.isPickup;
      singleStationCommand.position = c.position;
      singleStationCommand.nextBusStopId = c.nextBusStopId;
      totalDistance += c.distanceToNextBusStop;
      const station = CreateStationCommand.fromCommand(singleStationCommand);
      routeDomain.addStation(station);
    }
    routeDomain.totalDistance = Number(totalDistance.toFixed(2));
    const result = await this.routeRepository.update(routeDomain);
    if (!result) return [];
    await this.addAutomaticRoutePrices(result);

    const stationResponses = [];
    for (const station of result.stations) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: station.id,
        modelName: MODELS.STATION,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
      const stationResponse = StationResponse.fromDomain(station);
      const busStopResponse = await this.busStopQueries.getSingleBusStop(
        station.busStopId,
      );
      stationResponse.busStop = busStopResponse;
      stationResponses.push(stationResponse);
    }
    return stationResponses;
  }
  async updateStation(command: UpdateStationCommand): Promise<StationResponse> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    const oldPayload = routeDomain.stations.find((v) => v.id === command.id);
    const station = UpdateStationCommand.fromCommand(command);
    routeDomain.updateStation(station);
    const result = await this.routeRepository.update(routeDomain);
    if (!result) return null;
    this.eventEmitter.emit('activity-logger.store', {
      modelId: station.id,
      modelName: MODELS.STATION,
      action: ACTIONS.UPDATE,
      payload: { ...station },
      oldPayload: { ...oldPayload },
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return StationResponse.fromDomain(
      result.stations.find((station) => station.id === command.id),
    );
  }
  async deleteStation(command: DeleteStationCommand): Promise<boolean> {
    const stations = await this.stationRepository.find({
      where: { id: command.id },
      relations: [],
      withDeleted: true,
    });
    if (!stations[0]) {
      throw new NotFoundException(
        `Station does not found with id ${command.id}`,
      );
    }
    const result = await this.stationRepository.delete({ id: command.id });
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.STATION,
        action: ACTIONS.DELETE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  async archiveStation(command: ArchiveStationCommand): Promise<boolean> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    const station = routeDomain.stations.find((v) => v.id === command.id);
    station.deletedAt = new Date();
    routeDomain.updateStation(station);
    const result = await this.routeRepository.update(routeDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: station.id,
        modelName: MODELS.STATION,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
        reason: command.reason,
      });
    }
    return result ? true : false;
  }
  async restoreStation(
    command: DeleteStationCommand,
  ): Promise<StationResponse> {
    const station = await this.stationRepository.find({
      where: { id: command.id },
      relations: [],
      withDeleted: true,
    });
    if (!station[0]) {
      throw new NotFoundException(`Station not found with id ${command.id}`);
    }
    station[0].deletedAt = null;
    const result = await this.stationRepository.save(station[0]);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.STATION,
      action: ACTIONS.RESTORE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });

    return StationResponse.fromEntity(result);
  }
  async archiveStationsByRouteId(routeId: string): Promise<boolean> {
    const result = await this.stationRepository.update(
      { routeId: routeId },
      { deletedAt: new Date() },
    );
    return result ? true : false;
  }
  //RoutePrices
  async addAutomaticRoutePrices(route: Route): Promise<RoutePriceResponse[]> {
    const categories = await this.categoryRepository.find();
    route.prices = [];
    if (categories && categories.length > 0) {
      for (const category of categories) {
        const singleRoutePriceCommand = new CreateRoutePriceCommand();
        singleRoutePriceCommand.routeId = route.id;
        singleRoutePriceCommand.categoryId = category.id;
        singleRoutePriceCommand.driverCommission = category.driverCommission;
        singleRoutePriceCommand.defaultDriverCommission =
          category.driverCommission;

        let routeDifficultyLevelCost = 0;
        let routeTrafficJamDifficultyLevelCost = 0;
        switch (route.difficultyLevel) {
          case RouteLevel.EASY:
            routeDifficultyLevelCost = category.perKilometerCost.easy;
            break;
          case RouteLevel.MODERATE:
            routeDifficultyLevelCost = category.perKilometerCost.moderate;
            break;
          case RouteLevel.DIFFICULT:
            routeDifficultyLevelCost = category.perKilometerCost.difficult;
            break;
          case RouteLevel.EXTREME:
            routeDifficultyLevelCost = category.perKilometerCost.extreme;
            break;
        }
        switch (route.trafficJamLevel) {
          case RouteLevel.EASY:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.easy;
            break;
          case RouteLevel.MODERATE:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.moderate;
            break;
          case RouteLevel.DIFFICULT:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.difficult;
            break;
          case RouteLevel.EXTREME:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.extreme;
            break;
        }
        singleRoutePriceCommand.calculatedFee =
          category.initialFee +
          route.totalDistance * routeDifficultyLevelCost +
          routeTrafficJamDifficultyLevelCost;
        singleRoutePriceCommand.fee = singleRoutePriceCommand.calculatedFee;
        const routePrice = CreateRoutePriceCommand.fromCommand(
          singleRoutePriceCommand,
        );
        route.addPrice(routePrice);
      }
      const result = await this.routeRepository.update(route);
      return result.prices.map((price) => RoutePriceResponse.fromDomain(price));
    }
    return [];
  }
  @OnEvent('add.new.route.price.to.routes')
  async addNewRoutePricesByCategory(category: Category): Promise<void> {
    const routes = await this.routeRepository.getAll();
    if (routes && routes.length > 0) {
      for (const route of routes) {
        const singleRoutePriceCommand = new CreateRoutePriceCommand();
        singleRoutePriceCommand.routeId = route.id;
        singleRoutePriceCommand.categoryId = category.id;
        singleRoutePriceCommand.driverCommission = category.driverCommission;
        singleRoutePriceCommand.defaultDriverCommission =
          category.driverCommission;
        let routeDifficultyLevelCost = 0;
        let routeTrafficJamDifficultyLevelCost = 0;
        switch (route.difficultyLevel) {
          case RouteLevel.EASY:
            routeDifficultyLevelCost = category.perKilometerCost.easy;
            break;
          case RouteLevel.MODERATE:
            routeDifficultyLevelCost = category.perKilometerCost.moderate;
            break;
          case RouteLevel.DIFFICULT:
            routeDifficultyLevelCost = category.perKilometerCost.difficult;
            break;
          case RouteLevel.EXTREME:
            routeDifficultyLevelCost = category.perKilometerCost.extreme;
            break;
        }
        switch (route.trafficJamLevel) {
          case RouteLevel.EASY:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.easy;
            break;
          case RouteLevel.MODERATE:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.moderate;
            break;
          case RouteLevel.DIFFICULT:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.difficult;
            break;
          case RouteLevel.EXTREME:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.extreme;
            break;
        }
        singleRoutePriceCommand.calculatedFee =
          category.initialFee +
          route.totalDistance * routeDifficultyLevelCost +
          routeTrafficJamDifficultyLevelCost;
        singleRoutePriceCommand.fee = singleRoutePriceCommand.calculatedFee;
        const routePrice = CreateRoutePriceCommand.fromCommand(
          singleRoutePriceCommand,
        );
        route.addPrice(routePrice);
        await this.routeRepository.update(route);
      }
    }
  }
  @OnEvent('update.route.price.to.routes')
  async updateRoutePricesByCategory(category: Category): Promise<void> {
    const routes = await this.routeRepository.getAll();
    if (routes && routes.length > 0) {
      for (const route of routes) {
        let routeDifficultyLevelCost = 0;
        let routeTrafficJamDifficultyLevelCost = 0;
        switch (route.difficultyLevel) {
          case RouteLevel.EASY:
            routeDifficultyLevelCost = category.perKilometerCost.easy;
            break;
          case RouteLevel.MODERATE:
            routeDifficultyLevelCost = category.perKilometerCost.moderate;
            break;
          case RouteLevel.DIFFICULT:
            routeDifficultyLevelCost = category.perKilometerCost.difficult;
            break;
          case RouteLevel.EXTREME:
            routeDifficultyLevelCost = category.perKilometerCost.extreme;
            break;
        }
        switch (route.trafficJamLevel) {
          case RouteLevel.EASY:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.easy;
            break;
          case RouteLevel.MODERATE:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.moderate;
            break;
          case RouteLevel.DIFFICULT:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.difficult;
            break;
          case RouteLevel.EXTREME:
            routeTrafficJamDifficultyLevelCost =
              category.perTrafficJamCost.extreme;
            break;
        }
        const categoryFee =
          category.initialFee +
          route.totalDistance * routeDifficultyLevelCost +
          routeTrafficJamDifficultyLevelCost;

        const routePrice = route.prices.find(
          (price) => price.categoryId === category.id,
        );
        routePrice.fee = categoryFee;
        routePrice.calculatedFee = categoryFee;
        routePrice.driverCommission = category.driverCommission;
        routePrice.defaultDriverCommission = category.driverCommission;
        route.updatePrice(routePrice);
        await this.routeRepository.update(route);
      }
    }
  }
  @OnEvent('archive.route.price.to.routes')
  async archiveRoutePricesByCategory(category: Category): Promise<void> {
    await this.routePriceRepository.softDelete({ categoryId: category.id });
  }
  @OnEvent('restore.route.price.to.routes')
  async restoreRoutePricesByCategory(category: Category): Promise<void> {
    await this.routePriceRepository.restore({ categoryId: category.id });
  }
  @OnEvent('delete.route.price.to.routes')
  async deleteRoutePricesByCategory(category: Category): Promise<void> {
    await this.routePriceRepository.delete({ categoryId: category.id });
  }
  async updateRoutePriceFee(
    command: UpdateRoutePriceFeeCommand,
  ): Promise<RoutePriceResponse> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    const currentPrice = routeDomain.prices.find((v) => v.id === command.id);
    if (!currentPrice) {
      throw new NotFoundException(
        `Route Price does not found with id ${command.id}`,
      );
    }
    const oldPayload = { ...currentPrice };
    currentPrice.fee = command.fee;
    routeDomain.updatePrice(currentPrice);
    const result = await this.routeRepository.update(routeDomain);
    if (!result) return null;
    this.eventEmitter.emit('activity-logger.store', {
      modelId: currentPrice.id,
      modelName: MODELS.ROUTEPRICE,
      action: ACTIONS.UPDATE,
      payload: { ...currentPrice },
      oldPayload: { ...oldPayload },
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return RoutePriceResponse.fromDomain(
      result.prices.find((price) => price.id === command.id),
    );
  }
  async restoreRoutePriceFee(
    command: RestoreRoutePriceFeeCommand,
  ): Promise<RoutePriceResponse> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    const currentPrice = routeDomain.prices.find((v) => v.id === command.id);
    if (!currentPrice) {
      throw new NotFoundException(
        `Route Price does not found with id ${command.id}`,
      );
    }
    const oldPayload = { ...currentPrice };
    currentPrice.fee = currentPrice.calculatedFee;
    routeDomain.updatePrice(currentPrice);
    const result = await this.routeRepository.update(routeDomain);
    if (!result) return null;
    this.eventEmitter.emit('activity-logger.store', {
      modelId: currentPrice.id,
      modelName: MODELS.ROUTEPRICE,
      action: ACTIONS.UPDATE,
      payload: { ...currentPrice },
      oldPayload: { ...oldPayload },
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return RoutePriceResponse.fromDomain(
      result.prices.find((price) => price.id === command.id),
    );
  }
  async updateRoutePriceDriverCommission(
    command: UpdateRoutePriceDriverCommissionCommand,
  ): Promise<RoutePriceResponse> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    const currentPrice = routeDomain.prices.find((v) => v.id === command.id);
    if (!currentPrice) {
      throw new NotFoundException(
        `Route Price does not found with id ${command.id}`,
      );
    }
    const oldPayload = { ...currentPrice };
    currentPrice.driverCommission = command.driverCommission;
    routeDomain.updatePrice(currentPrice);
    const result = await this.routeRepository.update(routeDomain);
    if (!result) return null;
    this.eventEmitter.emit('activity-logger.store', {
      modelId: currentPrice.id,
      modelName: MODELS.ROUTEPRICE,
      action: ACTIONS.UPDATE,
      payload: { ...currentPrice },
      oldPayload: { ...oldPayload },
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return RoutePriceResponse.fromDomain(
      result.prices.find((price) => price.id === command.id),
    );
  }
  async restoreRoutePriceDriverCommission(
    command: RestoreRoutePriceDriverCommissionCommand,
  ): Promise<RoutePriceResponse> {
    const routeDomain = await this.routeRepository.getById(command.routeId);
    if (!routeDomain) {
      throw new NotFoundException(
        `Route does not found with id ${command.routeId}`,
      );
    }
    const currentPrice = routeDomain.prices.find((v) => v.id === command.id);
    if (!currentPrice) {
      throw new NotFoundException(
        `Route Price does not found with id ${command.id}`,
      );
    }
    const oldPayload = { ...currentPrice };
    currentPrice.driverCommission = currentPrice.defaultDriverCommission;
    routeDomain.updatePrice(currentPrice);
    const result = await this.routeRepository.update(routeDomain);
    if (!result) return null;
    this.eventEmitter.emit('activity-logger.store', {
      modelId: currentPrice.id,
      modelName: MODELS.ROUTEPRICE,
      action: ACTIONS.UPDATE,
      payload: { ...currentPrice },
      oldPayload: { ...oldPayload },
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return RoutePriceResponse.fromDomain(
      result.prices.find((price) => price.id === command.id),
    );
  }
}
