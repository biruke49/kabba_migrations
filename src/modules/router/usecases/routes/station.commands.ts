import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Station } from '@router/domains/routes/station';
import { IsNotEmpty } from 'class-validator';

export class CreateStationCommand {
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  busStopId: string;
  @ApiProperty()
  isPickup: boolean;
  @ApiProperty()
  isDestination: boolean;
  @ApiProperty()
  @IsNotEmpty()
  position: number;
  @ApiProperty()
  nextBusStopId: string;
  @ApiProperty()
  distanceToNextBusStop: number;
  currentUser: UserInfo;
  static fromCommand(command: CreateStationCommand): Station {
    const station = new Station();
    station.routeId = command.routeId;
    station.position = command.position;
    station.isPickup = command.isPickup;
    station.isDestination = command.isDestination;
    station.distanceToNextBusStop = command.distanceToNextBusStop;
    station.nextBusStopId = command.nextBusStopId;
    station.busStopId = command.busStopId;
    return station;
  }
}

export class UpdateStationCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  busStopId: string;
  @ApiProperty()
  isPickup: boolean;
  @ApiProperty()
  isDestination: boolean;
  @ApiProperty()
  @IsNotEmpty()
  position: number;
  @ApiProperty()
  nextBusStopId: string;
  @ApiProperty()
  distanceToNextBusStop: number;
  currentUser: UserInfo;
  static fromCommand(command: UpdateStationCommand): Station {
    const station = new Station();
    station.id = command.id;
    station.routeId = command.routeId;
    station.position = command.position;
    station.isPickup = command.isPickup;
    station.isDestination = command.isDestination;
    station.distanceToNextBusStop = command.distanceToNextBusStop;
    station.nextBusStopId = command.nextBusStopId;
    station.busStopId = command.busStopId;
    return station;
  }
}
export class DeleteStationCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  currentUser: UserInfo;
}
export class ArchiveStationCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class StationCommand {
  @ApiProperty()
  @IsNotEmpty()
  busStopId: string;
  @ApiProperty()
  isPickup: boolean;
  @ApiProperty()
  isDestination: boolean;
  @ApiProperty()
  @IsNotEmpty()
  position: number;
  @ApiProperty()
  nextBusStopId: string;
  @ApiProperty()
  distanceToNextBusStop: number;
}
export class AssignStationToRoute {
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty({ type: StationCommand, isArray: true })
  stations: StationCommand[];
  currentUser: UserInfo;
}
