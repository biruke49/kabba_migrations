import {
  PublicRouteResponse,
  RouteResponse,
} from '@router/usecases/routes/route.response';
import {
  BusStopResponse,
  PublicBusStopResponse,
} from '@router/usecases/cities/bus-stop.response';
import { ApiProperty } from '@nestjs/swagger';
import { Station } from '@router/domains/routes/station';
import { StationEntity } from '@router/persistence/routes/station.entity';

export class StationResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  busStopId: string;
  @ApiProperty()
  isPickup: boolean;
  @ApiProperty()
  isDestination: boolean;
  @ApiProperty()
  position: number;
  @ApiProperty()
  nextBusStopId: string;
  @ApiProperty()
  distanceToNextBusStop: number;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  busStop: BusStopResponse;
  route: RouteResponse;
  static fromEntity(stationEntity: StationEntity): StationResponse {
    const stationResponse = new StationResponse();
    stationResponse.id = stationEntity.id;
    stationResponse.routeId = stationEntity.routeId;
    stationResponse.busStopId = stationEntity.busStopId;
    stationResponse.distanceToNextBusStop = stationEntity.distanceToNextBusStop;
    stationResponse.nextBusStopId = stationEntity.nextBusStopId;
    stationResponse.position = stationEntity.position;
    stationResponse.isPickup = stationEntity.isPickup;
    stationResponse.isDestination = stationEntity.isDestination;
    stationResponse.createdBy = stationEntity.createdBy;
    stationResponse.updatedBy = stationEntity.updatedBy;
    stationResponse.deletedBy = stationEntity.deletedBy;
    stationResponse.createdAt = stationEntity.createdAt;
    stationResponse.updatedAt = stationEntity.updatedAt;
    stationResponse.deletedAt = stationEntity.deletedAt;
    if (stationEntity.busStop) {
      stationResponse.busStop = BusStopResponse.fromEntity(
        stationEntity.busStop,
      );
    }
    if (stationEntity.route) {
      stationResponse.route = RouteResponse.fromEntity(stationEntity.route);
    }
    return stationResponse;
  }
  static fromDomain(station: Station): StationResponse {
    const stationResponse = new StationResponse();
    stationResponse.id = station.id;
    stationResponse.routeId = station.routeId;
    stationResponse.busStopId = station.busStopId;
    stationResponse.distanceToNextBusStop = station.distanceToNextBusStop;
    stationResponse.nextBusStopId = station.nextBusStopId;
    stationResponse.position = station.position;
    stationResponse.isPickup = station.isPickup;
    stationResponse.isDestination = station.isDestination;
    stationResponse.createdBy = station.createdBy;
    stationResponse.updatedBy = station.updatedBy;
    stationResponse.deletedBy = station.deletedBy;
    stationResponse.createdAt = station.createdAt;
    stationResponse.updatedAt = station.updatedAt;
    stationResponse.deletedAt = station.deletedAt;
    if (station.busStop) {
      stationResponse.busStop = BusStopResponse.fromDomain(station.busStop);
    }
    if (station.route) {
      stationResponse.route = RouteResponse.fromDomain(station.route);
    }
    return stationResponse;
  }
}
export class PublicStationResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  busStopId: string;
  @ApiProperty()
  isPickup: boolean;
  @ApiProperty()
  isDestination: boolean;
  @ApiProperty()
  position: number;
  @ApiProperty()
  nextBusStopId: string;
  @ApiProperty()
  distanceToNextBusStop: number;
  busStop: PublicBusStopResponse;
  route: PublicRouteResponse;
  static fromEntity(stationEntity: StationEntity): PublicStationResponse {
    const stationResponse = new PublicStationResponse();
    stationResponse.id = stationEntity.id;
    stationResponse.routeId = stationEntity.routeId;
    stationResponse.busStopId = stationEntity.busStopId;
    stationResponse.distanceToNextBusStop = stationEntity.distanceToNextBusStop;
    stationResponse.nextBusStopId = stationEntity.nextBusStopId;
    stationResponse.position = stationEntity.position;
    stationResponse.isPickup = stationEntity.isPickup;
    stationResponse.isDestination = stationEntity.isDestination;
    if (stationEntity.busStop) {
      stationResponse.busStop = PublicBusStopResponse.fromEntity(
        stationEntity.busStop,
      );
    }
    if (stationEntity.route) {
      stationResponse.route = PublicRouteResponse.fromEntity(
        stationEntity.route,
      );
    }
    return stationResponse;
  }
  static fromDomain(station: Station): PublicStationResponse {
    const stationResponse = new PublicStationResponse();
    stationResponse.id = station.id;
    stationResponse.routeId = station.routeId;
    stationResponse.busStopId = station.busStopId;
    stationResponse.distanceToNextBusStop = station.distanceToNextBusStop;
    stationResponse.nextBusStopId = station.nextBusStopId;
    stationResponse.position = station.position;
    stationResponse.isPickup = station.isPickup;
    stationResponse.isDestination = station.isDestination;
    if (station.busStop) {
      stationResponse.busStop = PublicBusStopResponse.fromDomain(
        station.busStop,
      );
    }
    if (station.route) {
      stationResponse.route = PublicRouteResponse.fromDomain(station.route);
    }
    return stationResponse;
  }
}
