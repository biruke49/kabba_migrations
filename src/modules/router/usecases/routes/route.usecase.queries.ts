import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RouteEntity } from '@router/persistence/routes/route.entity';
import { Repository, DataSource } from 'typeorm';
import { PublicRouteResponse, RouteResponse } from './route.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { StationResponse } from './station.response';
import { StationEntity } from '@router/persistence/routes/station.entity';
import { RoutePriceResponse } from './route-price.response';
import { RoutePriceEntity } from '@router/persistence/routes/route-price.entity';
import { GetNearestRouteQuery } from './route.commands';
@Injectable()
export class RouteQueries {
  constructor(
    @InjectRepository(RouteEntity)
    private routeRepository: Repository<RouteEntity>,
    @InjectRepository(StationEntity)
    private stationRepository: Repository<StationEntity>,
    @InjectRepository(RoutePriceEntity)
    private routePriceRepository: Repository<RoutePriceEntity>,
    private dataSource: DataSource,
  ) {}
  async getRoute(id: string, withDeleted = false): Promise<RouteResponse> {
    const route = await this.routeRepository.find({
      where: { id: id },
      relations: ['city', 'stations', 'prices', 'stations.busStop'],
      withDeleted: withDeleted,
    });
    if (!route[0]) {
      throw new NotFoundException(`Route not found with id ${id}`);
    }
    return RouteResponse.fromEntity(route[0]);
  }
  async getRoutes(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RouteResponse>> {
    const dataQuery = QueryConstructor.constructQuery<RouteEntity>(
      this.routeRepository,
      query,
    );
    const d = new DataResponseFormat<RouteResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RouteResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async exploreRoutes(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PublicRouteResponse>> {
    const dataQuery = QueryConstructor.constructQuery<RouteEntity>(
      this.routeRepository,
      query,
    );
    const d = new DataResponseFormat<PublicRouteResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PublicRouteResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedRoutes(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RouteResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<RouteEntity>(
      this.routeRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<RouteResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RouteResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getRoutesByCity(
    cityId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RouteResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'cityId',
        operator: FilterOperators.EqualTo,
        value: cityId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<RouteEntity>(
      this.routeRepository,
      query,
    );
    const d = new DataResponseFormat<RouteResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RouteResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  //Stations
  async getStation(id: string, withDeleted = false): Promise<StationResponse> {
    const station = await this.stationRepository.find({
      where: { id: id },
      relations: ['driver', 'route'],
      withDeleted: withDeleted,
    });
    if (!station[0]) {
      throw new NotFoundException(`Station not found with id ${id}`);
    }
    return StationResponse.fromEntity(station[0]);
  }
  async getStations(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<StationResponse>> {
    const dataQuery = QueryConstructor.constructQuery<StationEntity>(
      this.stationRepository,
      query,
    );
    const d = new DataResponseFormat<StationResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => StationResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getRouteStations(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<StationResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'route_id',
        value: routeId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<StationEntity>(
      this.stationRepository,
      query,
    );
    const d = new DataResponseFormat<StationResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => StationResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedStations(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<StationResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<StationEntity>(
      this.stationRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<StationResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => StationResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }

  async getArchivedRouteStations(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<StationResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    query.filter.push([
      {
        field: 'route_id',
        value: routeId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<StationEntity>(
      this.stationRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<StationResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => StationResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }

  //RoutePrices
  async getRoutePrice(
    id: string,
    withDeleted = false,
  ): Promise<RoutePriceResponse> {
    const routePrices = await this.routePriceRepository.find({
      where: { id: id },
      relations: ['category', 'route'],
      withDeleted: withDeleted,
    });
    if (!routePrices[0]) {
      throw new NotFoundException(`RoutePrice not found with id ${id}`);
    }
    return RoutePriceResponse.fromEntity(routePrices[0]);
  }
  async getAllRoutePrices(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RoutePriceResponse>> {
    const dataQuery = QueryConstructor.constructQuery<RoutePriceEntity>(
      this.routePriceRepository,
      query,
    );
    const d = new DataResponseFormat<RoutePriceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RoutePriceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getRoutePrices(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RoutePriceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'route_id',
        value: routeId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<RoutePriceEntity>(
      this.routePriceRepository,
      query,
    );
    const d = new DataResponseFormat<RoutePriceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RoutePriceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getRoutePricesByCategoryId(
    categoryId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RoutePriceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'category_id',
        value: categoryId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<RoutePriceEntity>(
      this.routePriceRepository,
      query,
    );
    const d = new DataResponseFormat<RoutePriceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RoutePriceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getAllArchivedRoutePrices(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RoutePriceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<RoutePriceEntity>(
      this.routePriceRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<RoutePriceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RoutePriceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }

  async getArchivedRoutePrices(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<RoutePriceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    query.filter.push([
      {
        field: 'route_id',
        value: routeId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<RoutePriceEntity>(
      this.routePriceRepository,
      query,
    );
    dataQuery.withDeleted();
    //console.log(dataQuery.getSql(), dataQuery.getParameters());
    const d = new DataResponseFormat<RoutePriceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => RoutePriceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getNearestRoute(command: GetNearestRouteQuery) {
    const queryStr = `SELECT DISTINCT r.id
                        FROM routes r
                        INNER JOIN stations s ON s.route_id = r.id
                        INNER JOIN (
                        SELECT
                          id,
                          lat,
                          lng,
                          ST_DistanceSphere(
                            ST_MakePoint(${command.lat}, ${command.lng}),
                            ST_MakePoint(lng, lat)
                          ) AS distance
                        FROM bus_stops
                        WHERE
                          ST_DistanceSphere(
                            ST_MakePoint(${command.lat}, ${command.lng}),
                            ST_MakePoint(lng, lat)
                          ) > ${command.radius}
                        ) b ON b.id = s.bus_stop_id
                        WHERE r.is_active = true
                        AND r.deleted_at IS NULL
                        OFFSET 0
                        LIMIT ${command.top};`;
    const routeIds = await this.dataSource.query(queryStr);

    const arrayIds = [];
    routeIds.forEach((element) => {
      arrayIds.push(element.id);
    });
    if (arrayIds.length === 0) {
      return [];
    }
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'id',
          operator: FilterOperators.In,
          value: arrayIds,
        },
      ],
    ];
    query.orderBy = [
      {
        field: 'totalDistance',
        direction: 'ASC',
      },
    ];
    query.includes = ['stations', 'stations.busStop', 'prices'];
    const dataQuery = QueryConstructor.constructQuery<RouteEntity>(
      this.routeRepository,
      query,
    );
    const routes = await dataQuery.getMany();
    return routes.map((route) => RouteResponse.fromEntity(route));
  }
}
