import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { RoutePrice } from '@router/domains/routes/route-price';
import { IsNotEmpty, IsPositive } from 'class-validator';

export class CreateRoutePriceCommand {
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  fee: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  calculatedFee: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  driverCommission: number;
  defaultDriverCommission: number;
  currentUser?: UserInfo;
  static fromCommand(command: CreateRoutePriceCommand): RoutePrice {
    const routePrice = new RoutePrice();
    routePrice.routeId = command.routeId;
    routePrice.fee = command.fee;
    routePrice.calculatedFee = command.calculatedFee;
    routePrice.categoryId = command.categoryId;
    routePrice.driverCommission = command.driverCommission;
    routePrice.defaultDriverCommission = command.defaultDriverCommission;
    return routePrice;
  }
}

export class UpdateRoutePriceCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  fee: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  calculatedFee: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  driverCommission: number;
  currentUser: UserInfo;
  static fromCommand(command: UpdateRoutePriceCommand): RoutePrice {
    const routePrice = new RoutePrice();
    routePrice.id = command.id;
    routePrice.routeId = command.routeId;
    routePrice.fee = command.fee;
    routePrice.calculatedFee = command.calculatedFee;
    routePrice.categoryId = command.categoryId;
    routePrice.driverCommission = command.driverCommission;
    return routePrice;
  }
}
export class DeleteRoutePriceCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  currentUser: UserInfo;
}
export class ArchiveRoutePriceCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class RoutePriceCommand {
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  fee: number;
}
export class AssignPriceToRouteCommand {
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty({ type: RoutePriceCommand, isArray: true })
  prices: RoutePriceCommand[];
  currentUser: UserInfo;
}
export class UpdateRoutePriceFeeCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  fee: number;
  currentUser: UserInfo;
}
export class RestoreRoutePriceFeeCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  currentUser: UserInfo;
}
export class UpdateRoutePriceDriverCommissionCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  driverCommission: number;
  currentUser: UserInfo;
}
export class RestoreRoutePriceDriverCommissionCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  currentUser: UserInfo;
}
