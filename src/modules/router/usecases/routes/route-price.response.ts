import { CategoryResponse } from './../../../classification/usecases/categories/category.response';
import { ApiProperty } from '@nestjs/swagger';
import { RoutePrice } from '@router/domains/routes/route-price';
import { RoutePriceEntity } from '@router/persistence/routes/route-price.entity';

export class RoutePriceResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  fee: number;
  @ApiProperty()
  calculatedFee: number;
  @ApiProperty()
  defaultDriverCommission: number;
  @ApiProperty()
  driverCommission: number;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty()
  category: CategoryResponse;
  static fromEntity(routePriceEntity: RoutePriceEntity): RoutePriceResponse {
    const routePriceResponse = new RoutePriceResponse();
    routePriceResponse.id = routePriceEntity.id;
    routePriceResponse.routeId = routePriceEntity.routeId;
    routePriceResponse.categoryId = routePriceEntity.categoryId;
    routePriceResponse.fee = routePriceEntity.fee;
    routePriceResponse.calculatedFee = routePriceEntity.calculatedFee;
    routePriceResponse.driverCommission = routePriceEntity.driverCommission;
    routePriceResponse.defaultDriverCommission =
      routePriceEntity.defaultDriverCommission;
    if (routePriceEntity.category) {
      routePriceResponse.category = CategoryResponse.fromEntity(
        routePriceEntity.category,
      );
    }
    routePriceResponse.createdBy = routePriceEntity.createdBy;
    routePriceResponse.updatedBy = routePriceEntity.updatedBy;
    routePriceResponse.deletedBy = routePriceEntity.deletedBy;
    routePriceResponse.createdAt = routePriceEntity.createdAt;
    routePriceResponse.updatedAt = routePriceEntity.updatedAt;
    routePriceResponse.deletedAt = routePriceEntity.deletedAt;
    return routePriceResponse;
  }
  static fromDomain(routePrice: RoutePrice): RoutePriceResponse {
    const routePriceResponse = new RoutePriceResponse();
    routePriceResponse.id = routePrice.id;
    routePriceResponse.routeId = routePrice.routeId;
    routePriceResponse.categoryId = routePrice.categoryId;
    routePriceResponse.fee = routePrice.fee;
    routePriceResponse.calculatedFee = routePrice.calculatedFee;
    routePriceResponse.driverCommission = routePrice.driverCommission;
    routePriceResponse.defaultDriverCommission =
      routePrice.defaultDriverCommission;
    if (routePrice.category) {
      routePriceResponse.category = CategoryResponse.fromDomain(
        routePrice.category,
      );
    }
    routePriceResponse.createdBy = routePrice.createdBy;
    routePriceResponse.updatedBy = routePrice.updatedBy;
    routePriceResponse.deletedBy = routePrice.deletedBy;
    routePriceResponse.createdAt = routePrice.createdAt;
    routePriceResponse.updatedAt = routePrice.updatedAt;
    routePriceResponse.deletedAt = routePrice.deletedAt;
    return routePriceResponse;
  }
}
