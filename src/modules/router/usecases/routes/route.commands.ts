import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsNumberString, IsUUID } from 'class-validator';
import { Route } from '@router/domains/routes/route';
export class CreateRouteCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  // @IsUUID()
  cityId: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  @IsNotEmpty()
  difficultyLevel: string;
  @ApiProperty()
  @IsNotEmpty()
  trafficJamLevel: string;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  totalDistance: number;
  currentUser: UserInfo;
  static fromCommand(command: CreateRouteCommand): Route {
    const routeDomain = new Route();
    routeDomain.name = command.name;
    routeDomain.description = command.description;
    routeDomain.trafficJamLevel = command.trafficJamLevel;
    routeDomain.difficultyLevel = command.difficultyLevel;
    routeDomain.isActive = command.isActive;
    routeDomain.cityId = command.cityId;
    routeDomain.totalDistance = command.totalDistance;
    return routeDomain;
  }
}
export class UpdateRouteCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  // @IsUUID()
  cityId: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  @IsNotEmpty()
  difficultyLevel: string;
  @ApiProperty()
  @IsNotEmpty()
  trafficJamLevel: string;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  totalDistance: number;
  currentUser: UserInfo;
}
export class ArchiveRouteCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class GetNearestRouteQuery {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  lng: number;
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty()
  @IsNotEmpty()
  radius: number = 1;
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty()
  @IsNotEmpty()
  top: number = 5;
}
