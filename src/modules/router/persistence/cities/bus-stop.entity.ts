import { CommonEntity } from '@libs/common/common.entity';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { StationEntity } from '../routes/station.entity';
import { CityEntity } from './city.entity';

@Entity('bus_stops')
export class BusStopEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ name: 'city_id', type: 'uuid' })
  cityId: string;
  @Column({ type: 'text', nullable: true })
  description: string;
  @Column({ name: 'is_active', default: true })
  isActive: boolean;
  @Column({ type: 'decimal' })
  lat: number;
  @Column({ type: 'decimal' })
  lng: number;
  @ManyToOne(() => CityEntity, (city) => city.busStops, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'city_id' })
  city: CityEntity;
  @OneToMany(() => StationEntity, (station) => station.busStop, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  stations: StationEntity[];

  @OneToMany(() => BookingEntity, (booking) => booking.pickup, {
    onDelete: 'CASCADE',
  })
  pickups: BookingEntity[];

  @OneToMany(() => BookingEntity, (booking) => booking.destination, {
    onDelete: 'CASCADE',
  })
  destinations: BookingEntity[];
}
