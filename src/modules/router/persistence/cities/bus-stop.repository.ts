import { BusStopEntity } from '@router/persistence/cities/bus-stop.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IBusStopRepository } from '@router/domains/cities/bus-stop.repository.interface';
import { BusStop } from '@router/domains/cities/bus-stop';
@Injectable()
export class BusStopRepository implements IBusStopRepository {
  constructor(
    @InjectRepository(BusStopEntity)
    private busStopRepository: Repository<BusStopEntity>,
  ) {}
  async insert(busStop: BusStop): Promise<BusStop> {
    const busStopEntity = this.toBusStopEntity(busStop);
    // console.log(busStopEntity);
    const result = await this.busStopRepository.save(busStopEntity);
    return result ? this.toBusStop(result) : null;
  }
  async update(busStop: BusStop): Promise<BusStop> {
    const busStopEntity = this.toBusStopEntity(busStop);
    const result = await this.busStopRepository.save(busStopEntity);
    return result ? this.toBusStop(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.busStopRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<BusStop[]> {
    const cities = await this.busStopRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!cities.length) {
      return null;
    }
    return cities.map((busStop) => this.toBusStop(busStop));
  }
  async getById(id: string, withDeleted = false): Promise<BusStop> {
    const busStop = await this.busStopRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!busStop[0]) {
      return null;
    }
    return this.toBusStop(busStop[0]);
  }
  async getByCityId(cityId: string, withDeleted = false): Promise<BusStop> {
    const busStop = await this.busStopRepository.find({
      where: { cityId: cityId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!busStop[0]) {
      return null;
    }
    return this.toBusStop(busStop[0]);
  }
  async getAllByCityId(
    cityId: string,
    withDeleted: boolean,
  ): Promise<BusStop[]> {
    const cities = await this.busStopRepository.find({
      where: { cityId: cityId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!cities.length) {
      return null;
    }
    return cities.map((busStop) => this.toBusStop(busStop));
  }
  async getByName(name: string, withDeleted = false): Promise<BusStop> {
    const busStop = await this.busStopRepository.find({
      where: { name: name },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!busStop[0]) {
      return null;
    }
    return this.toBusStop(busStop[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.busStopRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.busStopRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toBusStop(busStopEntity: BusStopEntity): BusStop {
    const busStop = new BusStop();
    busStop.id = busStopEntity.id;
    busStop.name = busStopEntity.name;
    busStop.description = busStopEntity.description;
    busStop.cityId = busStopEntity.cityId;
    busStop.lat = busStopEntity.lat;
    busStop.lng = busStopEntity.lng;
    busStop.isActive = busStopEntity.isActive;
    busStop.createdBy = busStopEntity.createdBy;
    busStop.updatedBy = busStopEntity.updatedBy;
    busStop.deletedBy = busStopEntity.deletedBy;
    busStop.createdAt = busStopEntity.createdAt;
    busStop.updatedAt = busStopEntity.updatedAt;
    busStop.deletedAt = busStopEntity.deletedAt;
    busStop.archiveReason = busStopEntity.archiveReason;
    return busStop;
  }
  toBusStopEntity(busStop: BusStop): BusStopEntity {
    const busStopEntity = new BusStopEntity();
    busStopEntity.id = busStop.id;
    busStopEntity.name = busStop.name;
    busStopEntity.description = busStop.description;
    busStopEntity.cityId = busStop.cityId;
    busStopEntity.lat = busStop.lat;
    busStopEntity.lng = busStop.lng;
    busStopEntity.isActive = busStop.isActive;
    busStopEntity.createdBy = busStop.createdBy;
    busStopEntity.updatedBy = busStop.updatedBy;
    busStopEntity.deletedBy = busStop.deletedBy;
    busStopEntity.createdAt = busStop.createdAt;
    busStopEntity.updatedAt = busStop.updatedAt;
    busStopEntity.deletedAt = busStop.deletedAt;
    busStopEntity.archiveReason = busStop.archiveReason;
    return busStopEntity;
  }
}
