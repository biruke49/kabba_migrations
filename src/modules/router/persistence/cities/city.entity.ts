import { CommonEntity } from '@libs/common/common.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { RouteEntity } from '../routes/route.entity';
import { BusStopEntity } from './bus-stop.entity';

@Entity('cities')
export class CityEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ unique: true })
  name: string;
  @Column({ name: 'is_active', default: false })
  isActive: boolean;
  @OneToMany(() => BusStopEntity, (busStop) => busStop.city, {
    onDelete: 'CASCADE',
  })
  busStops: BusStopEntity[];
  @OneToMany(() => RouteEntity, (route) => route.city, {
    onDelete: 'CASCADE',
  })
  routes: RouteEntity[];
}
