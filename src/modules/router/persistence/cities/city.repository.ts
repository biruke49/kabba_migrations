import { CityEntity } from '@router/persistence/cities/city.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ICityRepository } from '@router/domains/cities/city.repository.interface';
import { City } from '@router/domains/cities/city';
@Injectable()
export class CityRepository implements ICityRepository {
  constructor(
    @InjectRepository(CityEntity)
    private cityRepository: Repository<CityEntity>,
  ) {}
  async insert(city: City): Promise<City> {
    const cityEntity = this.toCityEntity(city);
    // console.log(cityEntity);
    const result = await this.cityRepository.save(cityEntity);
    return result ? this.toCity(result) : null;
  }
  async update(city: City): Promise<City> {
    const cityEntity = this.toCityEntity(city);
    const result = await this.cityRepository.save(cityEntity);
    return result ? this.toCity(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.cityRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<City[]> {
    const cities = await this.cityRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!cities.length) {
      return null;
    }
    return cities.map((city) => this.toCity(city));
  }
  async getById(id: string, withDeleted = false): Promise<City> {
    const city = await this.cityRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!city[0]) {
      return null;
    }
    return this.toCity(city[0]);
  }
  async getByName(name: string, withDeleted = false): Promise<City> {
    const city = await this.cityRepository.find({
      where: { name: name },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!city[0]) {
      return null;
    }
    return this.toCity(city[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.cityRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.cityRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toCity(cityEntity: CityEntity): City {
    const city = new City();
    city.id = cityEntity.id;
    city.name = cityEntity.name;
    city.isActive = cityEntity.isActive;
    city.createdBy = cityEntity.createdBy;
    city.updatedBy = cityEntity.updatedBy;
    city.deletedBy = cityEntity.deletedBy;
    city.createdAt = cityEntity.createdAt;
    city.updatedAt = cityEntity.updatedAt;
    city.deletedAt = cityEntity.deletedAt;
    city.archiveReason = cityEntity.archiveReason;
    return city;
  }
  toCityEntity(city: City): CityEntity {
    const cityEntity = new CityEntity();
    cityEntity.id = city.id;
    cityEntity.name = city.name;
    cityEntity.isActive = city.isActive;
    cityEntity.createdBy = city.createdBy;
    cityEntity.updatedBy = city.updatedBy;
    cityEntity.deletedBy = city.deletedBy;
    cityEntity.createdAt = city.createdAt;
    cityEntity.updatedAt = city.updatedAt;
    cityEntity.deletedAt = city.deletedAt;
    cityEntity.archiveReason = city.archiveReason;
    return cityEntity;
  }
}
