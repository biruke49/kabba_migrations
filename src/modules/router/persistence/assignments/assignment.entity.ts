import { CommonEntity } from '@libs/common/common.entity';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { CancellationReason } from '@router/domains/assignments/cancelled-reason';
import {
  AssignmentStatus,
  DriverPaymentStatus,
} from '@router/domains/assignments/constants';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { RouteEntity } from '../routes/route.entity';

@Entity('assignments')
export class AssignmentEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'driver_id', type: 'uuid' })
  driverId: string;
  @Column({ name: 'route_id', type: 'uuid' })
  routeId: string;
  @Column({ name: 'vehicle_id', type: 'uuid', nullable: true })
  vehicleId: string;
  @Column({ name: 'vehicle_plate_number', nullable: true })
  vehiclePlateNumber: string;
  @Column({ name: 'vehicle_category_id', type: 'uuid', nullable: true })
  vehicleCategoryId: string;
  @Column({ name: 'vehicle_category_name', nullable: true })
  vehicleCategoryName: string;
  @Column({ default: AssignmentStatus.ASSIGNED })
  status: string;
  @Column({ name: 'payment_status', default: DriverPaymentStatus.UNPAID })
  paymentStatus: string;
  @Column({ default: 0, type: 'double precision' })
  fee: number;
  @Column({ name: 'company_earning', default: 0, type: 'double precision' })
  companyEarning: number;
  @Column({ name: 'assignment_date', type: 'date' })
  assignmentDate: Date;
  @Column({ type: 'text', nullable: true })
  remark: string;
  @Column({ type: 'jsonb', nullable: true, name: 'cancellation_reason' })
  cancellationReason: CancellationReason;
  @Column({ name: 'pickup_time', type: 'timetz' })
  pickupTime: string;
  @Column({ name: 'available_seats', type: 'int', default: 0 })
  availableSeats: number;
  @Column({ name: 'vehicle_model', nullable: true })
  vehicleModel: string;
  @Column({ name: 'route_name', nullable: true })
  routeName: string;
  @Column({ name: 'driver_name', nullable: true })
  driverName: string;
  @Column({ name: 'driver_phone', nullable: true })
  driverPhone: string;
  @Column({ name: 'starting_at', type: 'timestamptz', nullable: true })
  startingAt: Date;
  @Column({ name: 'completed_at', type: 'timestamptz', nullable: true })
  completedAt: Date;
  @Column({ name: 'paid_at', type: 'timestamptz', nullable: true })
  paidAt: Date;
  @Column({ name: 'is_paid', default: false })
  isPaid: boolean;
  @ManyToOne(() => RouteEntity, (route) => route.assignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'route_id' })
  route: RouteEntity;

  @ManyToOne(() => DriverEntity, (driver) => driver.assignments, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @OneToMany(() => BookingEntity, (booking) => booking.assignment, {
    onDelete: 'CASCADE',
  })
  bookings: BookingEntity[];
}
