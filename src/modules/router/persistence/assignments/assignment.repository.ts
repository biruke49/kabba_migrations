import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Assignment } from '@router/domains/assignments/assignment';
import { IAssignmentRepository } from '@router/domains/assignments/assignment.repository.interface';
import { Repository } from 'typeorm';
import { AssignmentEntity } from './assignment.entity';
import { AssignmentStatus } from '@router/domains/assignments/constants';

@Injectable()
export class AssignmentRepository implements IAssignmentRepository {
  constructor(
    @InjectRepository(AssignmentEntity)
    private assignmentRepository: Repository<AssignmentEntity>,
  ) {}
  async insert(assignment: Assignment): Promise<Assignment> {
    const assignmentEntity = this.toAssignmentEntity(assignment);
    // console.log(assignmentEntity);
    const results = await this.assignmentRepository.save(assignmentEntity);
    return results ? this.toAssignment(results) : null;
  }
  async insertMany(assignments: Assignment[]): Promise<Assignment[]> {
    const assignmentEntities = assignments.map((assignment) => {
      return this.toAssignmentEntity(assignment);
    });
    const results = await this.assignmentRepository.save(assignmentEntities);
    return results
      ? results.map((result) => {
          return this.toAssignment(result);
        })
      : null;
  }
  async update(assignment: Assignment): Promise<Assignment> {
    const assignmentEntity = this.toAssignmentEntity(assignment);
    const result = await this.assignmentRepository.save(assignmentEntity);
    return result ? this.toAssignment(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.assignmentRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Assignment[]> {
    const assignments = await this.assignmentRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!assignments.length) {
      return null;
    }
    return assignments.map((assignment) => this.toAssignment(assignment));
  }
  async getById(id: string, withDeleted = false): Promise<Assignment> {
    const assignment = await this.assignmentRepository.find({
      where: { id: id },
      relations: ['driver'],
      withDeleted: withDeleted,
    });
    if (!assignment[0]) {
      return null;
    }
    return this.toAssignment(assignment[0]);
  }
  async getByRouteId(
    routeId: string,
    withDeleted = false,
  ): Promise<Assignment> {
    const assignment = await this.assignmentRepository.find({
      where: { routeId: routeId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!assignment[0]) {
      return null;
    }
    return this.toAssignment(assignment[0]);
  }
  async getActiveAssignmentsByDriverId(
    driverId: string,
    withDeleted = false,
  ): Promise<Assignment> {
    const assignment = await this.assignmentRepository.find({
      where: [
        { driverId: driverId, status: AssignmentStatus.ASSIGNED },
        { driverId: driverId, status: AssignmentStatus.STARTED },
      ],
      relations: [],
      withDeleted: withDeleted,
    });
    if (!assignment[0]) {
      return null;
    }
    return this.toAssignment(assignment[0]);
  }
  async getByAssignedRouteId(
    routeId: string,
    withDeleted = false,
  ): Promise<Assignment> {
    const assignment = await this.assignmentRepository.find({
      where: { routeId: routeId, status: AssignmentStatus.ASSIGNED },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!assignment[0]) {
      return null;
    }
    return this.toAssignment(assignment[0]);
  }
  async getByStartedRouteId(
    routeId: string,
    withDeleted = false,
  ): Promise<Assignment> {
    const assignment = await this.assignmentRepository.find({
      where: { routeId: routeId, status: AssignmentStatus.STARTED },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!assignment[0]) {
      return null;
    }
    return this.toAssignment(assignment[0]);
  }
  async getByAssignedDate(
    date: Date,
    withDeleted = false,
  ): Promise<Assignment> {
    const assignment = await this.assignmentRepository.find({
      where: { assignmentDate: date },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!assignment[0]) {
      return null;
    }
    return this.toAssignment(assignment[0]);
  }
  async checkIfDriverIsAssigned(
    driverId: string,
    withDeleted = false,
  ): Promise<boolean> {
    const driverAssignment = await this.assignmentRepository
      .createQueryBuilder('assignments')
      .where(
        '(assignments.driverId = :driverId AND assignments.status = :status)',
      )
      .orWhere(
        '(assignments.driverId = :driverId AND assignments.status = :statuss)',
      )
      .setParameters({
        driverId: driverId,
        status: AssignmentStatus.ASSIGNED,
        statuss: AssignmentStatus.STARTED,
        deleted: withDeleted,
      })
      .getOne();
    if (!driverAssignment) {
      return false;
    }
    return true;
  }
  async getActiveDriverAssignmentsByAssignedDateAndPickupTime(
    driverId: string,
    assignmentDate: Date,
    pickupTime: string,
    status: string,
  ): Promise<Assignment> {
    const assignment = await this.assignmentRepository.find({
      where: {
        assignmentDate: assignmentDate,
        driverId: driverId,
        pickupTime: pickupTime,
        status: status,
      },

      take: 1,
    });
    if (!assignment[0]) {
      return null;
    }
    return this.toAssignment(assignment[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.assignmentRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.assignmentRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toAssignment(assignmentEntity: AssignmentEntity): Assignment {
    const assignment = new Assignment();
    assignment.id = assignmentEntity.id;
    assignment.routeId = assignmentEntity.routeId;
    assignment.driverId = assignmentEntity.driverId;
    assignment.vehicleId = assignmentEntity.vehicleId;
    assignment.vehiclePlateNumber = assignmentEntity.vehiclePlateNumber;
    assignment.vehicleCategoryId = assignmentEntity.vehicleCategoryId;
    assignment.vehicleCategoryName = assignmentEntity.vehicleCategoryName;
    assignment.fee = assignmentEntity.fee;
    assignment.companyEarning = assignmentEntity.companyEarning;
    assignment.status = assignmentEntity.status;
    assignment.paymentStatus = assignmentEntity.paymentStatus;
    assignment.remark = assignmentEntity.remark;
    assignment.cancellationReason = assignmentEntity.cancellationReason;
    assignment.pickupTime = assignmentEntity.pickupTime;
    assignment.startingAt = assignmentEntity.startingAt;
    assignment.completedAt = assignmentEntity.completedAt;
    assignment.assignmentDate = assignmentEntity.assignmentDate;
    assignment.paidAt = assignmentEntity.paidAt;
    assignment.isPaid = assignmentEntity.isPaid;
    assignment.availableSeats = assignmentEntity.availableSeats;
    assignment.vehicleModel = assignmentEntity.vehicleModel;
    assignment.routeName = assignmentEntity.routeName;
    assignment.driverName = assignmentEntity.driverName;
    assignment.driverPhone = assignmentEntity.driverPhone;
    assignment.createdBy = assignmentEntity.createdBy;
    assignment.updatedBy = assignmentEntity.updatedBy;
    assignment.deletedBy = assignmentEntity.deletedBy;
    assignment.createdAt = assignmentEntity.createdAt;
    assignment.updatedAt = assignmentEntity.updatedAt;
    assignment.deletedAt = assignmentEntity.deletedAt;
    return assignment;
  }
  toAssignmentEntity(assignment: Assignment): AssignmentEntity {
    const assignmentEntity = new AssignmentEntity();
    assignmentEntity.id = assignment.id;
    assignmentEntity.routeId = assignment.routeId;
    assignmentEntity.driverId = assignment.driverId;
    assignmentEntity.vehicleId = assignment.vehicleId;
    assignmentEntity.vehiclePlateNumber = assignment.vehiclePlateNumber;
    assignmentEntity.vehicleCategoryId = assignment.vehicleCategoryId;
    assignmentEntity.vehicleCategoryName = assignment.vehicleCategoryName;
    assignmentEntity.fee = assignment.fee;
    assignmentEntity.companyEarning = assignment.companyEarning;
    assignmentEntity.status = assignment.status;
    assignmentEntity.paymentStatus = assignment.paymentStatus;
    assignmentEntity.remark = assignment.remark;
    assignmentEntity.cancellationReason = assignment.cancellationReason;
    assignmentEntity.assignmentDate = assignment.assignmentDate;
    assignmentEntity.pickupTime = assignment.pickupTime;
    assignmentEntity.startingAt = assignment.startingAt;
    assignmentEntity.completedAt = assignment.completedAt;
    assignmentEntity.assignmentDate = assignment.assignmentDate;
    assignmentEntity.paidAt = assignment.paidAt;
    assignmentEntity.isPaid = assignment.isPaid;
    assignmentEntity.availableSeats = assignment.availableSeats;
    assignmentEntity.vehicleModel = assignment.vehicleModel;
    assignmentEntity.routeName = assignment.routeName;
    assignmentEntity.driverName = assignment.driverName;
    assignmentEntity.driverPhone = assignment.driverPhone;
    assignmentEntity.createdBy = assignment.createdBy;
    assignmentEntity.updatedBy = assignment.updatedBy;
    assignmentEntity.deletedBy = assignment.deletedBy;
    assignmentEntity.createdAt = assignment.createdAt;
    assignmentEntity.updatedAt = assignment.updatedAt;
    assignmentEntity.deletedAt = assignment.deletedAt;
    return assignmentEntity;
  }
}
