import { FavoriteEntity } from '@interaction/persistence/favorites/favorite.entity';
import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AssignmentEntity } from '../assignments/assignment.entity';
import { BusStopEntity } from '../cities/bus-stop.entity';
import { CityEntity } from '../cities/city.entity';
import { RoutePriceEntity } from './route-price.entity';
import { StationEntity } from './station.entity';

@Entity('routes')
export class RouteEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ type: 'text', nullable: true })
  description: string;
  @Column({ name: 'city_id', type: 'uuid' })
  cityId: string;
  @Column({ name: 'is_active', default: false })
  isActive: boolean;
  @Column({ name: 'difficulty_level' })
  difficultyLevel: string;
  @Column({ name: 'traffic_jam_level' })
  trafficJamLevel: string;
  @Column({ name: 'total_distance', default: 0, type: 'double precision' })
  totalDistance: number;
  @Column({ name: 'number_of_views', type: 'int', default: 0 })
  numberOfViews: number;
  @ManyToOne(() => CityEntity, (city) => city.routes, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'city_id' })
  city: CityEntity;
  @OneToMany(() => StationEntity, (station) => station.route, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  stations: StationEntity[];
  @OneToMany(() => RoutePriceEntity, (price) => price.route, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  prices: RoutePriceEntity[];

  @OneToMany(() => RoutePriceEntity, (assignment) => assignment.route, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  assignments: AssignmentEntity[];
  @OneToMany(() => BookingEntity, (booking) => booking.route, {
    onDelete: 'CASCADE',
  })
  bookings: BookingEntity[];
  @OneToMany(() => FavoriteEntity, (favorite) => favorite.route, {
    onDelete: 'CASCADE',
  })
  favorites: FavoriteEntity[];
  @OneToMany(() => PreferenceEntity, (preference) => preference.route, {
    onDelete: 'CASCADE',
  })
  preferences: PreferenceEntity[];
}
