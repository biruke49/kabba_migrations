import { CommonEntity } from '@libs/common/common.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BusStopEntity } from '../cities/bus-stop.entity';
import { RouteEntity } from './route.entity';

@Entity('stations')
export class StationEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'route_id', type: 'uuid' })
  routeId: string;
  @Column({ name: 'bus_stop_id', type: 'uuid' })
  busStopId: string;
  @Column({ name: 'is_pickup', default: false })
  isPickup: boolean;
  @Column({ name: 'is_destination', default: false })
  isDestination: boolean;
  @Column({ type: 'int', default: 1 })
  position: number;
  @Column({ name: 'next_bus_stop_id', nullable: true })
  nextBusStopId: string;
  @Column({
    name: 'distance_to_next_bus_stop',
    nullable: true,
    type: 'double precision',
  })
  distanceToNextBusStop: number;
  @ManyToOne(() => RouteEntity, (route) => route.stations, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'route_id' })
  route: RouteEntity;
  @ManyToOne(() => BusStopEntity, (busStop) => busStop.stations, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'bus_stop_id' })
  busStop: BusStopEntity;
}
