import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { CommonEntity } from '@libs/common/common.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { RouteEntity } from './route.entity';

@Entity('route_prices')
export class RoutePriceEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'route_id', type: 'uuid' })
  routeId: string;
  @Column({ name: 'category_id', type: 'uuid' })
  categoryId: string;
  @Column({ name: 'calculated_fee', default: 1, type: 'double precision' })
  calculatedFee: number;
  @Column({ name: 'fee', default: 1, type: 'double precision' })
  fee: number;
  @Column({
    name: 'default_driver_commission',
    default: 0,
    type: 'double precision',
  })
  defaultDriverCommission: number;
  @Column({ name: 'driver_commission', default: 1, type: 'double precision' })
  driverCommission: number;
  @ManyToOne(() => RouteEntity, (route) => route.prices, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'route_id' })
  route: RouteEntity;
  @ManyToOne(() => CategoryEntity, (category) => category.routePrices, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'category_id' })
  category: CategoryEntity;
}
