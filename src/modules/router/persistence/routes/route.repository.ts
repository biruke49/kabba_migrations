import { RouteEntity } from '@router/persistence/routes/route.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IRouteRepository } from '@router/domains/routes/route.repository.interface';
import { Route } from '@router/domains/routes/route';
import { Station } from '@router/domains/routes/station';
import { StationEntity } from './station.entity';
import { RoutePriceEntity } from './route-price.entity';
import { RoutePrice } from '@router/domains/routes/route-price';
@Injectable()
export class RouteRepository implements IRouteRepository {
  constructor(
    @InjectRepository(RouteEntity)
    private routeRepository: Repository<RouteEntity>,
    @InjectRepository(StationEntity)
    private stationRepository: Repository<StationEntity>,
  ) {}
  async insert(route: Route): Promise<Route> {
    const routeEntity = this.toRouteEntity(route);
    const result = await this.routeRepository.save(routeEntity);
    return result ? this.toRoute(result) : null;
  }
  async update(route: Route): Promise<Route> {
    const routeEntity = this.toRouteEntity(route);
    const result = await this.routeRepository.save(routeEntity);
    return result ? this.toRoute(result) : null;
  }
  async updateMany(routes: Route[]): Promise<Route[]> {
    const routeEntities = routes.map((assignment) => {
      return this.toRouteEntity(assignment);
    });
    const results = await this.routeRepository.save(routeEntities);
    return results
      ? results.map((result) => {
          return this.toRoute(result);
        })
      : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.routeRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted = false): Promise<Route[]> {
    const routes = await this.routeRepository.find({
      relations: ['stations', 'prices'],
      withDeleted: withDeleted,
    });
    if (!routes.length) {
      return null;
    }
    return routes.map((route) => this.toRoute(route));
  }
  async getById(id: string, withDeleted = false): Promise<Route> {
    const route = await this.routeRepository.find({
      where: { id: id },
      relations: ['stations', 'prices'],
      withDeleted: withDeleted,
    });
    if (!route[0]) {
      return null;
    }
    return this.toRoute(route[0]);
  }
  async getByCityIdAndName(
    cityId: string,
    name: string,
    withDeleted = false,
  ): Promise<Route> {
    const route = await this.routeRepository.find({
      where: { cityId: cityId, name: name },
      //relations: ['stations', 'prices'],
      withDeleted: withDeleted,
    });
    if (!route[0]) {
      return null;
    }
    return this.toRoute(route[0]);
  }
  async getByBusStopId(id: string, withDeleted = false): Promise<Station> {
    const route = await this.stationRepository.find({
      where: { busStopId: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!route[0]) {
      return null;
    }
    return this.toStation(route[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.routeRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.routeRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toRoute(routeEntity: RouteEntity): Route {
    const route = new Route();
    route.id = routeEntity.id;
    route.name = routeEntity.name;
    route.cityId = routeEntity.cityId;
    route.description = routeEntity.description;
    route.difficultyLevel = routeEntity.difficultyLevel;
    route.trafficJamLevel = routeEntity.trafficJamLevel;
    route.isActive = routeEntity.isActive;
    route.totalDistance = routeEntity.totalDistance;
    route.numberOfViews = routeEntity.numberOfViews;
    route.createdBy = routeEntity.createdBy;
    route.updatedBy = routeEntity.updatedBy;
    route.deletedBy = routeEntity.deletedBy;
    route.createdAt = routeEntity.createdAt;
    route.updatedAt = routeEntity.updatedAt;
    route.deletedAt = routeEntity.deletedAt;
    route.archiveReason = routeEntity.archiveReason;
    route.stations = routeEntity.stations
      ? routeEntity.stations.map((station) => this.toStation(station))
      : [];
    route.prices = routeEntity.prices
      ? routeEntity.prices.map((price) => this.toPrice(price))
      : [];
    return route;
  }
  toRouteEntity(route: Route): RouteEntity {
    const routeEntity = new RouteEntity();
    routeEntity.id = route.id;
    routeEntity.name = route.name;
    routeEntity.cityId = route.cityId;
    routeEntity.description = route.description;
    routeEntity.difficultyLevel = route.difficultyLevel;
    routeEntity.trafficJamLevel = route.trafficJamLevel;
    routeEntity.isActive = route.isActive;
    routeEntity.totalDistance = route.totalDistance;
    routeEntity.numberOfViews = route.numberOfViews;
    routeEntity.createdBy = route.createdBy;
    routeEntity.updatedBy = route.updatedBy;
    routeEntity.deletedBy = route.deletedBy;
    routeEntity.createdAt = route.createdAt;
    routeEntity.updatedAt = route.updatedAt;
    routeEntity.deletedAt = route.deletedAt;
    routeEntity.archiveReason = route.archiveReason;
    routeEntity.stations = route.stations
      ? route.stations.map((station) => this.toStationEntity(station))
      : [];
    routeEntity.prices = route.prices
      ? route.prices.map((price) => this.toPriceEntity(price))
      : [];
    return routeEntity;
  }
  toStation(stationEntity: StationEntity): Station {
    const station = new Station();
    station.id = stationEntity.id;
    station.routeId = stationEntity.routeId;
    station.busStopId = stationEntity.busStopId;
    station.isPickup = stationEntity.isPickup;
    station.isDestination = stationEntity.isDestination;
    station.position = stationEntity.position;
    station.distanceToNextBusStop = stationEntity.distanceToNextBusStop;
    station.nextBusStopId = stationEntity.nextBusStopId;
    station.createdBy = stationEntity.createdBy;
    station.updatedBy = stationEntity.updatedBy;
    station.deletedBy = stationEntity.deletedBy;
    station.createdAt = stationEntity.createdAt;
    station.updatedAt = stationEntity.updatedAt;
    station.deletedAt = stationEntity.deletedAt;
    return station;
  }
  toStationEntity(station: Station): StationEntity {
    const stationEntity = new StationEntity();
    stationEntity.id = station.id;
    stationEntity.routeId = station.routeId;
    stationEntity.busStopId = station.busStopId;
    stationEntity.isDestination = station.isDestination;
    stationEntity.isPickup = station.isPickup;
    stationEntity.distanceToNextBusStop = station.distanceToNextBusStop;
    stationEntity.nextBusStopId = station.nextBusStopId;
    stationEntity.position = station.position;
    stationEntity.createdBy = station.createdBy;
    stationEntity.updatedBy = station.updatedBy;
    stationEntity.deletedBy = station.deletedBy;
    stationEntity.createdAt = station.createdAt;
    stationEntity.updatedAt = station.updatedAt;
    stationEntity.deletedAt = station.deletedAt;
    return stationEntity;
  }

  toPrice(routePriceEntity: RoutePriceEntity): RoutePrice {
    const routePrice = new RoutePrice();
    routePrice.id = routePriceEntity.id;
    routePrice.routeId = routePriceEntity.routeId;
    routePrice.categoryId = routePriceEntity.categoryId;
    routePrice.calculatedFee = routePriceEntity.calculatedFee;
    routePrice.fee = routePriceEntity.fee;
    routePrice.driverCommission = routePriceEntity.driverCommission;
    routePrice.defaultDriverCommission =
      routePriceEntity.defaultDriverCommission;
    routePrice.createdBy = routePriceEntity.createdBy;
    routePrice.updatedBy = routePriceEntity.updatedBy;
    routePrice.deletedBy = routePriceEntity.deletedBy;
    routePrice.createdAt = routePriceEntity.createdAt;
    routePrice.updatedAt = routePriceEntity.updatedAt;
    routePrice.deletedAt = routePriceEntity.deletedAt;
    return routePrice;
  }
  toPriceEntity(routePrice: RoutePrice): RoutePriceEntity {
    const routePriceEntity = new RoutePriceEntity();
    routePriceEntity.id = routePrice.id;
    routePriceEntity.routeId = routePrice.routeId;
    routePriceEntity.categoryId = routePrice.categoryId;
    routePriceEntity.calculatedFee = routePrice.calculatedFee;
    routePriceEntity.fee = routePrice.fee;
    routePriceEntity.driverCommission = routePrice.driverCommission;
    routePriceEntity.defaultDriverCommission =
      routePrice.defaultDriverCommission;
    routePriceEntity.createdBy = routePrice.createdBy;
    routePriceEntity.updatedBy = routePrice.updatedBy;
    routePriceEntity.deletedBy = routePrice.deletedBy;
    routePriceEntity.createdAt = routePrice.createdAt;
    routePriceEntity.updatedAt = routePrice.updatedAt;
    routePriceEntity.deletedAt = routePrice.deletedAt;
    return routePriceEntity;
  }
}
