import { News } from './news';

export interface INewsRepository {
  insert(news: News): Promise<News>;
  update(user: News): Promise<News>;
  delete(id: string): Promise<boolean>;
  getAllNews(withDeleted: boolean): Promise<News[]>;
  getNewsById(id: string, withDeleted: boolean): Promise<News>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
