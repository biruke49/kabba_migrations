import { diskStorage } from 'multer';

import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ArchiveNewsCommand,
  CreateNewsCommand,
  UpdateNewsCommand,
} from '@publication/usecases/news/news.commands';
import { NewsResponse } from '@publication/usecases/news/news.response';
import { NewsCommands } from '@publication/usecases/news/news.usecase.commands';
import { NewsQuery } from '@publication/usecases/news/news.usecase.queries';

@Controller('news')
@ApiTags('news')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class NewsController {
  constructor(
    private command: NewsCommands,
    private queries: NewsQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-news/:id')
  @ApiOkResponse({ type: NewsResponse })
  async getNewsById(@Param('id') id: string) {
    return this.queries.getNewsById(id);
  }
  @Get('get-archived-news/:id')
  @ApiOkResponse({ type: NewsResponse })
  async getArchivedNewsById(@Param('id') id: string) {
    return this.queries.getArchivedNewsById(id, true);
  }
  @Get('get-news')
  @AllowAnonymous()
  @ApiPaginatedResponse(NewsResponse)
  async getNews(@Query() query: CollectionQuery) {
    return this.queries.getAllNews(query);
  }
  @Post('create-news')
  @UseGuards(PermissionsGuard('manage-news'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('coverImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: NewsResponse })
  async createNews(
    @CurrentUser() user: UserInfo,
    @Body() createNewsCommand: CreateNewsCommand,
    @UploadedFile() coverImage: Express.Multer.File,
  ) {
    if (coverImage) {
      const result = await this.fileManagerService.uploadFile(
        coverImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        createNewsCommand.coverImage = result;
      }
    }
    createNewsCommand.currentUser = user;
    return this.command.createNews(createNewsCommand);
  }
  @Put('update-news')
  @UseGuards(PermissionsGuard('manage-news'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('coverImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (file && !file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: NewsResponse })
  async updateNews(
    @CurrentUser() user: UserInfo,
    @Body() updateNewsCommand: UpdateNewsCommand,
    @UploadedFile() coverImage: Express.Multer.File,
  ) {
    if (coverImage) {
      const result = await this.fileManagerService.uploadFile(
        coverImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        updateNewsCommand.coverImage = result;
      }
    }
    updateNewsCommand.currentUser = user;
    return this.command.updateNews(updateNewsCommand);
  }
  @Delete('delete-news/:id')
  @UseGuards(PermissionsGuard('manage-news'))
  @ApiOkResponse({ type: Boolean })
  async deleteNews(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteNews(id, user);
  }
  @Delete('archive-news')
  @UseGuards(PermissionsGuard('manage-news'))
  @ApiOkResponse({ type: NewsResponse })
  async archiveNews(
    @CurrentUser() user: UserInfo,
    @Body() command: ArchiveNewsCommand,
  ) {
    command.currentUser = user;
    return this.command.archiveNews(command);
  }
  @Post('restore-news/:id')
  @UseGuards(PermissionsGuard('manage-news'))
  @ApiOkResponse({ type: NewsResponse })
  async restoreNews(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreNews(id, user);
  }
  @Get('get-archived-news')
  @ApiPaginatedResponse(NewsResponse)
  async getArchivedFeedbacks(@Query() query: CollectionQuery) {
    return this.queries.getArchivedNews(query);
  }
}
