import { CategoryStatus } from '@libs/common/enums';
import { Level } from './level';

export class Category {
  id: string;
  name: string;
  capacity: number;
  description: string;
  perKilometerCost: Level;
  perTrafficJamCost: Level;
  initialFee: number;
  kidInitialFee: number;
  driverCommission: number;
  isForKabbaKids: string;
  kidsPerKilometerCost: number;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
