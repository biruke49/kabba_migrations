import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { Level } from '@classification/domains/categories/level';
import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { CategoryStatus } from '@libs/common/enums';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import { RoutePriceEntity } from '@router/persistence/routes/route-price.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('categories')
export class CategoryEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ unique: true })
  name: string;
  @Column({ type: 'integer' })
  capacity: number;
  @Column({ nullable: true })
  description: string;
  @Column({ name: 'per_kilometer_cost', type: 'jsonb' })
  perKilometerCost: Level;
  @Column({ name: 'kids_per_kilometer_cost', nullable: true, default: 0 })
  kidsPerKilometerCost: number;
  @Column({ name: 'per_traffic_jam_cost', type: 'jsonb', nullable: true })
  perTrafficJamCost: Level;
  @Column({ name: 'initial_fee', default: 0 })
  initialFee: number;
  @Column({ name: 'kid_initial_fee', default: 0 })
  kidInitialFee: number;
  @Column({ name: 'driver_commission', type: 'double precision', default: 0.0 })
  driverCommission: number;
  @Column({ name: 'is_for_kabba_kids', default: CategoryStatus.ForKabbaMain })
  isForKabbaKids: string;
  @OneToMany(() => RoutePriceEntity, (routePrice) => routePrice.category, {
    onDelete: 'CASCADE',
  })
  routePrices: RoutePriceEntity[];
  @OneToMany(() => VehicleEntity, (vehicle) => vehicle.category, {
    onDelete: 'SET NULL',
  })
  vehicles: VehicleEntity[];
  @OneToMany(
    () => GroupAssignmentEntity,
    (groupAssignment) => groupAssignment.group,
    {
      cascade: true,
      onDelete: 'CASCADE',
    },
  )
  groupAssignments: GroupAssignmentEntity[];
  @OneToMany(() => KidEntity, (kid) => kid.category, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  kids: KidEntity[];
}
