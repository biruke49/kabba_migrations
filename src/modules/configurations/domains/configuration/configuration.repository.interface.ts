import { Configuration } from './configuration';

export interface IConfigurationRepository {
  insert(user: Configuration): Promise<Configuration>;
  update(user: Configuration): Promise<Configuration>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Configuration[]>;
  getById(id: string, withDeleted: boolean): Promise<Configuration>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
