import { Candidate } from '@job/domains/jobs/candidate';
import { Job } from '@job/domains/jobs/job';
import { IJobRepository } from '@job/domains/jobs/job.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CandidateEntity } from './candidate.entity';
import { JobEntity } from './job.entity';

@Injectable()
export class JobRepository implements IJobRepository {
  constructor(
    @InjectRepository(JobEntity)
    private jobRepository: Repository<JobEntity>,
    @InjectRepository(CandidateEntity)
    private candidateRepository: Repository<CandidateEntity>,
  ) {}

  async insert(job: Job): Promise<Job> {
    const jobEntity = this.toJobEntity(job);
    const result = await this.jobRepository.save(jobEntity);
    return result ? this.toJob(result) : null;
  }
  async update(job: Job): Promise<Job> {
    const jobEntity = this.toJobEntity(job);
    const result = await this.jobRepository.save(jobEntity);
    return result ? this.toJob(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.jobRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Job[]> {
    const jobs = await this.jobRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!jobs.length) {
      return null;
    }
    return jobs.map((user) => this.toJob(user));
  }
  async getById(id: string, withDeleted = false): Promise<Job> {
    const job = await this.jobRepository.find({
      where: { id: id },
      relations: ['candidates'],
      withDeleted: withDeleted,
    });
    if (!job[0]) {
      return null;
    }
    return this.toJob(job[0]);
  }
  async getByGroupId(groupId: string, withDeleted = false): Promise<Job> {
    const job = await this.jobRepository.find({
      where: { groupId: groupId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!job[0]) {
      return null;
    }
    return this.toJob(job[0]);
  }
  async getByCandidateId(
    jobId: string,
    driverId: string,
    withDeleted = false,
  ): Promise<Candidate> {
    const job = await this.candidateRepository.find({
      where: { driverId: driverId, jobId: jobId },
      relations: ['job'],
      withDeleted: withDeleted,
    });
    if (!job[0]) {
      return null;
    }
    return this.toCandidate(job[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.jobRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.jobRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }

  toJob(jobEntity: JobEntity): Job {
    const job = new Job();
    job.id = jobEntity.id;
    job.title = jobEntity.title;
    job.groupId = jobEntity.groupId;
    job.groupName = jobEntity.groupName;
    job.driverId = jobEntity.driverId;
    job.startLocation = jobEntity.startLocation;
    job.description = jobEntity.description;
    job.maxApplicantNumber = jobEntity.maxApplicantNumber;
    job.minimumRating = jobEntity.minimumRating;
    job.routePreference = jobEntity.routePreference;
    job.status = jobEntity.status;
    job.archiveReason = jobEntity.archiveReason;
    job.createdBy = jobEntity.createdBy;
    job.updatedBy = jobEntity.updatedBy;
    job.deletedBy = jobEntity.deletedBy;
    job.createdAt = jobEntity.createdAt;
    job.updatedAt = jobEntity.updatedAt;
    job.deletedAt = jobEntity.deletedAt;
    job.candidates = jobEntity.candidates
      ? jobEntity.candidates.map((candidate) => this.toCandidate(candidate))
      : [];
    return job;
  }
  toJobEntity(job: Job): JobEntity {
    const jobEntity = new JobEntity();
    jobEntity.id = job.id;
    jobEntity.title = job.title;
    jobEntity.groupId = job.groupId;
    jobEntity.groupName = job.groupName;
    jobEntity.driverId = job.driverId;
    jobEntity.startLocation = job.startLocation;
    jobEntity.description = job.description;
    jobEntity.maxApplicantNumber = job.maxApplicantNumber;
    jobEntity.minimumRating = job.minimumRating;
    jobEntity.routePreference = job.routePreference;
    jobEntity.status = job.status;
    jobEntity.archiveReason = job.archiveReason;
    jobEntity.createdBy = job.createdBy;
    jobEntity.updatedBy = job.updatedBy;
    jobEntity.deletedBy = job.deletedBy;
    jobEntity.createdAt = job.createdAt;
    jobEntity.updatedAt = job.updatedAt;
    jobEntity.deletedAt = job.deletedAt;
    jobEntity.candidates = job.candidates
      ? job.candidates.map((candidate) => this.toCandidateEntity(candidate))
      : [];
    return jobEntity;
  }
  toCandidate(candidateEntity: CandidateEntity): Candidate {
    const candidate = new Candidate();
    candidate.id = candidateEntity.id;
    candidate.driverId = candidateEntity.driverId;
    candidate.jobId = candidateEntity.jobId;
    candidate.status = candidateEntity.status;
    candidate.archiveReason = candidateEntity.archiveReason;
    candidate.createdBy = candidateEntity.createdBy;
    candidate.updatedBy = candidateEntity.updatedBy;
    candidate.deletedBy = candidateEntity.deletedBy;
    candidate.createdAt = candidateEntity.createdAt;
    candidate.updatedAt = candidateEntity.updatedAt;
    candidate.deletedAt = candidateEntity.deletedAt;
    return candidate;
  }
  toCandidateEntity(candidate: Candidate): CandidateEntity {
    const candidateEntity = new CandidateEntity();
    candidateEntity.id = candidate.id;
    candidateEntity.driverId = candidate.driverId;
    candidateEntity.jobId = candidate.jobId;
    candidateEntity.status = candidate.status;
    candidateEntity.archiveReason = candidate.archiveReason;
    candidateEntity.createdBy = candidate.createdBy;
    candidateEntity.updatedBy = candidate.updatedBy;
    candidateEntity.deletedBy = candidate.deletedBy;
    candidateEntity.createdAt = candidate.createdAt;
    candidateEntity.updatedAt = candidate.updatedAt;
    candidateEntity.deletedAt = candidate.deletedAt;
    return candidateEntity;
  }
}
