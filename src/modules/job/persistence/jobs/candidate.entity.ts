import { CommonEntity } from '@libs/common/common.entity';
import { CandidateStatus } from '@libs/common/enums';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { JobEntity } from '../jobs/job.entity';

@Entity('candidates')
export class CandidateEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'driver_id', type: 'uuid' })
  driverId: string;
  @Column({ name: 'job_id', type: 'uuid' })
  jobId: string;
  @Column({ default: CandidateStatus.Active })
  status: string;
  @ManyToOne(() => DriverEntity, (driver) => driver.candidates, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @ManyToOne(() => JobEntity, (job) => job.candidates, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'job_id' })
  job: JobEntity;
}
