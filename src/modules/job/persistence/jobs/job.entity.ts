import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { JobStatus } from '@libs/common/enums';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CandidateEntity } from './candidate.entity';
import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';

@Entity('jobs')
export class JobEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  title: string;
  @Column({ name: 'group_id', type: 'uuid' })
  groupId: string;
  @Column({ name: 'group_name', nullable: true })
  groupName: string;
  @Column({ name: 'driver_id', type: 'uuid', nullable: true })
  driverId: string;
  @Column({ name: 'start_location', nullable: true })
  startLocation: string;
  @Column({ name: 'maximum_applicant_number' })
  maxApplicantNumber: number;
  @Column({ name: 'minimum_rating', type: 'double precision', nullable: true })
  minimumRating: number;
  @Column({ name: 'route_preference', nullable: true })
  routePreference: string;
  @Column({ nullable: true })
  description: string;
  @Column({ default: JobStatus.Open })
  status: string;
  @ManyToOne(() => GroupEntity, (group) => group.jobs, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'group_id' })
  group: GroupEntity;
  @OneToMany(() => CandidateEntity, (candidate) => candidate.job, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  candidates: CandidateEntity[];
  @OneToMany(
    () => DriverAssignmentEntity,
    (driverAssignment) => driverAssignment.job,
    {
      cascade: ['remove', 'soft-remove', 'recover'],
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  driverAssignments: DriverAssignmentEntity;
}
