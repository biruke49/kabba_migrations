import { Candidate } from '@job/domains/jobs/candidate';
import { CandidateEntity } from '@job/persistence/jobs/candidate.entity';
import { CandidateStatus } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { JobResponse } from './job.response';

export class CandidateResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  jobId: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  status: string;
  job: JobResponse;
  driver: DriverResponse;
  static fromEntity(candidateEntity: CandidateEntity): CandidateResponse {
    const candidateResponse = new CandidateResponse();
    candidateResponse.id = candidateEntity.id;
    candidateResponse.jobId = candidateEntity.jobId;
    candidateResponse.driverId = candidateEntity.driverId;
    candidateResponse.status = candidateEntity.status;
    candidateResponse.status = candidateEntity.status;
    if (candidateEntity.job) {
      candidateResponse.job = JobResponse.fromEntity(candidateEntity.job);
    }
    if (candidateEntity.driver) {
      candidateResponse.driver = DriverResponse.fromEntity(
        candidateEntity.driver,
      );
    }
    return candidateResponse;
  }
  static fromDomain(candidate: Candidate): CandidateResponse {
    const candidateResponse = new CandidateResponse();
    candidateResponse.id = candidate.id;
    candidateResponse.driverId = candidate.driverId;
    candidateResponse.jobId = candidate.jobId;
    candidateResponse.status = candidate.status;
    if (candidate.job) {
      candidateResponse.job = JobResponse.fromDomain(candidate.job);
    }
    if (candidate.driver) {
      candidateResponse.driver = DriverResponse.fromDomain(candidate.driver);
    }
    return candidateResponse;
  }
}
