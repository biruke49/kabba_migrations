import { UserInfo } from '@account/dtos/user-info.dto';
import { Candidate } from '@job/domains/jobs/candidate';
import { CandidateStatus } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateCandidateCommand {
  @ApiProperty()
  @IsNotEmpty()
  jobId: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  currentUser: UserInfo;
  static fromCommand(command: CreateCandidateCommand): Candidate {
    const jonDomain = new Candidate();
    jonDomain.jobId = command.jobId;
    jonDomain.driverId = command.driverId;
    return jonDomain;
  }
}
export class UpdateCandidateCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  jobId: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  status: string;
  currentUser: UserInfo;
}
export class ArchiveCandidateCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  jobId: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class DeleteCandidateCommand {
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  jobId: string;
  currentUser: UserInfo;
}
