import { UserInfo } from '@account/dtos/user-info.dto';
import { JobStatus } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Job } from 'modules/job/domains/jobs/job';

export class CreateJobCommand {
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  groupName: string;
  @ApiProperty()
  @IsNotEmpty()
  startLocation: string;
  @ApiProperty()
  @IsNotEmpty()
  description: string;
  @ApiProperty()
  @IsNotEmpty()
  maxApplicantNumber: number;
  @ApiProperty()
  minimumRating: number;
  @ApiProperty()
  routePreference: string;
  currentUser: UserInfo;
  static fromCommand(command: CreateJobCommand): Job {
    const jonDomain = new Job();
    jonDomain.title = command.title;
    jonDomain.groupId = command.groupId;
    jonDomain.startLocation = command.startLocation;
    jonDomain.description = command.description;
    jonDomain.maxApplicantNumber = command.maxApplicantNumber;
    jonDomain.minimumRating = command.minimumRating;
    jonDomain.routePreference = command.routePreference;
    return jonDomain;
  }
}
export class UpdateJobCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  groupId: string;
  groupName: string;
  @ApiProperty()
  @IsNotEmpty()
  startLocation: string;
  @ApiProperty()
  @IsNotEmpty()
  description: string;
  @ApiProperty()
  @IsNotEmpty()
  maxApplicantNumber: number;
  @ApiProperty()
  minimumRating: number;
  @ApiProperty()
  routePreference: string;
  status: string;
  driverId: string;
  currentUser: UserInfo;
}
export class ChangeJobStatusCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  status: string;
  currentUser: UserInfo;
}
export class ArchiveJobCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class OpenCloseJobCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  currentUser: UserInfo;
}
