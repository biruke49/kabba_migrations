import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { UserInfo } from '@account/dtos/user-info.dto';
import { CandidateEntity } from '@job/persistence/jobs/candidate.entity';
import { JobRepository } from '@job/persistence/jobs/job.repository';
import { CandidateStatus, JobStatus } from '@libs/common/enums';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  ArchiveCandidateCommand,
  CreateCandidateCommand,
  DeleteCandidateCommand,
} from './candidate.commands';
import { CandidateResponse } from './candidate.response';
import {
  ArchiveJobCommand,
  ChangeJobStatusCommand,
  CreateJobCommand,
  OpenCloseJobCommand,
  UpdateJobCommand,
} from './job.commands';
import { JobResponse } from './job.response';
import { DriverAssignmentRepository } from '@assignment/persistence/driver-assignments/driver-assignment.repository';
import { Job } from '@job/domains/jobs/job';
import { GroupRepository } from '@assignment/persistence/groups/group.repository';

@Injectable()
export class JobCommands {
  constructor(
    private jobRepository: JobRepository,
    private groupRepository: GroupRepository,
    private driverAssignmentRepository: DriverAssignmentRepository,
    private readonly eventEmitter: EventEmitter2,
    @InjectRepository(CandidateEntity)
    private candidateRepository: Repository<CandidateEntity>,
  ) {}
  async createJob(command: CreateJobCommand): Promise<JobResponse> {
    const c = await this.jobRepository.getByGroupId(command.groupId);
    if (c) {
      throw new BadRequestException(`Job for this group already exists`);
    }
    const group = await this.groupRepository.getById(command.groupId);
    const jobDomain = CreateJobCommand.fromCommand(command);
    jobDomain.groupName = group.name;
    jobDomain.createdBy = command.currentUser.id;
    jobDomain.updatedBy = command.currentUser.id;
    const job = await this.jobRepository.insert(jobDomain);
    if (job) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: job.id,
        modelName: MODELS.JOB,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
    }
    return JobResponse.fromDomain(job);
  }
  async updateJob(command: UpdateJobCommand): Promise<JobResponse> {
    const jobDomain = await this.jobRepository.getById(command.id);
    if (!jobDomain) {
      throw new NotFoundException(`Job not found with id ${command.id}`);
    }
    const oldPayload = jobDomain;
    jobDomain.title = command.title;
    jobDomain.groupId = command.groupId;
    jobDomain.startLocation = command.startLocation;
    jobDomain.description = command.description;
    jobDomain.maxApplicantNumber = command.maxApplicantNumber;
    jobDomain.minimumRating = command.minimumRating;
    jobDomain.routePreference = command.routePreference;
    jobDomain.status = command.status;
    jobDomain.updatedBy = command.currentUser.id;
    const job = await this.jobRepository.update(jobDomain);
    if (job) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: job.id,
        modelName: MODELS.JOB,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: job,
        oldPayload: oldPayload,
      });
    }
    return JobResponse.fromDomain(job);
  }
  async archiveJob(command: ArchiveJobCommand): Promise<JobResponse> {
    const jobDomain = await this.jobRepository.getById(command.id);
    if (!jobDomain) {
      throw new NotFoundException(`Job not found with id ${command.id}`);
    }
    if (jobDomain.candidates && jobDomain.candidates.length > 0) {
      throw new BadRequestException(
        `Job can not be deleted because it has candidates`,
      );
    }
    jobDomain.deletedAt = new Date();
    jobDomain.deletedBy = command.currentUser.id;
    jobDomain.archiveReason = command.reason;
    const result = await this.jobRepository.update(jobDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.JOB,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
        reason: command.reason,
      });
    }
    return JobResponse.fromDomain(jobDomain);
  }
  async restoreJob(id: string, currentUser: UserInfo): Promise<JobResponse> {
    const jobDomain = await this.jobRepository.getById(id, true);
    if (!jobDomain) {
      throw new NotFoundException(`Job not found with id ${id}`);
    }
    const r = await this.jobRepository.restore(id);
    if (r) {
      jobDomain.deletedAt = null;
      jobDomain.archiveReason = null;
      jobDomain.deletedBy = null;
    }
    const result = await this.jobRepository.update(jobDomain);

    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.JOB,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    return JobResponse.fromDomain(jobDomain);
  }
  async deleteJob(id: string, currentUser: UserInfo): Promise<boolean> {
    const jobDomain = await this.jobRepository.getById(id, true);
    if (!jobDomain) {
      throw new NotFoundException(`Job not found with id ${id}`);
    }
    if (jobDomain.candidates && jobDomain.candidates.length > 0) {
      throw new BadRequestException(
        `Job can not be deleted because it has candidates`,
      );
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.JOB,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    return await this.jobRepository.delete(id);
  }
  @OnEvent('open.or.close.job')
  async openOrCloseJob(command: ChangeJobStatusCommand): Promise<JobResponse> {
    const jobDomain = await this.jobRepository.getById(command.id);
    if (!jobDomain) {
      throw new NotFoundException(`Job not found with id ${command.id}`);
    }
    if (command.status === JobStatus.Open) {
      jobDomain.status = JobStatus.Open;
    } else if (command.status === JobStatus.Close) {
      jobDomain.status = JobStatus.Close;
    } else if (command.status === JobStatus.Assigned) {
      jobDomain.status = JobStatus.Assigned;
    }
    const result = await this.jobRepository.update(jobDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.JOB,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return JobResponse.fromDomain(result);
  }
  //Candidates
  async addCandidate(
    command: CreateCandidateCommand,
  ): Promise<CandidateResponse> {
    const jobDomain = await this.jobRepository.getById(command.jobId);
    if (!jobDomain) {
      throw new NotFoundException(`Job not found with id ${command.jobId}`);
    }
    const existingCandidate = await this.candidateRepository.findOneBy({
      driverId: command.driverId,
      jobId: command.jobId,
    });
    if (existingCandidate) {
      throw new BadRequestException(
        `Candidate has already applied to this job`,
      );
    }
    if (jobDomain.maxApplicantNumber <= 0) {
      throw new BadRequestException(
        `Job has reached maximum number of applicants`,
      );
    }
    if (jobDomain.maxApplicantNumber > 0) {
      const candidate = CreateCandidateCommand.fromCommand(command);
      jobDomain.addCandidate(candidate);
      jobDomain.maxApplicantNumber -= 1;
      if (jobDomain.maxApplicantNumber === 0) {
        jobDomain.status = JobStatus.Close;
      }
      const result = await this.jobRepository.update(jobDomain);
      if (!result) return null;

      const candidateResponse = CandidateResponse.fromDomain(
        result.candidates[result.candidates.length - 1],
      );
      this.eventEmitter.emit('activity-logger.store', {
        modelId: candidateResponse.id,
        modelName: MODELS.CANDIDATE,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
      return candidateResponse;
    }
  }
  async deleteCandidate(command: DeleteCandidateCommand): Promise<boolean> {
    const candidate = await this.candidateRepository.find({
      where: { driverId: command.driverId, jobId: command.jobId },
      withDeleted: true,
    });
    if (!candidate[0]) {
      throw new NotFoundException(
        `Candidate not found with driver id ${command.driverId} and job id ${command.jobId}`,
      );
    }
    const result = await this.candidateRepository.delete({
      driverId: command.driverId,
      jobId: command.jobId,
    });
    if (result) {
      const driverAssignment =
        await this.driverAssignmentRepository.getByDriverIdAndJobId(
          command.driverId,
          command.jobId,
        );
      if (driverAssignment) {
        this.eventEmitter.emit(
          'delete.driver.assignment',
          driverAssignment.id,
          command.currentUser,
        );
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.driverId,
        modelName: MODELS.CANDIDATE,
        action: ACTIONS.DELETE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  async archiveCandidate(command: ArchiveCandidateCommand): Promise<boolean> {
    const jobDomain = await this.jobRepository.getById(command.jobId);
    if (!jobDomain) {
      throw new NotFoundException(
        `Job does not found with id ${command.jobId}`,
      );
    }
    const candidate = jobDomain.candidates.find(
      (candidate) => candidate.id === command.id,
    );
    candidate.deletedAt = new Date();
    candidate.deletedBy = command.currentUser.id;
    candidate.archiveReason = command.reason;
    jobDomain.updateCandidate(candidate);
    const result = await this.jobRepository.update(jobDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.CANDIDATE,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  async activateOrBlockCandidate(
    id: string,
    currentUser: UserInfo,
  ): Promise<CandidateResponse> {
    const candidate = await this.candidateRepository.find({
      where: { id: id },
      withDeleted: true,
    });
    if (!candidate[0]) {
      throw new NotFoundException(`Candidate not found with id ${id}`);
    }
    candidate[0].status =
      candidate[0].status === CandidateStatus.Active
        ? CandidateStatus.Inactive
        : CandidateStatus.Active;
    const result = await this.candidateRepository.save(candidate[0]);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.CANDIDATE,
        action:
          candidate[0].status === CandidateStatus.Active
            ? ACTIONS.BLOCK
            : ACTIONS.ACTIVATE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return CandidateResponse.fromEntity(result);
  }
  async restoreCandidate(
    command: DeleteCandidateCommand,
  ): Promise<CandidateResponse> {
    const candidate = await this.candidateRepository.find({
      where: { driverId: command.driverId, jobId: command.jobId },
      withDeleted: true,
    });
    if (!candidate[0]) {
      throw new NotFoundException(
        `Candidate not found with id ${command.driverId}`,
      );
    }
    candidate[0].deletedAt = null;
    const result = await this.candidateRepository.save(candidate[0]);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.driverId,
      modelName: MODELS.CANDIDATE,
      action: ACTIONS.RESTORE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });

    return CandidateResponse.fromEntity(result);
  }
}
