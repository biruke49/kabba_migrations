import { GroupResponse } from '@assignment/usecases/groups/group.response';
import { Job } from '@job/domains/jobs/job';
import { JobEntity } from '@job/persistence/jobs/job.entity';
import { JobStatus } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { CandidateResponse } from './candidate.response';

export class JobResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  title: string;
  @ApiProperty()
  groupId: string;
  groupName: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  startLocation: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  maxApplicantNumber: number;
  @ApiProperty()
  minimumRating: number;
  @ApiProperty()
  routePreference: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  archiveReason?: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty({ isArray: true, type: GroupResponse })
  group: GroupResponse;
  candidates: CandidateResponse[];
  static fromEntity(jobEntity: JobEntity): JobResponse {
    const jobResponse = new JobResponse();
    jobResponse.id = jobEntity.id;
    jobResponse.title = jobEntity.title;
    jobResponse.groupId = jobEntity.groupId;
    jobResponse.groupName = jobEntity.groupName;
    jobResponse.driverId = jobEntity.driverId;
    jobResponse.startLocation = jobEntity.startLocation;
    jobResponse.description = jobEntity.description;
    jobResponse.maxApplicantNumber = jobEntity.maxApplicantNumber;
    jobResponse.minimumRating = jobEntity.minimumRating;
    jobResponse.routePreference = jobEntity.routePreference;
    jobResponse.status = jobEntity.status;
    jobResponse.archiveReason = jobEntity.archiveReason;
    jobResponse.createdBy = jobEntity.createdBy;
    jobResponse.updatedBy = jobEntity.updatedBy;
    jobResponse.deletedBy = jobEntity.deletedBy;
    jobResponse.createdAt = jobEntity.createdAt;
    jobResponse.updatedAt = jobEntity.updatedAt;
    jobResponse.deletedAt = jobEntity.deletedAt;
    if (jobEntity.group) {
      jobResponse.group = GroupResponse.fromEntity(jobEntity.group);
    }
    if (jobEntity.candidates) {
      jobResponse.candidates = jobEntity.candidates
        ? jobEntity.candidates.map((candidate) => {
            return CandidateResponse.fromEntity(candidate);
          })
        : [];
    }
    return jobResponse;
  }
  static fromDomain(job: Job): JobResponse {
    const jobResponse = new JobResponse();
    jobResponse.id = job.id;
    jobResponse.title = job.title;
    jobResponse.groupId = job.groupId;
    jobResponse.groupName = job.groupName;
    jobResponse.driverId = job.driverId;
    jobResponse.startLocation = job.startLocation;
    jobResponse.description = job.description;
    jobResponse.maxApplicantNumber = job.maxApplicantNumber;
    jobResponse.minimumRating = job.minimumRating;
    jobResponse.routePreference = job.routePreference;
    jobResponse.archiveReason = job.archiveReason;
    jobResponse.status = job.status;
    jobResponse.createdBy = job.createdBy;
    jobResponse.updatedBy = job.updatedBy;
    jobResponse.deletedBy = job.deletedBy;
    jobResponse.createdAt = job.createdAt;
    jobResponse.updatedAt = job.updatedAt;
    jobResponse.deletedAt = job.deletedAt;
    if (job.group) {
      jobResponse.group = GroupResponse.fromDomain(job.group);
    }
    if (job.candidates) {
      jobResponse.candidates = job.candidates
        ? job.candidates.map((candidate) => {
            return CandidateResponse.fromDomain(candidate);
          })
        : [];
    }
    return jobResponse;
  }
}
