import { CandidateEntity } from '@job/persistence/jobs/candidate.entity';
import { JobEntity } from '@job/persistence/jobs/job.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CandidateResponse } from './candidate.response';
import { JobResponse } from './job.response';
import { JobStatus } from '@libs/common/enums';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { DriverAssignmentRepository } from '@assignment/persistence/driver-assignments/driver-assignment.repository';
import { AssignmentRepository } from '@router/persistence/assignments/assignment.repository';

@Injectable()
export class JobQueries {
  constructor(
    @InjectRepository(JobEntity)
    private jobRepository: Repository<JobEntity>,
    @InjectRepository(CandidateEntity)
    private candidateRepository: Repository<CandidateEntity>,
    private driverAssignmentRepository: DriverAssignmentRepository,
    private assignmentRepository: AssignmentRepository,
  ) {}
  async getJob(id: string, withDeleted = false): Promise<JobResponse> {
    const job = await this.jobRepository.find({
      where: { id: id },
      relations: ['group', 'group.groupAssignments', 'candidates'],
      withDeleted: withDeleted,
    });
    if (!job[0]) {
      throw new NotFoundException(`Job not found with id ${id}`);
    }
    return JobResponse.fromEntity(job[0]);
  }
  async getJobs(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<JobResponse>> {
    const dataQuery = QueryConstructor.constructQuery<JobEntity>(
      this.jobRepository,
      query,
    );
    const d = new DataResponseFormat<JobResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => JobResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  // async checkAssignment(driverId: string): Promise<boolean> {
  //   const check = await this.jobRepository.find({
  //     where: { driverId: driverId },
  //     withDeleted: false,
  //   });
  //   if (!check[0]) {
  //     return false;
  //   }
  //   return true;
  // }
  async getJobsForUnassignedDrivers(
    driverId: string,
    query: CollectionQuery,
  ): Promise<any> {
    const checkDriverAssignment =
      await this.driverAssignmentRepository.checkIfDriverIsAssigned(driverId);
    const checkRouteAssignment =
      await this.assignmentRepository.checkIfDriverIsAssigned(driverId);
    if (checkDriverAssignment || checkRouteAssignment) {
      return { data: [], count: 0 };
    } else {
      const dataQuery = QueryConstructor.constructQuery<JobEntity>(
        this.jobRepository,
        query,
      );
      const d = new DataResponseFormat<JobResponse>();
      if (query.count) {
        d.count = await dataQuery.getCount();
      } else {
        const [result, total] = await dataQuery.getManyAndCount();
        d.data = result.map((entity) => JobResponse.fromEntity(entity));
        d.count = total;
      }
      return d;
    }
  }
  async getArchivedJobs(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<JobResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<JobEntity>(
      this.jobRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<JobResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => JobResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }

  //Candidates
  async getCandidates(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CandidateResponse>> {
    const dataQuery = QueryConstructor.constructQuery<CandidateEntity>(
      this.candidateRepository,
      query,
    );
    const d = new DataResponseFormat<CandidateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CandidateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getCandidatesWithJobId(
    jobId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CandidateResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'jobId',
        value: jobId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CandidateEntity>(
      this.candidateRepository,
      query,
    );
    const d = new DataResponseFormat<CandidateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CandidateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getCandidatesWithDriverId(
    driverId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CandidateResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'driverId',
          value: driverId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'job.status',
          value: JobStatus.Open,
          operator: FilterOperators.EqualTo,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<CandidateEntity>(
      this.candidateRepository,
      query,
    );
    dataQuery.leftJoinAndSelect('candidates.job', 'job');
    const d = new DataResponseFormat<CandidateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CandidateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedCandidatesWithJobId(
    jobId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CandidateResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'jobId',
        value: jobId,
        operator: FilterOperators.EqualTo,
      },
      {
        field: 'deletedAt',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CandidateEntity>(
      this.candidateRepository,
      query,
    );
    const d = new DataResponseFormat<CandidateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CandidateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getCandidate(id: string): Promise<CandidateResponse> {
    const candidate = await this.candidateRepository.find({
      where: { id: id },
      relations: ['job', 'driver'],
    });
    if (!candidate[0]) {
      throw new NotFoundException(`Candidate not found with id ${id}`);
    }
    return CandidateResponse.fromEntity(candidate[0]);
  }
  async searchCandidates(
    query: CollectionQuery,
    routeId = null,
    datePreference = null,
    averageRate = null,
  ): Promise<DataResponseFormat<CandidateResponse>> {
    const dataQuery = QueryConstructor.constructQuery<CandidateEntity>(
      this.candidateRepository,
      query,
    );
    if (routeId) {
      dataQuery.andWhere(
        `candidates.driver_id in ${dataQuery
          .subQuery()
          .select('driver_id')
          .from(PreferenceEntity, 'preferences')
          .where(`preferences.route_id= :routeId`, {
            routeId: routeId,
          })
          .getQuery()}`,
      );
    }
    if (datePreference) {
      dataQuery.andWhere(
        `candidates.driver_id in ${dataQuery
          .subQuery()
          .select('id')
          .from(DriverEntity, 'driver')
          .where(`'${datePreference}'=any(driver.date_preferences)`)
          .getQuery()}`,
      );
    }
    if (averageRate) {
      dataQuery.andWhere(
        `candidates.driver_id in ${dataQuery
          .subQuery()
          .select('id')
          .from(DriverEntity, 'driver')
          .where(`driver.average_rate->>'rate\'>= :averageRate`, {
            averageRate: averageRate,
          })
          .getQuery()}`,
      );
    }
    const d = new DataResponseFormat<CandidateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery
        .leftJoinAndSelect('candidates.driver', 'driver')
        .leftJoinAndSelect('driver.vehicle', 'vehicle')
        .leftJoinAndSelect('vehicle.category', 'category')
        .leftJoinAndSelect('driver.preferences', 'preferences')
        .getManyAndCount();
      d.data = result.map((entity) => CandidateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedCandidates(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CandidateResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CandidateEntity>(
      this.candidateRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<CandidateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CandidateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
