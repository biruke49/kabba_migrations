import { Module } from '@nestjs/common';
import { JobController } from './controllers/job.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobEntity } from './persistence/jobs/job.entity';
import { JobRepository } from './persistence/jobs/job.repository';
import { AssignmentModule } from '@assignment/assignment.module';
import { JobCommands } from './usecases/jobs/job.usecase.commands';
import { JobQueries } from './usecases/jobs/job.usecase.queries';
import { CandidateEntity } from './persistence/jobs/candidate.entity';
import { DriverAssignmentRepository } from '@assignment/persistence/driver-assignments/driver-assignment.repository';
import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';
import { AssignmentRepository } from '@router/persistence/assignments/assignment.repository';
import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { GroupRepository } from '@assignment/persistence/groups/group.repository';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      JobEntity,
      CandidateEntity,
      DriverAssignmentEntity,
      AssignmentEntity,
      GroupEntity,
      GroupAssignmentEntity,
    ]),
    AssignmentModule,
  ],
  providers: [
    JobRepository,
    JobCommands,
    JobQueries,
    DriverAssignmentRepository,
    AssignmentRepository,
    GroupRepository,
  ],
  exports: [],
  controllers: [JobController],
})
export class JobModule {}
