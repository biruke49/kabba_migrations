import { Group } from '@assignment/domains/groups/group';
import { JobStatus } from '@libs/common/enums';
import { Candidate } from './candidate';

export class Job {
  id: string;
  title: string;
  groupId: string;
  groupName: string;
  driverId: string;
  startLocation: string;
  maxApplicantNumber: number;
  minimumRating: number;
  routePreference: string;
  description: string;
  status: string;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  group: Group;
  candidates?: Candidate[];
  async addCandidate(candidate: Candidate) {
    this.candidates.push(candidate);
  }
  async updateCandidate(candidate: Candidate) {
    const existIndex = this.candidates.findIndex(
      (element) => element.id == candidate.id,
    );
    this.candidates[existIndex] = candidate;
  }
  async removeCandidate(id: string) {
    this.candidates = this.candidates.filter((element) => element.id != id);
  }
  async updateCandidates(candidates: Candidate[]) {
    this.candidates = candidates;
  }
}
