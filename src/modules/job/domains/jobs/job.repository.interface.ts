import { Job } from './job';

export interface IJobRepository {
  insert(user: Job): Promise<Job>;
  update(user: Job): Promise<Job>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Job[]>;
  getById(id: string, withDeleted: boolean): Promise<Job>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
