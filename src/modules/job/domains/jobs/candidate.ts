import { CandidateStatus } from '@libs/common/enums';
import { Driver } from '@provider/domains/drivers/driver';
import { Job } from './job';

export class Candidate {
  id: string;
  driverId: string;
  jobId: string;
  status: string;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  job: Job;
  driver: Driver;
}
