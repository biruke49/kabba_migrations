import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { RolesGuard } from '@account/guards/role.quards';
import {
  ArchiveCandidateCommand,
  CreateCandidateCommand,
  DeleteCandidateCommand,
} from '@job/usecases/jobs/candidate.commands';
import { CandidateResponse } from '@job/usecases/jobs/candidate.response';
import {
  ArchiveJobCommand,
  ChangeJobStatusCommand,
  CreateJobCommand,
  UpdateJobCommand,
} from '@job/usecases/jobs/job.commands';
import { JobResponse } from '@job/usecases/jobs/job.response';
import { JobCommands } from '@job/usecases/jobs/job.usecase.commands';
import { JobQueries } from '@job/usecases/jobs/job.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('jobs')
@ApiTags('jobs')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class JobController {
  constructor(
    private readonly command: JobCommands,
    private readonly jobQueries: JobQueries,
  ) {}
  @Get('get-job/:id')
  @ApiOkResponse({ type: JobResponse })
  async getJob(@Param('id') id: string) {
    return this.jobQueries.getJob(id);
  }
  @Get('get-archived-job/:id')
  @ApiOkResponse({ type: JobResponse })
  async getArchivedJob(@Param('id') id: string) {
    return this.jobQueries.getJob(id, true);
  }
  @Get('get-jobs')
  @ApiPaginatedResponse(JobResponse)
  async getJobs(@Query() query: CollectionQuery) {
    return this.jobQueries.getJobs(query);
  }
  @Get('get-jobs-for-unassigned-drivers')
  @ApiPaginatedResponse(JobResponse)
  async getJobsForUnassignedDrivers(
    @Query() query: CollectionQuery,
    @CurrentUser() user: UserInfo,
  ) {
    return this.jobQueries.getJobsForUnassignedDrivers(user.id, query);
  }
  @Post('create-job')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiOkResponse({ type: JobResponse })
  async createJob(
    @CurrentUser() user: UserInfo,
    @Body() createJobCommand: CreateJobCommand,
  ) {
    createJobCommand.currentUser = user;
    return this.command.createJob(createJobCommand);
  }
  @Put('update-job')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiOkResponse({ type: JobResponse })
  async updateJob(
    @CurrentUser() user: UserInfo,
    @Body() updateJobCommand: UpdateJobCommand,
  ) {
    updateJobCommand.currentUser = user;
    return this.command.updateJob(updateJobCommand);
  }
  @Delete('archive-job')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiOkResponse({ type: Boolean })
  async archiveJob(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveJobCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveJob(archiveCommand);
  }
  @Delete('delete-job/:id')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: Boolean })
  async deleteJob(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteJob(id, user);
  }
  @Post('restore-job/:id')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: JobResponse })
  async restoreJob(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreJob(id, user);
  }
  @Get('get-archived-jobs')
  @ApiPaginatedResponse(JobResponse)
  async getArchivedJobs(@Query() query: CollectionQuery) {
    return this.jobQueries.getArchivedJobs(query);
  }
  @Post('open-or-close-job')
  @UseGuards(RolesGuard('admin'))
  @ApiOkResponse({ type: JobResponse })
  async openOrCloseJob(
    @CurrentUser() user: UserInfo,
    @Body() command: ChangeJobStatusCommand,
  ) {
    command.currentUser = user;
    return this.command.openOrCloseJob(command);
  }

  // Candidate
  @Post('add-candidate')
  @UseGuards(RolesGuard('admin|operator|driver'))
  @ApiOkResponse({ type: CandidateResponse })
  async addCandidate(
    @CurrentUser() user: UserInfo,
    @Body() createCandidateCommand: CreateCandidateCommand,
  ) {
    createCandidateCommand.currentUser = user;
    return this.command.addCandidate(createCandidateCommand);
  }
  // @Put('update-candidate')
  // @ApiOkResponse({ type: CandidateResponse })
  // async updateCandidate(
  //     @CurrentUser() user: UserInfo,
  //     @Body() updateCandidateCommand: UpdateCandidateCommand,
  // ) {
  //     updateCandidateCommand.currentUser = user;
  //     return this.command.updateCandidate(updateCandidateCommand);
  // }
  @Delete('remove-candidate')
  @UseGuards(RolesGuard('admin|operator|driver'))
  @ApiOkResponse({ type: Boolean })
  async removeCandidate(
    @CurrentUser() user: UserInfo,
    @Body() removeCandidateCommand: DeleteCandidateCommand,
  ) {
    removeCandidateCommand.currentUser = user;
    return this.command.deleteCandidate(removeCandidateCommand);
  }
  @Delete('archive-candidate')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiOkResponse({ type: Boolean })
  async archiveCandidate(
    @CurrentUser() user: UserInfo,
    @Body() archiveCandidateCommand: ArchiveCandidateCommand,
  ) {
    archiveCandidateCommand.currentUser = user;
    return this.command.archiveCandidate(archiveCandidateCommand);
  }
  @Get('get-candidate/:id')
  @ApiOkResponse({ type: CandidateResponse })
  async getCandidate(@Param('id') id: string) {
    return this.jobQueries.getCandidate(id);
  }
  @Get('get-candidates')
  @ApiPaginatedResponse(CandidateResponse)
  async getCandidates(@Query() query: CollectionQuery) {
    return this.jobQueries.getCandidates(query);
  }
  @Get('search-candidates')
  @ApiPaginatedResponse(CandidateResponse)
  async searchCandidates(
    @Query() query: CollectionQuery,
    @Query('routeId') routeId?: string,
    @Query('datePreference') datePreference?: string,
    @Query('averageRate') averageRate?: string,
  ) {
    return this.jobQueries.searchCandidates(
      query,
      routeId,
      datePreference,
      averageRate,
    );
  }
  @Get('get-candidates-with-job-id/:JobId')
  @ApiPaginatedResponse(CandidateResponse)
  async getCandidatesWithJobId(
    @Param('JobId') JobId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.jobQueries.getCandidatesWithJobId(JobId, query);
  }
  @Get('get-archived-candidates-with-job-id/:jobId')
  @ApiPaginatedResponse(CandidateResponse)
  async getArchivedCandidatesWithjobId(
    @Param('jobId') jobId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.jobQueries.getArchivedCandidatesWithJobId(jobId, query);
  }
  @Get('get-archived-candidates')
  @ApiPaginatedResponse(CandidateResponse)
  async getArchivedCandidates(@Query() query: CollectionQuery) {
    return this.jobQueries.getArchivedCandidates(query);
  }
  @Post('activate-or-block-candidate/:id')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiOkResponse({ type: CandidateResponse })
  async activateOrBlockCandidate(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockCandidate(id, user);
  }
  @Post('restore-candidate')
  @UseGuards(RolesGuard('admin|operator'))
  @ApiOkResponse({ type: CandidateResponse })
  async restoreCandidate(
    @CurrentUser() user: UserInfo,
    @Body() command: DeleteCandidateCommand,
  ) {
    command.currentUser = user;
    return this.command.restoreCandidate(command);
  }
  @Get('get-candidates-with-driver-id')
  @ApiOkResponse({ type: CandidateResponse })
  async getCandidatesWithDriverId(
    @CurrentUser() user: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.jobQueries.getCandidatesWithDriverId(user.id, query);
  }
}
