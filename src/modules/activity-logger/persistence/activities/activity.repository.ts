import { ActivityEntity } from '@activity-logger/persistence/activities/activity.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IActivityRepository } from '@activity-logger/domains/activities/activity.repository.interface';
import { Activity } from '@activity-logger/domains/activities/activity';
@Injectable()
export class ActivityRepository implements IActivityRepository {
  constructor(
    @InjectRepository(ActivityEntity)
    private activityRepository: Repository<ActivityEntity>,
  ) {}
  async insert(activity: Activity): Promise<Activity> {
    const activityEntity = this.toActivityEntity(activity);
    const result = await this.activityRepository.save(activityEntity);
    return result ? this.toActivity(result) : null;
  }
  async update(activity: Activity): Promise<Activity> {
    const activityEntity = this.toActivityEntity(activity);
    const result = await this.activityRepository.save(activityEntity);
    return result ? this.toActivity(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.activityRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Activity[]> {
    const activities = await this.activityRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!activities.length) {
      return null;
    }
    return activities.map((user) => this.toActivity(user));
  }
  async getById(id: string, withDeleted = false): Promise<Activity> {
    const activity = await this.activityRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!activity[0]) {
      return null;
    }
    return this.toActivity(activity[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.activityRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.activityRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toActivity(activityEntity: ActivityEntity): Activity {
    const activity = new Activity();
    activity.id = activityEntity.id;
    activity.userId = activityEntity.userId;
    activity.user = activityEntity.user;
    activity.modelId = activityEntity.modelId;
    activity.modelName = activityEntity.modelName;
    activity.ip = activityEntity.ip;
    activity.action = activityEntity.action;
    activity.oldPayload = activityEntity.oldPayload;
    activity.payload = activityEntity.payload;
    activity.createdBy = activityEntity.createdBy;
    activity.updatedBy = activityEntity.updatedBy;
    activity.deletedBy = activityEntity.deletedBy;
    activity.createdAt = activityEntity.createdAt;
    activity.updatedAt = activityEntity.updatedAt;
    activity.deletedAt = activityEntity.deletedAt;
    return activity;
  }
  toActivityEntity(activity: Activity): ActivityEntity {
    const activityEntity = new ActivityEntity();
    activityEntity.id = activity.id;
    activityEntity.userId = activity.userId;
    activityEntity.user = activity.user;
    activityEntity.modelId = activity.modelId;
    activityEntity.modelName = activity.modelName;
    activityEntity.action = activity.action;
    activityEntity.ip = activity.ip;
    activityEntity.oldPayload = activity.oldPayload;
    activityEntity.payload = activity.payload;
    activityEntity.createdBy = activity.createdBy;
    activityEntity.updatedBy = activity.updatedBy;
    activityEntity.deletedBy = activity.deletedBy;
    activityEntity.createdAt = activity.createdAt;
    activityEntity.updatedAt = activity.updatedAt;
    activityEntity.deletedAt = activity.deletedAt;
    return activityEntity;
  }
}
