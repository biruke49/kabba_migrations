import { UserInfo } from '@account/dtos/user-info.dto';
export class Activity {
  id: string;
  modelId: string;
  modelName: string;
  userId?: string;
  action: string;
  ip?: string;
  oldPayload?: any;
  payload?: any;
  user: UserInfo;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
