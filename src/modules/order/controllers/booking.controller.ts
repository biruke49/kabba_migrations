import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { TransactionTotalResponse } from '@libs/common/corporate-transaction-spending';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ArchiveBookingCommand,
  CancelBookingCommand,
  CreateBookingCommand2,
  CreateMultipleBookingCommand,
} from '@order/usecases/bookings/booking.commands';
import {
  BookingResponse,
  CountBookingByAssignmentResponse,
  CountByCreatedAtResponse,
  CountBookingByRouteResponse,
  CountWeeklyBookingByCreatedAtResponse,
} from '@order/usecases/bookings/booking.response';
import { BookingCommands } from '@order/usecases/bookings/booking.usecase.commands';
import { BookingQueries } from '@order/usecases/bookings/booking.usecase.queries';

@Controller('bookings')
@ApiTags('bookings')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class BookingsController {
  constructor(
    private readonly commands: BookingCommands,
    private readonly bookingQueries: BookingQueries,
  ) {}
  @Get('get-booking/:id')
  @ApiOkResponse({ type: BookingResponse })
  async getBooking(@Param('id') id: string) {
    return this.bookingQueries.getBooking(id);
  }
  @Get('get-archived-booking/:id')
  @ApiOkResponse({ type: BookingResponse })
  async getArchivedBooking(@Param('id') id: string) {
    return this.bookingQueries.getBooking(id, true);
  }
  @Get('get-bookings')
  @ApiPaginatedResponse(BookingResponse)
  async getBookings(@Query() query: CollectionQuery) {
    return this.bookingQueries.getBookings(query);
  }
  @Get('group-bookings-by-created-date/:format')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupBookingsByCreatedDate(
    @Query() query: CollectionQuery,
    @Param('format') format: string,
  ) {
    return this.bookingQueries.groupBookingsByCreatedDate(query, format);
  }
  @Get('group-bookings-by-status')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupBookingsByStatus(@Query() query: CollectionQuery) {
    return this.bookingQueries.groupBookingsByStatus(query);
  }
  @Get('get-most-booked-routes')
  @ApiOkResponse({ isArray: true })
  async getMostBookedRoutes(@Query() query: CollectionQuery) {
    return this.bookingQueries.getMostBookedRoutes(query);
  }
  @Get('get-booking-value')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getCompanyEarning(@Query() query: CollectionQuery) {
    return this.bookingQueries.getCompanyEarning(query);
  }
  @Get('group-booking-value-by-created-month')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupEarningsByCreatedDate(@Query() query: CollectionQuery) {
    return this.bookingQueries.groupEarningsByCreatedDate(query);
  }
  @Get('get-driver-earning/:driverId')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getDriverEarning(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getDriverEarning(driverId, query);
  }
  @Get('get-my-earning')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getMyEarning(
    @CurrentUser() profile: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getDriverEarning(profile.id, query);
  }
  @Get('get-my-weekly-earning')
  @ApiOkResponse({ type: CountWeeklyBookingByCreatedAtResponse })
  async getMyWeeklyEarning(
    @CurrentUser() profile: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getDriverWeeklyEarning(profile.id, query);
  }
  @Get('get-driver-weekly-earning/:driverId')
  @ApiOkResponse({ type: CountWeeklyBookingByCreatedAtResponse, isArray: true })
  async getDriverWeeklyEarning(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getDriverWeeklyEarning(driverId, query);
  }
  @Get('group-bookings-by-assignment')
  @ApiOkResponse({ type: CountBookingByAssignmentResponse, isArray: true })
  async groupBookingsByAssignments(@Query() query: CollectionQuery) {
    return this.bookingQueries.groupBookingsByAssignments(query);
  }
  @Get('group-bookings-by-route')
  @ApiOkResponse({ type: CountBookingByRouteResponse, isArray: true })
  async groupBookingsByRoute(@Query() query: CollectionQuery) {
    return this.bookingQueries.groupBookingsByRoute(query);
  }
  @Get('get-route-bookings/:routeId')
  @ApiPaginatedResponse(BookingResponse)
  async getRouteBookings(
    @Param('routeId') routeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getRouteBookings(routeId, query);
  }
  @Get('get-my-bookings')
  @UseGuards(RolesGuard('driver'))
  @ApiPaginatedResponse(BookingResponse)
  async getMyBookings(
    @CurrentUser() user: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getDriverBookings(user.id, query);
  }
  @Get('get-driver-bookings/:driverId')
  @ApiPaginatedResponse(BookingResponse)
  async getDriverBookings(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getDriverBookings(driverId, query);
  }
  @Get('get-passenger-bookings/:passengerId')
  @ApiPaginatedResponse(BookingResponse)
  async getPassengerBookings(
    @Param('passengerId') passengerId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.bookingQueries.getPassengerBookings(passengerId, query);
  }
  // @Post('create-booking')
  // @UseGuards(RolesGuard('passenger'))
  // @ApiOkResponse({ type: BookingResponse })
  // async createBooking(
  //   @CurrentUser() user: UserInfo,
  //   @Body() createBookingCommand: CreateBookingCommand,
  // ) {
  //   createBookingCommand.passengerId = user.id;
  //   createBookingCommand.passengerGender = user.gender;
  //   createBookingCommand.passengerName = user.name;
  //   createBookingCommand.passengerPhoneNumber = user.phoneNumber;
  //   return this.commands.createBooking(createBookingCommand);
  // }
  @Post('create-booking')
  // @UseGuards(RolesGuard('passenger'))
  @ApiOkResponse({ type: BookingResponse })
  async createBooking(
    @CurrentUser() user: UserInfo,
    @Body() createBookingCommand: CreateBookingCommand2,
  ) {
    createBookingCommand.passengerId = user.id;
    createBookingCommand.passengerGender = user.gender;
    createBookingCommand.passengerName = user.name;
    createBookingCommand.passengerPhoneNumber = user.phoneNumber;
    return this.commands.createBooking2(createBookingCommand);
  }
  @Post('create-multiple-bookings')
  // @UseGuards(RolesGuard('passenger'))
  @ApiOkResponse({ type: BookingResponse })
  async createMutipleBooking(
    @CurrentUser() user: UserInfo,
    @Body() createBookingCommand: CreateMultipleBookingCommand,
  ) {
    createBookingCommand.passengerId = user.id;
    createBookingCommand.passengerGender = user.gender;
    createBookingCommand.passengerName = user.name;
    createBookingCommand.passengerPhoneNumber = user.phoneNumber;
    return this.commands.createMultipleBookings(createBookingCommand);
  }
  @Delete('archive-booking')
  @UseGuards(PermissionsGuard('manage-booking'))
  @ApiOkResponse({ type: Boolean })
  async archiveBooking(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveBookingCommand,
  ) {
    return this.commands.archiveBooking(archiveCommand);
  }
  @Delete('delete-booking/:id')
  @UseGuards(PermissionsGuard('manage-booking'))
  @ApiOkResponse({ type: Boolean })
  async deleteBooking(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.commands.deleteBooking(id, user);
  }
  @Post('restore-booking/:id')
  @UseGuards(PermissionsGuard('manage-booking'))
  @ApiOkResponse({ type: BookingResponse })
  async restoreBooking(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.commands.restoreBooking(id, user);
  }
  @Get('get-archived-bookings')
  @ApiPaginatedResponse(BookingResponse)
  async getArchivedBookings(@Query() query: CollectionQuery) {
    return this.bookingQueries.getArchivedBookings(query);
  }
  @Post('start-booking/:id')
  @UseGuards(RolesGuard('driver|admin|operator'))
  @ApiOkResponse({ type: BookingResponse })
  async startBooking(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.commands.startBooking(id, user);
  }
  @Post('complete-booking/:id')
  @UseGuards(RolesGuard('driver|admin|operator'))
  @ApiOkResponse({ type: BookingResponse })
  async completeBooking(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.commands.completeBooking(id);
  }
  @Post('cancel-booking')
  @UseGuards(RolesGuard('passenger|admin|operator'))
  @ApiOkResponse({ type: BookingResponse })
  async cancelBooking(
    @CurrentUser() user: UserInfo,
    @Body() command: CancelBookingCommand,
  ) {
    command.reason.cancelledAt = new Date();
    return this.commands.cancelBooking(command);
  }
}
