import { CategoryEntity } from './../classification/persistence/categories/category.entity';
import { RoutePriceEntity } from '@router/persistence/routes/route-price.entity';
import { StationEntity } from '@router/persistence/routes/station.entity';
import { CreditModule } from '@credit/credit.module';
import { RouterModule } from '@router/router.module';
import { RouteQueries } from '@router/usecases/routes/route.usecase.queries';
import { RouteEntity } from '@router/persistence/routes/route.entity';
import { WalletEntity } from './../credit/persistence/wallets/wallet.entity';
import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { BookingCommands } from './usecases/bookings/booking.usecase.commands';
import { BookingQueries } from './usecases/bookings/booking.usecase.queries';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { BookingsController } from './controllers/booking.controller';
import { BookingRepository } from './persistance/bookings/booking.repository';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { AssignmentQueries } from '@router/usecases/assignments/assignment.usecase.queries';
import { WalletRepository } from '@credit/persistence/wallets/wallet.repository';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BookingEntity,
      DriverEntity,
      AssignmentEntity,
      WalletEntity,
      RouteEntity,
      StationEntity,
      RoutePriceEntity,
      VehicleEntity,
    ]),
    RouterModule,
    CreditModule,
  ],
  providers: [
    BookingRepository,
    BookingQueries,
    BookingCommands,
    DriverRepository,
    AssignmentQueries,
    WalletRepository,
    RouteQueries,
  ],
  controllers: [BookingsController],
})
export class OrderModule {}
