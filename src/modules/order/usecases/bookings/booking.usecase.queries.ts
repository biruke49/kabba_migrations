import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  BookingResponse,
  CountBookingByAssignmentResponse,
  CountByCreatedAtResponse,
  CountBookingByRouteResponse,
  CountEarningsByCreatedAtResponse,
  CountWeeklyBookingByCreatedAtResponse,
} from './booking.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import { Util } from '@libs/common/util';
import { BookingStatus } from '@order/domains/bookings/constants';
@Injectable()
export class BookingQueries {
  constructor(
    @InjectRepository(BookingEntity)
    private bookingRepository: Repository<BookingEntity>,
  ) {}
  async getBooking(id: string, withDeleted = false): Promise<BookingResponse> {
    const booking = await this.bookingRepository.find({
      where: { id: id },
      relations: [
        'driver',
        'route',
        'destination',
        'pickup',
        'assignment',
        'passenger',
        'driver.vehicle',
      ],
      withDeleted: withDeleted,
    });
    if (!booking[0]) {
      throw new NotFoundException(`Booking not found with id ${id}`);
    }
    return BookingResponse.fromEntity(booking[0]);
  }
  async getBookings(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BookingResponse>> {
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const d = new DataResponseFormat<BookingResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BookingResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async groupBookingsByCreatedDate(
    query: CollectionQuery,
    format: string,
  ): Promise<CountByCreatedAtResponse[]> {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 1);
    const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    let formatQ = '';
    if (format === 'year') {
      formatQ = 'YYYY';
    } else if (format === 'month') {
      formatQ = 'YYYY-MM';
    } else if (format === 'date') {
      formatQ = 'YYYY-MM-dd';
    }
    if (format !== 'year') {
      if (!query.filter) {
        query.filter = [];
      }
      query.filter.push([
        {
          field: 'bookings.created_at',
          value: [startOfYear.toISOString(), endOfYear.toISOString()],
          operator: FilterOperators.Between,
        },
      ]);
    }
    query.select = [];
    query.select.push(
      `to_char(bookings.created_at,'${formatQ}') as created_date`,
      'COUNT(bookings.id)',
    );
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupEarningsByCreatedDate(
    query: CollectionQuery,
  ): Promise<CountEarningsByCreatedAtResponse[]> {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 1);
    const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    query.select = [];
    query.select.push(
      `to_char(bookings.created_at,'MM') as created_date`,
      'SUM(bookings.fee)',
    );
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'bookings.created_at',
        value: [startOfYear.toISOString(), endOfYear.toISOString()],
        operator: FilterOperators.Between,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountEarningsByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.total = d.sum;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getMostBookedRoutes(query: CollectionQuery): Promise<any[]> {
    query.select = [];
    query.select.push(
      'assignment.route_name as assignment_route_name',
      'bookings.assignment_id as assignment, COUNT(bookings.assignment_id) as countAssignment',
    );
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`assignment`)
      .addGroupBy(`assignment.route_name`)
      .orderBy('countAssignment', 'DESC')
      .leftJoin('bookings.assignment', 'assignment')
      .getRawMany();
    return data;
  }
  async groupBookingsByStatus(
    query: CollectionQuery,
  ): Promise<CountByCreatedAtResponse[]> {
    query.select = [];
    query.select.push(`bookings.status as status`, 'COUNT(bookings.id)');
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const data = await dataQuery.groupBy(`status`).getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCreatedAtResponse();
      countResponse.createdAt = d.status;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getCompanyEarning(query: CollectionQuery): Promise<any> {
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const data = await dataQuery.select('SUM(fee)', 'total').getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }

  async getDriverEarning(
    driverId: string,
    query: CollectionQuery,
  ): Promise<any> {
    const today = new Date('Y-m-d');
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        value: driverId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const data = await dataQuery.select('SUM(fee)', 'total').getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async getDriverWeeklyEarning(
    driverId: string,
    query: CollectionQuery,
  ): Promise<CountWeeklyBookingByCreatedAtResponse[]> {
    const today = new Date();
    const lastMonday = Util.getTheLastMonday(today);
    const todayy = today.toDateString();
    const lastMondayy = lastMonday.toDateString();
    query.select = [];
    query.select.push(
      `to_char(bookings.created_at,'YYYY-MM-dd') as created_date`,
      'SUM(bookings.fee)',
    );
    query.select.push(`assignment_id as assignment`, 'COUNT(bookings.id)');
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'driverId',
          value: driverId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'created_at',
          value: `${lastMondayy},${todayy}`,
          operator: FilterOperators.Between,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .addGroupBy('assignment')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountWeeklyBookingByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.total = d.sum;
      countResponse.numberOfTrips = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupBookingsByRoute(
    query: CollectionQuery,
  ): Promise<CountBookingByRouteResponse[]> {
    query.select = [];
    query.select.push(`bookings.route_id as route`, 'COUNT(bookings.id)');
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );

    const data = await dataQuery
      .groupBy(`route`)
      .leftJoin('bookings.route', 'route')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountBookingByRouteResponse();
      countResponse.routeId = d.route;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupBookingsByAssignments(
    query: CollectionQuery,
  ): Promise<CountBookingByAssignmentResponse[]> {
    query.select = [];
    query.select.push(
      `bookings.assignment_id as assignment`,
      'COUNT(bookings.id)',
    );
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );

    const data = await dataQuery
      .groupBy(`assignment`)
      .leftJoin('bookings.assignment', 'assignment')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountBookingByAssignmentResponse();
      countResponse.assignment = d.assignment;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getRouteBookings(
    routeId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BookingResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'routeId',
        operator: FilterOperators.EqualTo,
        value: routeId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const d = new DataResponseFormat<BookingResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BookingResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getActiveAssignmentBookings(
    assignmentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BookingResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'assignmentId',
          operator: FilterOperators.EqualTo,
          value: assignmentId,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.NotIn,
          value: [BookingStatus.CANCELLED, BookingStatus.MISSED],
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const d = new DataResponseFormat<BookingResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BookingResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedBookings(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BookingResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<BookingResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BookingResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getDriverBookings(
    driverId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BookingResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        operator: FilterOperators.EqualTo,
        value: driverId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const d = new DataResponseFormat<BookingResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BookingResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getPassengerBookings(
    passengerId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BookingResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'passengerId',
        operator: FilterOperators.EqualTo,
        value: passengerId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BookingEntity>(
      this.bookingRepository,
      query,
    );
    const d = new DataResponseFormat<BookingResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BookingResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
