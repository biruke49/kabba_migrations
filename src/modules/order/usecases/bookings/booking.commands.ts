import { WalletType } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import { Booking } from '@order/domains/bookings/booking';
import { CancellationReason } from '@router/domains/assignments/cancelled-reason';
import { IsEnum, IsNotEmpty } from 'class-validator';

export class CreateBookingCommand {
  // @ApiProperty()
  // @IsNotEmpty()
  assignmentId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupName: string;
  @ApiProperty()
  @IsNotEmpty()
  destinationId: string;
  @ApiProperty()
  @IsNotEmpty()
  destinationName: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(WalletType, {
    message: 'Wallet Type must be either IndividualWallet or CorporateWallet',
  })
  walletType: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  assignmentDate: Date;
  passengerId: string;
  passengerName: string;
  passengerPhoneNumber: string;
  passengerGender: string;
  fee: number;
  categoryId: string;
  vehicleId: string;
  vehiclePlateNumber: string;
  driverId: string;
  static fromCommand(command: CreateBookingCommand): Booking {
    const bookingDomain = new Booking();
    bookingDomain.assignmentId = command.assignmentId;
    bookingDomain.assignmentDate = command.assignmentDate;
    bookingDomain.routeId = command.routeId;
    bookingDomain.walletType = command.walletType;
    bookingDomain.fee = command.fee ? command.fee : 0;
    bookingDomain.pickupId = command.pickupId;
    bookingDomain.passengerId = command.passengerId;
    bookingDomain.passengerGender = command.passengerGender;
    bookingDomain.passengerName = command.passengerName;
    bookingDomain.passengerPhoneNumber = command.passengerPhoneNumber;
    bookingDomain.pickupName = command.pickupName;
    bookingDomain.destinationId = command.destinationId;
    bookingDomain.destinationName = command.destinationName;
    return bookingDomain;
  }
}

export class CreateBookingCommand2 {
  // @ApiProperty()
  // @IsNotEmpty()
  assignmentId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupName: string;
  @ApiProperty()
  @IsNotEmpty()
  destinationId: string;
  @ApiProperty()
  @IsNotEmpty()
  destinationName: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(WalletType, {
    message: 'Wallet Type must be either IndividualWallet or CorporateWallet',
  })
  walletType: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  @ApiProperty()
  @IsNotEmpty()
  assignmentDate: Date;
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  passengerId: string;
  passengerName: string;
  passengerPhoneNumber: string;
  passengerGender: string;
  fee: number;
  vehicleId: string;
  vehiclePlateNumber: string;
  driverId: string;
  static fromCommand(command: CreateBookingCommand2): Booking {
    const bookingDomain = new Booking();
    bookingDomain.assignmentId = command.assignmentId;
    bookingDomain.assignmentDate = command.assignmentDate;
    bookingDomain.routeId = command.routeId;
    bookingDomain.walletType = command.walletType;
    bookingDomain.fee = command.fee ? command.fee : 0;
    bookingDomain.pickupId = command.pickupId;
    bookingDomain.passengerId = command.passengerId;
    bookingDomain.passengerGender = command.passengerGender;
    bookingDomain.passengerName = command.passengerName;
    bookingDomain.passengerPhoneNumber = command.passengerPhoneNumber;
    bookingDomain.pickupName = command.pickupName;
    bookingDomain.destinationId = command.destinationId;
    bookingDomain.destinationName = command.destinationName;
    return bookingDomain;
  }
}
export class CreateMultipleBookingCommand {
  // @ApiProperty()
  // @IsNotEmpty()
  assignmentId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupName: string;
  @ApiProperty()
  @IsNotEmpty()
  destinationId: string;
  @ApiProperty()
  @IsNotEmpty()
  destinationName: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(WalletType, {
    message: 'Wallet Type must be either IndividualWallet or CorporateWallet',
  })
  walletType: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  @ApiProperty()
  @IsNotEmpty()
  assignmentDate: Date[];
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  passengerId: string;
  passengerName: string;
  passengerPhoneNumber: string;
  passengerGender: string;
  fee: number;
  vehicleId: string;
  vehiclePlateNumber: string;
  driverId: string;
  // static fromCommand(command: CreateMultipleBookingCommand): Booking {
  //   const bookingDomain = new Booking();
  //   bookingDomain.assignmentId = command.assignmentId;
  //   bookingDomain.assignmentDate = command.assignmentDate;
  //   bookingDomain.routeId = command.routeId;
  //   bookingDomain.walletType = command.walletType;
  //   bookingDomain.fee = command.fee ? command.fee : 0;
  //   bookingDomain.pickupId = command.pickupId;
  //   bookingDomain.passengerId = command.passengerId;
  //   bookingDomain.passengerGender = command.passengerGender;
  //   bookingDomain.passengerName = command.passengerName;
  //   bookingDomain.passengerPhoneNumber = command.passengerPhoneNumber;
  //   bookingDomain.pickupName = command.pickupName;
  //   bookingDomain.destinationId = command.destinationId;
  //   bookingDomain.destinationName = command.destinationName;
  //   return bookingDomain;
  // }
}
export class UpdateBookingCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  assignmentId: string;
  @ApiProperty()
  @IsNotEmpty()
  routeId: string;
  @ApiProperty()
  @IsNotEmpty()
  pickupTime: string;
  @ApiProperty()
  @IsNotEmpty()
  bookingDate: Date;
  @ApiProperty()
  fee: number;
  @ApiProperty()
  remark: string;
  //   static fromCommand(command: UpdateBookingCommand): Booking {
  //     const bookingDomain = new Booking();
  //     bookingDomain.id = command.id;
  //     bookingDomain.assignmentId = command.assignmentId;
  //     bookingDomain.routeId = command.routeId;
  //     bookingDomain.bookingDate = command.bookingDate;
  //     bookingDomain.remark = command.remark;
  //     bookingDomain.fee = command.fee ? command.fee : 0;
  //     bookingDomain.pickupTime = command.pickupTime;
  //     bookingDomain.createdBy = command.currentUser.id;
  //     bookingDomain.updatedBy = command.currentUser.id;
  //     return bookingDomain;
  //   }
}
export class ArchiveBookingCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
}
export class CancelBookingCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  reason: CancellationReason;
}
