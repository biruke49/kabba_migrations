import { FilterOperators } from '@libs/collection-query/filter_operators';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { RouteQueries } from '@router/usecases/routes/route.usecase.queries';
import { WalletRepository } from '@credit/persistence/wallets/wallet.repository';
import { AssignmentQueries } from '@router/usecases/assignments/assignment.usecase.queries';
import {
  ArchiveBookingCommand,
  CancelBookingCommand,
  CreateBookingCommand,
  CreateBookingCommand2,
  CreateMultipleBookingCommand,
} from './booking.commands';
import { BookingResponse } from './booking.response';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { UserInfo } from '@account/dtos/user-info.dto';
import { BookingRepository } from '@order/persistance/bookings/booking.repository';
import { BookingStatus } from '@order/domains/bookings/constants';
import { InjectRepository } from '@nestjs/typeorm';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import { Repository } from 'typeorm';
import { WalletResponse } from '@credit/usecases/wallets/wallet.response';
import { CreateTransactionCommand } from '@credit/usecases/transactions/transaction.commands';
import { PaymentMethod, PaymentStatus, WalletType } from '@libs/common/enums';
import { AssignmentStatus } from '@router/domains/assignments/constants';
import { Assignment } from '@router/domains/assignments/assignment';
import { AssignmentResponse } from '@router/usecases/assignments/assignment.response';
@Injectable()
export class BookingCommands {
  constructor(
    private readonly bookingRepository: BookingRepository,
    private readonly assignmentQueries: AssignmentQueries,
    private readonly walletRepository: WalletRepository,
    private readonly routeQueries: RouteQueries,
    @InjectRepository(VehicleEntity)
    private readonly vehicleRepository: Repository<VehicleEntity>,
    private readonly eventEmitter: EventEmitter2,
  ) {}
  // async createBooking(command: CreateBookingCommand): Promise<BookingResponse> {
  //   const existingBook =
  //     await this.bookingRepository.getByAssignmentIdAndPassengerId(
  //       command.assignmentId,
  //       command.passengerId,
  //     );
  //   if (existingBook) {
  //     if (existingBook.status !== BookingStatus.CANCELLED)
  //       throw new NotFoundException(`You have already reserved`);
  //   }
  //   const assignment = await this.assignmentQueries.getAssignment(
  //     command.assignmentId,
  //   );
  //   if (!assignment) {
  //     throw new NotFoundException(
  //       `Assignment not found with id ${command.assignmentId}`,
  //     );
  //   }
  //   const customerWallet = await this.walletRepository.getByPassengerId(
  //     command.passengerId,
  //   );
  //   const route = await this.routeQueries.getRoute(command.routeId);
  //   const vehicle = await this.vehicleRepository.findOneBy({
  //     id: assignment.driver.vehicleId,
  //   });
  //   //Todo ==> Booking fee calculation
  //   const routePrice = route.prices.find(
  //     (price) => price.categoryId === vehicle.categoryId,
  //   );
  //   if (!routePrice) {
  //     throw new BadRequestException(` Route Price didn't configured`);
  //   }
  //   const customerWalletBalance =
  //     command.walletType === WalletType.IndividualWallet
  //       ? customerWallet.balance
  //       : customerWallet.corporateWalletBalance;
  //   if (!customerWallet || customerWalletBalance < routePrice.fee) {
  //     throw new BadRequestException(
  //       `You don't have enough balance to order this request`,
  //     );
  //   }
  //   if (assignment.availableSeats <= 0) {
  //     throw new BadRequestException(`All available seats already reserved`);
  //   }
  //   const bookingDomain = CreateBookingCommand.fromCommand(command);
  //   bookingDomain.vehicleId = assignment.vehicleId;
  //   bookingDomain.vehiclePlateNumber = assignment.vehiclePlateNumber;
  //   bookingDomain.driverId = assignment.driverId;
  //   bookingDomain.routeName = assignment.route.name;
  //   bookingDomain.assignmentDate = assignment.assignmentDate;
  //   bookingDomain.fee = routePrice.fee;
  //   bookingDomain.categoryId = vehicle.categoryId;
  //   const booking = await this.bookingRepository.insert(bookingDomain);
  //   const bookingResponse = BookingResponse.fromDomain(booking);
  //   if (booking) {
  //     if (command.walletType === WalletType.CorporateWallet) {
  //       customerWallet.corporateWalletBalance -= routePrice.fee;
  //     } else {
  //       customerWallet.balance -= routePrice.fee;
  //     }
  //     const wallet = await this.walletRepository.update(customerWallet);
  //     // console.log('wallet before booking.........', customerWallet);

  //     // console.log('wallet after booking.........', wallet);
  //     bookingResponse['wallet'] = WalletResponse.fromDomain(wallet);
  //     const withdrawBalance = -1 * routePrice.fee;
  //     const createSendTransactionCommand: CreateTransactionCommand = {
  //       passengerId: command.passengerId,
  //       amount: withdrawBalance,
  //       method: PaymentMethod.BookingWithdraw,
  //       status: PaymentStatus.Success,
  //       returnUrl: '',
  //       tradeNumber: '',
  //       tradeDate: null,
  //       transactionNumber: '',
  //       reason: `Booking Withdrawal`,
  //     };
  //     this.eventEmitter.emit(
  //       'create.transaction',
  //       createSendTransactionCommand,
  //     );
  //     this.eventEmitter.emit('decrease.assignment.seats', assignment.id);
  //   }
  //   return bookingResponse;
  // }
  async createBooking2(
    command: CreateBookingCommand2,
  ): Promise<BookingResponse> {
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'vehicleCategoryId',
          operator: FilterOperators.EqualTo,
          value: command.categoryId,
        },
      ],
      [
        {
          field: 'routeId',
          operator: FilterOperators.EqualTo,
          value: command.routeId,
        },
      ],
      [
        {
          field: 'assignmentDate',
          operator: FilterOperators.EqualTo,
          value: command.assignmentDate,
        },
      ],
      [
        {
          field: 'pickupTime',
          operator: FilterOperators.EqualTo,
          value: command.pickupTime,
        },
      ],
      [
        {
          field: 'status',
          operator: FilterOperators.NotIn,
          value: [
            AssignmentStatus.COMPLETED,
            AssignmentStatus.CANCELLED,
            AssignmentStatus.STARTED,
          ],
        },
      ],
      [
        {
          field: 'availableSeats',
          operator: FilterOperators.GreaterThan,
          value: 0,
        },
      ],
    ];
    query.top = 1;
    query.orderBy = [
      {
        field: 'availableSeats',
        direction: 'DESC',
      },
    ];
    query.includes = ['driver'];
    const assignmentsData = await this.assignmentQueries.getAssignments(query);
    if (!assignmentsData || !assignmentsData.data || !assignmentsData.data[0]) {
      throw new BadRequestException(
        `We didn't assigned a vehicle on ${command.pickupTime}`,
      );
    }
    const assignment = assignmentsData.data[0];
    if (!assignment) {
      throw new NotFoundException(
        `Assignment not found with id ${command.assignmentId}`,
      );
    }
    const existingBook =
      await this.bookingRepository.getByAssignmentIdAndPassengerId(
        assignment.id,
        command.passengerId,
      );
    if (existingBook) {
      if (existingBook.status !== BookingStatus.CANCELLED)
        throw new NotFoundException(`You have already reserved`);
    }
    const customerWallet = await this.walletRepository.getByPassengerId(
      command.passengerId,
    );
    const route = await this.routeQueries.getRoute(command.routeId);
    const vehicle = await this.vehicleRepository.findOneBy({
      id: assignment.driver.vehicleId,
    });
    //Todo ==> Booking fee calculation
    const routePrice = route.prices.find(
      (price) => price.categoryId === vehicle.categoryId,
    );
    if (!routePrice) {
      throw new BadRequestException(` Route Price didn't configured`);
    }
    const customerWalletBalance =
      command.walletType === WalletType.IndividualWallet
        ? customerWallet.balance
        : customerWallet.corporateWalletBalance;
    if (!customerWallet || customerWalletBalance < routePrice.fee) {
      throw new BadRequestException(
        `You don't have enough balance to order this request`,
      );
    }
    if (assignment.availableSeats <= 0) {
      throw new BadRequestException(`All available seats already reserved`);
    }
    const bookingDomain = CreateBookingCommand.fromCommand(command);
    bookingDomain.vehicleId = assignment.vehicleId;
    bookingDomain.vehiclePlateNumber = assignment.vehiclePlateNumber;
    bookingDomain.driverId = assignment.driverId;
    bookingDomain.routeName = route.name;
    bookingDomain.assignmentDate = command.assignmentDate;
    bookingDomain.fee = routePrice.fee;
    bookingDomain.categoryId = command.categoryId;
    bookingDomain.assignmentId = assignment.id;
    const booking = await this.bookingRepository.insert(bookingDomain);
    const bookingResponse = BookingResponse.fromDomain(booking);
    if (booking) {
      if (command.walletType === WalletType.CorporateWallet) {
        customerWallet.corporateWalletBalance -= routePrice.fee;
      } else {
        customerWallet.balance -= routePrice.fee;
      }
      const wallet = await this.walletRepository.update(customerWallet);
      // console.log('wallet before booking.........', customerWallet);

      // console.log('wallet after booking.........', wallet);
      bookingResponse['wallet'] = WalletResponse.fromDomain(wallet);
      const withdrawBalance = -1 * routePrice.fee;
      const createSendTransactionCommand: CreateTransactionCommand = {
        passengerId: command.passengerId,
        amount: withdrawBalance,
        method: PaymentMethod.BookingWithdraw,
        status: PaymentStatus.Success,
        returnUrl: '',
        tradeNumber: '',
        tradeDate: null,
        transactionNumber: '',
        reason: `Booking Withdrawal`,
      };
      this.eventEmitter.emit(
        'create.transaction',
        createSendTransactionCommand,
      );
      this.eventEmitter.emit('decrease.assignment.seats', assignment.id);
    }
    return bookingResponse;
  }
  async createMultipleBookings(
    command: CreateMultipleBookingCommand,
  ): Promise<BookingResponse[]> {
    const bookingResponses = [];
    let routePriceSum = 0;
    let assignments = [];
    const route = await this.routeQueries.getRoute(command.routeId);
    for (const assignmentDate of command.assignmentDate) {
      const query = new CollectionQuery();
      query.filter = [
        [
          {
            field: 'vehicleCategoryId',
            operator: FilterOperators.EqualTo,
            value: command.categoryId,
          },
        ],
        [
          {
            field: 'routeId',
            operator: FilterOperators.EqualTo,
            value: command.routeId,
          },
        ],
        [
          {
            field: 'assignmentDate',
            operator: FilterOperators.EqualTo,
            value: assignmentDate,
          },
        ],
        [
          {
            field: 'pickupTime',
            operator: FilterOperators.EqualTo,
            value: command.pickupTime,
          },
        ],
        [
          {
            field: 'status',
            operator: FilterOperators.NotIn,
            value: [
              AssignmentStatus.COMPLETED,
              AssignmentStatus.CANCELLED,
              AssignmentStatus.STARTED,
            ],
          },
        ],
        [
          {
            field: 'availableSeats',
            operator: FilterOperators.GreaterThan,
            value: 0,
          },
        ],
      ];
      query.top = 1;
      query.orderBy = [
        {
          field: 'availableSeats',
          direction: 'DESC',
        },
      ];
      query.includes = ['driver'];
      const assignmentsData = await this.assignmentQueries.getAssignments(
        query,
      );
      if (
        !assignmentsData ||
        !assignmentsData.data ||
        !assignmentsData.data[0]
      ) {
        throw new BadRequestException(
          `We didn't assigned a vehicle on ${command.pickupTime} of ${assignmentDate}`,
        );
      }
      const assignment = assignmentsData.data[0];

      const existingBook =
        await this.bookingRepository.getByAssignmentIdAndPassengerId(
          assignment.id,
          command.passengerId,
        );
      if (existingBook) {
        if (existingBook.status !== BookingStatus.CANCELLED)
          throw new NotFoundException(
            `You have already reserved on ${assignmentDate}`,
          );
      }

      const vehicle = await this.vehicleRepository.findOneBy({
        id: assignment.driver.vehicleId,
      });
      //Todo ==> Booking fee calculation
      const routePrice = route.prices.find(
        (price) => price.categoryId === vehicle.categoryId,
      );
      if (!routePrice) {
        throw new BadRequestException(`Route Price not configured`);
      }
      routePriceSum += routePrice.fee;
      assignments.push(assignment);
    }
    const customerWallet = await this.walletRepository.getByPassengerId(
      command.passengerId,
    );
    const customerWalletBalance =
      command.walletType === WalletType.IndividualWallet
        ? customerWallet.balance
        : customerWallet.corporateWalletBalance;
    if (!customerWallet || customerWalletBalance < routePriceSum) {
      throw new BadRequestException(
        `You don't have enough balance to order this request`,
      );
    }
    for (const assignment of assignments) {
      const vehicle = await this.vehicleRepository.findOneBy({
        id: assignment.driver.vehicleId,
      });
      // //Todo ==> Booking fee calculation
      const routePrice = route.prices.find(
        (price) => price.categoryId === vehicle.categoryId,
      );
      if (assignment.availableSeats <= 0) {
        throw new BadRequestException(`All available seats already reserved`);
      }
      const createBookingCommand = new CreateBookingCommand2();
      createBookingCommand.assignmentId = command.assignmentId;
      createBookingCommand.routeId = command.routeId;
      createBookingCommand.pickupId = command.pickupId;
      createBookingCommand.pickupName = command.pickupName;
      createBookingCommand.destinationId = command.destinationId;
      createBookingCommand.destinationName = command.destinationName;
      createBookingCommand.walletType = command.walletType;
      createBookingCommand.pickupTime = command.pickupTime;
      createBookingCommand.assignmentDate = assignment.assignmentDate;
      createBookingCommand.categoryId = command.categoryId;

      const bookingDomain =
        CreateBookingCommand2.fromCommand(createBookingCommand);
      bookingDomain.vehicleId = assignment.vehicleId;
      bookingDomain.vehiclePlateNumber = assignment.vehiclePlateNumber;
      bookingDomain.driverId = assignment.driverId;
      bookingDomain.routeName = route.name;
      bookingDomain.assignmentDate = assignment.assignmentDate;
      bookingDomain.fee = routePrice.fee;
      bookingDomain.categoryId = command.categoryId;
      bookingDomain.assignmentId = assignment.id;
      bookingDomain.passengerGender = command.passengerGender;
      bookingDomain.passengerId = command.passengerId;
      bookingDomain.passengerName = command.passengerName;
      bookingDomain.passengerPhoneNumber = command.passengerPhoneNumber;
      bookingDomain.createdAt = new Date();
      const booking = await this.bookingRepository.insert(bookingDomain);
      const bookingResponse = BookingResponse.fromDomain(booking);
      if (booking) {
        if (command.walletType === WalletType.CorporateWallet) {
          customerWallet.corporateWalletBalance -= routePrice.fee;
        } else {
          customerWallet.balance -= routePrice.fee;
        }
        const wallet = await this.walletRepository.update(customerWallet);
        // console.log('wallet before booking.........', customerWallet);

        // console.log('wallet after booking.........', wallet);
        bookingResponse['wallet'] = WalletResponse.fromDomain(wallet);
        const withdrawBalance = -1 * routePrice.fee;
        const createSendTransactionCommand: CreateTransactionCommand = {
          passengerId: command.passengerId,
          amount: withdrawBalance,
          method: PaymentMethod.BookingWithdraw,
          status: PaymentStatus.Success,
          returnUrl: '',
          tradeNumber: '',
          tradeDate: null,
          transactionNumber: '',
          reason: `Booking Withdrawal`,
        };
        this.eventEmitter.emit(
          'create.transaction',
          createSendTransactionCommand,
        );
        this.eventEmitter.emit('decrease.assignment.seats', assignment.id);
      }
      bookingResponses.push(bookingResponse);
    }
    return bookingResponses;
  }
  async archiveBooking(command: ArchiveBookingCommand): Promise<boolean> {
    const bookingDomain = await this.bookingRepository.getById(command.id);
    if (!bookingDomain) {
      throw new NotFoundException(`Booking not found with id ${command.id}`);
    }
    return await this.bookingRepository.archive(command.id);
  }
  async restoreBooking(
    id: string,
    currentUser: UserInfo,
  ): Promise<BookingResponse> {
    const bookingDomain = await this.bookingRepository.getById(id, true);
    if (!bookingDomain) {
      throw new NotFoundException(`Booking not found with id ${id}`);
    }
    const r = await this.bookingRepository.restore(id);
    if (r) {
      bookingDomain.deletedAt = null;
    }
    return BookingResponse.fromDomain(bookingDomain);
  }
  async deleteBooking(id: string, currentUser: UserInfo): Promise<boolean> {
    const bookingDomain = await this.bookingRepository.getById(id, true);
    if (!bookingDomain) {
      throw new NotFoundException(`Booking not found with id ${id}`);
    }
    return await this.bookingRepository.delete(id);
  }
  async completeBooking(id: string): Promise<BookingResponse> {
    const bookingDomain = await this.bookingRepository.getById(id);
    if (!bookingDomain) {
      throw new NotFoundException(`Booking not found with id ${id}`);
    }
    if (bookingDomain.status !== BookingStatus.BOARDED) {
      throw new BadRequestException(
        `This booking is either cancelled or already completed`,
      );
    }
    bookingDomain.status = BookingStatus.ARRIVED;
    bookingDomain.completedAt = new Date();
    const result = await this.bookingRepository.update(bookingDomain);
    return BookingResponse.fromDomain(result);
  }
  @OnEvent('complete.bookings')
  async completeAssignmentBooking(
    assignmentId: string,
  ): Promise<BookingResponse[]> {
    const trips = await this.bookingRepository.getByAssignmentId(
      assignmentId,
      BookingStatus.BOARDED,
    );
    const completedAt = new Date();
    trips.forEach((booking) => {
      booking.status = BookingStatus.ARRIVED;
      booking.completedAt = completedAt;
    });
    const result = await this.bookingRepository.updateMany(trips);
    const missedTrips = await this.bookingRepository.getByAssignmentId(
      assignmentId,
      BookingStatus.CREATED,
    );
    missedTrips.forEach((booking) => {
      booking.status = BookingStatus.MISSED;
    });
    await this.bookingRepository.updateMany(missedTrips);
    return result.map((booking) => BookingResponse.fromDomain(booking));
  }
  async startBooking(
    id: string,
    currentUser: UserInfo,
  ): Promise<BookingResponse> {
    const bookingDomain = await this.bookingRepository.getById(id);
    if (!bookingDomain) {
      throw new NotFoundException(`Booking not found with id ${id}`);
    }
    if (bookingDomain.status !== BookingStatus.CREATED) {
      throw new BadRequestException(
        `This booking is either cancelled or completed or already started`,
      );
    }
    bookingDomain.status = BookingStatus.BOARDED;
    bookingDomain.startingAt = new Date();
    const result = await this.bookingRepository.update(bookingDomain);
    return BookingResponse.fromDomain(result);
  }
  async cancelBooking(command: CancelBookingCommand): Promise<BookingResponse> {
    const bookingDomain = await this.bookingRepository.getById(command.id);
    if (!bookingDomain) {
      throw new NotFoundException(`Booking not found with id ${command.id}`);
    }
    if (
      bookingDomain.status === BookingStatus.BOARDED ||
      bookingDomain.status === BookingStatus.ARRIVED
    ) {
      throw new BadRequestException(`You can't Cancel this booking`);
    }

    bookingDomain.status = BookingStatus.CANCELLED;
    bookingDomain.cancellationReason = command.reason;
    bookingDomain.cancelledAt = new Date();
    const result = await this.bookingRepository.update(bookingDomain);
    if (result) {
      this.eventEmitter.emit(
        'increase.assignment.seats',
        bookingDomain.assignmentId,
      );
    }
    return BookingResponse.fromDomain(result);
  }
}
