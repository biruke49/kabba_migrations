import { BusStopResponse } from '@router/usecases/cities/bus-stop.response';
import { PassengerResponse } from '@customer/usecases/passengers/passenger.response';
import { ApiProperty } from '@nestjs/swagger';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { CancellationReason } from '@router/domains/assignments/cancelled-reason';
import { AssignmentResponse } from '@router/usecases/assignments/assignment.response';
import { RouteResponse } from '@router/usecases/routes/route.response';
import { Booking } from '@order/domains/bookings/booking';
import { VehicleResponse } from '@provider/usecases/owners/vehicle.response';

export class BookingResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  assignmentId: string;
  @ApiProperty()
  assignmentDate: Date;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  passengerId: string;
  @ApiProperty()
  passengerName: string;
  @ApiProperty()
  passengerPhoneNumber: string;
  @ApiProperty()
  passengerGender: string;
  @ApiProperty()
  vehicleId: string;
  @ApiProperty()
  vehiclePlateNumber: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  fee: number;
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  routeName: string;
  @ApiProperty()
  pickupId: string;
  @ApiProperty()
  destinationId: string;
  @ApiProperty()
  pickupName: string;
  @ApiProperty()
  destinationName: string;
  @ApiProperty()
  startingAt: Date;
  @ApiProperty()
  completedAt: Date;
  @ApiProperty()
  cancellationReason: CancellationReason;
  @ApiProperty()
  cancelledAt: Date;
  @ApiProperty()
  walletType: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  route?: RouteResponse;
  driver?: DriverResponse;
  assignment?: AssignmentResponse;
  passenger?: PassengerResponse;
  vehicle?: VehicleResponse;
  pickup?: BusStopResponse;
  destination?: BusStopResponse;
  static fromEntity(bookingEntity: BookingEntity): BookingResponse {
    const bookingResponse = new BookingResponse();
    bookingResponse.id = bookingEntity.id;
    bookingResponse.routeId = bookingEntity.routeId;
    bookingResponse.routeName = bookingEntity.routeName;
    bookingResponse.driverId = bookingEntity.driverId;
    bookingResponse.vehicleId = bookingEntity.vehicleId;
    bookingResponse.vehiclePlateNumber = bookingEntity.vehiclePlateNumber;
    bookingResponse.assignmentId = bookingEntity.assignmentId;
    bookingResponse.assignmentDate = bookingEntity.assignmentDate;
    bookingResponse.passengerId = bookingEntity.passengerId;
    bookingResponse.passengerName = bookingEntity.passengerName;
    bookingResponse.passengerPhoneNumber = bookingEntity.passengerPhoneNumber;
    bookingResponse.passengerGender = bookingEntity.passengerGender;
    bookingResponse.fee = bookingEntity.fee;
    bookingResponse.cancelledAt = bookingEntity.cancelledAt;
    bookingResponse.cancellationReason = bookingEntity.cancellationReason;
    bookingResponse.categoryId = bookingEntity.categoryId;
    bookingResponse.pickupId = bookingEntity.pickupId;
    bookingResponse.pickupName = bookingEntity.pickupName;
    bookingResponse.destinationId = bookingEntity.destinationId;
    bookingResponse.destinationName = bookingEntity.destinationName;
    bookingResponse.startingAt = bookingEntity.startingAt;
    bookingResponse.completedAt = bookingEntity.completedAt;
    bookingResponse.status = bookingEntity.status;
    bookingResponse.walletType = bookingEntity.walletType;
    bookingResponse.createdBy = bookingEntity.createdBy;
    bookingResponse.updatedBy = bookingEntity.updatedBy;
    bookingResponse.deletedBy = bookingEntity.deletedBy;
    bookingResponse.createdAt = bookingEntity.createdAt;
    bookingResponse.updatedAt = bookingEntity.updatedAt;
    bookingResponse.deletedAt = bookingEntity.deletedAt;
    if (bookingEntity.route) {
      bookingResponse.route = RouteResponse.fromEntity(bookingEntity.route);
    }
    if (bookingEntity.driver) {
      bookingResponse.driver = DriverResponse.fromEntity(bookingEntity.driver);
    }
    if (bookingEntity.passenger) {
      bookingResponse.passenger = PassengerResponse.fromEntity(
        bookingEntity.passenger,
      );
    }
    if (bookingEntity.assignment) {
      bookingResponse.assignment = AssignmentResponse.fromEntity(
        bookingEntity.assignment,
      );
    }
    if (bookingEntity.vehicle) {
      bookingResponse.vehicle = VehicleResponse.fromEntity(
        bookingEntity.vehicle,
      );
    }
    if (bookingEntity.pickup) {
      bookingResponse.pickup = BusStopResponse.fromEntity(bookingEntity.pickup);
    }
    if (bookingEntity.destination) {
      bookingResponse.destination = BusStopResponse.fromEntity(
        bookingEntity.destination,
      );
    }
    return bookingResponse;
  }
  static fromDomain(booking: Booking): BookingResponse {
    const bookingResponse = new BookingResponse();
    bookingResponse.id = booking.id;
    bookingResponse.routeId = booking.routeId;
    bookingResponse.routeName = booking.routeName;
    bookingResponse.driverId = booking.driverId;
    bookingResponse.vehicleId = booking.vehicleId;
    bookingResponse.vehiclePlateNumber = booking.vehiclePlateNumber;
    bookingResponse.assignmentId = booking.assignmentId;
    bookingResponse.assignmentDate = booking.assignmentDate;
    bookingResponse.passengerId = booking.passengerId;
    bookingResponse.passengerName = booking.passengerName;
    bookingResponse.passengerPhoneNumber = booking.passengerPhoneNumber;
    bookingResponse.passengerGender = booking.passengerGender;
    bookingResponse.fee = booking.fee;
    bookingResponse.cancelledAt = booking.cancelledAt;
    bookingResponse.cancellationReason = booking.cancellationReason;
    bookingResponse.categoryId = booking.categoryId;
    bookingResponse.pickupId = booking.pickupId;
    bookingResponse.pickupName = booking.pickupName;
    bookingResponse.destinationId = booking.destinationId;
    bookingResponse.destinationName = booking.destinationName;
    bookingResponse.startingAt = booking.startingAt;
    bookingResponse.completedAt = booking.completedAt;
    bookingResponse.status = booking.status;
    bookingResponse.walletType = booking.walletType;
    bookingResponse.createdBy = booking.createdBy;
    bookingResponse.updatedBy = booking.updatedBy;
    bookingResponse.deletedBy = booking.deletedBy;
    bookingResponse.createdAt = booking.createdAt;
    bookingResponse.updatedAt = booking.updatedAt;
    bookingResponse.deletedAt = booking.deletedAt;
    if (booking.route) {
      bookingResponse.route = RouteResponse.fromDomain(booking.route);
    }
    if (booking.driver) {
      bookingResponse.driver = DriverResponse.fromDomain(booking.driver);
    }
    if (booking.passenger) {
      bookingResponse.passenger = PassengerResponse.fromDomain(
        booking.passenger,
      );
    }
    if (booking.assignment) {
      bookingResponse.assignment = AssignmentResponse.fromDomain(
        booking.assignment,
      );
    }
    if (booking.vehicle) {
      bookingResponse.vehicle = VehicleResponse.fromDomain(booking.vehicle);
    }
    if (booking.pickup) {
      bookingResponse.pickup = BusStopResponse.fromDomain(booking.pickup);
    }
    if (booking.destination) {
      bookingResponse.destination = BusStopResponse.fromDomain(
        booking.destination,
      );
    }
    return bookingResponse;
  }
}
export class CountByCreatedAtResponse {
  @ApiProperty()
  createdAt: string;
  @ApiProperty()
  count: number;
}
export class CountEarningsByCreatedAtResponse {
  @ApiProperty()
  createdAt: string;
  @ApiProperty()
  total: number;
}

export class CountWeeklyBookingByCreatedAtResponse {
  @ApiProperty()
  createdAt: string;
  @ApiProperty()
  total: number;
  @ApiProperty()
  numberOfTrips: number;
}
export class CountBookingByAssignmentResponse {
  @ApiProperty()
  assignment: string;
  @ApiProperty()
  count: number;
}
export class CountBookingByRouteResponse {
  @ApiProperty()
  routeId: string;
  @ApiProperty()
  count: number;
}
