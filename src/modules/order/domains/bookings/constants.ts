export enum BookingStatus {
  CREATED = 'Created',
  BOARDED = 'Boarded',
  ARRIVED = 'Arrived',
  CANCELLED = 'Cancelled',
  MISSED = 'Missed',
}
