import { Booking } from './booking';
export interface IBookingRepository {
  insert(booking: Booking): Promise<Booking>;
  update(booking: Booking): Promise<Booking>;
  updateMany(bookings: Booking[]): Promise<Booking[]>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Booking[]>;
  getById(
    id: string,
    withDeleted: boolean,
    relations: string[],
  ): Promise<Booking>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
  getByAssignmentIdAndPassengerId(
    assignmentId: string,
    passengerId: string,
    withDeleted: boolean,
  ): Promise<Booking>;
  getByAssignmentId(
    assignmentId: string,
    status?: string,
    withDeleted?: boolean,
  ): Promise<Booking[]>;
}
