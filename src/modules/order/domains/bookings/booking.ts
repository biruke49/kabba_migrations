import { BusStop } from '@router/domains/cities/bus-stop';
import { Assignment } from '@router/domains/assignments/assignment';
import { Driver } from '@provider/domains/drivers/driver';
import { Passenger } from '@customer/domains/passengers/passenger';
import { CancellationReason } from '@router/domains/assignments/cancelled-reason';
import { Route } from '@router/domains/routes/route';
import { Vehicle } from '@provider/domains/owners/vehicle';

export class Booking {
  id: string;
  assignmentId: string;
  assignmentDate: Date;
  driverId: string;
  passengerId: string;
  passengerName: string;
  passengerPhoneNumber: string;
  passengerGender: string;
  vehicleId: string;
  vehiclePlateNumber: string;
  status: string;
  fee: number;
  routeId: string;
  routeName: string;
  pickupId: string;
  destinationId: string;
  pickupName: string;
  destinationName: string;
  startingAt: Date;
  completedAt: Date;
  cancellationReason: CancellationReason;
  cancelledAt: Date;
  walletType: string;
  categoryId: string;
  passenger?: Passenger;
  driver?: Driver;
  assignment?: Assignment;
  vehicle?: Vehicle;
  route?: Route;
  pickup?: BusStop;
  destination?: BusStop;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
