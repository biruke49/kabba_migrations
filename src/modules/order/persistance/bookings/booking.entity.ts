import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { WalletType } from '@libs/common/enums';
import { BookingStatus } from '@order/domains/bookings/constants';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import { CancellationReason } from '@router/domains/assignments/cancelled-reason';
import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { BusStopEntity } from '@router/persistence/cities/bus-stop.entity';
import { RouteEntity } from '@router/persistence/routes/route.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Entity('bookings')
export class BookingEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'assignment_id', type: 'uuid' })
  assignmentId: string;
  @Column({ name: 'driver_id', type: 'uuid' })
  driverId: string;
  @Column({ name: 'passenger_id', type: 'uuid' })
  passengerId: string;
  @Column({ name: 'passenger_name', nullable: true })
  passengerName: string;
  @Column({ name: 'passenger_phone_number', nullable: true })
  passengerPhoneNumber: string;
  @Column({ name: 'passenger_gender', nullable: true })
  passengerGender: string;
  @Column({ name: 'vehicle_id', type: 'uuid' })
  vehicleId: string;
  @Column({ name: 'vehicle_plate_number', nullable: true })
  vehiclePlateNumber: string;
  @Column({ default: BookingStatus.CREATED })
  status: string;
  @Column({ type: 'float' })
  fee: number;
  @Column({ name: 'route_id', type: 'uuid' })
  routeId: string;
  @Column({ name: 'route_name', nullable: true })
  routeName: string;
  @Column({ name: 'pickup_id', type: 'uuid' })
  pickupId: string;
  @Column({ name: 'destination_id', type: 'uuid' })
  destinationId: string;
  @Column({ name: 'pickup_name', nullable: true })
  pickupName: string;
  @Column({ name: 'destination_name', nullable: true })
  destinationName: string;
  @Column({ name: 'starting_at', type: 'timestamptz', nullable: true })
  startingAt: Date;
  @Column({ name: 'completed_at', type: 'timestamptz', nullable: true })
  completedAt: Date;
  @Column({ type: 'jsonb', nullable: true, name: 'cancellation_reason' })
  cancellationReason: CancellationReason;
  @Column({ name: 'cancelled_at', type: 'timestamptz', nullable: true })
  cancelledAt: Date;
  @Column({ name: 'wallet_type', default: WalletType.IndividualWallet })
  walletType: string;
  @Column({ name: 'category_id', nullable: true })
  categoryId: string;
  @Column({ name: 'assignment_date', type: 'date' })
  assignmentDate: Date;
  @ManyToOne(() => PassengerEntity, (passenger) => passenger.bookings, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'passenger_id' })
  passenger: PassengerEntity;
  @ManyToOne(() => DriverEntity, (driver) => driver.bookings, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @ManyToOne(() => RouteEntity, (route) => route.bookings, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'route_id' })
  route: RouteEntity;
  @ManyToOne(() => AssignmentEntity, (assignment) => assignment.bookings, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'assignment_id' })
  assignment: AssignmentEntity;
  @ManyToOne(() => VehicleEntity, (vehicle) => vehicle.bookings, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'vehicle_id' })
  vehicle: VehicleEntity;

  @ManyToOne(() => BusStopEntity, (pickup) => pickup.pickups, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'pickup_id' })
  pickup: BusStopEntity;

  @ManyToOne(() => BusStopEntity, (destination) => destination.destinations, {
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'destination_id' })
  destination: BusStopEntity;
}
