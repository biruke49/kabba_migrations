import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Booking } from '@order/domains/bookings/booking';
import { IBookingRepository } from '@order/domains/bookings/booking.repository.interface';
import { Repository } from 'typeorm';
import { BookingEntity } from './booking.entity';

@Injectable()
export class BookingRepository implements IBookingRepository {
  constructor(
    @InjectRepository(BookingEntity)
    private bookingRepository: Repository<BookingEntity>,
  ) {}
  async insert(booking: Booking): Promise<Booking> {
    const bookingEntity = this.toBookingEntity(booking);
    // console.log(bookingEntity);
    const results = await this.bookingRepository.save(bookingEntity);
    return results ? this.toBooking(results) : null;
  }
  async update(booking: Booking): Promise<Booking> {
    const bookingEntity = this.toBookingEntity(booking);
    const result = await this.bookingRepository.save(bookingEntity);
    return result ? this.toBooking(result) : null;
  }
  async updateMany(bookings: Booking[]): Promise<Booking[]> {
    const bookingEntities = bookings.map((booking) => {
      return this.toBookingEntity(booking);
    });
    const result = await this.bookingRepository.save(bookingEntities);
    return result && result.length > 0
      ? result.map((booking) => this.toBooking(booking))
      : [];
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.bookingRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Booking[]> {
    const bookings = await this.bookingRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!bookings.length) {
      return null;
    }
    return bookings.map((booking) => this.toBooking(booking));
  }
  async getById(
    id: string,
    withDeleted = false,
    relations = [],
  ): Promise<Booking> {
    const booking = await this.bookingRepository.find({
      where: { id: id },
      relations: relations,
      withDeleted: withDeleted,
    });
    if (!booking[0]) {
      return null;
    }
    return this.toBooking(booking[0]);
  }
  async getByAssignmentIdAndPassengerId(
    assignmentId: string,
    passengerId: string,
    withDeleted = false,
  ): Promise<Booking> {
    const booking = await this.bookingRepository.find({
      where: { assignmentId: assignmentId, passengerId: passengerId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!booking[0]) {
      return null;
    }
    return this.toBooking(booking[0]);
  }
  async getByAssignmentId(
    assignmentId: string,
    status?: string,
    withDeleted = false,
  ): Promise<Booking[]> {
    const bookings = await this.bookingRepository.find({
      where: { assignmentId: assignmentId, status: status },
      relations: [],
      withDeleted: withDeleted,
    });
    if (bookings.length === 0) {
      return [];
    }
    return bookings.map((booking) => this.toBooking(booking));
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.bookingRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.bookingRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toBooking(bookingEntity: BookingEntity): Booking {
    const booking = new Booking();
    booking.id = bookingEntity.id;
    booking.routeId = bookingEntity.routeId;
    booking.routeName = bookingEntity.routeName;
    booking.driverId = bookingEntity.driverId;
    booking.vehicleId = bookingEntity.vehicleId;
    booking.passengerId = bookingEntity.passengerId;
    booking.passengerName = bookingEntity.passengerName;
    booking.passengerPhoneNumber = bookingEntity.passengerPhoneNumber;
    booking.passengerGender = bookingEntity.passengerGender;
    booking.vehiclePlateNumber = bookingEntity.vehiclePlateNumber;
    booking.fee = bookingEntity.fee;
    booking.status = bookingEntity.status;
    booking.categoryId = bookingEntity.categoryId;
    booking.cancellationReason = bookingEntity.cancellationReason;
    booking.cancelledAt = bookingEntity.cancelledAt;
    booking.assignmentId = bookingEntity.assignmentId;
    booking.assignmentDate = bookingEntity.assignmentDate;
    booking.pickupId = bookingEntity.pickupId;
    booking.pickupName = bookingEntity.pickupName;
    booking.startingAt = bookingEntity.startingAt;
    booking.completedAt = bookingEntity.completedAt;
    booking.destinationId = bookingEntity.destinationId;
    booking.destinationName = bookingEntity.destinationName;
    booking.walletType = bookingEntity.walletType;
    booking.createdBy = bookingEntity.createdBy;
    booking.updatedBy = bookingEntity.updatedBy;
    booking.deletedBy = bookingEntity.deletedBy;
    booking.createdAt = bookingEntity.createdAt;
    booking.updatedAt = bookingEntity.updatedAt;
    booking.deletedAt = bookingEntity.deletedAt;
    return booking;
  }
  toBookingEntity(booking: Booking): BookingEntity {
    const bookingEntity = new BookingEntity();
    bookingEntity.id = booking.id;
    bookingEntity.routeId = booking.routeId;
    bookingEntity.routeName = booking.routeName;
    bookingEntity.driverId = booking.driverId;
    bookingEntity.vehicleId = booking.vehicleId;
    bookingEntity.passengerId = booking.passengerId;
    bookingEntity.passengerName = booking.passengerName;
    bookingEntity.passengerPhoneNumber = booking.passengerPhoneNumber;
    bookingEntity.passengerGender = booking.passengerGender;
    bookingEntity.vehiclePlateNumber = booking.vehiclePlateNumber;
    bookingEntity.fee = booking.fee;
    bookingEntity.status = booking.status;
    bookingEntity.categoryId = booking.categoryId;
    bookingEntity.cancellationReason = booking.cancellationReason;
    bookingEntity.cancelledAt = booking.cancelledAt;
    bookingEntity.assignmentId = booking.assignmentId;
    bookingEntity.assignmentDate = booking.assignmentDate;
    bookingEntity.pickupId = booking.pickupId;
    bookingEntity.pickupName = booking.pickupName;
    bookingEntity.startingAt = booking.startingAt;
    bookingEntity.completedAt = booking.completedAt;
    bookingEntity.destinationId = booking.destinationId;
    bookingEntity.destinationName = booking.destinationName;
    bookingEntity.walletType = booking.walletType;
    bookingEntity.createdBy = booking.createdBy;
    bookingEntity.updatedBy = booking.updatedBy;
    bookingEntity.deletedBy = booking.deletedBy;
    bookingEntity.createdAt = booking.createdAt;
    bookingEntity.updatedAt = booking.updatedAt;
    bookingEntity.deletedAt = booking.deletedAt;
    return bookingEntity;
  }
}
