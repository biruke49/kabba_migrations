import { EmergencyContact } from '@libs/common/emergency-contact';
import { FileDto } from '@libs/common/file-dto';
import { Address } from '@libs/common/address';
export class User {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  gender: string;
  emergencyContact: EmergencyContact;
  enabled: boolean;
  profileImage: FileDto;
  address: Address;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
