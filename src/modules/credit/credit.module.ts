import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { CorporateWalletsController } from './controllers/corporate-wallet.controller';
import { TransactionQuery } from './usecases/transactions/transaction.usecase.queries';
import { TransactionCommands } from './usecases/transactions/transaction.usecase.commands';
import { TransactionRepository } from './persistence/transactions/transaction.repository';
import { TransactionEntity } from '@credit/persistence/transactions/transaction.entity';
import { WalletCommands } from './usecases/wallets/wallet.usecase.commands';
import { WalletQuery } from '@credit/usecases/wallets/wallet.usecase.queries';
import { WalletRepository } from './persistence/wallets/wallet.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { WalletEntity } from './persistence/wallets/wallet.entity';
import { WalletsController } from './controllers/wallet.controller';
import { TransactionsController } from './controllers/transaction.controller';
import { PaymentService } from '@libs/payment/chapa/services/payment.service';
import { CorporateTransactionEntity } from './persistence/corporate_transaction/corporate-transaction.entity';
import { CorporateWalletEntity } from './persistence/corporate-wallet/corporate-wallet.entity';
import { CorporateTransactionsController } from './controllers/corporate-transaction.controller';
import { CorporateWalletRepository } from './persistence/corporate-wallet/corporate-wallet.repository';
import { CorporateWalletQuery } from './usecases/corporate-wallets/corporate-wallet.usecase.queries';
import { CorporateWalletCommands } from './usecases/corporate-wallets/corporate-wallet.usecase.commands';
import { CorporateTransactionRepository } from './persistence/corporate_transaction/corporate-transaction.repository';
import { CorporateTransactionCommands } from './usecases/corporate-transactions/corporate-transaction.usecase.commands';
import { CorporateTransactionQuery } from './usecases/corporate-transactions/corporate-transaction.usecase.queries';
import { CustomerModule } from '@customer/customer.module';
import { PassengerQuery } from '@customer/usecases/passengers/passenger.usecase.queries';
import { PassengerRepository } from '@customer/persistence/passengers/passenger.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      WalletEntity,
      TransactionEntity,
      CorporateWalletEntity,
      CorporateTransactionEntity,
      PassengerEntity,
    ]),
    CustomerModule,
  ],
  controllers: [
    WalletsController,
    TransactionsController,
    CorporateWalletsController,
    CorporateTransactionsController,
  ],
  providers: [
    WalletRepository,
    WalletQuery,
    WalletCommands,
    TransactionRepository,
    TransactionCommands,
    TransactionQuery,
    PaymentService,
    CorporateWalletRepository,
    CorporateWalletQuery,
    CorporateWalletCommands,
    CorporateTransactionRepository,
    CorporateTransactionCommands,
    CorporateTransactionQuery,
    PassengerRepository,
  ],
  exports: [],
})
export class CreditModule {}
