import { DepositedBy } from './../../domains/transactions/deposited-by';
import { Transaction } from '@credit/domains/transactions/transaction';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPositive } from 'class-validator';

export class UpdateTransactionCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  @ApiProperty()
  //  @IsNotEmpty()
  passengerId: string;
  @ApiProperty()
  depositedBy: DepositedBy;
  @ApiProperty()
  tradeDate: Date;
  @ApiProperty()
  tradeNumber: string;
  @ApiProperty()
  transactionNumber: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  method: string;
}
export class CreateTransactionCommand {
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  @ApiProperty()
  @IsNotEmpty()
  passengerId: string;
  @ApiProperty()
  depositedBy?: DepositedBy;
  @ApiProperty()
  tradeDate?: Date;
  @ApiProperty()
  tradeNumber?: string;
  @ApiProperty()
  transactionNumber?: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  method?: string;
  returnUrl?: string;
  reason?: string;
  static fromCommand(command: CreateTransactionCommand): Transaction {
    const transaction = new Transaction();
    transaction.passengerId = command.passengerId;
    transaction.amount = command.amount;
    transaction.tradeDate = command.tradeDate;
    transaction.transactionNumber = command.transactionNumber;
    transaction.status = command.status;
    transaction.method = command.method;
    transaction.depositedBy = command.depositedBy;
    transaction.tradeNumber = command.tradeNumber;
    transaction.reason = command.reason;
    return transaction;
  }
}
export class PayWithChapaCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  amount: number;
  // @ApiProperty()
  // @IsNotEmpty()
  passengerId: string;
  depositedBy: DepositedBy;
  // @ApiProperty()
  // tradeDate: Date;
  // @ApiProperty()
  // tradeNumber: string;
  // @ApiProperty()
  // transactionNumber: string;
  // @ApiProperty()
  status: string;
  //@ApiProperty()
  method: string;
  @ApiProperty()
  //@IsNotEmpty()
  returnUrl: string;
  reason?: string;
  static fromCommand(command: PayWithChapaCommand): Transaction {
    const transaction = new Transaction();
    transaction.passengerId = command.passengerId;
    transaction.amount = command.amount;
    transaction.status = command.status;
    transaction.method = command.method;
    transaction.depositedBy = command.depositedBy;
    transaction.reason = command.reason;
    return transaction;
  }
}
