import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TransactionEntity } from '@credit/persistence/transactions/transaction.entity';
import { Repository } from 'typeorm';
import {
  GroupByStatusResponse,
  TransactionResponse,
} from './transaction.response';
import { PaymentMethod, PaymentStatus } from '@libs/common/enums';
import { CountBookingByAssignmentResponse } from '@order/usecases/bookings/booking.response';
import { CorporateTransactionQuery } from '../corporate-transactions/corporate-transaction.usecase.queries';
@Injectable()
export class TransactionQuery {
  constructor(
    @InjectRepository(TransactionEntity)
    private transactionRepository: Repository<TransactionEntity>,
    private corporateQuery: CorporateTransactionQuery,
  ) {}
  async getTransaction(
    id: string,
    withDeleted = false,
  ): Promise<TransactionResponse> {
    const transaction = await this.transactionRepository.find({
      where: { id: id },
      relations: ['passenger'],
      withDeleted: withDeleted,
    });
    if (!transaction[0]) {
      throw new NotFoundException(`Transaction not found with id ${id}`);
    }
    return TransactionResponse.fromEntity(transaction[0]);
  }
  async getTransactions(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<TransactionResponse>> {
    const dataQuery = QueryConstructor.constructQuery<TransactionEntity>(
      this.transactionRepository,
      query,
    );
    const d = new DataResponseFormat<TransactionResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => TransactionResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }

  async getPassengerTransaction(
    passengerId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<TransactionResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'passengerId',
        value: passengerId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<TransactionEntity>(
      this.transactionRepository,
      query,
    );
    const d = new DataResponseFormat<TransactionResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => TransactionResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getTotalTransactionDeposits(query: CollectionQuery): Promise<any> {
    const dataQuery = QueryConstructor.constructQuery<TransactionEntity>(
      this.transactionRepository,
      query,
    );
    const data = await dataQuery
      .select('SUM(amount)', 'total')
      .andWhere('status= :status', { status: PaymentStatus.Success })
      .andWhere('method!= :method', { method: PaymentMethod.WalletTransfer })
      .andWhere('method!= :method', {
        method: PaymentMethod.WalletTransferFromCorporate,
      })
      .andWhere('amount> 0')
      .getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async getTotalDeposits(query: CollectionQuery): Promise<any> {
    const dataQuery = QueryConstructor.constructQuery<TransactionEntity>(
      this.transactionRepository,
      query,
    );
    const data = await dataQuery
      .select('SUM(amount)', 'total')
      .andWhere('status= :status', { status: PaymentStatus.Success })
      .andWhere('method!= :method', { method: PaymentMethod.WalletTransfer })
      .andWhere('method!= :method', {
        method: PaymentMethod.WalletTransferFromCorporate,
      })
      .andWhere('amount > 0')
      .getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async getTotalTransactionDepositsSum(query: CollectionQuery): Promise<any> {
    const passengerTotal = await this.getTotalDeposits(query);
    const corporateTotal = await this.corporateQuery.getTotalCorporateDeposits(
      query,
    );
    const sum = parseInt(passengerTotal.total) + parseInt(corporateTotal.total);
    return { total: sum ? sum : 0 };
  }
  async groupTransactionsByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push(
      `transactions.status as status`,
      'COUNT(transactions.id)',
    );
    const dataQuery = QueryConstructor.constructQuery<TransactionEntity>(
      this.transactionRepository,
      query,
    );

    const data = await dataQuery.groupBy(`status`).getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.status;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getArchivedTransactions(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<TransactionResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<TransactionEntity>(
      this.transactionRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<TransactionResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => TransactionResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
