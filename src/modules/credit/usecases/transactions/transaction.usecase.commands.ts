import {
  CreateTransactionCommand,
  UpdateTransactionCommand,
} from './transaction.commands';
import { TransactionRepository } from '@credit/persistence/transactions/transaction.repository';
import { TransactionResponse } from './transaction.response';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { InitiateTeleBirrResponse } from '@credit/domains/initiate-telebirr.response';
import { PaymentStatus } from '@libs/common/enums';
@Injectable()
export class TransactionCommands {
  constructor(
    private transactionRepository: TransactionRepository,
    private eventEmitter: EventEmitter2,
  ) {}
  @OnEvent('create.transaction')
  async createTransaction(
    command: CreateTransactionCommand,
  ): Promise<TransactionResponse> {
    const transactionDomain = CreateTransactionCommand.fromCommand(command);
    const transaction = await this.transactionRepository.insert(
      transactionDomain,
    );
    return TransactionResponse.fromDomain(transaction);
  }
  @OnEvent('update.passenger.transaction')
  async updateTransaction(
    command: UpdateTransactionCommand,
  ): Promise<TransactionResponse> {
    const transactionDomain = await this.transactionRepository.getById(
      command.id,
    );
    if (!transactionDomain) {
      throw new NotFoundException(
        `Transaction not found with id ${command.id}`,
      );
    }
    if (command.status == PaymentStatus.Success) {
      this.eventEmitter.emit('update.passenger.wallet', {
        balance: command.amount,
        passengerId: transactionDomain.passengerId,
      });
    }
    transactionDomain.amount = command.amount;
    transactionDomain.status = command.status;
    transactionDomain.tradeDate = command.tradeDate;
    transactionDomain.tradeNumber = command.tradeNumber;
    transactionDomain.transactionNumber = command.transactionNumber;
    transactionDomain.amount = command.amount;
    transactionDomain.updatedAt = new Date();
    const transaction = await this.transactionRepository.update(
      transactionDomain,
    );
    return TransactionResponse.fromDomain(transaction);
  }

  @OnEvent('archive.transaction')
  async archiveTransaction(id: string): Promise<boolean> {
    const transactionDomain = await this.transactionRepository.getById(id);
    if (!transactionDomain) {
      throw new NotFoundException(`Transaction not found with id ${id}`);
    }
    return await this.transactionRepository.archive(id);
  }
  @OnEvent('restore.transaction')
  async restoreTransaction(id: string): Promise<TransactionResponse> {
    const transactionDomain = await this.transactionRepository.getById(
      id,
      true,
    );
    if (!transactionDomain) {
      throw new NotFoundException(`Transaction not found with id ${id}`);
    }
    const r = await this.transactionRepository.restore(id);
    if (r) {
      transactionDomain.deletedAt = null;
    }
    return TransactionResponse.fromDomain(transactionDomain);
  }
  @OnEvent('delete.transaction')
  async deleteTransaction(id: string): Promise<boolean> {
    const transactionDomain = await this.transactionRepository.getById(
      id,
      true,
    );
    if (!transactionDomain) {
      throw new NotFoundException(`Transaction not found with id ${id}`);
    }
    return await this.transactionRepository.delete(id);
  }
  async approveChapaTransaction(transactionId: string, verificationData) {
    const transactionDomain = await this.transactionRepository.getById(
      transactionId,
    );
    if (transactionDomain) {
      transactionDomain.status = PaymentStatus.Success;
      transactionDomain.transactionNumber = verificationData.data.reference;
      // transactionDomain.amount = verificationData.amount;
      this.eventEmitter.emit('update.passenger.wallet', {
        balance: transactionDomain.amount,
        passengerId: transactionDomain.passengerId,
      });
      await this.transactionRepository.update(transactionDomain);
    }
  }
  async initiateTeleBirrPayment(
    payload: CreateTransactionCommand,
  ): Promise<InitiateTeleBirrResponse> {
    const transaction = await this.createTransaction(payload);
    // console.log(transaction);
    const response = new InitiateTeleBirrResponse();
    response.transactionId = transaction.id;
    response.outTradeNumber = transaction.id + ',passenger';
    response.message = '';
    response.code = '200';
    response.appId = process.env.AppId;
    response.receiverName = process.env.ReceiverName;
    response.shortCode = process.env.ShortCode;
    response.inAppPaymentUrl = process.env.InAppPaymentUrl;
    response.webPaymentUrl = process.env.WebPaymentUrl;
    response.subject = 'Pay wallet with telebirr';
    response.returnUrl = process.env.ReturnUrl;
    response.notifyUrl = `${process.env.API_URL}/api/${process.env.NotifyUrl}`;
    response.timeoutExpress = process.env.TimeoutExpress;
    response.appKey = process.env.AppKey;
    response.publicKey = process.env.PublicKey;
    return response;
  }
}
