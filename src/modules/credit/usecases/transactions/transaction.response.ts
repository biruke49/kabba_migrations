import { DepositedBy } from '@credit/domains/transactions/deposited-by';
import { Transaction } from '@credit/domains/transactions/transaction';
import { TransactionEntity } from '@credit/persistence/transactions/transaction.entity';
import { PassengerResponse } from '@customer/usecases/passengers/passenger.response';
import { ApiProperty } from '@nestjs/swagger';

export class TransactionResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  passengerId: string;
  @ApiProperty()
  method: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  depositedBy: DepositedBy;
  @ApiProperty()
  amount: number;
  @ApiProperty()
  transactionNumber: string;
  @ApiProperty()
  tradeNumber: string;
  @ApiProperty()
  tradeDate: Date;
  @ApiProperty()
  reason: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt: Date;
  passenger: PassengerResponse;
  static fromEntity(transactionEntity: TransactionEntity): TransactionResponse {
    const transactionResponse = new TransactionResponse();
    transactionResponse.id = transactionEntity.id;
    transactionResponse.passengerId = transactionEntity.passengerId;
    transactionResponse.tradeDate = transactionEntity.tradeDate;
    transactionResponse.tradeNumber = transactionEntity.tradeNumber;
    transactionResponse.transactionNumber = transactionEntity.transactionNumber;
    transactionResponse.depositedBy = transactionEntity.depositedBy;
    transactionResponse.method = transactionEntity.method;
    transactionResponse.status = transactionEntity.status;
    transactionResponse.reason = transactionEntity.reason;
    transactionResponse.amount = transactionEntity.amount;
    transactionResponse.createdAt = transactionEntity.createdAt;
    transactionResponse.updatedAt = transactionEntity.updatedAt;
    transactionResponse.deletedAt = transactionEntity.deletedAt;
    if (transactionEntity.passenger) {
      transactionResponse.passenger = PassengerResponse.fromEntity(
        transactionEntity.passenger,
      );
    }
    return transactionResponse;
  }
  static fromDomain(transaction: Transaction): TransactionResponse {
    const transactionResponse = new TransactionResponse();
    transactionResponse.id = transaction.id;
    transactionResponse.passengerId = transaction.passengerId;
    transactionResponse.tradeDate = transaction.tradeDate;
    transactionResponse.tradeNumber = transaction.tradeNumber;
    transactionResponse.transactionNumber = transaction.transactionNumber;
    transactionResponse.depositedBy = transaction.depositedBy;
    transactionResponse.method = transaction.method;
    transactionResponse.status = transaction.status;
    transactionResponse.reason = transaction.reason;
    transactionResponse.amount = transaction.amount;
    transactionResponse.createdAt = transaction.createdAt;
    transactionResponse.updatedAt = transaction.updatedAt;
    transactionResponse.deletedAt = transaction.deletedAt;
    if (transaction.passenger) {
      transactionResponse.passenger = PassengerResponse.fromDomain(
        transaction.passenger,
      );
    }
    return transactionResponse;
  }
}
export class GroupByStatusResponse {
  @ApiProperty()
  status: string;
  @ApiProperty()
  count: number;
}
