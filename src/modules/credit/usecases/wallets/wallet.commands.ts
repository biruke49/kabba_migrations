import { UserInfo } from '@account/dtos/user-info.dto';
import { DepositedBy } from '@credit/domains/transactions/deposited-by';
import { Wallet } from '@credit/domains/wallets/wallet';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPositive } from 'class-validator';

export class UpdateWalletCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  balance: number;
  @ApiProperty()
  @IsNotEmpty()
  passengerId: string;
  currentUser?: UserInfo;
  depositedBy?: DepositedBy;
}
export class UpdatePassengerWalletCommand {
  @ApiProperty()
  @IsNotEmpty()
  balance: number;
  @ApiProperty()
  @IsNotEmpty()
  passengerId: string;
  currentUser?: UserInfo;
  depositedBy?: DepositedBy;
}
export class CreateWalletCommand {
  @ApiProperty()
  @IsNotEmpty()
  balance: number;
  @ApiProperty()
  @IsNotEmpty()
  passengerId: string;
  static fromCommand(command: CreateWalletCommand): Wallet {
    const wallet = new Wallet();
    wallet.passengerId = command.passengerId;
    wallet.balance = command.balance;
    return wallet;
  }
}
export class TransferWalletCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  amount: number;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  depositedBy?: DepositedBy;
  reason: string;
}
