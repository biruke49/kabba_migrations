import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WalletEntity } from '@credit/persistence/wallets/wallet.entity';
import { Repository } from 'typeorm';
import { WalletResponse } from './wallet.response';
@Injectable()
export class WalletQuery {
  constructor(
    @InjectRepository(WalletEntity)
    private walletRepository: Repository<WalletEntity>,
  ) {}
  async getWallet(id: string, withDeleted = false): Promise<WalletResponse> {
    const wallet = await this.walletRepository.find({
      where: { id: id },
      relations: ['passenger'],
      withDeleted: withDeleted,
    });
    if (!wallet[0]) {
      throw new NotFoundException(`Wallet not found with id ${id}`);
    }
    return WalletResponse.fromEntity(wallet[0]);
  }
  async getWallets(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<WalletResponse>> {
    const dataQuery = QueryConstructor.constructQuery<WalletEntity>(
      this.walletRepository,
      query,
    );
    const d = new DataResponseFormat<WalletResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => WalletResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getTotalWalletBalance(query: CollectionQuery): Promise<any> {
    const dataQuery = QueryConstructor.constructQuery<WalletEntity>(
      this.walletRepository,
      query,
    );
    const data = await dataQuery.select('SUM(balance)', 'total').getRawOne();
    return { total: data.total };
  }
  async getPassengerWallet(passengerId: string): Promise<WalletResponse> {
    const query = new CollectionQuery();
    query.filter = [];
    query.filter.push([
      {
        field: 'passengerId',
        value: passengerId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<WalletEntity>(
      this.walletRepository,
      query,
    );
    const result = await dataQuery.getOne();
    return WalletResponse.fromEntity(result);
  }
  async getArchivedWallets(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<WalletResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<WalletEntity>(
      this.walletRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<WalletResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => WalletResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
