import {
  CreateWalletCommand,
  TransferWalletCommand,
  UpdatePassengerWalletCommand,
  UpdateWalletCommand,
} from './wallet.commands';
import { WalletRepository } from '@credit/persistence/wallets/wallet.repository';
import { WalletResponse } from './wallet.response';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { OnEvent, EventEmitter2 } from '@nestjs/event-emitter';
import { PassengerRepository } from '@customer/persistence/passengers/passenger.repository';
import { PaymentMethod, PaymentStatus } from '@libs/common/enums';
import { CreateTransactionCommand } from '../transactions/transaction.commands';
import { TransactionCommands } from '../transactions/transaction.usecase.commands';
import { TransactionResponse } from '../transactions/transaction.response';
@Injectable()
export class WalletCommands {
  constructor(
    private walletRepository: WalletRepository,
    private readonly passengerRepository: PassengerRepository,
    private readonly eventEmitter: EventEmitter2,
    private readonly transactionCommands: TransactionCommands,
  ) {}
  @OnEvent('create.wallet')
  async createWallet(command: CreateWalletCommand): Promise<WalletResponse> {
    const walletDomain = CreateWalletCommand.fromCommand(command);
    const wallet = await this.walletRepository.insert(walletDomain);
    // console.log('wallet', wallet);
    return WalletResponse.fromDomain(wallet);
  }
  async updateWallet(
    command: UpdatePassengerWalletCommand,
  ): Promise<WalletResponse> {
    const walletDomain = await this.walletRepository.getByPassengerId(
      command.passengerId,
    );
    if (!walletDomain) {
      throw new NotFoundException(
        `Wallet not found with id ${command.passengerId}`,
      );
    }
    walletDomain.balance += +command.balance;
    walletDomain.updatedAt = new Date();
    const wallet = await this.walletRepository.update(walletDomain);
    return WalletResponse.fromDomain(wallet);
  }
  @OnEvent('update.passenger.wallet')
  async updatePassengerWallet(
    command: UpdatePassengerWalletCommand,
  ): Promise<WalletResponse> {
    // console.log('update passenger wallet command', command);
    const walletDomain = await this.walletRepository.getByPassengerId(
      command.passengerId,
    );
    if (!walletDomain) {
      return await this.createWallet({
        passengerId: command.passengerId,
        balance: command.balance,
      });
    }
    walletDomain.balance += +command.balance;
    const wallet = await this.walletRepository.update(walletDomain);
    return WalletResponse.fromDomain(wallet);
  }
  @OnEvent('update.passenger.corporate.wallet')
  async updatePassengerCorporateWallet(
    command: UpdatePassengerWalletCommand,
  ): Promise<WalletResponse> {
    // console.log('update passenger corporate wallet command', command);
    const walletDomain = await this.walletRepository.getByPassengerId(
      command.passengerId,
    );
    if (!walletDomain) {
      return await this.createWallet({
        passengerId: command.passengerId,
        balance: command.balance,
      });
    }
    walletDomain.corporateWalletBalance += +command.balance;
    const wallet = await this.walletRepository.update(walletDomain);
    return WalletResponse.fromDomain(wallet);
  }
  @OnEvent('archive.wallet')
  async archiveWallet(id: string): Promise<boolean> {
    const walletDomain = await this.walletRepository.getById(id);
    if (!walletDomain) {
      throw new NotFoundException(`Wallet not found with id ${id}`);
    }
    return await this.walletRepository.archive(id);
  }
  @OnEvent('restore.wallet')
  async restoreWallet(id: string): Promise<WalletResponse> {
    const walletDomain = await this.walletRepository.getById(id, true);
    if (!walletDomain) {
      throw new NotFoundException(`Wallet not found with id ${id}`);
    }
    const r = await this.walletRepository.restore(id);
    if (r) {
      walletDomain.deletedAt = null;
    }
    return WalletResponse.fromDomain(walletDomain);
  }
  @OnEvent('delete.wallet')
  async deleteWallet(id: string): Promise<boolean> {
    const walletDomain = await this.walletRepository.getById(id, true);
    if (!walletDomain) {
      throw new NotFoundException(`Wallet not found with id ${id}`);
    }
    return await this.walletRepository.delete(id);
  }
  async transferWallet(
    passengerId: string,
    command: TransferWalletCommand,
  ): Promise<any> {
    const walletDomain = await this.walletRepository.getByPassengerId(
      passengerId,
    );
    if (walletDomain.balance < command.amount) {
      throw new BadRequestException(
        `You don't have sufficient balance in your wallet`,
      );
    }
    const receiver = await this.passengerRepository.getByPhoneNumber(
      command.phoneNumber,
    );
    if (!receiver) {
      throw new NotFoundException(
        `Receiver not found with phone number ${command.phoneNumber}`,
      );
    }
    const response = {};
    const receiverWallet = await this.walletRepository.getByPassengerId(
      receiver.id,
    );
    const withdrawBalance = -1 * command.amount;
    const updatedSenderWallet = await this.updatePassengerWallet({
      passengerId: passengerId,
      balance: withdrawBalance,
    });
    if (updatedSenderWallet) {
      response['wallet'] = updatedSenderWallet;
      // Create sender transaction
      const createSendTransactionCommand: CreateTransactionCommand = {
        passengerId: passengerId,
        amount: withdrawBalance,
        method: PaymentMethod.WalletTransfer,
        status: PaymentStatus.Success,
        returnUrl: '',
        depositedBy: command.depositedBy,
        tradeNumber: '',
        tradeDate: null,
        transactionNumber: '',
        reason: `Transfer wallet to ${command.phoneNumber}`,
      };
      this.eventEmitter.emit(
        'create.transaction',
        createSendTransactionCommand,
      );
      response['transaction'] = createSendTransactionCommand;
      const updatedReceiverWallet = await this.updatePassengerWallet({
        passengerId: receiverWallet.passengerId,
        balance: command.amount,
      });
      if (updatedReceiverWallet) {
        const createReceiverTransactionCommand: CreateTransactionCommand = {
          passengerId: receiverWallet.passengerId,
          amount: command.amount,
          method: PaymentMethod.WalletTransfer,
          status: PaymentStatus.Success,
          returnUrl: '',
          depositedBy: command.depositedBy,
          tradeNumber: '',
          tradeDate: null,
          transactionNumber: '',
          reason: `Get Wallet from ${command.depositedBy.phoneNumber}`,
        };
        this.eventEmitter.emit(
          'create.transaction',
          createReceiverTransactionCommand,
        );
      }
    }
    return response;
  }
  async updateWalletManually(
    command: UpdatePassengerWalletCommand,
  ): Promise<TransactionResponse> {
    const result = await this.updateWallet(command);
    if (result) {
      const createTransactionCommand: CreateTransactionCommand = {
        passengerId: command.passengerId,
        amount: command.balance,
        method: PaymentMethod.Manual,
        status: PaymentStatus.Success,
        returnUrl: '',
        depositedBy: command.depositedBy,
        tradeNumber: '',
        tradeDate: null,
        transactionNumber: '',
        reason: `Recharge wallet manually`,
      };

      return await this.transactionCommands.createTransaction(
        createTransactionCommand,
      );
    }
    return null;
  }
}
