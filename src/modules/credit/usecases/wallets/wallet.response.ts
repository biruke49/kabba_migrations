import { Wallet } from '@credit/domains/wallets/wallet';
import { WalletEntity } from '@credit/persistence/wallets/wallet.entity';
import { PassengerResponse } from '@customer/usecases/passengers/passenger.response';
import { ApiProperty } from '@nestjs/swagger';

export class WalletResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  balance: number;
  @ApiProperty()
  corporateWalletBalance: number;
  @ApiProperty()
  passengerId: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt: Date;
  passenger: PassengerResponse;
  static fromEntity(walletEntity: WalletEntity): WalletResponse {
    const walletResponse = new WalletResponse();
    walletResponse.id = walletEntity.id;
    walletResponse.passengerId = walletEntity.passengerId;
    walletResponse.balance = walletEntity.balance;
    walletResponse.corporateWalletBalance = walletEntity.corporateWalletBalance;
    walletResponse.createdAt = walletEntity.createdAt;
    walletResponse.updatedAt = walletEntity.updatedAt;
    walletResponse.deletedAt = walletEntity.deletedAt;
    if (walletEntity.passenger) {
      walletResponse.passenger = PassengerResponse.fromEntity(
        walletEntity.passenger,
      );
    }
    return walletResponse;
  }
  static fromDomain(wallet: Wallet): WalletResponse {
    const walletResponse = new WalletResponse();
    walletResponse.id = wallet.id;
    walletResponse.passengerId = wallet.passengerId;
    walletResponse.balance = wallet.balance;
    walletResponse.corporateWalletBalance = wallet.corporateWalletBalance;
    walletResponse.createdAt = wallet.createdAt;
    walletResponse.updatedAt = wallet.updatedAt;
    walletResponse.deletedAt = wallet.deletedAt;
    if (wallet.passenger) {
      walletResponse.passenger = PassengerResponse.fromDomain(wallet.passenger);
    }
    return walletResponse;
  }
}
