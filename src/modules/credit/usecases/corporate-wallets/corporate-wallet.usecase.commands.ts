import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { CorporateWalletRepository } from '@credit/persistence/corporate-wallet/corporate-wallet.repository';
import { CorporateWalletResponse } from './corporate-wallet.response';
import {
  CreateCorporateWalletCommand,
  UpdateCorporateWalletCommand,
} from './corporate-wallet.commands';
import { PaymentMethod, PaymentStatus } from '@libs/common/enums';
import { CorporateTransactionCommands } from '../corporate-transactions/corporate-transaction.usecase.commands';
import {
  CorporateTransactionResponse,
  WalletTransactionResponse,
} from '../corporate-transactions/corporate-transaction.response';
import {
  TransferToMultipleEmployeesCommand,
  TransferToSingleEmployeeCommand,
} from '../corporate-transactions/corporate-transaction.commands';
import { CreateTransactionCommand } from '../transactions/transaction.commands';
import { CorporateTransactionRepository } from '@credit/persistence/corporate_transaction/corporate-transaction.repository';
@Injectable()
export class CorporateWalletCommands {
  constructor(
    private walletRepository: CorporateWalletRepository,
    private eventEmitter: EventEmitter2,
    private corporateTransactionCommand: CorporateTransactionCommands,
    private corporateTransactionRepository: CorporateTransactionRepository,
  ) {}
  @OnEvent('create.corporate.wallet')
  async createCorporateWallet(
    command: CreateCorporateWalletCommand,
  ): Promise<CorporateWalletResponse> {
    const walletDomain = CreateCorporateWalletCommand.fromCommand(command);
    const wallet = await this.walletRepository.insert(walletDomain);

    return CorporateWalletResponse.fromDomain(wallet);
  }
  @OnEvent('update.corporate.wallet')
  async updateCorporateWalletEventHandler(
    command: UpdateCorporateWalletCommand,
  ): Promise<CorporateWalletResponse> {
    return this.updateCorporateWallet(command);
  }
  async updateCorporateWalletManually(
    command: UpdateCorporateWalletCommand,
  ): Promise<WalletTransactionResponse> {
    const result = await this.updateCorporateWallet(command);
    // if (result) {
    //   this.eventEmitter.emit('create.corporate.transaction', {
    //     amount: command.balance,
    //     corporateId: command.corporateId,
    //     status: PaymentStatus.Success,
    //     method: PaymentMethod.Manual,
    //     reason: `Recharge wallet manually`,
    //   });
    // }

    const transaction =
      await this.corporateTransactionCommand.createCorporateTransaction({
        amount: command.balance,
        corporateId: command.corporateId,
        status: PaymentStatus.Success,
        method: PaymentMethod.Manual,
        reason: `Recharge wallet manually`,
      });
    const response: WalletTransactionResponse = {
      wallet: CorporateWalletResponse.fromDomain(result),
      transaction: transaction,
    };
    return response;
  }
  async updateCorporateWallet(
    command: UpdateCorporateWalletCommand,
  ): Promise<CorporateWalletResponse> {
    const walletDomain = await this.walletRepository.getByCorporateId(
      command.corporateId,
    );
    if (!walletDomain) {
      return await this.createCorporateWallet({
        corporateId: command.corporateId,
        balance: command.balance,
      });
    }
    walletDomain.balance += +command.balance;
    walletDomain.updatedAt = new Date();
    const wallet = await this.walletRepository.update(walletDomain);
    return CorporateWalletResponse.fromDomain(wallet);
  }
  @OnEvent('archive.corporate.wallet')
  async archiveCorporateWallet(corporateId: string): Promise<boolean> {
    const walletDomain = await this.walletRepository.getByCorporateId(
      corporateId,
    );
    if (!walletDomain) {
      throw new NotFoundException(
        `CorporateWallet not found with id ${corporateId}`,
      );
    }
    return await this.walletRepository.archive(walletDomain.id);
  }
  @OnEvent('restore.corporate.wallet')
  async restoreCorporateWallet(
    corporateId: string,
  ): Promise<CorporateWalletResponse> {
    const walletDomain = await this.walletRepository.getByCorporateId(
      corporateId,
      true,
    );
    if (!walletDomain) {
      throw new NotFoundException(
        `CorporateWallet not found with corporateId ${corporateId}`,
      );
    }
    const r = await this.walletRepository.restore(walletDomain.id);
    if (r) {
      walletDomain.deletedAt = null;
    }
    return CorporateWalletResponse.fromDomain(walletDomain);
  }
  @OnEvent('delete.corporate.wallet')
  async deleteCorporateWallet(corporateId: string): Promise<boolean> {
    const walletDomain = await this.walletRepository.getByCorporateId(
      corporateId,
      true,
    );
    if (!walletDomain) {
      throw new NotFoundException(
        `CorporateWallet not found with corporateId ${corporateId}`,
      );
    }
    return await this.walletRepository.delete(walletDomain.id);
  }
  async transferToSingleEmployee(
    command: TransferToSingleEmployeeCommand,
  ): Promise<any> {
    const corporateWallet = await this.walletRepository.getByCorporateId(
      command.corporateId,
    );
    if (!corporateWallet) {
      throw new NotFoundException(
        `Corporate Wallet not found with corporateId ${command.corporateId}`,
      );
    }
    if (corporateWallet.balance < command.amount) {
      throw new BadRequestException(
        `You do not have enough balance. Please enter less than  ${command.amount}`,
      );
    }
    const originalAmount = command.amount;
    command.amount *= -1;
    const transactionDomain =
      TransferToSingleEmployeeCommand.fromCommand(command);
    transactionDomain.reason = 'Transfer wallet to employee';
    const transaction = await this.corporateTransactionRepository.insert(
      transactionDomain,
    );

    const transactionResponse =
      CorporateTransactionResponse.fromDomain(transaction);

    const wallet = await this.updateCorporateWallet({
      balance: command.amount,
      corporateId: command.corporateId,
    });
    if (wallet) {
      const createTransactionCommand: CreateTransactionCommand = {
        passengerId: command.employeeId,
        amount: originalAmount,
        method: PaymentMethod.WalletTransferFromCorporate,
        status: PaymentStatus.Success,
        returnUrl: '',
        depositedBy: command.depositedBy,
        tradeNumber: '',
        tradeDate: null,
        transactionNumber: '',
        reason: `Transfer wallet from Corporate`,
      };
      this.eventEmitter.emit('create.transaction', createTransactionCommand);
      this.eventEmitter.emit('update.passenger.corporate.wallet', {
        balance: originalAmount,
        passengerId: command.employeeId,
      });
    }
    return {
      balance: wallet.balance,
      transaction: transactionResponse,
    };
  }
  async transferToMultipleEmployees(
    command: TransferToMultipleEmployeesCommand,
  ): Promise<any> {
    const corporateWallet = await this.walletRepository.getByCorporateId(
      command.corporateId,
    );
    if (!corporateWallet) {
      throw new NotFoundException(
        `Corporate Wallet not found with corporateId ${command.corporateId}`,
      );
    }
    const totalAmount = command.amount * command.employees.length;
    if (corporateWallet.balance < totalAmount) {
      throw new BadRequestException(
        `You do not have enough balance on Your wallet. Please enter less than  ${totalAmount}`,
      );
    }
    const transactions = [];
    for (const employeeId of command.employees) {
      const singleTransferCommand = new TransferToSingleEmployeeCommand();
      singleTransferCommand.employeeId = employeeId;
      singleTransferCommand.amount = command.amount;
      singleTransferCommand.corporateId = command.corporateId;
      singleTransferCommand.method = command.method;
      singleTransferCommand.status = command.status;
      singleTransferCommand.depositedBy = command.depositedBy;
      const r = await this.transferToSingleEmployee(singleTransferCommand);
      transactions.push(r.transaction);
    }
    const response = {
      balance: corporateWallet.balance - totalAmount,
      transactions: transactions,
    };
    return response;
  }
}
