import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WalletEntity } from '@credit/persistence/wallets/wallet.entity';
import { Repository } from 'typeorm';
import { CorporateWalletResponse } from './corporate-wallet.response';
import { CorporateWalletEntity } from '@credit/persistence/corporate-wallet/corporate-wallet.entity';
@Injectable()
export class CorporateWalletQuery {
  constructor(
    @InjectRepository(CorporateWalletEntity)
    private corporateWalletRepository: Repository<CorporateWalletEntity>,
  ) {}
  async getWallet(
    id: string,
    withDeleted = false,
  ): Promise<CorporateWalletResponse> {
    const wallet = await this.corporateWalletRepository.find({
      where: { id: id },
      relations: ['corporate'],
      withDeleted: withDeleted,
    });
    if (!wallet[0]) {
      throw new NotFoundException(`Corporate Wallet not found with id ${id}`);
    }
    return CorporateWalletResponse.fromEntity(wallet[0]);
  }
  async getCorporateWallets(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CorporateWalletResponse>> {
    const dataQuery = QueryConstructor.constructQuery<CorporateWalletEntity>(
      this.corporateWalletRepository,
      query,
    );
    const d = new DataResponseFormat<CorporateWalletResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        CorporateWalletResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async getCorporateWallet(
    corporateId: string,
  ): Promise<CorporateWalletResponse> {
    const query = new CollectionQuery();
    query.filter = [];
    query.filter.push([
      {
        field: 'corporateId',
        value: corporateId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CorporateWalletEntity>(
      this.corporateWalletRepository,
      query,
    );
    const result = await dataQuery.getOne();
    if (!result) {
      throw new NotFoundException(`Corporate wallet does not exist`);
    }
    return CorporateWalletResponse.fromEntity(result);
  }
  async getTotalCorporateWalletBalance(query: CollectionQuery): Promise<any> {
    const dataQuery = QueryConstructor.constructQuery<CorporateWalletEntity>(
      this.corporateWalletRepository,
      query,
    );
    const data = await dataQuery.select('SUM(balance)', 'total').getRawOne();
    return { total: data.total };
  }
  async getArchivedCorporateWallets(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CorporateWalletResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CorporateWalletEntity>(
      this.corporateWalletRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<CorporateWalletResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        CorporateWalletResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
}
