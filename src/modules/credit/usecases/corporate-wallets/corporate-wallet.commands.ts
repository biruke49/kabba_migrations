import { UserInfo } from '@account/dtos/user-info.dto';
import { CorporateWallet } from '@credit/domains/corporate-wallets/corporate-wallet';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UpdateCorporateWalletCommand {
  @ApiProperty()
  @IsNotEmpty()
  balance: number;
  @ApiProperty()
  @IsNotEmpty()
  corporateId: string;
  currentUser?: UserInfo;
}
export class CreateCorporateWalletCommand {
  @ApiProperty()
  @IsNotEmpty()
  balance: number;
  @ApiProperty()
  @IsNotEmpty()
  corporateId: string;
  static fromCommand(command: CreateCorporateWalletCommand): CorporateWallet {
    const corporateWallet = new CorporateWallet();
    corporateWallet.corporateId = command.corporateId;
    corporateWallet.balance = command.balance;
    return corporateWallet;
  }
}
