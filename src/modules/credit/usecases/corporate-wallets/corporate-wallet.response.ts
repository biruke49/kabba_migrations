import { CorporateWallet } from '@credit/domains/corporate-wallets/corporate-wallet';
import { CorporateWalletEntity } from '@credit/persistence/corporate-wallet/corporate-wallet.entity';
import { CorporateResponse } from '@customer/usecases/corporates/corporate.response';
import { ApiProperty } from '@nestjs/swagger';

export class CorporateWalletResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  balance: number;
  @ApiProperty()
  corporateId: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt: Date;
  corporate: CorporateResponse;
  static fromEntity(
    corporateWalletEntity: CorporateWalletEntity,
  ): CorporateWalletResponse {
    const corporateWalletResponse = new CorporateWalletResponse();
    corporateWalletResponse.id = corporateWalletEntity.id;
    corporateWalletResponse.corporateId = corporateWalletEntity.corporateId;
    corporateWalletResponse.balance = corporateWalletEntity.balance;
    corporateWalletResponse.createdAt = corporateWalletEntity.createdAt;
    corporateWalletResponse.updatedAt = corporateWalletEntity.updatedAt;
    corporateWalletResponse.deletedAt = corporateWalletEntity.deletedAt;
    if (corporateWalletEntity.corporate) {
      corporateWalletResponse.corporate = CorporateResponse.fromEntity(
        corporateWalletEntity.corporate,
      );
    }
    return corporateWalletResponse;
  }
  static fromDomain(corporateWallet: CorporateWallet): CorporateWalletResponse {
    const corporateWalletResponse = new CorporateWalletResponse();
    corporateWalletResponse.id = corporateWallet.id;
    corporateWalletResponse.corporateId = corporateWallet.corporateId;
    corporateWalletResponse.balance = corporateWallet.balance;
    corporateWalletResponse.createdAt = corporateWallet.createdAt;
    corporateWalletResponse.updatedAt = corporateWallet.updatedAt;
    corporateWalletResponse.deletedAt = corporateWallet.deletedAt;
    if (corporateWallet.corporate) {
      corporateWalletResponse.corporate = CorporateResponse.fromDomain(
        corporateWallet.corporate,
      );
    }
    return corporateWalletResponse;
  }
}
