import { InitiateTeleBirrResponse } from '@credit/domains/initiate-telebirr.response';
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsNumber, Min } from 'class-validator';
import { CorporateTransaction } from '@credit/domains/corporate-transactions/corporate-transaction';
import { DepositedBy } from '@credit/domains/transactions/deposited-by';

export class UpdateCorporateTransactionCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  @ApiProperty()
  //@IsNotEmpty()
  corporateId?: string;
  @ApiProperty()
  tradeDate: Date;
  @ApiProperty()
  tradeNumber: string;
  @ApiProperty()
  transactionNumber: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  method: string;
}
export class CreateCorporateTransactionCommand {
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  @ApiProperty()
  @IsNotEmpty()
  corporateId: string;
  @ApiProperty()
  tradeDate?: Date;
  @ApiProperty()
  tradeNumber?: string;
  @ApiProperty()
  transactionNumber?: string;
  @ApiProperty()
  status?: string;
  @ApiProperty()
  method?: string;
  returnUrl?: string;
  reason?: string;
  employeeId?: string;
  depositedBy?: DepositedBy;
  static fromCommand(
    command: CreateCorporateTransactionCommand,
  ): CorporateTransaction {
    const transaction = new CorporateTransaction();
    transaction.corporateId = command.corporateId;
    transaction.amount = command.amount;
    transaction.tradeDate = command.tradeDate;
    transaction.transactionNumber = command.transactionNumber;
    transaction.status = command.status;
    transaction.method = command.method;
    transaction.tradeNumber = command.tradeNumber;
    transaction.reason = command.reason;
    transaction.employeeId = command.employeeId;
    return transaction;
  }
}
export class CreateChapaCorporateTransactionCommand {
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  @ApiProperty()
  @IsNotEmpty()
  corporateId: string;
  @ApiProperty()
  @IsNotEmpty()
  returnUrl: string;
  // @ApiProperty()
  // tradeDate: Date;
  // @ApiProperty()
  // tradeNumber: string;
  // @ApiProperty()
  // transactionNumber: string;
  // @ApiProperty()
  status: string;
  //@ApiProperty()
  method: string;
  static fromCommand(
    command: CreateChapaCorporateTransactionCommand,
  ): CorporateTransaction {
    const transaction = new CorporateTransaction();
    transaction.corporateId = command.corporateId;
    transaction.amount = command.amount;
    transaction.status = command.status;
    transaction.method = command.method;
    return transaction;
  }
}
export class PayCorporateWithChapaCommand {
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  // @ApiProperty()
  // @IsNotEmpty()
  corporateId: string;
  // @ApiProperty()
  // tradeDate: Date;
  // @ApiProperty()
  // tradeNumber: string;
  // @ApiProperty()
  // transactionNumber: string;
  // @ApiProperty()
  status: string;
  //@ApiProperty()
  method: string;
  @ApiProperty()
  @IsNotEmpty()
  returnUrl: string;
  reason: string;
  static fromCommand(
    command: PayCorporateWithChapaCommand,
  ): CorporateTransaction {
    const transaction = new CorporateTransaction();
    transaction.corporateId = command.corporateId;
    transaction.amount = command.amount;
    transaction.status = command.status;
    transaction.method = command.method;
    return transaction;
  }
}
export class TransferToSingleEmployeeCommand {
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  // @ApiProperty()
  // @IsNotEmpty()
  corporateId: string;
  @ApiProperty()
  @IsNotEmpty()
  employeeId: string;
  // @ApiProperty()
  // tradeDate: Date;
  // @ApiProperty()
  // tradeNumber: string;
  // @ApiProperty()
  // transactionNumber: string;
  // @ApiProperty()
  status: string;
  //@ApiProperty()
  method: string;
  depositedBy: DepositedBy;
  static fromCommand(
    command: TransferToSingleEmployeeCommand,
  ): CorporateTransaction {
    const transaction = new CorporateTransaction();
    transaction.corporateId = command.corporateId;
    transaction.employeeId = command.employeeId;
    transaction.amount = command.amount;
    transaction.status = command.status;
    transaction.method = command.method;
    return transaction;
  }
}
export class TransferToMultipleEmployeesCommand {
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
  // @ApiProperty()
  // @IsNotEmpty()
  corporateId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  employees: string[];
  // @ApiProperty()
  // tradeDate: Date;
  // @ApiProperty()
  // tradeNumber: string;
  // @ApiProperty()
  // transactionNumber: string;
  // @ApiProperty()
  status: string;
  //@ApiProperty()
  method: string;
  depositedBy: DepositedBy;
  static fromCommand(
    command: TransferToSingleEmployeeCommand,
  ): CorporateTransaction {
    const transaction = new CorporateTransaction();
    transaction.corporateId = command.corporateId;
    transaction.employeeId = command.employeeId;
    transaction.amount = command.amount;
    transaction.status = command.status;
    transaction.method = command.method;
    return transaction;
  }
}
export class InitiateCorporateTeleBirrPaymentCommand {
  @ApiProperty()
  // @IsNumber()
  @IsNotEmpty()
  //@Min(10)
  amount: number;
  @ApiProperty()
  @IsNotEmpty()
  returnUrl: string;
}
