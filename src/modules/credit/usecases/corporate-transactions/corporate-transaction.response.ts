import { CorporateTransaction } from '@credit/domains/corporate-transactions/corporate-transaction';
import { CorporateTransactionEntity } from '@credit/persistence/corporate_transaction/corporate-transaction.entity';
import { CorporateResponse } from '@customer/usecases/corporates/corporate.response';
import { PassengerResponse } from '@customer/usecases/passengers/passenger.response';
import { ApiProperty } from '@nestjs/swagger';
import { CorporateWalletResponse } from '../corporate-wallets/corporate-wallet.response';

export class CorporateTransactionResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  corporateId: string;
  @ApiProperty()
  method: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  amount: number;
  @ApiProperty()
  transactionNumber: string;
  @ApiProperty()
  tradeNumber: string;
  @ApiProperty()
  tradeDate: Date;
  @ApiProperty()
  reason: string;
  @ApiProperty()
  employeeId: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt: Date;
  corporate: CorporateResponse;
  employee: PassengerResponse;
  static fromEntity(
    corporateTransactionEntity: CorporateTransactionEntity,
  ): CorporateTransactionResponse {
    const corporateTransactionResponse = new CorporateTransactionResponse();
    corporateTransactionResponse.id = corporateTransactionEntity.id;
    corporateTransactionResponse.corporateId =
      corporateTransactionEntity.corporateId;
    corporateTransactionResponse.tradeDate =
      corporateTransactionEntity.tradeDate;
    corporateTransactionResponse.tradeNumber =
      corporateTransactionEntity.tradeNumber;
    corporateTransactionResponse.transactionNumber =
      corporateTransactionEntity.transactionNumber;
    corporateTransactionResponse.method = corporateTransactionEntity.method;
    corporateTransactionResponse.status = corporateTransactionEntity.status;
    corporateTransactionResponse.reason = corporateTransactionEntity.reason;
    corporateTransactionResponse.employeeId =
      corporateTransactionEntity.employeeId;
    corporateTransactionResponse.amount = corporateTransactionEntity.amount;
    corporateTransactionResponse.createdAt =
      corporateTransactionEntity.createdAt;
    corporateTransactionResponse.updatedAt =
      corporateTransactionEntity.updatedAt;
    corporateTransactionResponse.deletedAt =
      corporateTransactionEntity.deletedAt;
    if (corporateTransactionEntity.corporate) {
      corporateTransactionResponse.corporate = CorporateResponse.fromEntity(
        corporateTransactionEntity.corporate,
      );
    }
    if (corporateTransactionEntity.employee) {
      corporateTransactionResponse.employee = PassengerResponse.fromEntity(
        corporateTransactionEntity.employee,
      );
    }
    return corporateTransactionResponse;
  }
  static fromDomain(
    corporateTransaction: CorporateTransaction,
  ): CorporateTransactionResponse {
    const corporateTransactionResponse = new CorporateTransactionResponse();
    corporateTransactionResponse.id = corporateTransaction.id;
    corporateTransactionResponse.corporateId = corporateTransaction.corporateId;
    corporateTransactionResponse.tradeDate = corporateTransaction.tradeDate;
    corporateTransactionResponse.tradeNumber = corporateTransaction.tradeNumber;
    corporateTransactionResponse.transactionNumber =
      corporateTransaction.transactionNumber;
    corporateTransactionResponse.method = corporateTransaction.method;
    corporateTransactionResponse.status = corporateTransaction.status;
    corporateTransactionResponse.reason = corporateTransaction.reason;
    corporateTransactionResponse.employeeId = corporateTransaction.employeeId;
    corporateTransactionResponse.amount = corporateTransaction.amount;
    corporateTransactionResponse.createdAt = corporateTransaction.createdAt;
    corporateTransactionResponse.updatedAt = corporateTransaction.updatedAt;
    corporateTransactionResponse.deletedAt = corporateTransaction.deletedAt;
    if (corporateTransaction.corporate) {
      corporateTransactionResponse.corporate = CorporateResponse.fromDomain(
        corporateTransaction.corporate,
      );
    }
    if (corporateTransaction.employee) {
      corporateTransactionResponse.employee = PassengerResponse.fromDomain(
        corporateTransaction.employee,
      );
    }
    return corporateTransactionResponse;
  }
}

export class CorporateTransactionByPaymentResponse {
  @ApiProperty()
  paymentMethod: string;
  @ApiProperty()
  total: string;
}
export class WalletTransactionResponse {
  @ApiProperty()
  wallet: CorporateWalletResponse;
  @ApiProperty()
  transaction: CorporateTransactionResponse;
}
