import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CorporateTransactionEntity } from '@credit/persistence/corporate_transaction/corporate-transaction.entity';
import {
  CorporateTransactionByPaymentResponse,
  CorporateTransactionResponse,
} from './corporate-transaction.response';
import { PaymentStatus } from '@libs/common/enums';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import { GroupByStatusResponse } from '../transactions/transaction.response';
@Injectable()
export class CorporateTransactionQuery {
  constructor(
    @InjectRepository(CorporateTransactionEntity)
    private corporateTransactionRepository: Repository<CorporateTransactionEntity>,
  ) {}
  async getTransaction(
    id: string,
    withDeleted = false,
  ): Promise<CorporateTransactionResponse> {
    const transaction = await this.corporateTransactionRepository.find({
      where: { id: id },
      relations: ['corporate', 'employee'],
      withDeleted: withDeleted,
    });
    if (!transaction[0]) {
      throw new NotFoundException(
        `Corporate Transaction not found with id ${id}`,
      );
    }
    return CorporateTransactionResponse.fromEntity(transaction[0]);
  }
  async getTransactions(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CorporateTransactionResponse>> {
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const d = new DataResponseFormat<CorporateTransactionResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        CorporateTransactionResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }

  async getCorporateTransaction(
    corporateId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CorporateTransactionResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'corporateId',
        value: corporateId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const d = new DataResponseFormat<CorporateTransactionResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        CorporateTransactionResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
  async groupCorporateTransactionsByPaymentMethod(
    query: CollectionQuery,
  ): Promise<CorporateTransactionByPaymentResponse[]> {
    query.select = [];
    query.select.push(
      'method as method',
      'SUM(corporate_transactions.amount) as total',
    );
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'method',
        value: 'Chapa,Telebirr',
        operator: FilterOperators.In,
      },
    ]);
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CorporateTransactionByPaymentResponse();
      countResponse.paymentMethod = d.method;
      countResponse.total = d.total;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getTotalCorporateDeposits(query: CollectionQuery): Promise<any> {
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const data = await dataQuery
      .select('SUM(amount)', 'total')
      .andWhere('status= :status', { status: PaymentStatus.Success })
      .andWhere('amount> 0')
      .getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async groupCorporateTransactionsByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push(
      `corporate_transactions.status as status`,
      'COUNT(corporate_transactions.id)',
    );
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );

    const data = await dataQuery.groupBy(`status`).getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.status;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getCorporateTransactionSpending(
    corporateId: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'corporateId',
        value: corporateId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const data = await dataQuery
      .select('SUM(amount)', 'total')
      .andWhere('status= :status', { status: PaymentStatus.Success })
      .andWhere('amount< 0')
      .getRawOne();
    return { total: data.total * -1 };
  }
  async getCorporateTransactionSpendingCount(
    corporateId: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'corporateId',
        value: corporateId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const data = await dataQuery
      .select('COUNT(amount)', 'total')
      .andWhere('status= :status', { status: PaymentStatus.Success })
      .andWhere('amount< 0')
      .getRawOne();
    return { total: data.total };
  }
  async getCorporateTransactionDepositCount(
    corporateId: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'corporateId',
        value: corporateId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const data = await dataQuery
      .select('COUNT(amount)', 'total')
      .andWhere('status= :status', { status: PaymentStatus.Success })
      .andWhere('amount> 0')
      .getRawOne();
    return { total: data.total };
  }
  async getCorporateTransactionSpendingToEmployee(
    corporateId: string,
    employeeId: string,
    query: CollectionQuery,
  ): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'corporateId',
          value: corporateId,
          operator: FilterOperators.EqualTo,
        },
      ],
      [
        {
          field: 'employeeId',
          value: employeeId,
          operator: FilterOperators.EqualTo,
        },
      ],
    );

    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    const data = await dataQuery
      .select('SUM(amount)', 'total')
      .andWhere('status= :status', { status: PaymentStatus.Success })
      .andWhere('amount< 0')
      .getRawOne();
    return { total: data.total * -1 };
  }
  async getArchivedTransactions(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CorporateTransactionResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery =
      QueryConstructor.constructQuery<CorporateTransactionEntity>(
        this.corporateTransactionRepository,
        query,
      );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<CorporateTransactionResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) =>
        CorporateTransactionResponse.fromEntity(entity),
      );
      d.count = total;
    }
    return d;
  }
}
