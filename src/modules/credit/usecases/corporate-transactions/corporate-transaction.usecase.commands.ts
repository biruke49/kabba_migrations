import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { CorporateTransactionResponse } from './corporate-transaction.response';
import { CorporateTransactionRepository } from '@credit/persistence/corporate_transaction/corporate-transaction.repository';
import {
  CreateCorporateTransactionCommand,
  UpdateCorporateTransactionCommand,
} from './corporate-transaction.commands';
import { PaymentStatus } from '@libs/common/enums';
@Injectable()
export class CorporateTransactionCommands {
  constructor(
    private corporateTransactionRepository: CorporateTransactionRepository,
    private eventEmitter: EventEmitter2,
  ) {}
  @OnEvent('create.corporate.transaction')
  async createCorporateTransaction(
    command: CreateCorporateTransactionCommand,
  ): Promise<CorporateTransactionResponse> {
    const transactionDomain =
      CreateCorporateTransactionCommand.fromCommand(command);
    const transaction = await this.corporateTransactionRepository.insert(
      transactionDomain,
    );
    return CorporateTransactionResponse.fromDomain(transaction);
  }
  @OnEvent('update.corporate.transaction')
  async updateCorporateTransaction(
    command: UpdateCorporateTransactionCommand,
  ): Promise<CorporateTransactionResponse> {
    const transactionDomain = await this.corporateTransactionRepository.getById(
      command.id,
    );
    if (!transactionDomain) {
      throw new NotFoundException(
        `Corporate Transaction not found with id ${command.id}`,
      );
    }
    if (command.status == PaymentStatus.Success) {
      this.eventEmitter.emit('update.corporate.wallet', {
        balance: command.amount,
        corporateId: transactionDomain.corporateId,
      });
    }
    transactionDomain.amount = command.amount;
    transactionDomain.status = command.status;
    transactionDomain.tradeDate = command.tradeDate;
    transactionDomain.tradeNumber = command.tradeNumber;
    transactionDomain.transactionNumber = command.transactionNumber;
    transactionDomain.updatedAt = new Date();
    const transaction = await this.corporateTransactionRepository.update(
      transactionDomain,
    );
    return CorporateTransactionResponse.fromDomain(transaction);
  }
  @OnEvent('archive.corporate.transaction')
  async archiveCorporateTransaction(id: string): Promise<boolean> {
    const transactionDomain = await this.corporateTransactionRepository.getById(
      id,
    );
    if (!transactionDomain) {
      throw new NotFoundException(`Transaction not found with id ${id}`);
    }
    return await this.corporateTransactionRepository.archive(id);
  }
  @OnEvent('restore.corporate.transaction')
  async restoreCorporateTransaction(
    id: string,
  ): Promise<CorporateTransactionResponse> {
    const transactionDomain = await this.corporateTransactionRepository.getById(
      id,
      true,
    );
    if (!transactionDomain) {
      throw new NotFoundException(
        `Corporate Transaction not found with id ${id}`,
      );
    }
    const r = await this.corporateTransactionRepository.restore(id);
    if (r) {
      transactionDomain.deletedAt = null;
    }
    return CorporateTransactionResponse.fromDomain(transactionDomain);
  }
  @OnEvent('delete.corporate.transaction')
  async deleteCorporateTransaction(id: string): Promise<boolean> {
    const transactionDomain = await this.corporateTransactionRepository.getById(
      id,
      true,
    );
    if (!transactionDomain) {
      throw new NotFoundException(
        `Corporate Transaction not found with id ${id}`,
      );
    }
    return await this.corporateTransactionRepository.delete(id);
  }
  async approveCorporateChapaTransaction(
    transactionId: string,
    verificationData,
  ) {
    const transactionDomain = await this.corporateTransactionRepository.getById(
      transactionId,
    );
    if (transactionDomain) {
      transactionDomain.status = PaymentStatus.Success;
      transactionDomain.transactionNumber = verificationData.data.reference;
      // transactionDomain.amount = verificationData.amount;
      this.eventEmitter.emit('update.corporate.wallet', {
        balance: transactionDomain.amount,
        corporateId: transactionDomain.corporateId,
      });
      await this.corporateTransactionRepository.update(transactionDomain);
    }
  }

  async initiateTeleBirrPayment(
    payload: CreateCorporateTransactionCommand,
  ): Promise<any> {
    if (payload.amount <= 0) {
      throw new BadRequestException(`Amount must be positive number`);
    }
    const transaction = await this.createCorporateTransaction(payload);
    return {
      appId: process.env.AppId,
      nonce: transaction.id + ',corporate',
      notifyUrl: `${process.env.API_URL}/api/${process.env.NotifyUrl}`,
      outTradeNo: transaction.id + ',corporate',
      receiveName: 'Social Impact PLC',
      returnUrl: payload.returnUrl,
      shortCode: process.env.ShortCode,
      subject: 'Recharge wallet with telebirr',
      timeoutExpress: process.env.TimeoutExpress,
      timestamp: Date.now().toString(),
      totalAmount: transaction.amount,
    };
  }
}
