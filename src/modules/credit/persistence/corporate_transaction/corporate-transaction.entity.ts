import { PaymentStatus } from '@libs/common/enums';
import { CorporateEntity } from '@customer/persistence/corporates/corporate.entity';
import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('corporate_transactions')
export class CorporateTransactionEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'corporate_id', type: 'uuid' })
  corporateId: string;
  @Column()
  method: string;
  @Column({ default: PaymentStatus.Pending })
  status: string;
  @Column({ type: 'decimal' })
  amount: number;
  @Column({ name: 'transaction_number', nullable: true })
  transactionNumber: string;
  @Column({ name: 'trade_number', nullable: true })
  tradeNumber: string;
  @Column({ type: 'timestamptz', name: 'trade_date', nullable: true })
  tradeDate: Date;
  @Column({ nullable: true })
  reason: string;
  @Column({ name: 'employee_id', nullable: true, type: 'uuid' })
  employeeId: string;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamptz',
    name: 'updated_at',
  })
  updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamptz', nullable: true, name: 'deleted_at' })
  deletedAt: Date;
  @ManyToOne(() => CorporateEntity, (corporate) => corporate.transactions, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'corporate_id' })
  corporate: CorporateEntity;
  @ManyToOne(
    () => PassengerEntity,
    (employee) => employee.corporateTransactions,
  )
  @JoinColumn({ name: 'employee_id' })
  employee: PassengerEntity;
}
