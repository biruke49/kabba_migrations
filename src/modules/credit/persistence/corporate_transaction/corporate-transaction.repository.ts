import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CorporateTransaction } from '@credit/domains/corporate-transactions/corporate-transaction';
import { ICorporateTransactionRepository } from '@credit/domains/corporate-transactions/corporate-transaction.repository.interface';
import { CorporateTransactionEntity } from './corporate-transaction.entity';
@Injectable()
export class CorporateTransactionRepository
  implements ICorporateTransactionRepository
{
  constructor(
    @InjectRepository(CorporateTransactionEntity)
    private corporateTransactionRepository: Repository<CorporateTransactionEntity>,
  ) {}
  async insert(
    transaction: CorporateTransaction,
  ): Promise<CorporateTransaction> {
    const corporateTransactionEntity =
      this.toCorporateTransactionEntity(transaction);
    const result = await this.corporateTransactionRepository.save(
      corporateTransactionEntity,
    );
    return result ? this.toCorporateTransaction(result) : null;
  }
  async update(
    transaction: CorporateTransaction,
  ): Promise<CorporateTransaction> {
    const corporateTransactionEntity =
      this.toCorporateTransactionEntity(transaction);
    const result = await this.corporateTransactionRepository.save(
      corporateTransactionEntity,
    );
    return result ? this.toCorporateTransaction(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.corporateTransactionRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<CorporateTransaction[]> {
    const corporateTransactions =
      await this.corporateTransactionRepository.find({
        relations: [],
        withDeleted: withDeleted,
      });
    if (!corporateTransactions.length) {
      return null;
    }
    return corporateTransactions.map((user) =>
      this.toCorporateTransaction(user),
    );
  }
  async getById(
    id: string,
    withDeleted = false,
  ): Promise<CorporateTransaction> {
    const corporateTransaction = await this.corporateTransactionRepository.find(
      {
        where: { id: id },
        relations: [],
        withDeleted: withDeleted,
      },
    );
    if (!corporateTransaction[0]) {
      return null;
    }
    return this.toCorporateTransaction(corporateTransaction[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.corporateTransactionRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.corporateTransactionRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toCorporateTransaction(
    corporateTransactionEntity: CorporateTransactionEntity,
  ): CorporateTransaction {
    const corporateTransaction = new CorporateTransaction();
    corporateTransaction.id = corporateTransactionEntity.id;
    corporateTransaction.corporateId = corporateTransactionEntity.corporateId;
    corporateTransaction.tradeDate = corporateTransactionEntity.tradeDate;
    corporateTransaction.tradeNumber = corporateTransactionEntity.tradeNumber;
    corporateTransaction.transactionNumber =
      corporateTransactionEntity.transactionNumber;
    corporateTransaction.status = corporateTransactionEntity.status;
    corporateTransaction.method = corporateTransactionEntity.method;
    corporateTransaction.reason = corporateTransactionEntity.reason;
    corporateTransaction.employeeId = corporateTransactionEntity.employeeId;
    corporateTransaction.amount = corporateTransactionEntity.amount;
    corporateTransaction.createdAt = corporateTransactionEntity.createdAt;
    corporateTransaction.updatedAt = corporateTransactionEntity.updatedAt;
    corporateTransaction.deletedAt = corporateTransactionEntity.deletedAt;
    return corporateTransaction;
  }
  toCorporateTransactionEntity(
    corporateTransaction: CorporateTransaction,
  ): CorporateTransactionEntity {
    const corporateTransactionEntity = new CorporateTransactionEntity();
    corporateTransactionEntity.id = corporateTransaction.id;
    corporateTransactionEntity.corporateId = corporateTransaction.corporateId;
    corporateTransactionEntity.tradeDate = corporateTransaction.tradeDate;
    corporateTransactionEntity.tradeNumber = corporateTransaction.tradeNumber;
    corporateTransactionEntity.transactionNumber =
      corporateTransaction.transactionNumber;
    corporateTransactionEntity.status = corporateTransaction.status;
    corporateTransactionEntity.method = corporateTransaction.method;
    corporateTransactionEntity.reason = corporateTransaction.reason;
    corporateTransactionEntity.employeeId = corporateTransaction.employeeId;
    corporateTransactionEntity.amount = corporateTransaction.amount;
    corporateTransactionEntity.createdAt = corporateTransaction.createdAt;
    corporateTransactionEntity.updatedAt = corporateTransaction.updatedAt;
    corporateTransactionEntity.deletedAt = corporateTransaction.deletedAt;
    return corporateTransactionEntity;
  }
}
