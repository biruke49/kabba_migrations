import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('wallets')
export class WalletEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ type: 'double precision', default: 0 })
  balance: number;
  @Column({
    name: 'corporate_wallet_balance',
    type: 'double precision',
    default: 0,
  })
  corporateWalletBalance: number;
  @Column({ name: 'passenger_id', type: 'uuid' })
  passengerId: string;
  @OneToOne(() => PassengerEntity, (passenger) => passenger.wallet, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'passenger_id' })
  passenger: PassengerEntity;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamptz',
    name: 'updated_at',
  })
  updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamptz', nullable: true, name: 'deleted_at' })
  deletedAt: Date;
}
