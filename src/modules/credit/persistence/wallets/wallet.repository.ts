import { WalletEntity } from '@credit/persistence/wallets/wallet.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Wallet } from '@credit/domains/wallets/wallet';
import { IWalletRepository } from '@credit/domains/wallets/wallet.repository.interface';
@Injectable()
export class WalletRepository implements IWalletRepository {
  constructor(
    @InjectRepository(WalletEntity)
    private walletRepository: Repository<WalletEntity>,
  ) {}
  async insert(wallet: Wallet): Promise<Wallet> {
    const walletEntity = this.toWalletEntity(wallet);
    const result = await this.walletRepository.save(walletEntity);
    return result ? this.toWallet(result) : null;
  }
  async update(wallet: Wallet): Promise<Wallet> {
    const walletEntity = this.toWalletEntity(wallet);
    const result = await this.walletRepository.save(walletEntity);
    return result ? this.toWallet(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.walletRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Wallet[]> {
    const wallets = await this.walletRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!wallets.length) {
      return null;
    }
    return wallets.map((user) => this.toWallet(user));
  }
  async getById(id: string, withDeleted = false): Promise<Wallet> {
    const wallet = await this.walletRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!wallet[0]) {
      return null;
    }
    return this.toWallet(wallet[0]);
  }
  async getByPassengerId(
    passengerId: string,
    withDeleted = false,
  ): Promise<Wallet> {
    const wallet = await this.walletRepository.find({
      where: { passengerId: passengerId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!wallet[0]) {
      return null;
    }
    return this.toWallet(wallet[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.walletRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.walletRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toWallet(walletEntity: WalletEntity): Wallet {
    const wallet = new Wallet();
    wallet.id = walletEntity.id;
    wallet.passengerId = walletEntity.passengerId;
    wallet.balance = walletEntity.balance;
    wallet.corporateWalletBalance = walletEntity.corporateWalletBalance;
    wallet.createdAt = walletEntity.createdAt;
    wallet.updatedAt = walletEntity.updatedAt;
    wallet.deletedAt = walletEntity.deletedAt;
    return wallet;
  }
  toWalletEntity(wallet: Wallet): WalletEntity {
    const walletEntity = new WalletEntity();
    walletEntity.id = wallet.id;
    walletEntity.passengerId = wallet.passengerId;
    walletEntity.balance = wallet.balance;
    walletEntity.corporateWalletBalance = wallet.corporateWalletBalance;
    walletEntity.createdAt = wallet.createdAt;
    walletEntity.updatedAt = wallet.updatedAt;
    walletEntity.deletedAt = wallet.deletedAt;
    return walletEntity;
  }
}
