import { CorporateEntity } from '@customer/persistence/corporates/corporate.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('corporate_wallets')
export class CorporateWalletEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ type: 'double precision', default: 0 })
  balance: number;
  @Column({ name: 'corporate_id', type: 'uuid' })
  corporateId: string;
  @OneToOne(() => CorporateEntity, (corporate) => corporate.wallet, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'corporate_id' })
  corporate: CorporateEntity;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamptz',
    name: 'updated_at',
  })
  updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamptz', nullable: true, name: 'deleted_at' })
  deletedAt: Date;
}
