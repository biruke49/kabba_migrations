import { CorporateWallet } from '@credit/domains/corporate-wallets/corporate-wallet';
import { ICorporateWalletRepository } from '@credit/domains/corporate-wallets/corporate-wallet.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CorporateWalletEntity } from './corporate-wallet.entity';
@Injectable()
export class CorporateWalletRepository implements ICorporateWalletRepository {
  constructor(
    @InjectRepository(CorporateWalletEntity)
    private corporateWalletRepository: Repository<CorporateWalletEntity>,
  ) {}
  async insert(wallet: CorporateWallet): Promise<CorporateWallet> {
    const corporateWalletEntity = this.toCorporateWalletEntity(wallet);
    const result = await this.corporateWalletRepository.save(
      corporateWalletEntity,
    );
    return result ? this.toCorporateWallet(result) : null;
  }
  async update(wallet: CorporateWallet): Promise<CorporateWallet> {
    const corporateWalletEntity = this.toCorporateWalletEntity(wallet);
    const result = await this.corporateWalletRepository.save(
      corporateWalletEntity,
    );
    return result ? this.toCorporateWallet(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.corporateWalletRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<CorporateWallet[]> {
    const corporateWallets = await this.corporateWalletRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporateWallets.length) {
      return null;
    }
    return corporateWallets.map((user) => this.toCorporateWallet(user));
  }
  async getById(id: string, withDeleted = false): Promise<CorporateWallet> {
    const corporateWallet = await this.corporateWalletRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporateWallet[0]) {
      return null;
    }
    return this.toCorporateWallet(corporateWallet[0]);
  }
  async getByCorporateId(
    corporateId: string,
    withDeleted = false,
  ): Promise<CorporateWallet> {
    const corporateWallet = await this.corporateWalletRepository.find({
      where: { corporateId: corporateId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporateWallet[0]) {
      return null;
    }
    return this.toCorporateWallet(corporateWallet[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.corporateWalletRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.corporateWalletRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toCorporateWallet(
    corporateWalletEntity: CorporateWalletEntity,
  ): CorporateWallet {
    const corporateWallet = new CorporateWallet();
    corporateWallet.id = corporateWalletEntity.id;
    corporateWallet.corporateId = corporateWalletEntity.corporateId;
    corporateWallet.balance = corporateWalletEntity.balance;
    corporateWallet.createdAt = corporateWalletEntity.createdAt;
    corporateWallet.updatedAt = corporateWalletEntity.updatedAt;
    corporateWallet.deletedAt = corporateWalletEntity.deletedAt;
    return corporateWallet;
  }
  toCorporateWalletEntity(
    corporateWallet: CorporateWallet,
  ): CorporateWalletEntity {
    const corporateWalletEntity = new CorporateWalletEntity();
    corporateWalletEntity.id = corporateWallet.id;
    corporateWalletEntity.corporateId = corporateWallet.corporateId;
    corporateWalletEntity.balance = corporateWallet.balance;
    corporateWalletEntity.createdAt = corporateWallet.createdAt;
    corporateWalletEntity.updatedAt = corporateWallet.updatedAt;
    corporateWalletEntity.deletedAt = corporateWallet.deletedAt;
    return corporateWalletEntity;
  }
}
