import { PaymentStatus } from './../../../../libs/common/enums';
import { DepositedBy } from '@credit/domains/transactions/deposited-by';
import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('transactions')
export class TransactionEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'passenger_id', type: 'uuid' })
  passengerId: string;
  @Column()
  method: string;
  @Column({ default: PaymentStatus.Pending })
  status: string;
  @Column({ name: 'deposited_by', nullable: true, type: 'jsonb' })
  depositedBy: DepositedBy;
  @Column({ type: 'decimal' })
  amount: number;
  @Column({ name: 'transaction_number', nullable: true })
  transactionNumber: string;
  @Column({ name: 'trade_number', nullable: true })
  tradeNumber: string;
  @Column({ type: 'timestamptz', name: 'trade_date', nullable: true })
  tradeDate: Date;
  @Column({ nullable: true })
  reason: string;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamptz',
    name: 'updated_at',
  })
  updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamptz', nullable: true, name: 'deleted_at' })
  deletedAt: Date;
  @ManyToOne(() => PassengerEntity, (passenger) => passenger.transactions, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'passenger_id' })
  passenger: PassengerEntity;
}
