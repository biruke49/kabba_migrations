import { TransactionEntity } from '@credit/persistence/transactions/transaction.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Transaction } from '@credit/domains/transactions/transaction';
import { ITransactionRepository } from '@credit/domains/transactions/transaction.repository.interface';
@Injectable()
export class TransactionRepository implements ITransactionRepository {
  constructor(
    @InjectRepository(TransactionEntity)
    private transactionRepository: Repository<TransactionEntity>,
  ) {}
  async insert(transaction: Transaction): Promise<Transaction> {
    const transactionEntity = this.toTransactionEntity(transaction);
    const result = await this.transactionRepository.save(transactionEntity);
    return result ? this.toTransaction(result) : null;
  }
  async update(transaction: Transaction): Promise<Transaction> {
    const transactionEntity = this.toTransactionEntity(transaction);
    const result = await this.transactionRepository.save(transactionEntity);
    return result ? this.toTransaction(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.transactionRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Transaction[]> {
    const transactions = await this.transactionRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!transactions.length) {
      return null;
    }
    return transactions.map((user) => this.toTransaction(user));
  }
  async getById(id: string, withDeleted = false): Promise<Transaction> {
    const transaction = await this.transactionRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!transaction[0]) {
      return null;
    }
    return this.toTransaction(transaction[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.transactionRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.transactionRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toTransaction(transactionEntity: TransactionEntity): Transaction {
    const transaction = new Transaction();
    transaction.id = transactionEntity.id;
    transaction.passengerId = transactionEntity.passengerId;
    transaction.tradeDate = transactionEntity.tradeDate;
    transaction.tradeNumber = transactionEntity.tradeNumber;
    transaction.transactionNumber = transactionEntity.transactionNumber;
    transaction.depositedBy = transactionEntity.depositedBy;
    transaction.status = transactionEntity.status;
    transaction.method = transactionEntity.method;
    transaction.reason = transactionEntity.reason;
    transaction.amount = transactionEntity.amount;
    transaction.createdAt = transactionEntity.createdAt;
    transaction.updatedAt = transactionEntity.updatedAt;
    transaction.deletedAt = transactionEntity.deletedAt;
    return transaction;
  }
  toTransactionEntity(transaction: Transaction): TransactionEntity {
    const transactionEntity = new TransactionEntity();
    transactionEntity.id = transaction.id;
    transactionEntity.passengerId = transaction.passengerId;
    transactionEntity.tradeDate = transaction.tradeDate;
    transactionEntity.tradeNumber = transaction.tradeNumber;
    transactionEntity.transactionNumber = transaction.transactionNumber;
    transactionEntity.depositedBy = transaction.depositedBy;
    transactionEntity.status = transaction.status;
    transactionEntity.method = transaction.method;
    transactionEntity.reason = transaction.reason;
    transactionEntity.amount = transaction.amount;
    transactionEntity.createdAt = transaction.createdAt;
    transactionEntity.updatedAt = transaction.updatedAt;
    transactionEntity.deletedAt = transaction.deletedAt;
    return transactionEntity;
  }
}
