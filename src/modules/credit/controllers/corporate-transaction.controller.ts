import axios from 'axios';

import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { InitiateTeleBirrResponse } from '@credit/domains/initiate-telebirr.response';
import {
  CreateCorporateTransactionCommand,
  InitiateCorporateTeleBirrPaymentCommand,
  PayCorporateWithChapaCommand,
  TransferToMultipleEmployeesCommand,
  TransferToSingleEmployeeCommand,
} from '@credit/usecases/corporate-transactions/corporate-transaction.commands';
import {
  CorporateTransactionByPaymentResponse,
  CorporateTransactionResponse,
} from '@credit/usecases/corporate-transactions/corporate-transaction.response';
import { CorporateTransactionCommands } from '@credit/usecases/corporate-transactions/corporate-transaction.usecase.commands';
import { CorporateTransactionQuery } from '@credit/usecases/corporate-transactions/corporate-transaction.usecase.queries';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { TransactionTotalResponse } from '@libs/common/corporate-transaction-spending';
import { PaymentMethod, PaymentStatus } from '@libs/common/enums';
import { Util } from '@libs/common/util';
import { PaymentService } from '@libs/payment/chapa/services/payment.service';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('corporate-transactions')
@ApiTags('corporate-transactions')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class CorporateTransactionsController {
  constructor(
    private commands: CorporateTransactionCommands,
    private queries: CorporateTransactionQuery,
    private readonly chapaPaymentService: PaymentService,
  ) {}
  @Get('get-transaction/:id')
  @ApiOkResponse({ type: CorporateTransactionResponse })
  async getTransaction(@Param('id') id: string) {
    return this.queries.getTransaction(id);
  }
  @Get('get-archived-transaction/:id')
  @ApiOkResponse({ type: CorporateTransactionResponse })
  async getArchivedCorporateTransaction(@Param('id') id: string) {
    return this.queries.getTransaction(id, true);
  }
  @Get('get-transactions')
  @ApiPaginatedResponse(CorporateTransactionResponse)
  async getTransactions(@Query() query: CollectionQuery) {
    return this.queries.getTransactions(query);
  }
  @Get('get-my-transactions')
  @ApiPaginatedResponse(CorporateTransactionResponse)
  async getMyTransactions(
    @CurrentUser() profile: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransaction(profile.id, query);
  }
  @Get('get-corporate-transaction/:corporateId')
  @ApiPaginatedResponse(CorporateTransactionResponse)
  async getCorporateTransactions(
    @Param('corporateId') corporateId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransaction(corporateId, query);
  }
  @Get('get-total-corporate-deposits')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getTotalCorporateDeposits(@Query() query: CollectionQuery) {
    return this.queries.getTotalCorporateDeposits(query);
  }
  @Get('group-corporate-transactions-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupCorporateTransactionsByStatus(@Query() query: CollectionQuery) {
    return this.queries.groupCorporateTransactionsByStatus(query);
  }
  @Get('get-my-transaction-spending')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getMyTransactionSpending(
    @CurrentUser() profile: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransactionSpending(profile.id, query);
  }
  @Get('get-corporate-transaction-spending/:corporateId')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getCorporateTransactionSpending(
    @Param('corporateId') corporateId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransactionSpending(corporateId, query);
  }
  @Get('get-corporate-transaction-spending-count/:corporateId')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getCorporateTransactionSpendingCount(
    @Param('corporateId') corporateId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransactionSpendingCount(
      corporateId,
      query,
    );
  }
  @Get('get-corporate-transaction-deposit-count/:corporateId')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getCorporateTransactionDepositCount(
    @Param('corporateId') corporateId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransactionDepositCount(corporateId, query);
  }
  @Get(
    'get-corporate-transaction-spending-to-employee/:corporateId/:employeeId',
  )
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getCorporateTransactionSpendingToEmployee(
    @Param('corporateId') corporateId: string,
    @Param('employeeId') employeeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransactionSpendingToEmployee(
      corporateId,
      employeeId,
      query,
    );
  }
  @Get('get-my-transaction-spending-to-employee/:employeeId')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getMyTransactionSpendingToEmployee(
    @CurrentUser() profile: UserInfo,
    @Param('employeeId') employeeId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.queries.getCorporateTransactionSpendingToEmployee(
      profile.id,
      employeeId,
      query,
    );
  }
  @Post('create-transaction')
  @UseGuards(RolesGuard('corporate|admin|finance'))
  @ApiOkResponse({ type: CorporateTransactionResponse })
  async createTransaction(
    @Body() createTransactionCommand: CreateCorporateTransactionCommand,
  ) {
    return this.commands.createCorporateTransaction(createTransactionCommand);
  }
  @Get('get-archived-transactions')
  @ApiPaginatedResponse(CorporateTransactionResponse)
  async getArchivedTransactions(@Query() query: CollectionQuery) {
    return this.queries.getArchivedTransactions(query);
  }
  @Get('group-corporate-transactions-by-payment-method')
  @ApiOkResponse({ type: CorporateTransactionByPaymentResponse, isArray: true })
  async groupCorporateTransactionsByPaymentMethod(
    @Query() query: CollectionQuery,
  ) {
    return this.queries.groupCorporateTransactionsByPaymentMethod(query);
  }
  //For corporate payment
  @Post('pay-with-chapa')
  @UseGuards(RolesGuard('corporate'))
  async updateMyWalletWithChapa(
    @CurrentUser() user: UserInfo,
    @Body() command: PayCorporateWithChapaCommand,
  ) {
    command.corporateId = user.id;
    command.status = PaymentStatus.Pending;
    command.method = PaymentMethod.Chapa;
    command.reason = 'Recharge wallet with Chapa';
    const transaction = await this.commands.createCorporateTransaction(
      command as CreateCorporateTransactionCommand,
    );
    if (!transaction) {
      throw new BadRequestException(`Unable to process the payment`);
    }
    const userName = user.name.split(' ');
    let firstName = ' ';
    if (userName[0]) {
      firstName = userName[0];
    }
    let lastName = ' ';
    if (userName[userName.length - 1]) {
      lastName = userName[userName.length - 1];
    }
    const formData = {
      amount: command.amount,
      currency: 'ETB',
      email: user.email ? user.email : process.env.DEFAULT_CHAPA_EMAIL,
      first_name: firstName,
      last_name: lastName.trim() ? lastName.trim() : 'No name',
      tx_ref: transaction.id,
      callback_url: `${process.env.API_URL}/api/corporate-transactions/chapa-callback`,
      return_url: command.returnUrl,
    };
    const response = {
      paymentInfo: await this.chapaPaymentService.initializePayment(formData),
      transaction,
    };
    return response;
  }
  @Get('chapa-callback')
  @AllowAnonymous()
  async chapaCallback(@Body() paymentPayload: any) {
    // console.log('paymentPayload', paymentPayload);
    if (paymentPayload.status === 'success') {
      const verificationData = await this.chapaPaymentService.verifyPayment(
        paymentPayload.trx_ref,
      );
      if (verificationData) {
        this.commands.approveCorporateChapaTransaction(
          paymentPayload.trx_ref,
          verificationData,
        );
      }
    }
  }

  @Post('initiate-telebirr')
  @UseGuards(RolesGuard('corporate'))
  @ApiOkResponse({ type: InitiateTeleBirrResponse })
  async initiateCorporateTelebirr(
    @CurrentUser() user: UserInfo,
    @Body() command: InitiateCorporateTeleBirrPaymentCommand,
  ) {
    const createTransactionCommand = new CreateCorporateTransactionCommand();
    createTransactionCommand.corporateId = user.id;
    createTransactionCommand.method = PaymentMethod.Telebirr;
    createTransactionCommand.status = PaymentStatus.Pending;
    createTransactionCommand.amount = command.amount;
    createTransactionCommand.reason = 'Recharge wallet with Telebirr';
    createTransactionCommand.returnUrl = command.returnUrl;
    createTransactionCommand.depositedBy = {
      id: user.id,
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
      isCorporate: true,
    };
    const result = await this.commands.initiateTeleBirrPayment(
      createTransactionCommand,
    );
    const ussd = Util.encryptData(JSON.stringify(result));
    const signJson = {
      ...result,
      appKey: process.env.AppKey,
    };
    // console.log(signJson, result);

    const signature = Util.signData(signJson);
    const payload = {
      appid: result.appId,
      sign: signature,
      ussd: ussd,
    };
    const axiosInstance = axios.create({
      baseURL: process.env.CorporateBaseUrl,
      timeout: Number(process.env.TimeoutCorporate),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const query = await axiosInstance.post('/toTradeWebPay', payload);
    return query.data;
  }
}
