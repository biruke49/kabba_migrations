import { RolesGuard } from '@account/guards/role.quards';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import {
  GroupByStatusResponse,
  TransactionResponse,
} from '@credit/usecases/transactions/transaction.response';
import {
  CreateTransactionCommand,
  PayWithChapaCommand,
} from '@credit/usecases/transactions/transaction.commands';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { TransactionCommands } from '@credit/usecases/transactions/transaction.usecase.commands';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { TransactionQuery } from '@credit/usecases/transactions/transaction.usecase.queries';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PaymentService } from '@libs/payment/chapa/services/payment.service';
import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { StartChapaPaymentResponse } from '@libs/payment/chapa/dtos/payment.dto';
import { InitiateTeleBirrResponse } from '@credit/domains/initiate-telebirr.response';
import { PaymentMethod, PaymentStatus } from '@libs/common/enums';
import { TransactionTotalResponse } from '@libs/common/corporate-transaction-spending';
import { CountBookingByAssignmentResponse } from '@order/usecases/bookings/booking.response';

@Controller('transactions')
@ApiTags('transactions')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class TransactionsController {
  constructor(
    private command: TransactionCommands,
    private transactionQuery: TransactionQuery,
    private readonly chapaPaymentService: PaymentService,
  ) {}
  @Get('get-transaction/:id')
  @ApiOkResponse({ type: TransactionResponse })
  async getTransaction(@Param('id') id: string) {
    return this.transactionQuery.getTransaction(id);
  }
  @Get('get-archived-transaction/:id')
  @ApiOkResponse({ type: TransactionResponse })
  async getArchivedTransaction(@Param('id') id: string) {
    return this.transactionQuery.getTransaction(id, true);
  }
  @Get('get-transactions')
  @ApiPaginatedResponse(TransactionResponse)
  async getTransactions(@Query() query: CollectionQuery) {
    return this.transactionQuery.getTransactions(query);
  }
  @Get('get-my-transactions')
  @ApiPaginatedResponse(TransactionResponse)
  async getMyTransactions(
    @CurrentUser() profile: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.transactionQuery.getPassengerTransaction(profile.id, query);
  }
  @Get('get-passenger-transactions/:passengerId')
  @ApiPaginatedResponse(TransactionResponse)
  async getCorporateTransactions(
    @Param('passengerId') passengerId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.transactionQuery.getPassengerTransaction(passengerId, query);
  }
  @Get('get-total-transaction-deposits')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getTotalTransactionDeposits(@Query() query: CollectionQuery) {
    return this.transactionQuery.getTotalTransactionDeposits(query);
  }
  @Get('get-total-deposits')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getTotalDeposits(@Query() query: CollectionQuery) {
    return this.transactionQuery.getTotalDeposits(query);
  }
  @Get('get-total-transaction-deposits-sum')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getTotalTransactionDepositsSum(@Query() query: CollectionQuery) {
    return this.transactionQuery.getTotalTransactionDepositsSum(query);
  }
  @Get('group-transactions-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupTransactionsByStatus(@Query() query: CollectionQuery) {
    return this.transactionQuery.groupTransactionsByStatus(query);
  }
  @Post('create-transaction')
  @ApiOkResponse({ type: TransactionResponse })
  async createTransaction(
    @Body() createTransactionCommand: CreateTransactionCommand,
  ) {
    return this.command.createTransaction(createTransactionCommand);
  }
  @Get('get-archived-transactions')
  @ApiPaginatedResponse(TransactionResponse)
  async getArchivedTransactions(@Query() query: CollectionQuery) {
    return this.transactionQuery.getArchivedTransactions(query);
  }
  //For passenger payment
  @Post('update-my-wallet-with-chapa')
  @ApiOkResponse({ type: StartChapaPaymentResponse })
  @UseGuards(RolesGuard('passenger'))
  async updateMyWalletWithChapa(
    @CurrentUser() user: UserInfo,
    @Body() command: PayWithChapaCommand,
  ) {
    command.passengerId = user.id;
    command.depositedBy = {
      id: user.id,
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
      isCorporate: false,
    };
    command.status = PaymentStatus.Pending;
    command.method = PaymentMethod.Chapa;
    command.reason = 'Recharge wallet with Chapa';
    const transaction = await this.command.createTransaction(
      command as CreateTransactionCommand,
    );
    if (!transaction) {
      throw new BadRequestException(`Unable to process the payment`);
    }
    const userName = user.name.trim().split(' ');
    let firstName = ' ';
    if (userName[0]) {
      firstName = userName[0];
    }
    let lastName = ' ';
    if (userName[userName.length - 1]) {
      lastName = userName[userName.length - 1];
    }
    const formData = {
      amount: command.amount,
      currency: 'ETB',
      email: user.email ? user.email : process.env.DEFAULT_CHAPA_EMAIL,
      first_name: firstName,
      last_name: lastName.trim() ? lastName.trim() : 'No name',
      tx_ref: transaction.id,
      callback_url: `${process.env.API_URL}/api/transactions/chapa-callback`,
      return_url: command.returnUrl,
    };
    const paymentInfo = await this.chapaPaymentService.initializePayment(
      formData,
    );

    const response = {
      checkout_url: paymentInfo.data.checkout_url,
      status: paymentInfo.status,
      message: paymentInfo.message,
      transactionId: transaction.id,
    };

    return response;
    // return {
    //   firstName: firstName,
    //   lastName: lastName,
    //   txRef: transaction.id,
    //   publicKey: process.env.CHAPA_PUBLIC_KEY,
    //   secretKey: process.env.CHAPA_SECRET_KEY,
    //   currency: 'ETB',
    //   amount: transaction.amount,
    //   email: user.email,
    // };
  }
  @Get('chapa-callback')
  @AllowAnonymous()
  async chapaCallback(@Body() paymentPayload: any) {
    if (paymentPayload.status === 'success') {
      const verificationData = await this.chapaPaymentService.verifyPayment(
        paymentPayload.trx_ref,
      );
      // console.log('verificationData', verificationData);
      if (verificationData) {
        this.command.approveChapaTransaction(
          paymentPayload.trx_ref,
          verificationData,
        );
      }
    }
  }
  // @Post('initiate-teleBirr')
  // async initiateTeleBirr(
  //   @CurrentUser() passenger: UserInfo,
  //   @Body() command: InitiateTelebirr,
  // ) {
  //   try {
  //     const inputs = req.body;
  //     inputs.driver_id = res.jwtPayload.userId;
  //     inputs.deposited_by = {
  //       id: res.jwtPayload.userId,
  //       name: res.jwtPayload.name,
  //       email: res.jwtPayload.email,
  //       phone_number: res.jwtPayload.phone_number,
  //     };
  //     const transaction = await this.command.InitiateTeleBirrPayment(inputs);
  //     if (transaction) {
  //       const response = {
  //         outTradeNumber: transaction.id,
  //         message: '',
  //         code: '',
  //         appId: teleBirrConfig.AppId,
  //         receiverName: res.jwtPayload.name,
  //         shortCode: teleBirrConfig.ShortCode,
  //         inAppPaymentUrl: teleBirrConfig.InAppPaymentUrl,
  //         subject: 'Pay credit with telebirr',
  //         returnUrl: teleBirrConfig.ReturnUrl,
  //         notifyUrl: teleBirrConfig.NotifyUrl,
  //         timeoutExpress: teleBirrConfig.TimeoutExpress,
  //         appKey: teleBirrConfig.AppKey,
  //         publicKey: teleBirrConfig.PublicKey,
  //       };
  //       console.log('InitiateTeleBirr', response);
  //       res.status(200).json(response);
  //     } else {
  //       res
  //         .status(400)
  //         .json('Cannot initiate telebirr payment process, Please try again');
  //     }
  //   } catch (err) {
  //     console.log('Sample Log', err);
  //     res.status(500).json('Unable to process your request, Please try again');
  //   }
  // }
  @Get('initiate-telebirr')
  @UseGuards(RolesGuard('passenger'))
  @ApiOkResponse({ type: InitiateTeleBirrResponse })
  async initiateCorporateTelebirr(@CurrentUser() user: UserInfo) {
    const createTransactionCommand = new CreateTransactionCommand();
    createTransactionCommand.passengerId = user.id;
    createTransactionCommand.method = PaymentMethod.Telebirr;
    createTransactionCommand.status = PaymentStatus.Pending;
    createTransactionCommand.amount = 0;
    createTransactionCommand.reason = 'Recharge wallet with Telebirr';
    createTransactionCommand.depositedBy = {
      id: user.id,
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
      isCorporate: false,
    };
    return await this.command.initiateTeleBirrPayment(createTransactionCommand);
  }
}
