import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { TransferToMultipleEmployeesCommand } from '@credit/usecases/corporate-transactions/corporate-transaction.commands';
import {
  CorporateTransactionResponse,
  WalletTransactionResponse,
} from '@credit/usecases/corporate-transactions/corporate-transaction.response';
import { UpdateCorporateWalletCommand } from '@credit/usecases/corporate-wallets/corporate-wallet.commands';
import { CorporateWalletResponse } from '@credit/usecases/corporate-wallets/corporate-wallet.response';
import { CorporateWalletCommands } from '@credit/usecases/corporate-wallets/corporate-wallet.usecase.commands';
import { CorporateWalletQuery } from '@credit/usecases/corporate-wallets/corporate-wallet.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { TransactionTotalResponse } from '@libs/common/corporate-transaction-spending';
import { PaymentMethod, PaymentStatus } from '@libs/common/enums';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('corporate-wallets')
@ApiTags('corporate-wallets')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiResponse({ status: 400, description: 'Bad request' })
@ApiExtraModels(DataResponseFormat)
export class CorporateWalletsController {
  constructor(
    private command: CorporateWalletCommands,
    private queries: CorporateWalletQuery,
  ) {}
  @Get('get-wallet/:id')
  @ApiOkResponse({ type: CorporateWalletResponse })
  async getWallet(@Param('id') id: string) {
    return this.queries.getWallet(id);
  }
  @Get('get-archived-wallet/:id')
  @ApiOkResponse({ type: CorporateWalletResponse })
  async getArchivedWallet(@Param('id') id: string) {
    return this.queries.getWallet(id, true);
  }
  @Get('get-wallets')
  @ApiPaginatedResponse(CorporateWalletResponse)
  async getWallets(@Query() query: CollectionQuery) {
    return this.queries.getCorporateWallets(query);
  }
  @Get('get-corporate-wallet/:corporateId')
  @ApiOkResponse({ type: CorporateWalletResponse })
  async getCorporateWallets(@Param('corporateId') corporateId: string) {
    return this.queries.getCorporateWallet(corporateId);
  }
  @Get('get-my-wallet')
  @ApiOkResponse({ type: CorporateWalletResponse })
  async getMyCorporateWallets(@CurrentUser() profile: UserInfo) {
    return this.queries.getCorporateWallet(profile.id);
  }
  @Get('get-total-corporate-wallet-balance')
  @ApiOkResponse({ type: WalletTransactionResponse })
  async getTotalCorporateWalletBalance(@Query() query: CollectionQuery) {
    return this.queries.getTotalCorporateWalletBalance(query);
  }
  @Post('update-wallet')
  @UseGuards(RolesGuard('corporate|admin|finance'))
  @ApiOkResponse({ type: CorporateTransactionResponse })
  async updateWallet(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateCorporateWalletCommand,
  ) {
    command.currentUser = user;
    return this.command.updateCorporateWalletManually(command);
  }
  @Get('get-archived-wallets')
  @ApiPaginatedResponse(CorporateWalletResponse)
  async getArchivedWallets(@Query() query: CollectionQuery) {
    return this.queries.getArchivedCorporateWallets(query);
  }
  @Post('transfer-wallet-to-multiple-employees')
  @UseGuards(RolesGuard('corporate|admin|finance'))
  async transferMultipleEmployees(
    @CurrentUser() corporate: UserInfo,
    @Body() command: TransferToMultipleEmployeesCommand,
  ) {
    command.corporateId = corporate.id;
    command.method = PaymentMethod.WalletTransferToEmployee;
    command.status = PaymentStatus.Success;
    command.depositedBy = {
      id: corporate.id,
      name: corporate.name,
      phoneNumber: corporate.phoneNumber,
      isCorporate: true,
      email: corporate.email,
    };
    return this.command.transferToMultipleEmployees(command);
  }
  @Post('transfer-wallet-to-corporate-employees/:corporateId')
  @UseGuards(RolesGuard('corporate|admin|finance'))
  async transferCorporateEmployees(
    @CurrentUser() user: UserInfo,
    @Param('corporateId') corporateId: string,
    @Body() command: TransferToMultipleEmployeesCommand,
  ) {
    command.corporateId = corporateId;
    command.method = PaymentMethod.WalletTransferToEmployee;
    command.status = PaymentStatus.Success;
    command.depositedBy = {
      id: user.id,
      name: user.name,
      phoneNumber: user.phoneNumber,
      isCorporate: true,
      email: user.email,
    };
    return this.command.transferToMultipleEmployees(command);
  }
}
