import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { RolesGuard } from '@account/guards/role.quards';
import { TransactionResponse } from '@credit/usecases/transactions/transaction.response';
import {
  CreateWalletCommand,
  TransferWalletCommand,
  UpdatePassengerWalletCommand,
  UpdateWalletCommand,
} from '@credit/usecases/wallets/wallet.commands';
import { WalletResponse } from '@credit/usecases/wallets/wallet.response';
import { WalletCommands } from '@credit/usecases/wallets/wallet.usecase.commands';
import { WalletQuery } from '@credit/usecases/wallets/wallet.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { TransactionTotalResponse } from '@libs/common/corporate-transaction-spending';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@Controller('wallets')
@ApiTags('wallets')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class WalletsController {
  constructor(
    private commands: WalletCommands,
    private walletQueries: WalletQuery,
  ) {}
  @Get('get-wallet/:id')
  @ApiOkResponse({ type: WalletResponse })
  async getWallet(@Param('id') id: string) {
    return this.walletQueries.getWallet(id);
  }
  @Get('get-archived-wallet/:id')
  @ApiOkResponse({ type: WalletResponse })
  async getArchivedWallet(@Param('id') id: string) {
    return this.walletQueries.getWallet(id, true);
  }
  @Get('get-wallets')
  @ApiPaginatedResponse(WalletResponse)
  async getWallets(@Query() query: CollectionQuery) {
    return this.walletQueries.getWallets(query);
  }
  @Get('get-total-wallet-balance')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getTotalWalletBalance(@Query() query: CollectionQuery) {
    return this.walletQueries.getTotalWalletBalance(query);
  }
  @Get('get-passenger-wallet/:passengerId')
  @ApiOkResponse({ type: WalletResponse })
  async getCorporateWallets(@Param('passengerId') passengerId: string) {
    return this.walletQueries.getPassengerWallet(passengerId);
  }
  @Get('get-my-wallet')
  @ApiOkResponse({ type: WalletResponse })
  async getMyCorporateWallets(@CurrentUser() profile: UserInfo) {
    return this.walletQueries.getPassengerWallet(profile.id);
  }
  @Post('create-wallet')
  @ApiOkResponse({ type: WalletResponse })
  async createWallet(@Body() createWalletCommand: CreateWalletCommand) {
    return this.commands.createWallet(createWalletCommand);
  }
  @Get('get-archived-wallets')
  @ApiPaginatedResponse(WalletResponse)
  async getArchivedWallets(@Query() query: CollectionQuery) {
    return this.walletQueries.getArchivedWallets(query);
  }
  @Post('transfer')
  @UseGuards(RolesGuard('passenger'))
  @ApiOkResponse({ type: WalletResponse })
  async transferWallet(
    @CurrentUser() passenger: UserInfo,
    @Body() command: TransferWalletCommand,
  ) {
    if (command.amount <= 0) {
      throw new BadRequestException(`Transfer Amount must be greater than 0`);
    }
    // if (!command.phoneNumber.startsWith('+')) {
    // command.phoneNumber = '+' + command.phoneNumber.trim();
    command.phoneNumber = command.phoneNumber.trim();
    // }
    command.depositedBy = {
      id: passenger.id,
      name: passenger.name,
      email: passenger.email,
      phoneNumber: passenger.phoneNumber,
      isCorporate: false,
      isInternalUser: false,
    };
    return this.commands.transferWallet(passenger.id, command);
  }
  @Post('update-wallet')
  @UseGuards(RolesGuard('admin|finance'))
  @ApiOkResponse({ type: TransactionResponse })
  async updateWallet(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdatePassengerWalletCommand,
  ) {
    command.currentUser = user;
    command.depositedBy = {
      id: user.id,
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
      isCorporate: false,
      isInternalUser: true,
    };
    return this.commands.updateWalletManually(command);
  }
}
