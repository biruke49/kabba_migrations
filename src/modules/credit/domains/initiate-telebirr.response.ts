import { ApiProperty } from '@nestjs/swagger';

export class InitiateTeleBirrResponse {
  @ApiProperty()
  transactionId: string;
  @ApiProperty()
  outTradeNumber: string;
  @ApiProperty()
  message?: string;
  @ApiProperty()
  code?: string;
  @ApiProperty()
  appId: string;
  @ApiProperty()
  receiverName: string;
  @ApiProperty()
  shortCode: string;
  @ApiProperty()
  inAppPaymentUrl: string;
  @ApiProperty()
  webPaymentUrl: string;
  @ApiProperty()
  subject: string;
  @ApiProperty()
  returnUrl: string;
  @ApiProperty()
  notifyUrl: string;
  @ApiProperty()
  timeoutExpress: string;
  @ApiProperty()
  appKey: string;
  @ApiProperty()
  publicKey: string;
}
