import { Passenger } from '@customer/domains/passengers/passenger';

export class Wallet {
  id: string;
  balance: number;
  corporateWalletBalance?: number;
  passengerId: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  passenger: Passenger;
}
