import { Wallet } from '@credit/domains/wallets/wallet';
export interface IWalletRepository {
  insert(user: Wallet): Promise<Wallet>;
  update(user: Wallet): Promise<Wallet>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Wallet[]>;
  getById(id: string, withDeleted: boolean): Promise<Wallet>;
  getByPassengerId(passengerId: string, withDeleted: boolean): Promise<Wallet>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
