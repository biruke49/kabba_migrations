import { Corporate } from '@customer/domains/corporates/corporate';

export class CorporateWallet {
  id: string;
  balance: number;
  corporateId: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  corporate: Corporate;
}
