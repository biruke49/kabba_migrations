import { CorporateWallet } from '@credit/domains/corporate-wallets/corporate-wallet';
export interface ICorporateWalletRepository {
  insert(user: CorporateWallet): Promise<CorporateWallet>;
  update(user: CorporateWallet): Promise<CorporateWallet>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<CorporateWallet[]>;
  getById(id: string, withDeleted: boolean): Promise<CorporateWallet>;
  getByCorporateId(
    corporateId: string,
    withDeleted: boolean,
  ): Promise<CorporateWallet>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
