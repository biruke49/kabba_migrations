import { Transaction } from '@credit/domains/transactions/transaction';
export interface ITransactionRepository {
  insert(user: Transaction): Promise<Transaction>;
  update(user: Transaction): Promise<Transaction>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Transaction[]>;
  getById(id: string, withDeleted: boolean): Promise<Transaction>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
