import { Passenger } from '@customer/domains/passengers/passenger';
import { DepositedBy } from './deposited-by';

export class Transaction {
  id: string;
  passengerId: string;
  method: string;
  status: string;
  depositedBy: DepositedBy;
  amount: number;
  transactionNumber: string;
  tradeNumber: string;
  tradeDate: Date;
  reason: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  passenger: Passenger;
}
