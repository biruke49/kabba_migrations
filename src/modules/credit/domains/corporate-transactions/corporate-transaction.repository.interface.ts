import { CorporateTransaction } from '@credit/domains/corporate-transactions/corporate-transaction';
export interface ICorporateTransactionRepository {
  insert(user: CorporateTransaction): Promise<CorporateTransaction>;
  update(user: CorporateTransaction): Promise<CorporateTransaction>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<CorporateTransaction[]>;
  getById(id: string, withDeleted: boolean): Promise<CorporateTransaction>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
