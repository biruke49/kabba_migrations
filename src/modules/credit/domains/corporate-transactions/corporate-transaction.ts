import { Corporate } from '@customer/domains/corporates/corporate';
import { Passenger } from '@customer/domains/passengers/passenger';

export class CorporateTransaction {
  id: string;
  corporateId: string;
  method: string;
  status: string;
  amount: number;
  transactionNumber: string;
  tradeNumber: string;
  tradeDate: Date;
  reason: string;
  employeeId: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  corporate: Corporate;
  employee: Passenger;
}
