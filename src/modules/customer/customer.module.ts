import { CorporateQuery } from '@customer/usecases/corporates/corporate.usecase.queries';
import { CorporateCommands } from '@customer/usecases/corporates/corporate.usecase.commands';
import { CorporateRepository } from '@customer/persistence/corporates/corporate.repository';
import { CorporateEntity } from '@customer/persistence/corporates/corporate.entity';
import { CorporatesController } from './controllers/corporate.controller';
import { PassengerQuery } from '@customer/usecases/passengers/passenger.usecase.queries';
import { PassengerCommands } from '@customer/usecases/passengers/passenger.usecase.commands';
import { PassengerRepository } from './persistence/passengers/passenger.repository';
import { PassengerEntity } from './persistence/passengers/passenger.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountModule } from '@account/account.module';
import { Module } from '@nestjs/common';
import { PassengersController } from './controllers/passenger.controller';
import { FileManagerService } from '@libs/common/file-manager';
import { ParentRepository } from './persistence/parents/parent.repository';
import { ParentCommands } from './usecases/parents/parent.usecase.commands';
import { ParentQuery } from './usecases/parents/parent.usecase.queries';
import { SchoolRepository } from './persistence/schools/school.repository';
import { SchoolCommands } from './usecases/schools/school.usecase.commands';
import { SchoolQuery } from './usecases/schools/school.usecase.queries';
import { ParentEntity } from './persistence/parents/parent.entity';
import { KidEntity } from './persistence/parents/kid.entity';
import { SchoolEntity } from './persistence/schools/school.entity';
import { ParentsController } from './controllers/parent.controller';
import { SchoolsController } from './controllers/school.controller';
import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { InteractionModule } from '@interaction/interaction.module';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { CategoryRepository } from '@classification/persistence/categories/category.repository';
import { GroupRepository } from '@assignment/persistence/groups/group.repository';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { GroupCommands } from '@assignment/usecases/groups/group.usecase.commands';
import { ConfigurationQuery } from '@configurations/usecases/configuration/configuration.usecase.queries';
import { AppService } from 'app.service';
import { ConfigurationEntity } from '@configurations/persistence/configuration/configuration.entity';
import { GroupQuery } from '@assignment/usecases/groups/group.usecase.queries';
import { ChatQuery } from '@chat/usecases/chat.usecase.queries';
import { ConfigurationRepository } from '@configurations/persistence/configuration/configuration.repository';
import { ChatRepository } from '@chat/persistence/chat.repository';
import { ChatEntity } from '@chat/persistence/chat.entity';
import { AccountEntity } from '@account/persistence/accounts/account.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PassengerEntity,
      CorporateEntity,
      ParentEntity,
      KidEntity,
      SchoolEntity,
      ParentPreferenceEntity,
      CategoryEntity,
      GroupEntity,
      GroupAssignmentEntity,
      ConfigurationEntity,
      ChatEntity,
      AccountEntity,
    ]),
    AccountModule,
    InteractionModule,
  ],
  providers: [
    PassengerRepository,
    PassengerCommands,
    PassengerQuery,
    CorporateRepository,
    CorporateCommands,
    CorporateQuery,
    ParentRepository,
    ParentCommands,
    ParentQuery,
    SchoolRepository,
    SchoolCommands,
    SchoolQuery,
    CategoryRepository,
    FileManagerService,
    GroupRepository,
    AppService,
    ConfigurationQuery,
    GroupQuery,
    ChatQuery,
    ConfigurationRepository,
    ChatRepository,
  ],
  controllers: [
    PassengersController,
    CorporatesController,
    ParentsController,
    SchoolsController,
  ],
  exports: [PassengerQuery, ParentCommands, ParentRepository, ParentQuery],
})
export class CustomerModule {}
