import { Parent } from './parent';

export interface IParentRepository {
  insert(parent: Parent): Promise<Parent>;
  update(parent: Parent): Promise<Parent>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Parent[]>;
  getById(id: string, withDeleted: boolean): Promise<Parent>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
  getByPhoneNumber(phoneNumber: string, withDeleted: boolean): Promise<Parent>;
}
