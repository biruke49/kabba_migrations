import { FileDto } from '@libs/common/file-dto';
import { School } from '../schools/school';
import { Parent } from './parent';
import { Category } from '@classification/domains/categories/category';
import { GroupAssignment } from '@assignment/domains/groups/group-assignment';

export class Kid {
  id: string;
  parentId: string;
  schoolId: string;
  schoolName: string;
  name: string;
  grade: string;
  age: number;
  gender: string;
  profileImage: FileDto;
  remark: string;
  status: string;
  kidTravelStatus: string;
  categoryId: string;
  categoryName: string;
  transportationTime: string;
  startDate: Date;
  enabled: boolean;
  assigned: boolean;
  distanceFromSchool: number;
  kidFee: number;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  school?: School;
  parent?: Parent;
  category?: Category;
  groupAssignment?: GroupAssignment;
}
