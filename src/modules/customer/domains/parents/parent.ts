import { ParentPreference } from '@interaction/domains/parent-preference/parent-preference';
import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';
import { Kid } from './kid';
export class Parent {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  gender: string;
  profileImage: FileDto;
  address: Address;
  status: string;
  totalPayment: number;
  enabled: boolean;
  // isActive: boolean;
  lat: number;
  lng: number;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  kids?: Kid[];
  parentPreferences?: ParentPreference[];
  async addKid(kid: Kid) {
    this.kids.push(kid);
  }
  async updateKid(kid: Kid) {
    const existIndex = this.kids.findIndex((element) => element.id == kid.id);
    this.kids[existIndex] = kid;
  }
  async removeKid(id: string) {
    this.kids = this.kids.filter((element) => element.id != id);
  }
  async updateKids(kids: Kid[]) {
    this.kids = kids;
  }
}
