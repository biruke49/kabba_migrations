import { Address } from '@libs/common/address';
import { ContactPerson } from '@libs/common/emergency-contact';
import { FileDto } from '@libs/common/file-dto';
import { Kid } from '../parents/kid';

export class School {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  website: string;
  logo: FileDto;
  address: Address;
  lat: number;
  lng: number;
  status: string;
  isPrivate: boolean;
  enabled: boolean;
  contactPerson: ContactPerson;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  kids?: Kid[];
}
