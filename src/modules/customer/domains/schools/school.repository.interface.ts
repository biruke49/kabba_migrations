import { School } from './school';

export interface ISchoolRepository {
  insert(school: School): Promise<School>;
  update(school: School): Promise<School>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<School[]>;
  getById(id: string, withDeleted: boolean): Promise<School>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
  getByPhoneNumber(phoneNumber: string, withDeleted: boolean): Promise<School>;
}
