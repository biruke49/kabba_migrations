import { Corporate } from '@customer/domains/corporates/corporate';
export interface ICorporateRepository {
  insert(user: Corporate): Promise<Corporate>;
  update(user: Corporate): Promise<Corporate>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Corporate[]>;
  getById(id: string, withDeleted: boolean): Promise<Corporate>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
  getByPhoneNumber(
    phoneNumber: string,
    withDeleted: boolean,
  ): Promise<Corporate>;
  getByTinNumber(tinNumber: string, withDeleted: boolean): Promise<Corporate>;
  getByEmail(email: string, withDeleted: boolean): Promise<Corporate>;
}
