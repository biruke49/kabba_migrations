import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';
import { ContactPerson } from '@customer/domains/corporates/contact-person';
import { CorporateWallet } from '@credit/domains/corporate-wallets/corporate-wallet';
export class Corporate {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  enabled: boolean;
  tinNumber: string;
  website: string;
  address: Address;
  logo: FileDto;
  contactPerson: ContactPerson;
  landmark: string;
  lat: number;
  lng: number;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  wallet: CorporateWallet;
}
