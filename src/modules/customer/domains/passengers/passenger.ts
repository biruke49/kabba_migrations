import { Corporate } from '@customer/domains/corporates/corporate';
import { FileDto } from '@libs/common/file-dto';

import { Address } from '@libs/common/address';
import { EmergencyContact } from '@libs/common/emergency-contact';
import { Wallet } from '@credit/domains/wallets/wallet';

export class Passenger {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  gender: string;
  profileImage: FileDto;
  emergencyContact: EmergencyContact;
  address: Address;
  isActive: boolean;
  enabled: boolean;
  fcmId: string;
  corporateId?: string;
  corporateName: string;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  corporate: Corporate;
  wallet: Wallet;
}
