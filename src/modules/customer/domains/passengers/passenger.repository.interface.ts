import { Passenger } from '@customer/domains/passengers/passenger';
export interface IPassengerRepository {
  insert(user: Passenger): Promise<Passenger>;
  update(user: Passenger): Promise<Passenger>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Passenger[]>;
  getById(id: string, withDeleted: boolean): Promise<Passenger>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
  getByPhoneNumber(
    phoneNumber: string,
    withDeleted: boolean,
  ): Promise<Passenger>;
  getByEmail(email: string, withDeleted: boolean): Promise<Passenger>;
}
