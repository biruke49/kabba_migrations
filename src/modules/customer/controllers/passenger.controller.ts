import { diskStorage } from 'multer';
import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import {
  AddOrRemoveCorporatePassengerCommand,
  ArchivePassengerCommand,
  CreateCorporatePassengerCommand,
  CreatePassengerCommand,
  UpdatePassengerCommand,
  UpdateProfileCommand,
} from '@customer/usecases/passengers/passenger.commands';
import { PassengerResponse } from '@customer/usecases/passengers/passenger.response';
import { PassengerCommands } from '@customer/usecases/passengers/passenger.usecase.commands';
import { PassengerQuery } from '@customer/usecases/passengers/passenger.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  StreamableFile,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { FileInterceptor } from '@nestjs/platform-express';
import * as XLSX from 'xlsx';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';
@Controller('passengers')
@ApiTags('passengers')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class PassengersController {
  constructor(
    private command: PassengerCommands,
    private passengerQuery: PassengerQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-passenger/:id')
  @ApiOkResponse({ type: PassengerResponse })
  async getPassenger(@Param('id') id: string) {
    return this.passengerQuery.getPassenger(id);
  }
  @Get('get-archived-passenger/:id')
  @ApiOkResponse({ type: PassengerResponse })
  async getArchivedPassenger(@Param('id') id: string) {
    return this.passengerQuery.getPassenger(id, true);
  }
  @Get('get-passengers')
  @ApiPaginatedResponse(PassengerResponse)
  async getPassengers(@Query() query: CollectionQuery) {
    return this.passengerQuery.getPassengers(query);
  }
  @Get('group-passengers-by-gender')
  @ApiOkResponse({ type: CountByGenderResponse, isArray: true })
  async groupPassengersByGender(@Query() query: CollectionQuery) {
    return this.passengerQuery.groupPassengersByGender(query);
  }
  @Get('group-passengers-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupPassengersByStatus(@Query() query: CollectionQuery) {
    return this.passengerQuery.groupPassengersByStatus(query);
  }
  @Get('group-passengers-by-created-date/:format')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async groupBookingsByCreatedDate(
    @Query() query: CollectionQuery,
    @Param('format') format: string,
  ) {
    return this.passengerQuery.groupPassengersByCreatedDate(query, format);
  }
  @Get('group-passengers-by-address/:address')
  @ApiOkResponse({ type: GroupByAddressResponse, isArray: true })
  async groupPassengersByAddress(
    @Param('address') address: string,
    @Query() query: CollectionQuery,
  ) {
    return this.passengerQuery.groupPassengersByAddress(address, query);
  }
  @Get('get-corporate-customers/:corporateId')
  @ApiPaginatedResponse(PassengerResponse)
  async getCorporatePassengers(
    @Param('corporateId') corporateId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.passengerQuery.getCorporatePassengers(corporateId, query);
  }
  @Get('get-our-corporate-customers')
  @UseGuards(RolesGuard('corporate'))
  @ApiPaginatedResponse(PassengerResponse)
  async getOurCorporatePassengers(
    @CurrentUser() user: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.passengerQuery.getCorporatePassengers(user.id, query);
  }
  @Post('create-passenger')
  @AllowAnonymous()
  @ApiOkResponse({ type: PassengerResponse })
  async createPassenger(
    @Body() createPassengerCommand: CreatePassengerCommand,
  ) {
    return this.command.createPassenger(createPassengerCommand);
  }
  @Post('create-corporate-passenger')
  @UseGuards(RolesGuard('admin|operator|corporate'))
  @ApiOkResponse({ type: PassengerResponse })
  async createCorporateEmployee(
    @CurrentUser() user: UserInfo,
    @Body() createCorporatePassengerCommand: CreateCorporatePassengerCommand,
  ) {
    return this.command.createCorporatePassenger(
      createCorporatePassengerCommand,
      user,
    );
  }
  @Put('update-passenger')
  @ApiOkResponse({ type: PassengerResponse })
  async updatePassenger(
    @Body() updatePassengerCommand: UpdatePassengerCommand,
  ) {
    return this.command.updatePassenger(updatePassengerCommand);
  }
  @Put('update-profile')
  @UseGuards(RolesGuard('passenger'))
  @ApiOkResponse({ type: PassengerResponse })
  async updateProfile(
    @CurrentUser() passenger: UserInfo,
    @Body() updateProfileCommand: UpdateProfileCommand,
  ) {
    updateProfileCommand.id = passenger.id;
    return this.command.updatePassenger(updateProfileCommand);
  }
  @Delete('archive-passenger')
  @UseGuards(PermissionsGuard('manage-passengers'))
  @ApiOkResponse({ type: PassengerResponse })
  async archivePassenger(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchivePassengerCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archivePassenger(archiveCommand);
  }
  @Delete('delete-passenger/:id')
  @UseGuards(PermissionsGuard('manage-passengers'))
  @ApiOkResponse({ type: Boolean })
  async deletePassenger(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.deletePassenger(id, user);
  }
  @Post('restore-passenger/:id')
  @UseGuards(PermissionsGuard('manage-passengers'))
  @ApiOkResponse({ type: PassengerResponse })
  async restorePassenger(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.restorePassenger(id, user);
  }
  @Get('get-archived-passengers')
  @ApiPaginatedResponse(PassengerResponse)
  async getArchivedPassengers(@Query() query: CollectionQuery) {
    return this.passengerQuery.getArchivedPassengers(query);
  }
  @Post('activate-or-block-passenger/:id')
  @UseGuards(PermissionsGuard('activate-or-block-passenger'))
  @ApiOkResponse({ type: PassengerResponse })
  async activateOrBlockPassenger(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockPassenger(id, user);
  }
  @Delete('remove-employee-from-corporate')
  @UseGuards(RolesGuard('admin|operator|corporate'))
  @ApiOkResponse({ type: PassengerResponse })
  async removeCorporateEmployee(
    @CurrentUser() user: UserInfo,
    @Body() command: AddOrRemoveCorporatePassengerCommand,
  ) {
    command.currentUser = user;
    return this.command.removeCorporateEmployee(command);
  }
  @Post('add-employee-to-corporate')
  @UseGuards(RolesGuard('admin|operator|corporate'))
  @ApiOkResponse({ type: PassengerResponse })
  async addCorporateEmployee(
    @CurrentUser() user: UserInfo,
    @Body() command: AddOrRemoveCorporatePassengerCommand,
  ) {
    command.currentUser = user;
    return this.command.addCorporateEmployee(command);
  }
  @Post('import-our-employees')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('users', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (
          !file.mimetype.includes(
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          )
        ) {
          return callback(
            new BadRequestException('Provide a valid excel file'),
            false,
          );
        }
        callback(null, true);
      },

      limits: { fileSize: Math.pow(1024, 3) },
    }),
  )
  @UseGuards(RolesGuard('corporate'))
  async importOurEmployees(
    @CurrentUser() corporate: UserInfo,
    @UploadedFile() users: Express.Multer.File,
  ) {
    if (users) {
      const result = await this.fileManagerService.uploadFile(
        users,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        const fileName = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${result.filename}`;
        const file = XLSX.readFile(fileName);

        const failedEmployees = [];
        const newEmployees = [];
        //  const response = {};
        const sheets = file.SheetNames;
        const temp = XLSX.utils.sheet_to_json(file.Sheets[sheets[0]]);
        for (const res of temp) {
          if (res) {
            const createEmployeeCommand = new CreateCorporatePassengerCommand();
            createEmployeeCommand.corporateId = corporate.id;
            createEmployeeCommand.name = res['Name'];
            createEmployeeCommand.email = res['Email'];
            createEmployeeCommand.phoneNumber = res['Phone'];
            createEmployeeCommand.gender = res['Gender'];
            createEmployeeCommand.address = {
              country: res['Country'],
              city: res['City'],
              woreda: res['Woreda'],
              houseNumber: res['HouseNumber'],
              subCity: res['SubCity'],
            };
            const isValid = await this.command.isValidEmployee(
              createEmployeeCommand,
            );
            // console.log(isValid);
            if (!isValid['hasError']) {
              const employee = await this.command.createCorporatePassenger(
                createEmployeeCommand,
                corporate,
                true,
              );
              if (employee && employee !== null) {
                newEmployees.push(employee);
              } else {
                failedEmployees.push(createEmployeeCommand);
              }
            } else {
              res['incorrectAttributes'] = isValid['error'];
              failedEmployees.push(res);
            }
          }
        }
        await this.fileManagerService.removeFile(
          result,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
        return { data: newEmployees, failedEmployees };
      }
    }
    throw new BadRequestException(`Bad Request`);
  }

  @Post('import-corporate-employees/:corporateId')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('users', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (
          !file.mimetype.includes(
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          )
        ) {
          return callback(
            new BadRequestException('Provide a valid excel file'),
            false,
          );
        }
        callback(null, true);
      },

      limits: { fileSize: Math.pow(1024, 3) },
    }),
  )
  @UseGuards(RolesGuard('admin|operator'))
  async importCorporateEmployees(
    @Param('corporateId') corporateId: string,
    @CurrentUser() corporate: UserInfo,
    @UploadedFile() users: Express.Multer.File,
  ) {
    if (users) {
      const result = await this.fileManagerService.uploadFile(
        users,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        const fileName = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${result.filename}`;
        const file = XLSX.readFile(fileName);

        const failedEmployees = [];
        const newEmployees = [];
        //  const response = {};
        const sheets = file.SheetNames;
        const temp = XLSX.utils.sheet_to_json(file.Sheets[sheets[0]]);
        for (const res of temp) {
          if (res) {
            const createEmployeeCommand = new CreateCorporatePassengerCommand();
            createEmployeeCommand.corporateId = corporateId;
            createEmployeeCommand.name = res['Name'];
            createEmployeeCommand.email = res['Email'];
            createEmployeeCommand.phoneNumber = res['Phone'];
            createEmployeeCommand.gender = res['Gender'];
            createEmployeeCommand.address = {
              country: res['Country'],
              city: res['City'],
              woreda: res['Woreda'],
              houseNumber: res['HouseNumber'],
              subCity: res['SubCity'],
            };
            const isValid = await this.command.isValidEmployee(
              createEmployeeCommand,
            );
            if (!isValid['hasError']) {
              const employee = await this.command.createCorporatePassenger(
                createEmployeeCommand,
                corporate,
                true,
              );
              if (employee && employee !== null) {
                newEmployees.push(employee);
              } else {
                failedEmployees.push(createEmployeeCommand);
              }
            } else {
              res['incorrectAttributes'] = isValid['error'];
              failedEmployees.push(res);
            }
          }
        }
        await this.fileManagerService.removeFile(
          result,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
        return { data: newEmployees, failedEmployees };
      }
    }
    throw new BadRequestException(`Bad Request`);
  }
  @Post('update-profile-image')
  @UseGuards(RolesGuard('passenger'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('profileImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  async uploadProfileImage(
    @CurrentUser() passenger: UserInfo,
    @UploadedFile() profileImage: Express.Multer.File,
  ) {
    if (profileImage) {
      const result = await this.fileManagerService.uploadFile(
        profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        return this.command.updateProfileImage(passenger.id, result);
      }
    }
    throw new BadRequestException(`Bad Request`);
  }
  @Get('check-passenger')
  @AllowAnonymous()
  async checkPassenger(@Query('phoneNumber') phoneNumber: string) {
    // if (!phoneNumber.startsWith('+')) {
    phoneNumber = phoneNumber.trim();
    // phoneNumber = '+' + phoneNumber.trim();
    // }
    return this.command.isPassengerExist(phoneNumber);
  }
  @Get('export-passengers')
  @AllowAnonymous()
  async exportPassengers(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    const fileName = `passengers-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const passengers = await this.passengerQuery.getPassengers(query);
    const passengerData = passengers.data;
    const rows = passengerData.map((passenger) => {
      return {
        Name: passenger.name,
        'Phone Number': passenger.phoneNumber,
        Email: passenger?.email,
        Gender: passenger?.gender,
        City: passenger?.address?.city,
        'Sub City': passenger?.address?.subCity,
        'Created Date': passenger.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Passengers`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [
        [
          'Name',
          'Phone Number',
          'Email',
          'Gender',
          'City',
          'Sub City',
          'Created Date',
        ],
      ],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
}
