import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import {
  ArchiveKidCommand,
  CreateKidCommand,
  DeleteKidCommand,
  UpdateKidCommand,
} from '@customer/usecases/parents/kid.commands';
import { KidResponse } from '@customer/usecases/parents/kid.response';
import {
  ArchiveParentCommand,
  CreateParentCommand,
  GetNearestParentQuery,
  ParentStatusCommand,
  RegisterParentCommand,
  UpdateParentCommand,
  UpdateParentPaymentCommand,
} from '@customer/usecases/parents/parent.commands';
import { ParentResponse } from '@customer/usecases/parents/parent.response';
import { ParentCommands } from '@customer/usecases/parents/parent.usecase.commands';
import { ParentQuery } from '@customer/usecases/parents/parent.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  StreamableFile,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';
import * as XLSX from 'xlsx';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
@Controller('parents')
@ApiTags('parents')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class ParentsController {
  constructor(
    private command: ParentCommands,
    private parentQuery: ParentQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-parent/:id')
  @AllowAnonymous()
  @ApiOkResponse({ type: ParentResponse })
  async getParent(@Param('id') id: string) {
    return this.parentQuery.getParent(id);
  }
  @Get('check-parent')
  @AllowAnonymous()
  async checkParent(@Query('phoneNumber') phoneNumber: string) {
    if (!phoneNumber.startsWith('+')) {
      phoneNumber = '+' + phoneNumber.trim();
    }
    return this.command.isParentExist(phoneNumber);
  }
  @Get('get-archived-parent/:id')
  @ApiOkResponse({ type: ParentResponse })
  async getArchivedParent(@Param('id') id: string) {
    return this.parentQuery.getParent(id, true);
  }
  @Get('get-nearest-parents')
  @AllowAnonymous()
  async getNearestParents(@Query() command: GetNearestParentQuery) {
    return this.parentQuery.getNearestParents(command);
  }
  @Get('get-parents')
  @AllowAnonymous()
  @ApiPaginatedResponse(ParentResponse)
  async getParents(@Query() query: CollectionQuery) {
    return this.parentQuery.getParents(query);
  }
  @Get('get-parents-with-school-id/:schoolId')
  @ApiPaginatedResponse(KidResponse)
  async getParentsBySchoolId(
    @Param('schoolId') schoolId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.parentQuery.getParentsBySchoolId(schoolId);
  }
  @Post('create-parent')
  @UseGuards(PermissionsGuard('manage-parents'))
  @ApiOkResponse({ type: ParentResponse })
  async createParent(
    @CurrentUser() user: UserInfo,
    @Body() createParentCommand: CreateParentCommand,
  ) {
    createParentCommand.currentUser = user;
    return this.command.createParent(createParentCommand);
  }
  @Post('register-parent')
  @AllowAnonymous()
  @ApiOkResponse({ type: ParentResponse })
  async registerParent(
    @CurrentUser() user: UserInfo,
    @Body() createParentCommand: RegisterParentCommand,
  ) {
    return this.command.registerParent(createParentCommand);
  }
  @Put('update-parent')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiOkResponse({ type: ParentResponse })
  async updateParent(
    @CurrentUser() user: UserInfo,
    @Body() updateParentCommand: UpdateParentCommand,
  ) {
    updateParentCommand.currentUser = user;
    return this.command.updateParent(updateParentCommand);
  }
  @Delete('archive-parent')
  @UseGuards(PermissionsGuard('manage-parents'))
  @ApiOkResponse({ type: ParentResponse })
  async archiveParent(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveParentCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveParent(archiveCommand);
  }
  @Delete('delete-parent/:id')
  @UseGuards(PermissionsGuard('manage-parents'))
  @ApiOkResponse({ type: Boolean })
  async deleteParent(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteParent(id, user);
  }
  @Post('restore-parent/:id')
  @UseGuards(PermissionsGuard('manage-parents'))
  @ApiOkResponse({ type: ParentResponse })
  async restoreParent(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreParent(id, user);
  }
  @Get('get-archived-parents')
  @ApiPaginatedResponse(ParentResponse)
  async getArchivedParents(@Query() query: CollectionQuery) {
    return this.parentQuery.getArchivedParents(query);
  }
  @Post('activate-or-block-parent/:id')
  @UseGuards(PermissionsGuard('activate-or-block-parents'))
  @ApiOkResponse({ type: ParentResponse })
  async activateOrBlockParent(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockParent(id, user);
  }
  @Post('change-status')
  @UseGuards(RolesGuard('parent|operator|admin|super_admin'))
  @ApiOkResponse({ type: ParentResponse })
  async changeParentStatus(
    @CurrentUser() user: UserInfo,
    @Body() command: ParentStatusCommand,
  ) {
    command.currentUser = user;
    return this.command.changeParentStatus(command);
  }
  @Post('update-total-parent-payment')
  @ApiOkResponse({ type: ParentResponse })
  async updateTotalParentPayment(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateParentPaymentCommand,
  ) {
    command.id = user.id;
    return this.command.updateTotalParentPayment(command);
  }
  @Post('update-parent-profile-image/:id')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('profileImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  async uploadParentProfileImage(
    @Param('id') id: string,
    @UploadedFile() profileImage: Express.Multer.File,
  ) {
    if (profileImage) {
      const result = await this.fileManagerService.uploadFile(
        profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        return this.command.updateParentProfileImage(id, result);
      }
    }
    throw new BadRequestException(`Bad Request`);
  }

  // Kid
  @Post('add-kid')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('profileImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: KidResponse })
  async addKid(
    @CurrentUser() user: UserInfo,
    @Body() createKidCommand: CreateKidCommand,
    @UploadedFile() profileImage: Express.Multer.File,
  ) {
    if (profileImage) {
      const result = await this.fileManagerService.uploadFile(
        profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        createKidCommand.profileImage = result;
      }
    }
    createKidCommand.currentUser = user;
    return this.command.addKid(createKidCommand);
  }
  @Put('update-kid')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('profileImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: KidResponse })
  async updateKid(
    @CurrentUser() user: UserInfo,
    @Body() updateKidCommand: UpdateKidCommand,
    @UploadedFile() profileImage: Express.Multer.File,
  ) {
    if (profileImage) {
      const result = await this.fileManagerService.uploadFile(
        profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        updateKidCommand.profileImage = result;
      }
    }
    updateKidCommand.currentUser = user;
    return this.command.updateKid(updateKidCommand);
  }
  @Delete('remove-kid')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiOkResponse({ type: Boolean })
  async removeKid(
    @CurrentUser() user: UserInfo,
    @Body() removeKidCommand: DeleteKidCommand,
  ) {
    removeKidCommand.currentUser = user;
    return this.command.deleteKid(removeKidCommand);
  }
  @Delete('archive-kid')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiOkResponse({ type: Boolean })
  async archiveKid(
    @CurrentUser() user: UserInfo,
    @Body() archiveKidCommand: ArchiveKidCommand,
  ) {
    archiveKidCommand.currentUser = user;
    return this.command.archiveKid(archiveKidCommand);
  }
  @Get('get-kid/:id')
  @ApiOkResponse({ type: KidResponse })
  async getKid(@Param('id') id: string) {
    return this.parentQuery.getKid(id);
  }
  @Get('get-kids')
  @ApiPaginatedResponse(KidResponse)
  async getKids(@Query() query: CollectionQuery) {
    return this.parentQuery.getKids(query);
  }
  @Get('get-kids-with-parent-id/:parentId')
  @ApiPaginatedResponse(KidResponse)
  async getKidsWithParentId(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.parentQuery.getKidsWithParentId(parentId, query);
  }
  @Get('get-kids-with-school-id/:schoolId')
  @ApiPaginatedResponse(KidResponse)
  async getKidsWithSchoolId(
    @Param('schoolId') schoolId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.parentQuery.getKidsWithSchoolId(schoolId, query);
  }
  @Get('get-archived-kids-with-parent-id/:parentId')
  @ApiPaginatedResponse(KidResponse)
  async getArchivedKidsWithParentId(
    @Param('parentId') parentId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.parentQuery.getArchivedKidsWithParentId(parentId, query);
  }
  @Get('count-kids-by-created-month')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async countKidsByCreatedMonth(@Query() query: CollectionQuery) {
    return this.parentQuery.countKidsByCreatedMonth(query);
  }
  @Get('get-archived-kids')
  @ApiPaginatedResponse(KidResponse)
  async getArchivedKids(@Query() query: CollectionQuery) {
    return this.parentQuery.getArchivedKids(query);
  }
  @Post('activate-or-block-kid/:id')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiOkResponse({ type: KidResponse })
  async activateOrBlockKid(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockKid(id, user);
  }
  @Post('restore-kid')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiOkResponse({ type: KidResponse })
  async restoreKid(
    @CurrentUser() user: UserInfo,
    @Body() command: DeleteKidCommand,
  ) {
    command.currentUser = user;
    return this.command.restoreKid(command);
  }

  @Post('update-kid-profile-image/:id')
  @UseGuards(RolesGuard('admin|parent|operator'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('profileImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  async uploadKidProfileImage(
    @Param('id') id: string,
    @UploadedFile() profileImage: Express.Multer.File,
  ) {
    if (profileImage) {
      const result = await this.fileManagerService.uploadFile(
        profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        return this.command.updateKidProfileImage(id, result);
      }
    }
    throw new BadRequestException(`Bad Request`);
  }
  @Get('export-parents')
  @AllowAnonymous()
  async exportParents(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    const fileName = `parents-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const parents = await this.parentQuery.getParents(query);
    const parentData = parents.data;
    const rows = parentData.map((parent) => {
      return {
        Name: parent.name,
        'Phone Number': parent.phoneNumber,
        Email: parent.email,
        City: parent?.address?.city,
        'Sub City': parent?.address?.subCity,
        'Created Date': parent.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Parents`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [['Name', 'Phone Number', 'Email', 'City', 'Sub City', 'Created Date']],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
  @Get('group-parents-by-gender')
  @ApiOkResponse({ type: CountByGenderResponse, isArray: true })
  async groupParentsByGender(@Query() query: CollectionQuery) {
    return this.parentQuery.groupParentsByGender(query);
  }
  @Get('group-parents-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupParentsByStatus(@Query() query: CollectionQuery) {
    return this.parentQuery.groupParentsByStatus(query);
  }
  @Get('group-parents-by-enabled')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupParentsByEnabled(@Query() query: CollectionQuery) {
    return this.parentQuery.groupParentsByEnabled(query);
  }
  @Get('group-parents-by-address/:field')
  @ApiOkResponse({ type: GroupByAddressResponse, isArray: true })
  async groupParentsByAddress(
    @Param('field') field: string,
    @Query() query: CollectionQuery,
  ) {
    return this.parentQuery.groupParentsByAddress(field, query);
  }

  @Get('group-kids-by-gender')
  @ApiOkResponse({ type: CountByGenderResponse, isArray: true })
  async groupKidsByGender(@Query() query: CollectionQuery) {
    return this.parentQuery.groupKidsByGender(query);
  }
  @Get('group-kids-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupKidsByStatus(@Query() query: CollectionQuery) {
    return this.parentQuery.groupKidsByStatus(query);
  }
  @Get('group-kids-by-transportation-time')
  @ApiOkResponse({ type: GroupByAddressResponse, isArray: true })
  async groupKidsByTransportationTime(@Query() query: CollectionQuery) {
    return this.parentQuery.groupKidsByTransportationTime(query);
  }
  @Get('export-kids')
  @AllowAnonymous()
  async exportKids(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    if (!query) {
      query = new CollectionQuery();
    }
    if (!query.includes) {
      query.includes = [];
    }
    if (!query.includes.includes('parent')) {
      query.includes.push('parent');
    }
    const fileName = `kids-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const kids = await this.parentQuery.getKids(query);
    const kidData = kids.data;
    const rows = kidData.map((kid) => {
      let address = '';
      const kidParent = kid?.parent;
      if (kidParent && kidParent.address) {
        if (kidParent.address.subCity) {
          address += ` ${kidParent.address.subCity}`;
        }
        if (kidParent.address.woreda) {
          address += ` ${kidParent.address.woreda}`;
        }
        if (kidParent.address.houseNumber) {
          address += ` ${kidParent.address.houseNumber}`;
        }
      }
      return {
        Name: kid.name,
        School: kid.schoolName,
        Gender: kid.gender,
        Grade: kid?.grade,
        'Distance from School': kid?.distanceFromSchool,
        'Transportation Time': kid?.transportationTime,
        'Car Type': kid?.categoryName,
        'Parent Name': kid?.parent?.name,
        'Parent Phone': kid?.parent?.phoneNumber,
        Address: address && address.trim() !== '' ? address : '',
        'Parent Status': kid?.parent?.status,
        Price: kid?.kidFee,
        'Created Date': kid.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Kids`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [
        [
          'Name',
          'School',
          'Gender',
          'Grade',
          'Distance from School',
          'Transportation Time',
          'Car Type',
          'Parent Name',
          'Parent Phone',
          'Address',
          'Parent Status',
          'Price',
          'Created Date',
        ],
      ],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
}
