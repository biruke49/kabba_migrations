import { diskStorage } from 'multer';

import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import {
  ArchiveSchoolCommand,
  CreateSchoolCommand,
  UpdateSchoolCommand,
} from '@customer/usecases/schools/school.commands';
import { SchoolResponse } from '@customer/usecases/schools/school.response';
import { SchoolCommands } from '@customer/usecases/schools/school.usecase.commands';
import { SchoolQuery } from '@customer/usecases/schools/school.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  StreamableFile,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';
import * as XLSX from 'xlsx';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
@Controller('schools')
@ApiTags('schools')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class SchoolsController {
  constructor(
    private command: SchoolCommands,
    private schoolQuery: SchoolQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-school/:id')
  @AllowAnonymous()
  @ApiOkResponse({ type: SchoolResponse })
  async getSchool(@Param('id') id: string) {
    return this.schoolQuery.getSchool(id);
  }
  @Get('get-archived-school/:id')
  @ApiOkResponse({ type: SchoolResponse })
  async getArchivedSchool(@Param('id') id: string) {
    return this.schoolQuery.getSchool(id, true);
  }
  @Get('get-schools')
  @ApiPaginatedResponse(SchoolResponse)
  async getSchools(@Query() query: CollectionQuery) {
    return this.schoolQuery.getSchools(query);
  }
  @Post('create-school')
  @UseGuards(PermissionsGuard('manage-schools'))
  // @UseInterceptors(
  //   FileInterceptor('attachments', {
  //     storage: diskStorage({
  //       destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
  //     }),
  //     fileFilter: (request, file, callback) => {
  //       if (!file.mimetype.includes('image')) {
  //         return callback(
  //           new BadRequestException('Provide a valid image'),
  //           false,
  //         );
  //       }
  //       callback(null, true);
  //     },
  //     limits: { fileSize: Math.pow(1024, 2) },
  //   }),
  // )
  @ApiOkResponse({ type: SchoolResponse })
  async createSchool(
    @CurrentUser() user: UserInfo,
    @Body() createSchoolCommand: CreateSchoolCommand,
    // @UploadedFile() logo: Express.Multer.File,
  ) {
    // if (logo) {
    //   const result = await this.fileManagerService.uploadFile(
    //     logo,
    //     FileManagerHelper.UPLOADED_FILES_DESTINATION,
    //   );
    //   if (result) {
    //     createSchoolCommand.logo = result;
    //   }
    // }
    createSchoolCommand.currentUser = user;
    return this.command.createSchool(createSchoolCommand);
  }
  @Put('update-school')
  @UseGuards(PermissionsGuard('manage-schools'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('logo', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: SchoolResponse })
  async updateSchool(
    @CurrentUser() user: UserInfo,
    @Body() updateSchoolCommand: UpdateSchoolCommand,
    @UploadedFile() logo: Express.Multer.File,
  ) {
    if (logo) {
      const result = await this.fileManagerService.uploadFile(
        logo,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        updateSchoolCommand.logo = result;
      }
    }
    updateSchoolCommand.currentUser = user;
    return this.command.updateSchool(updateSchoolCommand);
  }
  @Delete('archive-school')
  @UseGuards(PermissionsGuard('manage-schools'))
  @ApiOkResponse({ type: SchoolResponse })
  async archiveSchool(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveSchoolCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveSchool(archiveCommand);
  }
  @Delete('delete-school/:id')
  @UseGuards(PermissionsGuard('manage-schools'))
  @ApiOkResponse({ type: Boolean })
  async deleteSchool(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteSchool(id, user);
  }
  @Post('restore-school/:id')
  @UseGuards(PermissionsGuard('manage-schools'))
  @ApiOkResponse({ type: SchoolResponse })
  async restoreSchool(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreSchool(id, user);
  }
  @Get('get-archived-schools')
  @ApiPaginatedResponse(SchoolResponse)
  async getArchivedSchools(@Query() query: CollectionQuery) {
    return this.schoolQuery.getArchivedSchools(query);
  }
  @Post('activate-or-block-school/:id')
  @UseGuards(PermissionsGuard('activate-or-block-schools'))
  @ApiOkResponse({ type: SchoolResponse })
  async activateOrBlockSchool(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockSchool(id, user);
  }
  @Post('update-school-logo/:id')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('logo', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  async uploadSchoolLogo(
    @Param('id') id: string,
    @UploadedFile() logo: Express.Multer.File,
  ) {
    if (logo) {
      const result = await this.fileManagerService.uploadFile(
        logo,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        return this.command.updateSchoolLogo(id, result);
      }
    }
    throw new BadRequestException(`Bad Request`);
  }
  @Get('export-schools')
  @AllowAnonymous()
  async exportSchools(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    const fileName = `schools-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const schools = await this.schoolQuery.getSchools(query);
    const schoolData = schools.data;
    const rows = schoolData.map((school) => {
      return {
        Name: school.name,
        'Phone Number': school.phoneNumber,
        Email: school?.email,
        City: school?.address?.city,
        'Sub City': school?.address?.subCity,
        'Created Date': school.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Schools`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [['Name', 'Phone Number', 'Email', 'City', 'Sub City', 'Created Date']],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
  @Get('group-schools-by-enabled')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupSchoolsByEnabled(@Query() query: CollectionQuery) {
    return this.schoolQuery.groupSchoolsByStatus(query);
  }
  @Get('group-schools-by-type')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupSchoolsByType(@Query() query: CollectionQuery) {
    return this.schoolQuery.groupSchoolsByType(query);
  }
  @Get('group-schools-by-address/:address')
  @ApiOkResponse({ type: GroupByAddressResponse, isArray: true })
  async groupSchoolsByAddress(
    @Param('field') field: string,
    @Query() query: CollectionQuery,
  ) {
    return this.schoolQuery.groupSchoolsByAddress(field, query);
  }
}
