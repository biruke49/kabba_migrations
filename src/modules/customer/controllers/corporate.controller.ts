import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileResponseDto } from '@libs/common/file-manager';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import {
  ArchiveCorporateCommand,
  CreateCorporateCommand,
  UpdateCorporateCommand,
} from '@customer/usecases/corporates/corporate.commands';
import { CorporateResponse } from '@customer/usecases/corporates/corporate.response';
import { CorporateCommands } from '@customer/usecases/corporates/corporate.usecase.commands';
import { CorporateQuery } from '@customer/usecases/corporates/corporate.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  StreamableFile,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';
import * as XLSX from 'xlsx';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
@Controller('corporates')
@ApiTags('corporates')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class CorporatesController {
  constructor(
    private command: CorporateCommands,
    private corporateQuery: CorporateQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-corporate/:id')
  @ApiOkResponse({ type: CorporateResponse })
  async getCorporate(@Param('id') id: string) {
    return this.corporateQuery.getCorporate(id);
  }
  @Get('get-archived-corporate/:id')
  @ApiOkResponse({ type: CorporateResponse })
  async getArchivedCorporate(@Param('id') id: string) {
    return this.corporateQuery.getCorporate(id, true);
  }
  @Get('get-corporates')
  @ApiPaginatedResponse(CorporateResponse)
  async getCorporates(@Query() query: CollectionQuery) {
    return this.corporateQuery.getCorporates(query);
  }
  @Get('count-corporates-by-created-month')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async countCorporatesByCreatedMonth(@Query() query: CollectionQuery) {
    return this.corporateQuery.countCorporatesByCreatedMonth(query);
  }
  @Get('group-corporates-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupOwnersByStatus(@Query() query: CollectionQuery) {
    return this.corporateQuery.groupCorporatesByStatus(query);
  }
  @Post('create-corporate')
  @UseGuards(PermissionsGuard('manage-corporates'))
  @ApiOkResponse({ type: CorporateResponse })
  async createCorporate(
    @CurrentUser() user: UserInfo,
    @Body() createCorporateCommand: CreateCorporateCommand,
  ) {
    createCorporateCommand.currentUser = user;
    return this.command.createCorporate(createCorporateCommand);
  }
  @Put('update-corporate')
  @UseGuards(PermissionsGuard('manage-corporates'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('logo', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: CorporateResponse })
  async updateCorporate(
    @CurrentUser() user: UserInfo,
    @Body() updateCorporateCommand: UpdateCorporateCommand,
    @UploadedFile() logo: Express.Multer.File,
  ) {
    if (logo) {
      const result = await this.fileManagerService.uploadFile(
        logo,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        updateCorporateCommand.logo = result;
      }
    }
    updateCorporateCommand.currentUser = user;
    return this.command.updateCorporate(updateCorporateCommand);
  }
  @Delete('archive-corporate')
  @UseGuards(PermissionsGuard('manage-corporates'))
  @ApiOkResponse({ type: CorporateResponse })
  async archiveCorporate(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveCorporateCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveCorporate(archiveCommand);
  }
  @Delete('delete-corporate/:id')
  @UseGuards(PermissionsGuard('manage-corporates'))
  @ApiOkResponse({ type: Boolean })
  async deleteCorporate(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.deleteCorporate(id, user);
  }
  @Post('restore-corporate/:id')
  @UseGuards(PermissionsGuard('manage-corporates'))
  @ApiOkResponse({ type: CorporateResponse })
  async restoreCorporate(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.restoreCorporate(id, user);
  }
  @Get('get-archived-corporates')
  @ApiPaginatedResponse(CorporateResponse)
  async getArchivedCorporates(@Query() query: CollectionQuery) {
    return this.corporateQuery.getArchivedCorporates(query);
  }
  @Post('activate-or-block-corporate/:id')
  @UseGuards(PermissionsGuard('activate-or-block-corporate'))
  @ApiOkResponse({ type: CorporateResponse })
  async activateOrBlockCorporate(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockCorporate(id, user);
  }
  @Get('export-corporates')
  @AllowAnonymous()
  async exportCorporates(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    const fileName = `corporates-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const corporates = await this.corporateQuery.getCorporates(query);
    const corporateData = corporates.data;
    const rows = corporateData.map((corporate) => {
      return {
        Name: corporate.name,
        'Phone Number': corporate.phoneNumber,
        Email: corporate.email,
        TIN: corporate.tinNumber,
        City: corporate?.address?.city,
        'Sub City': corporate?.address?.subCity,
        'Created Date': corporate.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Corporates`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [
        [
          'Name',
          'Phone Number',
          'Email',
          'TIN',
          'City',
          'Sub City',
          'Created Date',
        ],
      ],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
}
