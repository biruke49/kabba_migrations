import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { Address } from '@libs/common/address';
import { CommonEntity } from '@libs/common/common.entity';
import { ContactPerson } from '@libs/common/emergency-contact';
import { SchoolStatus } from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { KidEntity } from '../parents/kid.entity';

@Entity('schools')
export class SchoolEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ nullable: true, unique: true })
  email: string;
  @Column({ name: 'phone_number', unique: true })
  phoneNumber: string;
  @Column({ nullable: true, unique: true })
  website: string;
  @Column({ nullable: true, type: 'jsonb' })
  logo: FileDto;
  @Column({ nullable: true, type: 'jsonb' })
  address: Address;
  @Column({ name: 'latitude', nullable: true, type: 'decimal' })
  lat: number;
  @Column({ name: 'longitude', nullable: true, type: 'decimal' })
  lng: number;
  @Column({ name: 'status', default: SchoolStatus.Open })
  status: string;
  @Column({ name: 'is_private', default: true })
  isPrivate: boolean;
  @Column({ default: true })
  enabled: boolean;
  @Column({ name: 'contact_person', type: 'jsonb', nullable: true })
  contactPerson: ContactPerson;
  @OneToMany(() => KidEntity, (kid) => kid.school, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  kids: KidEntity[];
  @OneToMany(
    () => GroupAssignmentEntity,
    (groupAssignment) => groupAssignment.school,
    {
      cascade: true,
      onDelete: 'CASCADE',
    },
  )
  groupAssignments: GroupAssignmentEntity[];
  // @OneToMany(() => PreferenceEntity, (preference) => preference.school, {
  //   onDelete: 'CASCADE',
  // })
  // preferences: PreferenceEntity[];
}
