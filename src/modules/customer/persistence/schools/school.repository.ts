import { School } from '@customer/domains/schools/school';
import { ISchoolRepository } from '@customer/domains/schools/school.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SchoolEntity } from './school.entity';

@Injectable()
export class SchoolRepository implements ISchoolRepository {
  constructor(
    @InjectRepository(SchoolEntity)
    private schoolRepository: Repository<SchoolEntity>,
  ) {}
  async insert(school: School): Promise<School> {
    const schoolEntity = this.toSchoolEntity(school);
    const result = await this.schoolRepository.save(schoolEntity);
    return result ? this.toSchool(result) : null;
  }
  async update(school: School): Promise<School> {
    const schoolEntity = this.toSchoolEntity(school);
    const result = await this.schoolRepository.save(schoolEntity);
    return result ? this.toSchool(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.schoolRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<School[]> {
    const schools = await this.schoolRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!schools.length) {
      return null;
    }
    return schools.map((user) => this.toSchool(user));
  }
  async getById(id: string, withDeleted = false): Promise<School> {
    const school = await this.schoolRepository.find({
      where: { id: id },
      relations: ['kids'],
      withDeleted: withDeleted,
    });
    if (!school[0]) {
      return null;
    }
    return this.toSchool(school[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.schoolRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.schoolRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getByPhoneNumber(
    phoneNumber: string,
    withDeleted = false,
  ): Promise<School> {
    const school = await this.schoolRepository.find({
      where: { phoneNumber: phoneNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!school[0]) {
      return null;
    }
    return this.toSchool(school[0]);
  }
  async getByName(name: string, withDeleted = false): Promise<School> {
    const school = await this.schoolRepository.find({
      where: { name: name },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!school[0]) {
      return null;
    }
    return this.toSchool(school[0]);
  }
  async getByEmail(email: string, withDeleted = false): Promise<School> {
    const school = await this.schoolRepository.find({
      where: { email: email },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!school[0]) {
      return null;
    }
    return this.toSchool(school[0]);
  }
  async getByWebsite(website: string, withDeleted = false): Promise<School> {
    const school = await this.schoolRepository.find({
      where: { website: website },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!school[0]) {
      return null;
    }
    return this.toSchool(school[0]);
  }

  // async getByTinNumber(
  //   tinNumber: string,
  //   withDeleted = false,
  // ): Promise<Parent> {
  //   const parent = await this.schoolRepository.find({
  //     where: { tinNumber: tinNumber },
  //     relations: [],
  //     withDeleted: withDeleted,
  //   });
  //   if (!parent[0]) {
  //     return null;
  //   }
  //   return this.toparent(parent[0]);
  // }

  toSchool(schoolEntity: SchoolEntity): School {
    const school = new School();
    school.id = schoolEntity.id;
    school.name = schoolEntity.name;
    school.email = schoolEntity.email;
    school.phoneNumber = schoolEntity.phoneNumber;
    school.website = schoolEntity.website;
    school.logo = schoolEntity.logo;
    school.address = schoolEntity.address;
    school.lng = schoolEntity.lng;
    school.lat = schoolEntity.lat;
    school.status = schoolEntity.status;
    school.enabled = schoolEntity.enabled;
    school.isPrivate = schoolEntity.isPrivate;
    school.contactPerson = schoolEntity.contactPerson;
    school.archiveReason = schoolEntity.archiveReason;
    school.createdBy = schoolEntity.createdBy;
    school.updatedBy = schoolEntity.updatedBy;
    school.deletedBy = schoolEntity.deletedBy;
    school.createdAt = schoolEntity.createdAt;
    school.updatedAt = schoolEntity.updatedAt;
    school.deletedAt = schoolEntity.deletedAt;
    return school;
  }
  toSchoolEntity(school: School): SchoolEntity {
    const schoolEntity = new SchoolEntity();
    schoolEntity.id = school.id;
    schoolEntity.name = school.name;
    schoolEntity.email = school.email;
    schoolEntity.phoneNumber = school.phoneNumber;
    schoolEntity.website = school.website;
    schoolEntity.logo = school.logo;
    schoolEntity.address = school.address;
    schoolEntity.lng = school.lng;
    schoolEntity.lat = school.lat;
    schoolEntity.status = school.status;
    schoolEntity.enabled = school.enabled;
    schoolEntity.isPrivate = school.isPrivate;
    schoolEntity.contactPerson = school.contactPerson;
    schoolEntity.archiveReason = school.archiveReason;
    schoolEntity.createdBy = school.createdBy;
    schoolEntity.updatedBy = school.updatedBy;
    schoolEntity.deletedBy = school.deletedBy;
    schoolEntity.createdAt = school.createdAt;
    schoolEntity.updatedAt = school.updatedAt;
    schoolEntity.deletedAt = school.deletedAt;
    return schoolEntity;
  }
}
