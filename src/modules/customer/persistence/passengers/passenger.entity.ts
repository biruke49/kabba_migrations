import { FileDto } from '@libs/common/file-dto';
import { CommonEntity } from '@libs/common/common.entity';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Address } from '@libs/common/address';
import { ReviewEntity } from '@interaction/persistence/reviews/review.entity';
import { CorporateEntity } from '../corporates/corporate.entity';
import { EmergencyContact } from '@libs/common/emergency-contact';
import { WalletEntity } from '@credit/persistence/wallets/wallet.entity';
import { TransactionEntity } from '@credit/persistence/transactions/transaction.entity';
import { CorporateTransactionEntity } from '@credit/persistence/corporate_transaction/corporate-transaction.entity';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import { FavoriteEntity } from '@interaction/persistence/favorites/favorite.entity';

@Entity('passengers')
export class PassengerEntity extends CommonEntity {
  @Index()
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Index()
  @Column({ nullable: true })
  email: string;
  @Index()
  @Column({ name: 'phone_number', unique: true })
  phoneNumber: string;
  @Column({ nullable: true })
  gender: string;
  @Column({ name: 'profile_image', nullable: true, type: 'jsonb' })
  profileImage: FileDto;
  @Column({ name: 'emergency_contact', nullable: true, type: 'jsonb' })
  emergencyContact: EmergencyContact;
  @Column({ nullable: true, type: 'jsonb' })
  address: Address;
  @Column({ name: 'is_active', default: true })
  isActive: boolean;
  @Column({ default: true })
  enabled: boolean;
  @Column({ name: 'fcm_id', type: 'uuid', nullable: true })
  fcmId: string;

  @OneToMany(() => ReviewEntity, (review) => review.passenger, {
    cascade: ['remove', 'soft-remove', 'recover'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  reviews: ReviewEntity[];

  @ManyToOne(() => CorporateEntity, (corporate) => corporate.employees, {
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'corporate_id' })
  corporate: CorporateEntity;
  @Column({ name: 'corporate_id', type: 'uuid', nullable: true })
  corporateId: string;
  @Column({ name: 'corporate_name', nullable: true })
  corporateName: string;
  @OneToOne(() => WalletEntity, (wallet) => wallet.passenger)
  wallet: WalletEntity;
  @OneToMany(() => TransactionEntity, (transaction) => transaction.passenger, {
    onDelete: 'CASCADE',
  })
  transactions: TransactionEntity[];
  @OneToMany(
    () => CorporateTransactionEntity,
    (corporateTransaction) => corporateTransaction.employee,
  )
  corporateTransactions: CorporateTransactionEntity[];

  @OneToMany(() => BookingEntity, (booking) => booking.passenger, {
    onDelete: 'CASCADE',
  })
  bookings: BookingEntity[];

  @OneToMany(() => FavoriteEntity, (favorite) => favorite.passenger, {
    onDelete: 'CASCADE',
  })
  favorites: FavoriteEntity[];
}
