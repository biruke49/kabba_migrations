import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IPassengerRepository } from '@customer/domains/passengers/passenger.repository.interface';
import { Passenger } from '@customer/domains/passengers/passenger';
import { WalletEntity } from '@credit/persistence/wallets/wallet.entity';
import { Wallet } from '@credit/domains/wallets/wallet';
@Injectable()
export class PassengerRepository implements IPassengerRepository {
  constructor(
    @InjectRepository(PassengerEntity)
    private passengerRepository: Repository<PassengerEntity>,
  ) {}
  async insert(passenger: Passenger): Promise<Passenger> {
    const passengerEntity = this.toPassengerEntity(passenger);
    const result = await this.passengerRepository.save(passengerEntity);
    return result ? this.toPassenger(result) : null;
  }
  async update(passenger: Passenger): Promise<Passenger> {
    const passengerEntity = this.toPassengerEntity(passenger);
    const result = await this.passengerRepository.save(passengerEntity);
    return result ? this.toPassenger(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.passengerRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Passenger[]> {
    const passengers = await this.passengerRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!passengers.length) {
      return null;
    }
    return passengers.map((user) => this.toPassenger(user));
  }
  async getById(id: string, withDeleted = false): Promise<Passenger> {
    const passenger = await this.passengerRepository.find({
      where: { id: id },
      relations: ['wallet'],
      withDeleted: withDeleted,
    });
    if (!passenger[0]) {
      return null;
    }
    return this.toPassenger(passenger[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.passengerRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.passengerRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getByPhoneNumber(
    phoneNumber: string,
    withDeleted = false,
  ): Promise<Passenger> {
    const passenger = await this.passengerRepository.find({
      where: { phoneNumber: phoneNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!passenger[0]) {
      return null;
    }
    return this.toPassenger(passenger[0]);
  }
  async getByEmail(email: string, withDeleted = false): Promise<Passenger> {
    const passenger = await this.passengerRepository.find({
      where: { email: email },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!passenger[0]) {
      return null;
    }
    return this.toPassenger(passenger[0]);
  }
  async getCountByStatusNotification(withDeleted = false): Promise<number> {
    const count = await this.passengerRepository.count({
      where: { enabled: true },
      relations: [],
      withDeleted: withDeleted,
    });
    return count;
  }
  async getThousandForNotification(
    withDeleted: boolean,
    skip: number = 0,
    take: number = 1,
  ): Promise<Passenger[]> {
    const accounts = await this.passengerRepository.find({
      where: { enabled: true },
      relations: [],
      withDeleted: withDeleted,
      skip,
      take,
    });
    if (!accounts.length) {
      return null;
    }
    return accounts.map((account) => this.toPassenger(account));
  }
  async getAllForNotification(
    status: string,
    withDeleted: boolean,
  ): Promise<Passenger[]> {
    const accounts = await this.passengerRepository.find({
      where: { enabled: true },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!accounts.length) {
      return null;
    }
    return accounts.map((account) => this.toPassenger(account));
  }
  toPassenger(passengerEntity: PassengerEntity): Passenger {
    const passenger = new Passenger();
    passenger.id = passengerEntity.id;
    passenger.name = passengerEntity.name;
    passenger.email = passengerEntity.email;
    passenger.phoneNumber = passengerEntity.phoneNumber;
    passenger.gender = passengerEntity.gender;
    passenger.enabled = passengerEntity.enabled;
    passenger.profileImage = passengerEntity.profileImage;
    passenger.address = passengerEntity.address;
    passenger.isActive = passengerEntity.isActive;
    passenger.emergencyContact = passengerEntity.emergencyContact;
    passenger.corporateId = passengerEntity.corporateId;
    passenger.corporateName = passengerEntity.corporateName;
    passenger.archiveReason = passengerEntity.archiveReason;
    passenger.createdBy = passengerEntity.createdBy;
    passenger.updatedBy = passengerEntity.updatedBy;
    passenger.deletedBy = passengerEntity.deletedBy;
    passenger.createdAt = passengerEntity.createdAt;
    passenger.updatedAt = passengerEntity.updatedAt;
    passenger.deletedAt = passengerEntity.deletedAt;
    if (passengerEntity.wallet) {
      passenger.wallet = this.toWallet(passengerEntity.wallet);
    }
    return passenger;
  }
  toPassengerEntity(passenger: Passenger): PassengerEntity {
    const passengerEntity = new PassengerEntity();
    passengerEntity.id = passenger.id;
    passengerEntity.name = passenger.name;
    passengerEntity.email = passenger.email;
    passengerEntity.phoneNumber = passenger.phoneNumber;
    passengerEntity.gender = passenger.gender;
    passengerEntity.enabled = passenger.enabled;
    passengerEntity.profileImage = passenger.profileImage;
    passengerEntity.address = passenger.address;
    passengerEntity.isActive = passenger.isActive;
    passengerEntity.emergencyContact = passenger.emergencyContact;
    passengerEntity.corporateId = passenger.corporateId;
    passengerEntity.corporateName = passenger.corporateName;
    passengerEntity.archiveReason = passenger.archiveReason;
    passengerEntity.createdBy = passenger.createdBy;
    passengerEntity.updatedBy = passenger.updatedBy;
    passengerEntity.deletedBy = passenger.deletedBy;
    passengerEntity.createdAt = passenger.createdAt;
    passengerEntity.updatedAt = passenger.updatedAt;
    passengerEntity.deletedAt = passenger.deletedAt;
    return passengerEntity;
  }
  toWallet(walletEntity: WalletEntity): Wallet {
    const wallet = new Wallet();
    wallet.id = walletEntity.id;
    wallet.passengerId = walletEntity.passengerId;
    wallet.corporateWalletBalance = walletEntity.corporateWalletBalance;
    wallet.balance = walletEntity.balance;
    wallet.createdAt = walletEntity.createdAt;
    wallet.deletedAt = walletEntity.deletedAt;
    wallet.updatedAt = walletEntity.updatedAt;
    return wallet;
  }
}
