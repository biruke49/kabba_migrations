import { CorporateEntity } from '@customer/persistence/corporates/corporate.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ICorporateRepository } from '@customer/domains/corporates/corporate.repository.interface';
import { Corporate } from '@customer/domains/corporates/corporate';
@Injectable()
export class CorporateRepository implements ICorporateRepository {
  constructor(
    @InjectRepository(CorporateEntity)
    private corporateRepository: Repository<CorporateEntity>,
  ) {}
  async insert(corporate: Corporate): Promise<Corporate> {
    const corporateEntity = this.toCorporateEntity(corporate);
    const result = await this.corporateRepository.save(corporateEntity);
    return result ? this.toCorporate(result) : null;
  }
  async update(corporate: Corporate): Promise<Corporate> {
    const corporateEntity = this.toCorporateEntity(corporate);
    const result = await this.corporateRepository.save(corporateEntity);
    return result ? this.toCorporate(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.corporateRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Corporate[]> {
    const corporates = await this.corporateRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporates.length) {
      return null;
    }
    return corporates.map((user) => this.toCorporate(user));
  }
  async getById(id: string, withDeleted = false): Promise<Corporate> {
    const corporate = await this.corporateRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporate[0]) {
      return null;
    }
    return this.toCorporate(corporate[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.corporateRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.corporateRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getByPhoneNumber(
    phoneNumber: string,
    withDeleted = false,
  ): Promise<Corporate> {
    const corporate = await this.corporateRepository.find({
      where: { phoneNumber: phoneNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporate[0]) {
      return null;
    }
    return this.toCorporate(corporate[0]);
  }
  async getByEmail(email: string, withDeleted = false): Promise<Corporate> {
    const corporate = await this.corporateRepository.find({
      where: { email: email },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporate[0]) {
      return null;
    }
    return this.toCorporate(corporate[0]);
  }
  async getByTinNumber(
    tinNumber: string,
    withDeleted = false,
  ): Promise<Corporate> {
    const corporate = await this.corporateRepository.find({
      where: { tinNumber: tinNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!corporate[0]) {
      return null;
    }
    return this.toCorporate(corporate[0]);
  }

  toCorporate(corporateEntity: CorporateEntity): Corporate {
    const corporate = new Corporate();
    corporate.id = corporateEntity.id;
    corporate.name = corporateEntity.name;
    corporate.email = corporateEntity.email;
    corporate.website = corporateEntity.website;
    corporate.tinNumber = corporateEntity.tinNumber;
    corporate.phoneNumber = corporateEntity.phoneNumber;
    corporate.enabled = corporateEntity.enabled;
    corporate.address = corporateEntity.address;
    corporate.logo = corporateEntity.logo;
    corporate.contactPerson = corporateEntity.contactPerson;
    corporate.landmark = corporateEntity.landmark;
    corporate.lat = corporateEntity.lat;
    corporate.lng = corporateEntity.lng;
    corporate.archiveReason = corporateEntity.archiveReason;
    corporate.createdBy = corporateEntity.createdBy;
    corporate.updatedBy = corporateEntity.updatedBy;
    corporate.deletedBy = corporateEntity.deletedBy;
    corporate.createdAt = corporateEntity.createdAt;
    corporate.updatedAt = corporateEntity.updatedAt;
    corporate.deletedAt = corporateEntity.deletedAt;
    return corporate;
  }
  toCorporateEntity(corporate: Corporate): CorporateEntity {
    const corporateEntity = new CorporateEntity();
    corporateEntity.id = corporate.id;
    corporateEntity.name = corporate.name;
    corporateEntity.email = corporate.email;
    corporateEntity.phoneNumber = corporate.phoneNumber;
    corporateEntity.logo = corporate.logo;
    corporateEntity.enabled = corporate.enabled;
    corporateEntity.address = corporate.address;
    corporateEntity.tinNumber = corporate.tinNumber;
    corporateEntity.website = corporate.website;
    corporateEntity.contactPerson = corporate.contactPerson;
    corporateEntity.landmark = corporate.landmark;
    corporateEntity.lat = corporate.lat;
    corporateEntity.lng = corporate.lng;
    corporateEntity.archiveReason = corporate.archiveReason;
    corporateEntity.createdBy = corporate.createdBy;
    corporateEntity.updatedBy = corporate.updatedBy;
    corporateEntity.deletedBy = corporate.deletedBy;
    corporateEntity.createdAt = corporate.createdAt;
    corporateEntity.updatedAt = corporate.updatedAt;
    corporateEntity.deletedAt = corporate.deletedAt;
    return corporateEntity;
  }
}
