import { CommonEntity } from '@libs/common/common.entity';
import { Address } from '@libs/common/address';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { FileDto } from '@libs/common/file-dto';
import { ContactPerson } from '@customer/domains/corporates/contact-person';
import { PassengerEntity } from '../passengers/passenger.entity';
import { CorporateWalletEntity } from '@credit/persistence/corporate-wallet/corporate-wallet.entity';
import { CorporateTransactionEntity } from '@credit/persistence/corporate_transaction/corporate-transaction.entity';
@Entity({ name: 'corporates' })
export class CorporateEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ nullable: true })
  email: string;
  @Column({ name: 'phone_number', unique: true })
  phoneNumber: string;
  @Column({ default: true })
  enabled: boolean;
  @Column({ nullable: true, name: 'tin_number' })
  tinNumber: string;
  @Column({ nullable: true })
  website: string;
  @Column({ type: 'jsonb', nullable: true })
  address: Address;
  @Column({ type: 'jsonb', nullable: true })
  logo: FileDto;
  @Column({ type: 'jsonb', nullable: true, name: 'contact_person' })
  contactPerson: ContactPerson;
  @Column({ nullable: true })
  landmark: string;
  @Column({ nullable: true, type: 'decimal' })
  lat: number;
  @Column({ nullable: true, type: 'decimal' })
  lng: number;
  @OneToMany(() => PassengerEntity, (passenger) => passenger.corporate, {
    cascade: true,
    onDelete: 'SET NULL',
  })
  employees: PassengerEntity[];
  @OneToOne(() => CorporateWalletEntity, (wallet) => wallet.corporate)
  wallet: CorporateWalletEntity;
  @OneToMany(
    () => CorporateTransactionEntity,
    (transaction) => transaction.corporate,
    {
      onDelete: 'CASCADE',
    },
  )
  transactions: CorporateTransactionEntity[];
}
