import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { CommonEntity } from '@libs/common/common.entity';
import {
  Gender,
  KidStatus,
  KidTravelStatus,
  TransportationTime,
} from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { SchoolEntity } from '../schools/school.entity';
import { ParentEntity } from './parent.entity';

@Entity('kids')
export class KidEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ nullable: true })
  grade: string;
  @Column({ nullable: true })
  age: number;
  @Column({ nullable: true })
  gender: string;
  @Column({ name: 'profile_image', nullable: true, type: 'jsonb' })
  profileImage: FileDto;
  @Column({ nullable: true })
  remark: string;
  @Column({ name: 'status', default: KidStatus.AtHome })
  status: string;
  @Column({ name: 'kid_travel_status', default: KidTravelStatus.WithOthers })
  kidTravelStatus: string;
  @Column({ name: 'transportation_time', default: TransportationTime.Both })
  transportationTime: string;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'start_date',
  })
  startDate: Date;
  @Column({ name: 'category_id', nullable: true })
  categoryId: string;
  @Column({ default: true })
  enabled: boolean;
  @Column({ default: false })
  assigned: boolean;
  @Column({ name: 'parent_id' })
  parentId: string;
  @Column({ name: 'school_id' })
  schoolId: string;
  @Column({ name: 'school_name', nullable: true })
  schoolName: string;
  @Column({ name: 'category_name', nullable: true })
  categoryName: string;
  @Column({
    name: 'distance_from_school',
    nullable: true,
    type: 'decimal',
    default: 0,
  })
  distanceFromSchool: number;
  @Column({
    name: 'kid_fee',
    nullable: true,
    type: 'decimal',
    default: 0,
  })
  kidFee: number;
  @ManyToOne(() => ParentEntity, (parent) => parent.kids, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'parent_id' })
  parent: ParentEntity;
  @ManyToOne(() => CategoryEntity, (parent) => parent.kids, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'category_id' })
  category: CategoryEntity;
  @ManyToOne(() => SchoolEntity, (school) => school.kids, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'school_id' })
  school: SchoolEntity;
  @OneToOne(
    () => GroupAssignmentEntity,
    (groupAssignment) => groupAssignment.kid,
  )
  groupAssignment: GroupAssignmentEntity;
}
