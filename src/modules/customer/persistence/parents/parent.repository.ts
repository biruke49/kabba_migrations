import { Kid } from '@customer/domains/parents/kid';
import { Parent } from '@customer/domains/parents/parent';
import { IParentRepository } from '@customer/domains/parents/parent.repository.interface';
import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { KidEntity } from './kid.entity';
import { ParentEntity } from './parent.entity';

@Injectable()
export class ParentRepository implements IParentRepository {
  constructor(
    @InjectRepository(ParentEntity)
    private parentRepository: Repository<ParentEntity>,
    @InjectRepository(KidEntity)
    private kidRepository: Repository<KidEntity>,
    @InjectRepository(ParentPreferenceEntity)
    private parentPreferenceRepository: Repository<ParentPreferenceEntity>,
  ) {}

  async insert(parent: Parent): Promise<Parent> {
    const parentEntity = this.toParentEntity(parent);
    const result = await this.parentRepository.save(parentEntity);
    return result ? this.toParent(result) : null;
  }
  async update(parent: Parent): Promise<Parent> {
    const parentEntity = this.toParentEntity(parent);
    const result = await this.parentRepository.save(parentEntity);
    return result ? this.toParent(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.parentRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Parent[]> {
    const parents = await this.parentRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!parents.length) {
      return null;
    }
    return parents.map((user) => this.toParent(user));
  }
  async getById(id: string, withDeleted = false): Promise<Parent> {
    const parent = await this.parentRepository.find({
      where: { id: id },
      relations: ['kids'],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      return null;
    }
    return this.toParent(parent[0]);
  }
  async getByIdKidDelete(id: string, withDeleted = false): Promise<Parent> {
    const parent = await this.parentRepository.find({
      where: { id: id },
      relations: ['kids', 'kids.groupAssignment'],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      return null;
    }
    return this.toParent(parent[0]);
  }
  async getByIdNotification(id: string, withDeleted = false): Promise<Parent> {
    const parent = await this.parentRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      return null;
    }
    return this.toParent(parent[0]);
  }
  async getCountByStatusNotification(
    status: string,
    withDeleted = false,
  ): Promise<number> {
    if (status === 'all') {
      const count = await this.parentRepository.count({
        where: { enabled: true },
        relations: [],
        withDeleted: withDeleted,
      });
      return count;
    } else {
      const count = await this.parentRepository.count({
        where: { enabled: true, status: status },
        relations: [],
        withDeleted: withDeleted,
      });
      return count;
    }
  }
  async getThousandForNotification(
    status: string,
    withDeleted: boolean,
    skip: number = 0,
    take: number = 1,
  ): Promise<Parent[]> {
    if (status === 'all') {
      const accounts = await this.parentRepository.find({
        where: { enabled: true },
        relations: [],
        withDeleted: withDeleted,
        skip,
        take,
      });
      if (!accounts.length) {
        return null;
      }
      return accounts.map((account) => this.toParent(account));
    } else {
      const accounts = await this.parentRepository.find({
        where: { enabled: true, status: status },
        relations: [],
        withDeleted: withDeleted,
        skip,
        take,
      });
      if (!accounts.length) {
        return null;
      }
      return accounts.map((account) => this.toParent(account));
    }
  }
  async getAllForNotification(
    status: string,
    withDeleted: boolean,
  ): Promise<Parent[]> {
    if (status === 'all') {
      const accounts = await this.parentRepository.find({
        where: { enabled: true },
        relations: [],
        withDeleted: withDeleted,
      });
      if (!accounts.length) {
        return null;
      }
      return accounts.map((account) => this.toParent(account));
    } else {
      const accounts = await this.parentRepository.find({
        where: { enabled: true, status: status },
        relations: [],
        withDeleted: withDeleted,
      });
      if (!accounts.length) {
        return null;
      }
      return accounts.map((account) => this.toParent(account));
    }
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.parentRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.parentRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getByPhoneNumber(
    phoneNumber: string,
    withDeleted = false,
  ): Promise<Parent> {
    const parent = await this.parentRepository.find({
      where: { phoneNumber: phoneNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      return null;
    }
    return this.toParent(parent[0]);
  }
  async getByEmail(email: string, withDeleted = false): Promise<Parent> {
    const parent = await this.parentRepository.find({
      where: { email: email },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      return null;
    }
    return this.toParent(parent[0]);
  }
  async getBySchoolId(schoolId: string, withDeleted = false): Promise<Kid> {
    const kids = await this.kidRepository.find({
      where: { schoolId: schoolId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!kids[0]) {
      return null;
    }
    return this.toKid(kids[0]);
  }
  async getByKidId(kidId: string, withDeleted = false): Promise<Kid> {
    const kids = await this.kidRepository.find({
      where: { id: kidId },
      relations: ['school', 'category'],
      withDeleted: withDeleted,
    });
    if (!kids[0]) {
      return null;
    }
    return this.toKid(kids[0]);
  }
  async getAllKidsByParentId(
    parentId: string,
    withDeleted: boolean,
  ): Promise<Kid[]> {
    const accounts = await this.kidRepository.find({
      where: { parentId: parentId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!accounts.length) {
      return null;
    }
    return accounts.map((account) => this.toKid(account));
  }
  toParent(parentEntity: ParentEntity): Parent {
    const parent = new Parent();
    parent.id = parentEntity.id;
    parent.name = parentEntity.name;
    parent.email = parentEntity.email;
    parent.phoneNumber = parentEntity.phoneNumber;
    parent.status = parentEntity.status;
    parent.enabled = parentEntity.enabled;
    parent.address = parentEntity.address;
    parent.profileImage = parentEntity.profileImage;
    parent.gender = parentEntity.gender;
    parent.lng = parentEntity.lng;
    parent.lat = parentEntity.lat;
    parent.totalPayment = parentEntity.totalPayment;
    // parent.isActive = parentEntity.isActive;
    parent.archiveReason = parentEntity.archiveReason;
    parent.createdBy = parentEntity.createdBy;
    parent.updatedBy = parentEntity.updatedBy;
    parent.deletedBy = parentEntity.deletedBy;
    parent.createdAt = parentEntity.createdAt;
    parent.updatedAt = parentEntity.updatedAt;
    parent.deletedAt = parentEntity.deletedAt;
    parent.kids = parentEntity.kids
      ? parentEntity.kids.map((kid) => this.toKid(kid))
      : [];
    return parent;
  }
  toParentEntity(parent: Parent): ParentEntity {
    const parentEntity = new ParentEntity();
    parentEntity.id = parent.id;
    parentEntity.name = parent.name;
    parentEntity.email = parent.email;
    parentEntity.phoneNumber = parent.phoneNumber;
    parentEntity.enabled = parent.enabled;
    // parentEntity.isActive = parent.isActive;
    parentEntity.status = parent.status;
    parentEntity.address = parent.address;
    parentEntity.profileImage = parent.profileImage;
    parentEntity.gender = parent.gender;
    parentEntity.lng = parent.lng;
    parentEntity.lat = parent.lat;
    parentEntity.totalPayment = parent.totalPayment;
    parentEntity.archiveReason = parent.archiveReason;
    parentEntity.createdBy = parent.createdBy;
    parentEntity.updatedBy = parent.updatedBy;
    parentEntity.deletedBy = parent.deletedBy;
    parentEntity.createdAt = parent.createdAt;
    parentEntity.updatedAt = parent.updatedAt;
    parentEntity.deletedAt = parent.deletedAt;
    parentEntity.kids = parent.kids
      ? parent.kids.map((kid) => this.toKidEntity(kid))
      : [];
    return parentEntity;
  }
  //Kids
  toKid(kidEntity: KidEntity): Kid {
    const kid = new Kid();
    kid.id = kidEntity.id;
    kid.name = kidEntity.name;
    kid.parentId = kidEntity.parentId;
    kid.schoolId = kidEntity.schoolId;
    kid.schoolName = kidEntity.schoolName;
    kid.categoryName = kidEntity.categoryName;
    kid.age = kidEntity.age;
    kid.grade = kidEntity.grade;
    kid.gender = kidEntity.gender;
    kid.profileImage = kidEntity.profileImage;
    kid.status = kidEntity.status;
    kid.kidTravelStatus = kidEntity.kidTravelStatus;
    kid.categoryId = kidEntity.categoryId;
    kid.startDate = kidEntity.startDate;
    kid.transportationTime = kidEntity.transportationTime;
    kid.enabled = kidEntity.enabled;
    kid.remark = kidEntity.remark;
    kid.assigned = kidEntity.assigned;
    kid.distanceFromSchool = kidEntity.distanceFromSchool;
    kid.kidFee = kidEntity.kidFee;
    kid.archiveReason = kidEntity.archiveReason;
    kid.createdBy = kidEntity.createdBy;
    kid.updatedBy = kidEntity.updatedBy;
    kid.deletedBy = kidEntity.deletedBy;
    kid.createdAt = kidEntity.createdAt;
    kid.updatedAt = kidEntity.updatedAt;
    kid.deletedAt = kidEntity.deletedAt;
    return kid;
  }
  toKidEntity(kid: Kid): KidEntity {
    const kidEntity = new KidEntity();
    kidEntity.id = kid.id;
    kidEntity.name = kid.name;
    kidEntity.parentId = kid.parentId;
    kidEntity.schoolId = kid.schoolId;
    kidEntity.schoolName = kid.schoolName;
    kidEntity.categoryName = kid.categoryName;
    kidEntity.age = kid.age;
    kidEntity.grade = kid.grade;
    kidEntity.gender = kid.gender;
    kidEntity.profileImage = kid.profileImage;
    kidEntity.status = kid.status;
    kidEntity.kidTravelStatus = kid.kidTravelStatus;
    kidEntity.startDate = kid.startDate;
    kidEntity.categoryId = kid.categoryId;
    kidEntity.transportationTime = kid.transportationTime;
    kidEntity.enabled = kid.enabled;
    kidEntity.remark = kid.remark;
    kidEntity.assigned = kid.assigned;
    kidEntity.distanceFromSchool = kid.distanceFromSchool;
    kidEntity.kidFee = kid.kidFee;
    kidEntity.archiveReason = kid.archiveReason;
    kidEntity.createdBy = kid.createdBy;
    kidEntity.updatedBy = kid.updatedBy;
    kidEntity.deletedBy = kid.deletedBy;
    kidEntity.createdAt = kid.createdAt;
    kidEntity.updatedAt = kid.updatedAt;
    kidEntity.deletedAt = kid.deletedAt;
    return kidEntity;
  }
}
