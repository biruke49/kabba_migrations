import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { PaymentEntity } from '@assignment/persistence/payments/payment.entity';
import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { ReviewEntity } from '@interaction/persistence/reviews/review.entity';
import { Address } from '@libs/common/address';
import { CommonEntity } from '@libs/common/common.entity';
import { ParentStatus } from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { KidEntity } from './kid.entity';

@Entity('parents')
export class ParentEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ nullable: true, unique: true })
  email: string;
  @Column({ name: 'phone_number', unique: true })
  phoneNumber: string;
  @Column({ nullable: true })
  gender: string;
  @Column({ name: 'profile_image', nullable: true, type: 'jsonb' })
  profileImage: FileDto;
  @Column({ nullable: true, type: 'jsonb' })
  address: Address;
  @Column({ name: 'status', default: ParentStatus.New })
  status: string;
  // @Column({ name: 'is_active', default: true })
  // isActive: boolean;
  @Column({ default: true })
  enabled: boolean;
  @Column({ name: 'latitude', nullable: true, type: 'double precision' })
  lat: number;
  @Column({ name: 'longitude', nullable: true, type: 'double precision' })
  lng: number;
  @Column({ name: 'total_payment', type: 'double precision', default: 0.0 })
  totalPayment: number;
  @OneToMany(() => KidEntity, (kid) => kid.parent, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  kids: KidEntity[];
  @OneToMany(() => ReviewEntity, (review) => review.parent, {
    cascade: ['remove', 'soft-remove', 'recover'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  reviews: ReviewEntity[];
  @OneToMany(() => ParentPreferenceEntity, (preference) => preference.parent, {
    onDelete: 'CASCADE',
  })
  parentPreferences: ParentPreferenceEntity[];
  @OneToMany(() => PaymentEntity, (payment) => payment.parent, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  payments: PaymentEntity[];
  @OneToMany(
    () => GroupAssignmentEntity,
    (groupAssignment) => groupAssignment.kid,
    {
      cascade: true,
      onDelete: 'CASCADE',
    },
  )
  groupAssignments: GroupAssignmentEntity[];
}
