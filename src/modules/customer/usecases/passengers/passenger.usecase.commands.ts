import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserInfo } from '@account/dtos/user-info.dto';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import {
  AddOrRemoveCorporatePassengerCommand,
  ArchivePassengerCommand,
  CreateCorporatePassengerCommand,
  CreatePassengerCommand,
  UpdatePassengerCommand,
} from './passenger.commands';
import { PassengerRepository } from '@customer/persistence/passengers/passenger.repository';
import { PassengerResponse } from './passenger.response';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { AccountCommands } from '@account/usecases/accounts/account.usecase.commands';
import { CreateAccountCommand } from '@account/usecases/accounts/account.commands';
import {
  CredentialType,
  PaymentMethod,
  PaymentStatus,
} from '@libs/common/enums';
import { Util } from '@libs/common/util';
import { validate } from 'class-validator';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { FileDto } from '@libs/common/file-dto';
import { CorporateRepository } from '@customer/persistence/corporates/corporate.repository';
@Injectable()
export class PassengerCommands {
  constructor(
    private passengerRepository: PassengerRepository,
    private corporateRepository: CorporateRepository,
    private readonly accountCommands: AccountCommands,
    private readonly eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createPassenger(
    command: CreatePassengerCommand,
  ): Promise<PassengerResponse> {
    if (
      await this.passengerRepository.getByPhoneNumber(command.phoneNumber, true)
    ) {
      throw new BadRequestException(
        `Customer already exist with this phone number`,
      );
    }
    if (
      command.email &&
      (await this.passengerRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Customer already exist with this email Address`,
      );
    }
    const passengerDomain = CreatePassengerCommand.fromCommand(command);
    const passenger = await this.passengerRepository.insert(passengerDomain);
    if (passenger) {
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = passenger.id;
      createAccountCommand.type = CredentialType.Passenger;
      createAccountCommand.role = ['passenger'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(command.password);
      createAccountCommand.address = command.address;
      createAccountCommand.gender = command.gender;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      if (account) {
        this.eventEmitter.emit('create.wallet', {
          passengerId: passenger.id,
          balance: 0,
        });
      }
    }
    return PassengerResponse.fromDomain(passenger);
  }
  async createCorporatePassenger(
    command: CreateCorporatePassengerCommand,
    currentUser: UserInfo,
    silent = false,
  ): Promise<PassengerResponse> {
    let existingPassenger = await this.passengerRepository.getByPhoneNumber(
      command.phoneNumber,
      true,
    );
    if (existingPassenger) {
      if (existingPassenger.corporateId !== command.corporateId) {
        existingPassenger.corporateId = command.corporateId;
        existingPassenger.enabled = true;
        existingPassenger.isActive = true;
        existingPassenger = await this.passengerRepository.update(
          existingPassenger,
        );
      }
      return PassengerResponse.fromDomain(existingPassenger);
    }
    if (
      command.email &&
      (await this.passengerRepository.getByEmail(command.email, true))
    ) {
      if (silent) return null;
      throw new BadRequestException(
        `Customer already exist with this email Address`,
      );
    }
    const passengerDomain =
      CreateCorporatePassengerCommand.fromCommand(command);
    const passenger = await this.passengerRepository.insert(passengerDomain);
    if (passenger) {
      const password = Util.generatePassword(6);
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = passenger.id;
      createAccountCommand.type = CredentialType.Passenger;
      createAccountCommand.role = ['passenger'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(password);
      createAccountCommand.address = command.address;
      createAccountCommand.gender = command.gender;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      if (account && account.email) {
        this.eventEmitter.emit('send.email.credential', {
          name: account.name,
          email: account.email,
          phoneNumber: account.phoneNumber,
          password: password,
        });
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: passenger.id,
        modelName: MODELS.PASSENGER,
        action: ACTIONS.CREATE,
        userId: currentUser.id,
        user: currentUser,
      });
      this.eventEmitter.emit('create.wallet', {
        passengerId: passenger.id,
        balance: 0,
      });
    }
    return PassengerResponse.fromDomain(passenger);
  }
  async updatePassenger(
    command: UpdatePassengerCommand,
  ): Promise<PassengerResponse> {
    const passengerDomain = await this.passengerRepository.getById(command.id);
    if (!passengerDomain) {
      throw new NotFoundException(`Passenger not found with id ${command.id}`);
    }
    passengerDomain.email = command.email;
    passengerDomain.name = command.name;
    passengerDomain.address = command.address;
    passengerDomain.phoneNumber = command.phoneNumber;
    passengerDomain.gender = command.gender;
    passengerDomain.emergencyContact = command.emergencyContact;
    const passenger = await this.passengerRepository.update(passengerDomain);
    if (passenger) {
      this.eventEmitter.emit('update.account', {
        accountId: passenger.id,
        name: passenger.name,
        email: passenger.email,
        type: CredentialType.Passenger,
        phoneNumber: passenger.phoneNumber,
        address: passenger.address,
        gender: passenger.gender,
        profileImage: passenger.profileImage,
      });
    }
    return PassengerResponse.fromDomain(passenger);
  }
  async archivePassenger(
    archiveCommand: ArchivePassengerCommand,
  ): Promise<PassengerResponse> {
    const passengerDomain = await this.passengerRepository.getById(
      archiveCommand.id,
    );
    if (!passengerDomain) {
      throw new NotFoundException(
        `Passenger not found with id ${archiveCommand.id}`,
      );
    }

    passengerDomain.deletedAt = new Date();
    passengerDomain.deletedBy = archiveCommand.currentUser.id;
    passengerDomain.archiveReason = archiveCommand.reason;
    const result = await this.passengerRepository.update(passengerDomain);
    if (result) {
      this.eventEmitter.emit('account.archived', {
        phoneNumber: passengerDomain.phoneNumber,
        id: passengerDomain.id,
      });
      this.eventEmitter.emit('archive.wallet', passengerDomain.wallet.id);
      this.eventEmitter.emit('activity-logger.store', {
        modelId: archiveCommand.id,
        modelName: MODELS.PASSENGER,
        action: ACTIONS.ARCHIVE,
        userId: archiveCommand.currentUser.id,
        user: archiveCommand.currentUser,
      });
    }
    return PassengerResponse.fromDomain(result);
  }
  async restorePassenger(
    id: string,
    currentUser: UserInfo,
  ): Promise<PassengerResponse> {
    const passengerDomain = await this.passengerRepository.getById(id, true);
    if (!passengerDomain) {
      throw new NotFoundException(`Passenger not found with id ${id}`);
    }
    const r = await this.passengerRepository.restore(id);
    if (r) {
      this.eventEmitter.emit('account.restored', {
        phoneNumber: passengerDomain.phoneNumber,
        id: passengerDomain.id,
      });
      passengerDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PASSENGER,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
      this.eventEmitter.emit('restore.wallet', passengerDomain.wallet.id);
    }
    return PassengerResponse.fromDomain(passengerDomain);
  }
  async deletePassenger(id: string, currentUser: UserInfo): Promise<boolean> {
    const passengerDomain = await this.passengerRepository.getById(id, true);
    if (!passengerDomain) {
      throw new NotFoundException(`Passenger not found with id ${id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.PASSENGER,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    const result = await this.passengerRepository.delete(id);
    if (result) {
      this.eventEmitter.emit('account.deleted', {
        phoneNumber: passengerDomain.phoneNumber,
        id: passengerDomain.id,
      });
      this.eventEmitter.emit('delete.wallet', passengerDomain.wallet.id);
    }
    return result;
  }
  async activateOrBlockPassenger(
    id: string,
    currentUser: UserInfo,
  ): Promise<PassengerResponse> {
    const passengerDomain = await this.passengerRepository.getById(id);
    if (!passengerDomain) {
      throw new NotFoundException(`Passenger not found with id ${id}`);
    }
    passengerDomain.enabled = !passengerDomain.enabled;
    passengerDomain.isActive = !passengerDomain.isActive;
    const result = await this.passengerRepository.update(passengerDomain);
    if (result) {
      this.eventEmitter.emit('account.activate-or-block', {
        phoneNumber: passengerDomain.phoneNumber,
        id: passengerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PASSENGER,
        action: passengerDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return PassengerResponse.fromDomain(result);
  }
  async removeCorporateEmployee(
    command: AddOrRemoveCorporatePassengerCommand,
  ): Promise<PassengerResponse> {
    let passengerDomain = await this.passengerRepository.getById(command.id);
    if (!passengerDomain) {
      throw new NotFoundException(`Employee not found with id ${command.id}`);
    }
    if (passengerDomain.corporateId === command.corporateId) {
      if (
        passengerDomain.wallet &&
        passengerDomain.wallet.corporateWalletBalance > 0
      ) {
        this.eventEmitter.emit('update.corporate.wallet', {
          corporateId: passengerDomain.corporateId,
          balance: passengerDomain.wallet.corporateWalletBalance,
          currentUser: command?.currentUser,
        });
        this.eventEmitter.emit('create.corporate.transaction', {
          amount: passengerDomain?.wallet?.corporateWalletBalance,
          corporateId: command.corporateId,
          status: PaymentStatus.Success,
          method: PaymentMethod.RefundFromEmployee,
          reason: `Refund wallet from employee balance`,
        });
        this.eventEmitter.emit('update.passenger.corporate.wallet', {
          passengerId: command.id,
          balance: -1 * passengerDomain?.wallet?.corporateWalletBalance,
        });
      }
      passengerDomain.corporateId = null;
      passengerDomain = await this.passengerRepository.update(passengerDomain);
    }
    return PassengerResponse.fromDomain(passengerDomain);
  }
  async addCorporateEmployee(
    command: AddOrRemoveCorporatePassengerCommand,
  ): Promise<PassengerResponse> {
    let passengerDomain = await this.passengerRepository.getById(command.id);
    if (!passengerDomain) {
      throw new NotFoundException(`Employee not found with id ${command.id}`);
    }
    const corporate = await this.corporateRepository.getById(
      command.corporateId,
    );
    passengerDomain.corporateId = command.corporateId;
    passengerDomain = await this.passengerRepository.update(passengerDomain);
    return PassengerResponse.fromDomain(passengerDomain);
  }
  async isValidEmployee(
    employee: CreateCorporatePassengerCommand,
  ): Promise<{ hasError: boolean; error: string }> {
    return validate(employee).then(async (errors) => {
      // errors is an array of validation errors
      if (errors.length > 0) {
        // console.log('validation failed. errors: ', errors);
        const stringError = [];
        errors.forEach((error) => {
          stringError.push(error.property);
        });
        // console.log(errors[0].property);
        return { hasError: true, error: stringError.join(',') };
      } else {
        return { hasError: false, error: null };
      }
    });
  }
  async updateProfileImage(
    id: string,
    fileDto: FileResponseDto,
  ): Promise<PassengerResponse> {
    const passengerDomain = await this.passengerRepository.getById(id, true);
    if (!passengerDomain) {
      throw new NotFoundException(`Passenger not found with id ${id}`);
    }
    if (passengerDomain.profileImage && fileDto) {
      await this.fileManagerService.removeFile(
        passengerDomain.profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    passengerDomain.profileImage = fileDto as FileDto;
    const result = await this.passengerRepository.update(passengerDomain);
    if (result) {
      this.eventEmitter.emit('update-account-profile', {
        id: result.id,
        profileImage: result.profileImage,
      });
    }
    return PassengerResponse.fromDomain(result);
  }
  async isPassengerExist(phoneNumber: string) {
    const passenger = await this.passengerRepository.getByPhoneNumber(
      phoneNumber,
    );
    return passenger ? true : false;
  }
}
