import { CorporateResponse } from '@customer/usecases/corporates/corporate.response';
import { FileDto } from '@libs/common/file-dto';

import { Address } from '@libs/common/address';
import { ApiProperty } from '@nestjs/swagger';
import { Passenger } from '@customer/domains/passengers/passenger';
import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { EmergencyContact } from '@libs/common/emergency-contact';
import { WalletResponse } from '@credit/usecases/wallets/wallet.response';

export class PassengerResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  profileImage: FileDto;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  fcmId: string;
  @ApiProperty()
  corporateId?: string;
  @ApiProperty()
  corporateName?: string;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  corporate: CorporateResponse;
  wallet?: WalletResponse;
  static fromEntity(passengerEntity: PassengerEntity): PassengerResponse {
    const passengerResponse = new PassengerResponse();
    passengerResponse.id = passengerEntity.id;
    passengerResponse.name = passengerEntity.name;
    passengerResponse.email = passengerEntity.email;
    passengerResponse.phoneNumber = passengerEntity.phoneNumber;
    passengerResponse.gender = passengerEntity.gender;
    passengerResponse.enabled = passengerEntity.enabled;
    passengerResponse.profileImage = passengerEntity.profileImage;
    passengerResponse.address = passengerEntity.address;
    passengerResponse.corporateId = passengerEntity.corporateId;
    passengerResponse.corporateName = passengerEntity.corporateName;
    passengerResponse.fcmId = passengerEntity.fcmId;
    passengerResponse.emergencyContact = passengerEntity.emergencyContact;
    passengerResponse.isActive = passengerEntity.isActive;
    passengerResponse.archiveReason = passengerEntity.archiveReason;
    passengerResponse.createdBy = passengerEntity.createdBy;
    passengerResponse.updatedBy = passengerEntity.updatedBy;
    passengerResponse.deletedBy = passengerEntity.deletedBy;
    passengerResponse.createdAt = passengerEntity.createdAt;
    passengerResponse.updatedAt = passengerEntity.updatedAt;
    passengerResponse.deletedAt = passengerEntity.deletedAt;
    if (passengerEntity.corporate) {
      passengerResponse.corporate = CorporateResponse.fromEntity(
        passengerEntity.corporate,
      );
    }
    if (passengerEntity.wallet) {
      passengerResponse.wallet = WalletResponse.fromEntity(
        passengerEntity.wallet,
      );
    }
    return passengerResponse;
  }
  static fromDomain(passenger: Passenger): PassengerResponse {
    const passengerResponse = new PassengerResponse();
    passengerResponse.id = passenger.id;
    passengerResponse.name = passenger.name;
    passengerResponse.email = passenger.email;
    passengerResponse.phoneNumber = passenger.phoneNumber;
    passengerResponse.gender = passenger.gender;
    passengerResponse.enabled = passenger.enabled;
    passengerResponse.profileImage = passenger.profileImage;
    passengerResponse.address = passenger.address;
    passengerResponse.corporateId = passenger.corporateId;
    passengerResponse.corporateName = passenger.corporateName;
    passengerResponse.fcmId = passenger.fcmId;
    passengerResponse.emergencyContact = passenger.emergencyContact;
    passengerResponse.isActive = passenger.isActive;
    passengerResponse.archiveReason = passenger.archiveReason;
    passengerResponse.createdBy = passenger.createdBy;
    passengerResponse.updatedBy = passenger.updatedBy;
    passengerResponse.deletedBy = passenger.deletedBy;
    passengerResponse.createdAt = passenger.createdAt;
    passengerResponse.updatedAt = passenger.updatedAt;
    passengerResponse.deletedAt = passenger.deletedAt;
    if (passenger.corporate) {
      passengerResponse.corporate = CorporateResponse.fromDomain(
        passenger.corporate,
      );
    }
    if (passenger.wallet) {
      passengerResponse.wallet = WalletResponse.fromDomain(passenger.wallet);
    }
    return passengerResponse;
  }
}
