import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PassengerEntity } from '@customer/persistence/passengers/passenger.entity';
import { Repository } from 'typeorm';
import { PassengerResponse } from './passenger.response';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
@Injectable()
export class PassengerQuery {
  constructor(
    @InjectRepository(PassengerEntity)
    private passengerRepository: Repository<PassengerEntity>,
  ) {}
  async getPassenger(
    id: string,
    withDeleted = false,
  ): Promise<PassengerResponse> {
    const passenger = await this.passengerRepository.findOne({
      where: { id: id },
      relations: ['corporate'],
      withDeleted: withDeleted,
    });
    if (!passenger) {
      throw new NotFoundException(`Passenger not found with id ${id}`);
    }
    return PassengerResponse.fromEntity(passenger);
  }
  async getPassengers(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PassengerResponse>> {
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );
    const d = new DataResponseFormat<PassengerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PassengerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async groupPassengersByGender(
    query: CollectionQuery,
  ): Promise<CountByGenderResponse[]> {
    query.select = [];
    query.select.push('gender', 'COUNT(passengers.id)');
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );
    dataQuery.groupBy('passengers.gender');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByGenderResponse();
      countResponse.gender = d.passengers_gender;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupPassengersByStatus(
    query: CollectionQuery,
  ): Promise<CountByGenderResponse[]> {
    query.select = [];
    query.select.push('is_active as isActive', 'COUNT(passengers.id)');
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );
    dataQuery.groupBy('passengers.isActive');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.isactive ? 'Active' : 'InActive';
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupPassengersByCreatedDate(
    query: CollectionQuery,
    format: string,
  ): Promise<CountByCreatedAtResponse[]> {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 1);
    const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    let formatQ = '';
    if (format === 'year') {
      formatQ = 'YYYY';
    } else if (format === 'month') {
      formatQ = 'MM';
    } else if (format === 'date') {
      formatQ = 'MM-dd';
    }
    if (format !== 'year') {
      if (!query.filter) {
        query.filter = [];
      }
      query.filter.push([
        {
          field: 'passengers.created_at',
          value: [startOfYear.toISOString(), endOfYear.toISOString()],
          operator: FilterOperators.Between,
        },
      ]);
    }
    query.select = [];
    query.select.push(
      `to_char(passengers.created_at,'${formatQ}') as created_date`,
      'COUNT(passengers.id)',
    );
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupPassengersByAddress(
    field: string,
    query: CollectionQuery,
  ): Promise<GroupByAddressResponse[]> {
    query.select = [];
    query.select.push(
      `passengers.address->>'${field}' as ${field}`,
      `COUNT(passengers.id)`,
    );
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );

    dataQuery.groupBy(`passengers.address->>'${field}'`);
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByAddressResponse();
      countResponse[field] = d[field];
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getPassengersByFilter(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PassengerResponse>> {
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );
    const d = new DataResponseFormat<PassengerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PassengerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getCorporatePassengers(
    corporateId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PassengerResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'corporateId',
        value: corporateId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );
    const d = new DataResponseFormat<PassengerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PassengerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedPassengers(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<PassengerResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<PassengerEntity>(
      this.passengerRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<PassengerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => PassengerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
