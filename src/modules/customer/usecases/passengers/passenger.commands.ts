import { UserInfo } from '@account/dtos/user-info.dto';
import { Address } from '@libs/common/address';
import { ApiProperty } from '@nestjs/swagger';
import { Passenger } from '@customer/domains/passengers/passenger';
import { IsEnum, IsNotEmpty, IsUUID } from 'class-validator';
import { EmergencyContact } from '@libs/common/emergency-contact';
import { Gender } from '@libs/common/enums';
export class CreatePassengerCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  password: string;
  @ApiProperty()
  //@IsNotEmpty()
  gender: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  static fromCommand(command: CreatePassengerCommand): Passenger {
    const passenger = new Passenger();
    passenger.name = command.name;
    passenger.email = command.email;
    passenger.phoneNumber = command.phoneNumber;
    passenger.gender = command.gender;
    passenger.address = command.address;
    passenger.emergencyContact = command.emergencyContact;
    return passenger;
  }
}
export class UpdatePassengerCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  static fromCommand(command: UpdatePassengerCommand): Passenger {
    const passenger = new Passenger();
    passenger.id = command.id;
    passenger.name = command.name;
    passenger.email = command.email;
    passenger.phoneNumber = command.phoneNumber;
    passenger.gender = command.gender;
    passenger.address = command.address;
    passenger.emergencyContact = command.emergencyContact;
    return passenger;
  }
}
export class CreateCorporatePassengerCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  corporateId: string;
  @ApiProperty()
  corporateName: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  @ApiProperty()
  address: Address;
  static fromCommand(command: CreateCorporatePassengerCommand): Passenger {
    const passenger = new Passenger();
    passenger.name = command.name;
    passenger.email = command.email;
    passenger.phoneNumber = command.phoneNumber;
    passenger.gender = command.gender;
    passenger.address = command.address;
    passenger.emergencyContact = command.emergencyContact;
    passenger.corporateId = command.corporateId;
    passenger.corporateName = command.corporateName;
    return passenger;
  }
}
export class UpdateProfileCommand {
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  static fromCommand(command: UpdateProfileCommand): Passenger {
    const passenger = new Passenger();
    passenger.id = command.id;
    passenger.name = command.name;
    passenger.email = command.email;
    passenger.phoneNumber = command.phoneNumber;
    passenger.gender = command.gender;
    passenger.address = command.address;
    passenger.emergencyContact = command.emergencyContact;
    return passenger;
  }
}
export class AddOrRemoveCorporatePassengerCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  corporateId: string;
  currentUser: UserInfo;
}
export class ArchivePassengerCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
