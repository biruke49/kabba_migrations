import { UserInfo } from '@account/dtos/user-info.dto';
import { School } from '@customer/domains/schools/school';
import { ContactPerson } from '@libs/common/emergency-contact';
import { SchoolStatus } from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class SchoolAddressCommand {
  @ApiProperty()
  @IsNotEmpty()
  country: string;
  @ApiProperty()
  @IsNotEmpty()
  city: string;
  @ApiProperty()
  @IsNotEmpty()
  subCity: string;
  @ApiProperty()
  @IsNotEmpty()
  woreda: string;
  @ApiProperty()
  houseNumber: string;
}
export class CreateSchoolCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  @IsEmail()
  email: string;
  @ApiProperty({
    example: '+251911111111',
  })
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty({
    example: {
      country: 'Ethiopia',
      city: 'Addis Ababa',
      subCity: 'Kolfe',
      woreda: '01',
      houseNumber: '1000002',
    },
  })
  @IsNotEmpty()
  address: SchoolAddressCommand;
  @ApiProperty()
  website: string;
  @ApiProperty()
  logo: FileDto;
  @ApiProperty()
  @IsNotEmpty()
  isPrivate: boolean;
  @ApiProperty()
  @IsNotEmpty()
  enabled: boolean;
  @ApiProperty()
  @IsNotEmpty()
  lng: number;
  @ApiProperty()
  @IsNotEmpty()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  status: string;
  @ApiProperty()
  contactPerson: ContactPerson;
  currentUser: UserInfo;
  static fromCommand(command: CreateSchoolCommand): School {
    const kidDomain = new School();
    kidDomain.name = command.name;
    kidDomain.email = command.email;
    kidDomain.phoneNumber = command.phoneNumber;
    kidDomain.address = { ...command.address };
    kidDomain.website = command.website;
    kidDomain.logo = command.logo;
    kidDomain.isPrivate = command.isPrivate;
    kidDomain.enabled = command.enabled;
    kidDomain.lng = command.lng;
    kidDomain.lat = command.lat;
    kidDomain.status = command.status;
    kidDomain.contactPerson = command.contactPerson;
    return kidDomain;
  }
}
export class UpdateSchoolCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  @IsEmail()
  email: string;
  @ApiProperty({
    example: '+251911111111',
  })
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty({
    example: {
      country: 'Ethiopia',
      city: 'Addis Ababa',
      subCity: 'Kolfe',
      woreda: '01',
      houseNumber: '1000002',
    },
  })
  @IsNotEmpty()
  address: SchoolAddressCommand;
  @ApiProperty()
  website: string;
  @ApiProperty()
  logo: FileDto;
  @ApiProperty()
  @IsNotEmpty()
  isPrivate: boolean;
  @ApiProperty()
  @IsNotEmpty()
  lng: number;
  @ApiProperty()
  @IsNotEmpty()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  status: string;
  @ApiProperty()
  @IsNotEmpty()
  enabled: boolean;
  @ApiProperty()
  contactPerson: ContactPerson;
  currentUser: UserInfo;
}
export class ArchiveSchoolCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
