import { SchoolEntity } from '@customer/persistence/schools/school.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { SchoolResponse } from './school.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';

@Injectable()
export class SchoolQuery {
  constructor(
    @InjectRepository(SchoolEntity)
    private schoolRepository: Repository<SchoolEntity>,
    private dataSource: DataSource,
  ) {}
  async getSchool(id: string, withDeleted = false): Promise<SchoolResponse> {
    const parent = await this.schoolRepository.find({
      where: { id: id },
      relations: ['kids'],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      throw new NotFoundException(`School not found with id ${id}`);
    }
    return SchoolResponse.fromEntity(parent[0]);
  }
  async getSchools(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<SchoolResponse>> {
    const dataQuery = QueryConstructor.constructQuery<SchoolEntity>(
      this.schoolRepository,
      query,
    );
    const d = new DataResponseFormat<SchoolResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => SchoolResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedSchools(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<SchoolResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<SchoolEntity>(
      this.schoolRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<SchoolResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => SchoolResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async groupSchoolsByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('enabled as enabled', 'COUNT(schools.id)');
    const dataQuery = QueryConstructor.constructQuery<SchoolEntity>(
      this.schoolRepository,
      query,
    );
    dataQuery.groupBy('schools.enabled');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.enabled ? 'Active' : 'InActive';
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupSchoolsByType(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('is_private as is_private', 'COUNT(schools.id)');
    const dataQuery = QueryConstructor.constructQuery<SchoolEntity>(
      this.schoolRepository,
      query,
    );
    dataQuery.groupBy('schools.is_private');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse: any = {};
      countResponse.type = d.is_private ? 'Private' : 'Public';
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupSchoolsByAddress(
    field: string,
    query: CollectionQuery,
  ): Promise<GroupByAddressResponse[]> {
    query.select = [];
    query.select.push(
      `schools.address->>'${field}' as ${field}`,
      `COUNT(schools.id)`,
    );
    const dataQuery = QueryConstructor.constructQuery<SchoolEntity>(
      this.schoolRepository,
      query,
    );

    dataQuery.groupBy(`schools.address->>'${field}'`);
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByAddressResponse();
      countResponse[field] = d[field];
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
}
