import { School } from '@customer/domains/schools/school';
import { SchoolEntity } from '@customer/persistence/schools/school.entity';
import { Address } from '@libs/common/address';
import { ContactPerson } from '@libs/common/emergency-contact';
import { SchoolStatus } from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { KidResponse } from '../parents/kid.response';

export class SchoolResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  logo: FileDto;
  @ApiProperty()
  website: string;
  @ApiProperty()
  isPrivate: boolean;
  @ApiProperty()
  contactPerson: ContactPerson;
  @ApiProperty()
  status: string;
  @ApiProperty()
  lng: number;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty({ isArray: true, type: KidResponse })
  kids: KidResponse[];
  static fromEntity(schoolEntity: SchoolEntity): SchoolResponse {
    const schoolResponse = new SchoolResponse();
    schoolResponse.id = schoolEntity.id;
    schoolResponse.name = schoolEntity.name;
    schoolResponse.email = schoolEntity.email;
    schoolResponse.phoneNumber = schoolEntity.phoneNumber;
    schoolResponse.website = schoolEntity.website;
    schoolResponse.isPrivate = schoolEntity.isPrivate;
    schoolResponse.contactPerson = schoolEntity.contactPerson;
    schoolResponse.lng = schoolEntity.lng;
    schoolResponse.lat = schoolEntity.lat;
    schoolResponse.status = schoolEntity.status;
    schoolResponse.enabled = schoolEntity.enabled;
    schoolResponse.address = schoolEntity.address;
    schoolResponse.archiveReason = schoolEntity.archiveReason;
    schoolResponse.createdBy = schoolEntity.createdBy;
    schoolResponse.updatedBy = schoolEntity.updatedBy;
    schoolResponse.deletedBy = schoolEntity.deletedBy;
    schoolResponse.createdAt = schoolEntity.createdAt;
    schoolResponse.updatedAt = schoolEntity.updatedAt;
    schoolResponse.deletedAt = schoolEntity.deletedAt;
    if (schoolEntity.kids) {
      schoolResponse.kids = schoolEntity.kids
        ? schoolEntity.kids.map((kid) => {
            return KidResponse.fromEntity(kid);
          })
        : [];
    }
    if (schoolEntity.logo) {
      schoolResponse.logo = schoolEntity.logo;
    }
    return schoolResponse;
  }
  static fromDomain(school: School): SchoolResponse {
    const schoolResponse = new SchoolResponse();
    schoolResponse.id = school.id;
    schoolResponse.name = school.name;
    schoolResponse.email = school.email;
    schoolResponse.phoneNumber = school.phoneNumber;
    schoolResponse.logo = school.logo;
    schoolResponse.website = school.website;
    schoolResponse.isPrivate = school.isPrivate;
    schoolResponse.contactPerson = school.contactPerson;
    schoolResponse.lng = school.lng;
    schoolResponse.lat = school.lat;
    schoolResponse.status = school.status;
    schoolResponse.enabled = school.enabled;
    schoolResponse.address = school.address;
    schoolResponse.archiveReason = school.archiveReason;
    schoolResponse.createdBy = school.createdBy;
    schoolResponse.updatedBy = school.updatedBy;
    schoolResponse.deletedBy = school.deletedBy;
    schoolResponse.createdAt = school.createdAt;
    schoolResponse.updatedAt = school.updatedAt;
    schoolResponse.deletedAt = school.deletedAt;
    if (school.kids) {
      schoolResponse.kids = school.kids
        ? school.kids.map((kid) => {
            return KidResponse.fromDomain(kid);
          })
        : [];
    }
    if (school.logo) {
      schoolResponse.logo = school.logo;
    }
    return schoolResponse;
  }
}
