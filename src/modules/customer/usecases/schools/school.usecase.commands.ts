import { CreateAccountCommand } from '@account/usecases/accounts/account.commands';
import { AccountCommands } from '@account/usecases/accounts/account.usecase.commands';
import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { UserInfo } from '@account/dtos/user-info.dto';
import { SchoolRepository } from '@customer/persistence/schools/school.repository';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { CredentialType } from '@libs/common/enums';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { Util } from '@libs/common/util';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { ParentQuery } from '../parents/parent.usecase.queries';
import {
  ArchiveSchoolCommand,
  CreateSchoolCommand,
  UpdateSchoolCommand,
} from './school.commands';
import { SchoolResponse } from './school.response';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import { FileDto } from '@libs/common/file-dto';
import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SchoolCommands {
  constructor(
    private schoolRepository: SchoolRepository,
    private readonly accountCommands: AccountCommands,
    private readonly parentRepository: ParentRepository,
    @InjectRepository(KidEntity)
    private kidRepository: Repository<KidEntity>,
    private eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}

  async createSchool(command: CreateSchoolCommand): Promise<SchoolResponse> {
    if (
      command.phoneNumber &&
      (await this.schoolRepository.getByPhoneNumber(command.phoneNumber, true))
    ) {
      throw new BadRequestException(
        `School already exist with this phone number`,
      );
    }
    if (
      command.name &&
      (await this.schoolRepository.getByName(command.name, true))
    ) {
      throw new BadRequestException(`School already exist with this name`);
    }
    if (
      command.email &&
      (await this.schoolRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `School already exist with this email Address`,
      );
    }
    if (
      command.website &&
      (await this.schoolRepository.getByWebsite(command.website, true))
    ) {
      throw new BadRequestException(`School already exist with this website`);
    }
    const schoolDomain = CreateSchoolCommand.fromCommand(command);
    const school = await this.schoolRepository.insert(schoolDomain);
    if (school) {
      const password = Util.generatePassword(6);
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = school.id;
      createAccountCommand.type = CredentialType.School;
      createAccountCommand.role = ['school'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(password);
      createAccountCommand.address = command.address;
      createAccountCommand.profileImage = command.logo;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      // if (account && account.email) {
      //   this.eventEmitter.emit('send.email.credential', {
      //     name: account.name,
      //     email: account.email,
      //     phoneNumber: account.phoneNumber,
      //     password: password,
      //   });
      // }

      this.eventEmitter.emit('activity-logger.store', {
        modelId: school.id,
        modelName: MODELS.SCHOOL,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return SchoolResponse.fromDomain(school);
  }
  async updateSchool(command: UpdateSchoolCommand): Promise<SchoolResponse> {
    const schoolDomain = await this.schoolRepository.getById(command.id);
    if (!schoolDomain) {
      throw new NotFoundException(`School not found with id ${command.id}`);
    }
    if (
      command.phoneNumber &&
      schoolDomain.phoneNumber !== command.phoneNumber &&
      (await this.schoolRepository.getByPhoneNumber(command.phoneNumber, true))
    ) {
      throw new BadRequestException(
        `School already exist with this phone number`,
      );
    }
    if (
      command.name &&
      schoolDomain.name !== command.name &&
      (await this.schoolRepository.getByName(command.name, true))
    ) {
      throw new BadRequestException(`School already exist with this name`);
    }
    if (
      command.email &&
      schoolDomain.email !== command.email &&
      (await this.schoolRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `School already exist with this email Address`,
      );
    }
    if (schoolDomain.name !== command.name) {
      const pdated = await this.kidRepository.update(
        { schoolId: command.id },
        { schoolName: command.name },
      );
    }
    const oldPayload = { ...schoolDomain };
    schoolDomain.email = command.email;
    schoolDomain.name = command.name;
    schoolDomain.address = command.address;
    schoolDomain.phoneNumber = command.phoneNumber;
    schoolDomain.website = command.website;
    schoolDomain.lat = command.lat;
    schoolDomain.lng = command.lng;
    schoolDomain.status = command.status;
    schoolDomain.isPrivate = command.isPrivate;
    schoolDomain.enabled = command.enabled;
    schoolDomain.contactPerson = command.contactPerson;
    if (command.logo && schoolDomain.logo) {
      await this.fileManagerService.removeFile(
        schoolDomain.logo,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    schoolDomain.logo = command.logo;
    const school = await this.schoolRepository.update(schoolDomain);
    if (school) {
      this.eventEmitter.emit('update.account', {
        accountId: school.id,
        name: school.name,
        email: school.email,
        type: CredentialType.School,
        phoneNumber: school.phoneNumber,
        profileImage: school.logo,
        address: school.address,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: school.id,
        modelName: MODELS.SCHOOL,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: { ...oldPayload },
        payload: { ...school },
      });
    }
    return SchoolResponse.fromDomain(school);
  }
  async archiveSchool(
    archiveCommand: ArchiveSchoolCommand,
  ): Promise<SchoolResponse> {
    const schoolDomain = await this.schoolRepository.getById(archiveCommand.id);
    if (!schoolDomain) {
      throw new NotFoundException(
        `School not found with id ${archiveCommand.id}`,
      );
    }
    const kidsWithSchooId = await this.parentRepository.getBySchoolId(
      archiveCommand.id,
    );
    if (kidsWithSchooId) {
      throw new NotFoundException(
        `School can not be archived because there are kids registered in it.`,
      );
    }
    schoolDomain.deletedAt = new Date();
    schoolDomain.deletedBy = archiveCommand.currentUser.id;
    schoolDomain.archiveReason = archiveCommand.reason;
    const result = await this.schoolRepository.update(schoolDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: archiveCommand.id,
        modelName: MODELS.SCHOOL,
        action: ACTIONS.ARCHIVE,
        userId: archiveCommand.currentUser.id,
        user: archiveCommand.currentUser,
      });
      this.eventEmitter.emit('account.archived', {
        phoneNumber: schoolDomain.phoneNumber,
        id: schoolDomain.id,
      });
    }
    return SchoolResponse.fromDomain(result);
  }
  async restoreSchool(
    id: string,
    currentUser: UserInfo,
  ): Promise<SchoolResponse> {
    const schoolDomain = await this.schoolRepository.getById(id, true);
    if (!schoolDomain) {
      throw new NotFoundException(`School not found with id ${id}`);
    }
    const r = await this.schoolRepository.restore(id);
    if (r) {
      this.eventEmitter.emit('account.restored', {
        phoneNumber: schoolDomain.phoneNumber,
        id: schoolDomain.id,
      });
      schoolDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.SCHOOL,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return SchoolResponse.fromDomain(schoolDomain);
  }
  async deleteSchool(id: string, currentUser: UserInfo): Promise<boolean> {
    const schoolDomain = await this.schoolRepository.getById(id, true);
    if (!schoolDomain) {
      throw new NotFoundException(`School not found with id ${id}`);
    }
    const kidsWithSchooId = await this.parentRepository.getBySchoolId(id);
    if (kidsWithSchooId) {
      throw new NotFoundException(
        `School can not be deleted because there are kids registered in it.`,
      );
    }
    const result = await this.schoolRepository.delete(id);
    if (result) {
      if (schoolDomain.logo) {
        await this.fileManagerService.removeFile(
          schoolDomain.logo,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      this.eventEmitter.emit('account.deleted', {
        phoneNumber: schoolDomain.phoneNumber,
        id: schoolDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.SCHOOL,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
  async activateOrBlockSchool(
    id: string,
    currentUser: UserInfo,
  ): Promise<SchoolResponse> {
    const schoolDomain = await this.schoolRepository.getById(id);
    if (!schoolDomain) {
      throw new NotFoundException(`School not found with id ${id}`);
    }
    schoolDomain.enabled = !schoolDomain.enabled;
    const result = await this.schoolRepository.update(schoolDomain);
    if (result) {
      this.eventEmitter.emit('account.activate-or-block', {
        phoneNumber: schoolDomain.phoneNumber,
        id: schoolDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.SCHOOL,
        action: schoolDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return SchoolResponse.fromDomain(result);
  }
  async updateSchoolLogo(id: string, fileDto: FileResponseDto) {
    const schoolDomain = await this.schoolRepository.getById(id);
    if (!schoolDomain) {
      throw new NotFoundException(`Kid not found with id ${id}`);
    }
    if (schoolDomain.logo && fileDto) {
      await this.fileManagerService.removeFile(
        schoolDomain.logo,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    schoolDomain.logo = fileDto as FileDto;
    const result = await this.schoolRepository.update(schoolDomain);

    return SchoolResponse.fromDomain(result);
  }
}
