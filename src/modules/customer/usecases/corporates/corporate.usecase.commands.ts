import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { UserInfo } from '@account/dtos/user-info.dto';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import {
  ArchiveCorporateCommand,
  CreateCorporateCommand,
  UpdateCorporateCommand,
} from './corporate.commands';
import { CorporateRepository } from '@customer/persistence/corporates/corporate.repository';
import { CorporateResponse } from './corporate.response';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { AccountCommands } from '@account/usecases/accounts/account.usecase.commands';
import { CreateAccountCommand } from '@account/usecases/accounts/account.commands';
import { CredentialType } from '@libs/common/enums';
import { Util } from '@libs/common/util';
@Injectable()
export class CorporateCommands {
  constructor(
    private corporateRepository: CorporateRepository,
    private readonly accountCommands: AccountCommands,
    private readonly eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createCorporate(
    command: CreateCorporateCommand,
  ): Promise<CorporateResponse> {
    if (
      await this.corporateRepository.getByPhoneNumber(command.phoneNumber, true)
    ) {
      throw new BadRequestException(
        `Corporate already exist with this phone number`,
      );
    }
    if (
      command.email &&
      (await this.corporateRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Corporate already exist with this email Address`,
      );
    }
    if (
      command.tinNumber &&
      (await this.corporateRepository.getByTinNumber(command.tinNumber, true))
    ) {
      throw new BadRequestException(
        `Corporate already exist with this tin number`,
      );
    }
    const corporateDomain = CreateCorporateCommand.fromCommand(command);
    const corporate = await this.corporateRepository.insert(corporateDomain);
    if (corporate) {
      const password = 'P@ssw0rd'; //Util.generatePassword(6);
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = corporate.id;
      createAccountCommand.type = CredentialType.Corporate;
      createAccountCommand.role = ['corporate'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(password);
      createAccountCommand.address = command.address;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      // if (account && account.email) {
      //   this.eventEmitter.emit('send.email.credential', {
      //     name: account.name,
      //     email: account.email,
      //     phoneNumber: account.phoneNumber,
      //     password: password,
      //   });
      // }

      this.eventEmitter.emit('activity-logger.store', {
        modelId: corporate.id,
        modelName: MODELS.CORPORATE,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
      this.eventEmitter.emit('create.corporate.wallet', {
        corporateId: corporate.id,
        balance: 0,
      });
    }
    return CorporateResponse.fromDomain(corporate);
  }
  async updateCorporate(
    command: UpdateCorporateCommand,
  ): Promise<CorporateResponse> {
    const corporateDomain = await this.corporateRepository.getById(command.id);
    if (!corporateDomain) {
      throw new NotFoundException(`Corporate not found with id ${command.id}`);
    }
    if (
      corporateDomain.phoneNumber !== command.phoneNumber &&
      (await this.corporateRepository.getByPhoneNumber(
        command.phoneNumber,
        true,
      ))
    ) {
      throw new BadRequestException(
        `Corporate already exist with this phone number`,
      );
    }
    if (
      command.email &&
      corporateDomain.email !== command.email &&
      (await this.corporateRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Corporate already exist with this email Address`,
      );
    }
    if (
      command.tinNumber &&
      corporateDomain.tinNumber !== command.tinNumber &&
      (await this.corporateRepository.getByTinNumber(command.tinNumber, true))
    ) {
      throw new BadRequestException(
        `Corporate already exist with this tin number`,
      );
    }
    const oldPayload = { ...corporateDomain };
    corporateDomain.email = command.email;
    corporateDomain.name = command.name;
    corporateDomain.address = command.address;
    corporateDomain.phoneNumber = command.phoneNumber;
    corporateDomain.website = command.website;
    corporateDomain.contactPerson = command.contactPerson;
    corporateDomain.tinNumber = command.tinNumber;
    corporateDomain.landmark = command.landmark;
    corporateDomain.lat = command.lat;
    corporateDomain.lng = command.lng;
    if (command.logo && corporateDomain.logo) {
      await this.fileManagerService.removeFile(
        corporateDomain.logo,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    corporateDomain.logo = command.logo;
    const corporate = await this.corporateRepository.update(corporateDomain);
    if (corporate) {
      this.eventEmitter.emit('update.account', {
        accountId: corporate.id,
        name: corporate.name,
        email: corporate.email,
        type: CredentialType.Corporate,
        phoneNumber: corporate.phoneNumber,
        profileImage: corporate.logo,
        address: corporate.address,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: corporate.id,
        modelName: MODELS.CORPORATE,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: { ...oldPayload },
        payload: { ...corporate },
      });
    }
    return CorporateResponse.fromDomain(corporate);
  }
  async archiveCorporate(
    archiveCommand: ArchiveCorporateCommand,
  ): Promise<CorporateResponse> {
    const corporateDomain = await this.corporateRepository.getById(
      archiveCommand.id,
    );
    if (!corporateDomain) {
      throw new NotFoundException(
        `Corporate not found with id ${archiveCommand.id}`,
      );
    }

    corporateDomain.deletedAt = new Date();
    corporateDomain.deletedBy = archiveCommand.currentUser.id;
    corporateDomain.archiveReason = archiveCommand.reason;
    const result = await this.corporateRepository.update(corporateDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: archiveCommand.id,
        modelName: MODELS.CORPORATE,
        action: ACTIONS.ARCHIVE,
        userId: archiveCommand.currentUser.id,
        user: archiveCommand.currentUser,
      });
      this.eventEmitter.emit('account.archived', {
        phoneNumber: corporateDomain.phoneNumber,
        id: corporateDomain.id,
      });
      this.eventEmitter.emit('archive.corporate.wallet', corporateDomain.id);
    }
    return CorporateResponse.fromDomain(result);
  }
  async restoreCorporate(
    id: string,
    currentUser: UserInfo,
  ): Promise<CorporateResponse> {
    const corporateDomain = await this.corporateRepository.getById(id, true);
    if (!corporateDomain) {
      throw new NotFoundException(`Corporate not found with id ${id}`);
    }
    const r = await this.corporateRepository.restore(id);
    if (r) {
      this.eventEmitter.emit('account.restored', {
        phoneNumber: corporateDomain.phoneNumber,
        id: corporateDomain.id,
      });
      corporateDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.CORPORATE,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
      this.eventEmitter.emit('restore.corporate.wallet', corporateDomain.id);
    }
    return CorporateResponse.fromDomain(corporateDomain);
  }
  async deleteCorporate(id: string, currentUser: UserInfo): Promise<boolean> {
    const corporateDomain = await this.corporateRepository.getById(id, true);
    if (!corporateDomain) {
      throw new NotFoundException(`Corporate not found with id ${id}`);
    }

    const result = await this.corporateRepository.delete(id);
    if (result) {
      if (corporateDomain.logo) {
        await this.fileManagerService.removeFile(
          corporateDomain.logo,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      this.eventEmitter.emit('account.deleted', {
        phoneNumber: corporateDomain.phoneNumber,
        id: corporateDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.CORPORATE,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
      this.eventEmitter.emit('delete.corporate.wallet', {
        corporateId: corporateDomain.id,
      });
    }
    return result;
  }
  async activateOrBlockCorporate(
    id: string,
    currentUser: UserInfo,
  ): Promise<CorporateResponse> {
    const corporateDomain = await this.corporateRepository.getById(id);
    if (!corporateDomain) {
      throw new NotFoundException(`Corporate not found with id ${id}`);
    }
    corporateDomain.enabled = !corporateDomain.enabled;
    const result = await this.corporateRepository.update(corporateDomain);
    if (result) {
      this.eventEmitter.emit('account.activate-or-block', {
        phoneNumber: corporateDomain.phoneNumber,
        id: corporateDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.CORPORATE,
        action: corporateDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return CorporateResponse.fromDomain(result);
  }
}
