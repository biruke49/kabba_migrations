import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CorporateEntity } from '@customer/persistence/corporates/corporate.entity';
import { Repository } from 'typeorm';
import { CorporateResponse } from './corporate.response';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
@Injectable()
export class CorporateQuery {
  constructor(
    @InjectRepository(CorporateEntity)
    private corporateRepository: Repository<CorporateEntity>,
  ) {}
  async getCorporate(
    id: string,
    withDeleted = false,
  ): Promise<CorporateResponse> {
    const corporate = await this.corporateRepository.find({
      where: { id: id },
      relations: ['wallet'],
      withDeleted: withDeleted,
    });
    if (!corporate[0]) {
      throw new NotFoundException(`Corporate not found with id ${id}`);
    }
    return CorporateResponse.fromEntity(corporate[0]);
  }
  async getCorporates(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CorporateResponse>> {
    const dataQuery = QueryConstructor.constructQuery<CorporateEntity>(
      this.corporateRepository,
      query,
    );
    const d = new DataResponseFormat<CorporateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CorporateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async countCorporatesByCreatedMonth(
    query: CollectionQuery,
  ): Promise<CountByCreatedAtResponse[]> {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 1);
    const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    query.select = [];
    query.select.push(
      `to_char(corporates.created_at,'MM') as created_date`,
      'COUNT(corporates.id)',
    );
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'corporates.created_at',
        value: [startOfYear.toISOString(), endOfYear.toISOString()],
        operator: FilterOperators.Between,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CorporateEntity>(
      this.corporateRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupCorporatesByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('enabled as enabled', 'COUNT(corporates.id)');
    const dataQuery = QueryConstructor.constructQuery<CorporateEntity>(
      this.corporateRepository,
      query,
    );
    const data = await dataQuery.groupBy('enabled').getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.enabled;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getArchivedCorporates(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CorporateResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CorporateEntity>(
      this.corporateRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<CorporateResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CorporateResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
