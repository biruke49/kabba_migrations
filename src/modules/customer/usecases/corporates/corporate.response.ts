import { CorporateEntity } from '@customer/persistence/corporates/corporate.entity';
import { Corporate } from '@customer/domains/corporates/corporate';
import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';
import { ContactPerson } from '@customer/domains/corporates/contact-person';
import { ApiProperty } from '@nestjs/swagger';
import { CorporateWalletResponse } from '@credit/usecases/corporate-wallets/corporate-wallet.response';
export class CorporateResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  tinNumber: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  logo: FileDto;
  @ApiProperty()
  contactPerson: ContactPerson;
  @ApiProperty()
  landmark: string;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  lng: number;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  wallet: CorporateWalletResponse;
  static fromEntity(corporateEntity: CorporateEntity): CorporateResponse {
    const corporateResponse = new CorporateResponse();
    corporateResponse.id = corporateEntity.id;
    corporateResponse.name = corporateEntity.name;
    corporateResponse.email = corporateEntity.email;
    corporateResponse.phoneNumber = corporateEntity.phoneNumber;
    corporateResponse.website = corporateEntity.website;
    corporateResponse.enabled = corporateEntity.enabled;
    corporateResponse.logo = corporateEntity.logo;
    corporateResponse.address = corporateEntity.address;
    corporateResponse.contactPerson = corporateEntity.contactPerson;
    corporateResponse.tinNumber = corporateEntity.tinNumber;
    corporateResponse.landmark = corporateEntity.landmark;
    corporateResponse.lat = corporateEntity.lat;
    corporateResponse.lng = corporateEntity.lng;
    corporateResponse.archiveReason = corporateEntity.archiveReason;
    corporateResponse.createdBy = corporateEntity.createdBy;
    corporateResponse.updatedBy = corporateEntity.updatedBy;
    corporateResponse.deletedBy = corporateEntity.deletedBy;
    corporateResponse.createdAt = corporateEntity.createdAt;
    corporateResponse.updatedAt = corporateEntity.updatedAt;
    corporateResponse.deletedAt = corporateEntity.deletedAt;
    if (corporateEntity.wallet) {
      corporateResponse.wallet = CorporateWalletResponse.fromEntity(
        corporateEntity.wallet,
      );
    }
    return corporateResponse;
  }
  static fromDomain(corporate: Corporate): CorporateResponse {
    const corporateResponse = new CorporateResponse();
    corporateResponse.id = corporate.id;
    corporateResponse.name = corporate.name;
    corporateResponse.email = corporate.email;
    corporateResponse.phoneNumber = corporate.phoneNumber;
    corporateResponse.website = corporate.website;
    corporateResponse.enabled = corporate.enabled;
    corporateResponse.logo = corporate.logo;
    corporateResponse.address = corporate.address;
    corporateResponse.contactPerson = corporate.contactPerson;
    corporateResponse.tinNumber = corporate.tinNumber;
    corporateResponse.landmark = corporate.landmark;
    corporateResponse.lat = corporate.lat;
    corporateResponse.lng = corporate.lng;
    corporateResponse.archiveReason = corporate.archiveReason;
    corporateResponse.createdBy = corporate.createdBy;
    corporateResponse.updatedBy = corporate.updatedBy;
    corporateResponse.deletedBy = corporate.deletedBy;
    corporateResponse.createdAt = corporate.createdAt;
    corporateResponse.updatedAt = corporate.updatedAt;
    corporateResponse.deletedAt = corporate.deletedAt;
    if (corporate.wallet) {
      corporateResponse.wallet = CorporateWalletResponse.fromDomain(
        corporate.wallet,
      );
    }
    return corporateResponse;
  }
}
