import { UserInfo } from '@account/dtos/user-info.dto';
import { FileDto } from '@libs/common/file-dto';
import { ContactPerson } from './../../domains/corporates/contact-person';
import { Address } from '@libs/common/address';
import { ApiProperty } from '@nestjs/swagger';
import { Corporate } from '@customer/domains/corporates/corporate';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class CreateCorporateCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  tinNumber: string;
  @ApiProperty()
  contactPerson: ContactPerson;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  landmark: string;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  lng: number;
  logo: FileDto;
  currentUser: UserInfo;
  static fromCommand(command: CreateCorporateCommand): Corporate {
    const corporate = new Corporate();
    corporate.name = command.name;
    corporate.email = command.email;
    corporate.website = command.website;
    corporate.phoneNumber = command.phoneNumber;
    corporate.tinNumber = command.tinNumber;
    corporate.address = command.address;
    corporate.contactPerson = command.contactPerson;
    corporate.logo = command.logo;
    corporate.landmark = command.landmark;
    corporate.lat = command.lat;
    corporate.lng = command.lng;
    return corporate;
  }
}
export class UpdateCorporateCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  tinNumber: string;
  @ApiProperty()
  contactPerson: ContactPerson;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  landmark: string;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  lng: number;
  logo: FileDto;
  currentUser: UserInfo;
  static fromCommand(command: UpdateCorporateCommand): Corporate {
    const corporate = new Corporate();
    corporate.id = command.id;
    corporate.name = command.name;
    corporate.email = command.email;
    corporate.website = command.website;
    corporate.phoneNumber = command.phoneNumber;
    corporate.tinNumber = command.tinNumber;
    corporate.address = command.address;
    corporate.contactPerson = command.contactPerson;
    corporate.logo = command.logo;
    corporate.landmark = command.landmark;
    corporate.lat = command.lat;
    corporate.lng = command.lng;
    return corporate;
  }
}

export class ArchiveCorporateCommand {
  @ApiProperty()
  @IsNotEmpty()
  // @IsUUID()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
