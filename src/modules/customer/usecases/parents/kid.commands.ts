import { UserInfo } from '@account/dtos/user-info.dto';
import { Kid } from '@customer/domains/parents/kid';
import {
  Gender,
  KidStatus,
  KidTravelStatus,
  TransportationTime,
} from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';

export class KidCommand {
  @ApiProperty()
  @IsNotEmpty()
  schoolId: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  grade: string;
  @ApiProperty()
  @IsNotEmpty()
  age: number;
  @ApiProperty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: Gender;
  @ApiProperty()
  remark: string;
  @ApiProperty()
  // @IsNotEmpty()
  profileImage: FileDto;
  @ApiProperty()
  status: string;
  @ApiProperty()
  kidTravelStatus: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  transportationTime: string;
  @ApiProperty()
  startDate: Date;
}
export class CreateKidCommand {
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolId: string;
  @ApiProperty()
  schoolName: string;
  @ApiProperty()
  categoryName: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  grade: string;
  @ApiProperty()
  @IsNotEmpty()
  age: number;
  @ApiProperty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: Gender;
  @ApiProperty()
  remark: string;
  @ApiProperty()
  // @IsNotEmpty()
  profileImage: FileDto;
  @ApiProperty()
  distanceFromSchool: number;
  @ApiProperty()
  kidFee: number;
  @ApiProperty()
  status: string;
  @ApiProperty()
  kidTravelStatus: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  transportationTime: string;
  @ApiProperty()
  startDate: Date;
  currentUser: UserInfo;
  static fromCommand(command: CreateKidCommand): Kid {
    const kidDomain = new Kid();
    kidDomain.parentId = command.parentId;
    kidDomain.schoolId = command.schoolId;
    kidDomain.schoolName = command.schoolName;
    kidDomain.categoryName = command.categoryName;
    kidDomain.name = command.name;
    kidDomain.age = command.age;
    kidDomain.grade = command.grade;
    kidDomain.gender = command.gender;
    kidDomain.remark = command.remark;
    kidDomain.distanceFromSchool = command.distanceFromSchool;
    kidDomain.kidFee = command.kidFee;
    kidDomain.profileImage = command.profileImage;
    kidDomain.status = command.status;
    kidDomain.kidTravelStatus = command.kidTravelStatus;
    kidDomain.categoryId = command.categoryId;
    kidDomain.enabled = command.enabled;
    kidDomain.transportationTime = command.transportationTime;
    kidDomain.startDate = command.startDate;
    return kidDomain;
  }
}
export class CreateKidsCommand {
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  kids: KidCommand[];
  currentUser: UserInfo;
}
export class UpdateKidCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  schoolId: string;
  @ApiProperty()
  schoolName: string;
  @ApiProperty()
  categoryName: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  age: number;
  @ApiProperty()
  @IsNotEmpty()
  grade: string;
  @ApiProperty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: Gender;
  @ApiProperty()
  remark: string;
  @ApiProperty()
  // @IsNotEmpty()
  profileImage: FileDto;
  @ApiProperty()
  distanceFromSchool: number;
  @ApiProperty()
  kidFee: number;
  @ApiProperty()
  status: string;
  @ApiProperty()
  kidTravelStatus: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  transportationTime: string;
  @ApiProperty()
  startDate: Date;
  enabled: boolean;
  assigned: boolean;
  currentUser: UserInfo;
  static fromCommand(updateKid: UpdateKidCommand): Kid {
    const kid = new Kid();
    kid.id = updateKid.id;
    kid.parentId = updateKid.parentId;
    kid.schoolId = updateKid.schoolId;
    kid.schoolName = updateKid.schoolName;
    kid.categoryName = updateKid.categoryName;
    kid.name = updateKid.name;
    kid.age = updateKid.age;
    kid.grade = updateKid.grade;
    kid.gender = updateKid.gender;
    kid.remark = updateKid.remark;
    kid.profileImage = updateKid.profileImage;
    kid.distanceFromSchool = updateKid.distanceFromSchool;
    kid.kidFee = updateKid.kidFee;
    kid.status = updateKid.status;
    kid.kidTravelStatus = updateKid.kidTravelStatus;
    kid.categoryId = updateKid.categoryId;
    kid.enabled = updateKid.enabled;
    kid.startDate = updateKid.startDate;
    kid.transportationTime = updateKid.transportationTime;
    kid.assigned = updateKid.assigned;
    return kid;
  }
}
export class DeleteKidCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  currentUser: UserInfo;
}
export class ArchiveKidCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  parentId: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
