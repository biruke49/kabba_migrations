import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Query } from '@nestjs/common/decorators';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { KidResponse } from './kid.response';
import { GetNearestParentQuery } from './parent.commands';
import { ParentResponse } from './parent.response';
import { ParentStatus } from '@libs/common/enums';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';

@Injectable()
export class ParentQuery {
  constructor(
    @InjectRepository(ParentEntity)
    private parentRepository: Repository<ParentEntity>,
    @InjectRepository(KidEntity)
    private kidRepository: Repository<KidEntity>,
    private dataSource: DataSource,
    @InjectRepository(ParentPreferenceEntity)
    private parentPreferenceRepository: Repository<ParentPreferenceEntity>,
  ) {}
  async getParent(id: string, withDeleted = false): Promise<ParentResponse> {
    const parent = await this.parentRepository.find({
      where: { id: id },
      relations: ['kids', 'kids.groupAssignment'],
      withDeleted: withDeleted,
    });
    if (!parent[0]) {
      throw new NotFoundException(`Parent not found with id ${id}`);
    }
    return ParentResponse.fromEntity(parent[0]);
  }
  async getNearestParents(@Query() command: GetNearestParentQuery) {
    // lat: 8.99230076822041, lng: 38.7503269485228
    const parentIds = await this.dataSource.query(
      `SELECT id, latitude,longitude,float8 (point(${command.lat},${command.lng}) <@> point(latitude, longitude)) as distance 
      FROM parents 
      WHERE (float8 (point(9.003033,38.769472) <@> point(latitude, longitude)) < ${command.radius})  
      ORDER BY distance 
      OFFSET 0
      LIMIT ${command.top}`,
    );
    const arrayIds = [];
    parentIds.forEach((element) => {
      arrayIds.push(element.id);
    });
    if (arrayIds.length === 0) {
      return [];
    }
    const query = new CollectionQuery();
    query.filter = [
      [
        {
          field: 'id',
          operator: FilterOperators.In,
          value: arrayIds,
        },
      ],
    ];
    query.includes = ['kids'];
    const dataQuery = QueryConstructor.constructQuery<ParentEntity>(
      this.parentRepository,
      query,
    );
    const routes = await dataQuery.getMany();
    return routes.map((route) => ParentResponse.fromEntity(route));
  }
  async getParentsBySchoolId(schoolId: string) {
    // lat: 8.99230076822041, lng: 38.7503269485228
    const parentIds = await this.dataSource.query(
      `select * from parents where status = ${ParentStatus.WaitingToBeAssigned} and exists (select * from kids where parents.id = kids.parent_id and exists (select * from schools where kids.school_id = schools.id and schools.id = '${schoolId}' order by schools.created_at desc) order by kids.created_at desc) order by parents.created_at desc`,
    );
    return parentIds;
  }
  async getParents(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ParentResponse>> {
    const dataQuery = QueryConstructor.constructQuery<ParentEntity>(
      this.parentRepository,
      query,
    );
    const d = new DataResponseFormat<ParentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => ParentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedParents(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ParentResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<ParentEntity>(
      this.parentRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<ParentResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => ParentResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  //kids
  async getKids(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<KidResponse>> {
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    const d = new DataResponseFormat<KidResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => KidResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getKidsWithParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<KidResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'parentId',
        value: parentId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    const d = new DataResponseFormat<KidResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => KidResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getKidsWithSchoolId(
    schoolId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<KidResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'schoolId',
        value: schoolId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    const d = new DataResponseFormat<KidResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => KidResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async countKidsByCreatedMonth(
    query: CollectionQuery,
  ): Promise<CountByCreatedAtResponse[]> {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 1);
    const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    query.select = [];
    query.select.push(
      `to_char(kids.created_at,'MM') as created_date`,
      'COUNT(kids.id)',
    );
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'kids.created_at',
        value: [startOfYear.toISOString(), endOfYear.toISOString()],
        operator: FilterOperators.Between,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  // async countKidsWithSchoolId(
  //   id: string,
  //   withDeleted = false,
  // ): Promise<KidResponse> {
  //   const parent = await this.kidRepository.find({
  //     where: { schoolId: id },
  //     relations: [],
  //     withDeleted: withDeleted,
  //   });
  //   if (!parent[0]) {
  //     throw new NotFoundException(`Kid not found with id ${id}`);
  //   }
  //   return KidResponse.fromEntity(parent[0]);
  // }
  async getArchivedKidsWithParentId(
    parentId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<KidResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'parentId',
        value: parentId,
        operator: FilterOperators.EqualTo,
      },
      {
        field: 'deletedAt',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    const d = new DataResponseFormat<KidResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => KidResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getKid(id: string): Promise<KidResponse> {
    const kid = await this.kidRepository.find({
      where: { id: id },
      relations: ['school', 'parent', 'category'],
    });
    if (!kid[0]) {
      throw new NotFoundException(`Kid not found with id ${id}`);
    }
    return KidResponse.fromEntity(kid[0]);
  }
  async getArchivedKids(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<KidResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<KidResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => KidResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async groupParentsByGender(
    query: CollectionQuery,
  ): Promise<CountByGenderResponse[]> {
    query.select = [];
    query.select.push('gender', 'COUNT(parents.id)');
    const dataQuery = QueryConstructor.constructQuery<ParentEntity>(
      this.parentRepository,
      query,
    );
    dataQuery.groupBy('parents.gender');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByGenderResponse();
      countResponse.gender = d.parents_gender;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupParentsByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('is_active as isActive', 'COUNT(parents.id)');
    const dataQuery = QueryConstructor.constructQuery<ParentEntity>(
      this.parentRepository,
      query,
    );
    dataQuery.groupBy('parents.isActive');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.isactive ? 'Active' : 'InActive';
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupParentsByEnabled(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('enabled as enabled', 'COUNT(parents.id)');
    const dataQuery = QueryConstructor.constructQuery<ParentEntity>(
      this.parentRepository,
      query,
    );
    dataQuery.groupBy('parents.enabled');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.enabled ? 'Active' : 'Blocked';
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupParentsByAddress(
    field: string,
    query: CollectionQuery,
  ): Promise<GroupByAddressResponse[]> {
    query.select = [];
    query.select.push(
      `parents.address->>'${field}' as ${field}`,
      `COUNT(parents.id)`,
    );
    const dataQuery = QueryConstructor.constructQuery<ParentEntity>(
      this.parentRepository,
      query,
    );

    dataQuery.groupBy(`parents.address->>'${field}'`);
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByAddressResponse();
      countResponse[field] = d[field];
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupKidsByGender(
    query: CollectionQuery,
  ): Promise<CountByGenderResponse[]> {
    query.select = [];
    query.select.push('gender', 'COUNT(kids.id)');
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    dataQuery.groupBy('kids.gender');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByGenderResponse();
      countResponse.gender = d.kids_gender;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupKidsByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('enabled as enabled', 'COUNT(kids.id)');
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );
    dataQuery.groupBy('kids.enabled');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.enabled ? 'Active' : 'InActive';
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupKidsByTransportationTime(
    query: CollectionQuery,
  ): Promise<GroupByAddressResponse[]> {
    query.select = [];
    query.select.push(`kids.transportation_time`, `COUNT(kids.id)`);
    const dataQuery = QueryConstructor.constructQuery<KidEntity>(
      this.kidRepository,
      query,
    );

    dataQuery.groupBy(`kids.transportation_time`);
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse: any = {};
      countResponse.TransportationTime = d.transportation_time;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
}
