import { Parent } from '@customer/domains/parents/parent';
import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { KidResponse } from './kid.response';
import { ParentPreferenceResponse } from '@interaction/usecases/parent-preference/parent-preference.response';

export class ParentResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  enabled: boolean;
  // @ApiProperty()
  // isActive: boolean;
  @ApiProperty()
  status: string;
  @ApiProperty()
  lng: number;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  totalPayment: number;
  @ApiProperty()
  profileImage: FileDto;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty({ isArray: true, type: KidResponse })
  kids: KidResponse[];
  parentPreferences: ParentPreferenceResponse[];
  static fromEntity(parentEntity: ParentEntity): ParentResponse {
    const parentResponse = new ParentResponse();
    parentResponse.id = parentEntity.id;
    parentResponse.name = parentEntity.name;
    parentResponse.email = parentEntity.email;
    parentResponse.phoneNumber = parentEntity.phoneNumber;
    parentResponse.gender = parentEntity.gender;
    parentResponse.enabled = parentEntity.enabled;
    // parentResponse.isActive = parentEntity.isActive;
    parentResponse.status = parentEntity.status;
    parentResponse.address = parentEntity.address;
    parentResponse.lng = parentEntity.lng;
    parentResponse.lat = parentEntity.lat;
    parentResponse.totalPayment = parentEntity.totalPayment;
    parentResponse.profileImage = parentEntity.profileImage;
    parentResponse.archiveReason = parentEntity.archiveReason;
    parentResponse.createdBy = parentEntity.createdBy;
    parentResponse.updatedBy = parentEntity.updatedBy;
    parentResponse.deletedBy = parentEntity.deletedBy;
    parentResponse.createdAt = parentEntity.createdAt;
    parentResponse.updatedAt = parentEntity.updatedAt;
    parentResponse.deletedAt = parentEntity.deletedAt;
    if (parentEntity.kids) {
      parentResponse.kids = parentEntity.kids
        ? parentEntity.kids.map((kid) => {
            return KidResponse.fromEntity(kid);
          })
        : [];
    }
    if (parentEntity.parentPreferences) {
      parentResponse.parentPreferences = parentEntity.parentPreferences
        ? parentEntity.parentPreferences.map((parentPreference) => {
            return ParentPreferenceResponse.fromEntity(parentPreference);
          })
        : [];
    }

    return parentResponse;
  }
  static fromDomain(parent: Parent): ParentResponse {
    const parentResponse = new ParentResponse();
    parentResponse.id = parent.id;
    parentResponse.name = parent.name;
    parentResponse.email = parent.email;
    parentResponse.phoneNumber = parent.phoneNumber;
    parentResponse.gender = parent.gender;
    parentResponse.enabled = parent.enabled;
    // parentResponse.isActive = parent.isActive;
    parentResponse.status = parent.status;
    parentResponse.address = parent.address;
    parentResponse.lng = parent.lng;
    parentResponse.lat = parent.lat;
    parentResponse.totalPayment = parent.totalPayment;
    parentResponse.profileImage = parent.profileImage;
    parentResponse.archiveReason = parent.archiveReason;
    parentResponse.createdBy = parent.createdBy;
    parentResponse.updatedBy = parent.updatedBy;
    parentResponse.deletedBy = parent.deletedBy;
    parentResponse.createdAt = parent.createdAt;
    parentResponse.updatedAt = parent.updatedAt;
    parentResponse.deletedAt = parent.deletedAt;
    if (parent.kids) {
      parentResponse.kids = parent.kids
        ? parent.kids.map((kid) => {
            return KidResponse.fromDomain(kid);
          })
        : [];
    }
    if (parent.parentPreferences) {
      parentResponse.parentPreferences = parent.parentPreferences
        ? parent.parentPreferences.map((parentPreference) => {
            return ParentPreferenceResponse.fromDomain(parentPreference);
          })
        : [];
    }

    return parentResponse;
  }
}
