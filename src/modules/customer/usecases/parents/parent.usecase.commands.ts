import { CreateAccountCommand } from '@account/usecases/accounts/account.commands';
import { AccountCommands } from '@account/usecases/accounts/account.usecase.commands';
import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import {
  CalculateGroupAssignmentPaymentCommand,
  CreateGroupAssignmentsCommand,
} from '@assignment/usecases/groups/group-assignment.commands';
import { CreateGroupCommand } from '@assignment/usecases/groups/group.commands';
import { GroupQuery } from '@assignment/usecases/groups/group.usecase.queries';
import { UserInfo } from '@account/dtos/user-info.dto';
import { CategoryQuery } from '@classification/usecases/categories/category.usecase.queries';
import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import {
  CredentialType,
  GroupAssignmentStatus,
  KidTravelStatus,
  ParentStatus,
  PaymentMethod,
  PaymentStatus,
} from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import axios from 'axios';

import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { Util } from '@libs/common/util';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { createGrpcMethodMetadata } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  ArchiveKidCommand,
  CreateKidCommand,
  DeleteKidCommand,
  UpdateKidCommand,
} from './kid.commands';
import { KidResponse } from './kid.response';
import {
  ArchiveParentCommand,
  CreateParentCommand,
  ParentStatusCommand,
  RegisterParentCommand,
  UpdateParentCommand,
  UpdateParentPaymentCommand,
} from './parent.commands';
import { ParentResponse } from './parent.response';
import { SchoolQuery } from '../schools/school.usecase.queries';
import { CategoryRepository } from '@classification/persistence/categories/category.repository';
import { ParentQuery } from './parent.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { GroupRepository } from '@assignment/persistence/groups/group.repository';
import { ConfigurationQuery } from '@configurations/usecases/configuration/configuration.usecase.queries';
import { GroupCommands } from '@assignment/usecases/groups/group.usecase.commands';
import { AppService } from 'app.service';
import { Kid } from '@customer/domains/parents/kid';
import {
  CreatePaymentCommand,
  PayWithChapaCommand,
} from '@assignment/usecases/payments/payment.commands';

@Injectable()
export class ParentCommands {
  private apiKey = process.env.GOOGLE_DISTANCE_API;
  constructor(
    private readonly accountCommands: AccountCommands,
    private schoolQuery: SchoolQuery,
    private parentQuery: ParentQuery,
    private parentRepository: ParentRepository,
    private categoryRepository: CategoryRepository,
    private groupRepository: GroupRepository,
    @InjectRepository(KidEntity)
    private kidRepository: Repository<KidEntity>,
    private eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
    private configQuery: ConfigurationQuery,
    private groupQuery: GroupQuery,
    private appService: AppService,
  ) {}
  async isParentExist(phoneNumber: string) {
    const parent = await this.parentRepository.getByPhoneNumber(phoneNumber);
    return parent ? true : false;
  }
  async createParent(command: CreateParentCommand): Promise<ParentResponse> {
    if (
      command.phoneNumber &&
      (await this.parentRepository.getByPhoneNumber(command.phoneNumber, true))
    ) {
      throw new BadRequestException(
        `Parent already exist with this phone number`,
      );
    }
    if (
      command.email &&
      (await this.parentRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Parent already exist with this email Address`,
      );
    }
    const parentDomain = CreateParentCommand.fromCommand(command);
    const parent = await this.parentRepository.insert(parentDomain);
    if (parent) {
      const password = Util.generatePassword(6);
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = parent.id;
      createAccountCommand.type = CredentialType.Parent;
      createAccountCommand.role = ['parent'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(password);
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      this.eventEmitter.emit('activity-logger.store', {
        modelId: parent.id,
        modelName: MODELS.PARENT,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return ParentResponse.fromDomain(parent);
  }
  async registerParent(
    command: RegisterParentCommand,
  ): Promise<ParentResponse> {
    if (
      await this.parentRepository.getByPhoneNumber(command.phoneNumber, true)
    ) {
      throw new BadRequestException(
        `Customer already exist with this phone number`,
      );
    }
    if (
      command.email &&
      (await this.parentRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Customer already exist with this email Address`,
      );
    }
    const parentDomain = RegisterParentCommand.fromCommand(command);
    const parent = await this.parentRepository.insert(parentDomain);
    if (parent) {
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = parent.id;
      createAccountCommand.password = Util.hashPassword(command.password);
      createAccountCommand.type = CredentialType.Parent;
      createAccountCommand.role = ['parent'];
      createAccountCommand.isActive = true;
      createAccountCommand.address = command.address;
      createAccountCommand.gender = command.gender;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
    }
    return ParentResponse.fromDomain(parent);
  }
  @OnEvent('update.parent.payment')
  async updateParentTotalPayment(id: string, amount: number): Promise<boolean> {
    const parentDomain = await this.parentRepository.getById(id);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${id}`);
    }
    parentDomain.totalPayment = amount;
    const parent = await this.parentRepository.update(parentDomain);
    if (!parent) {
      return false;
    }
    return true;
  }
  async updateParent(command: UpdateParentCommand): Promise<ParentResponse> {
    const parentDomain = await this.parentRepository.getById(command.id);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${command.id}`);
    }
    if (
      parentDomain.phoneNumber !== command.phoneNumber &&
      (await this.parentRepository.getByPhoneNumber(command.phoneNumber, true))
    ) {
      throw new BadRequestException(
        `Parent already exist with this phone number`,
      );
    }
    if (
      command.email &&
      parentDomain.email !== command.email &&
      (await this.parentRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Parent already exist with this email Address`,
      );
    }
    const oldPayload = { ...parentDomain };
    parentDomain.email = command.email;
    parentDomain.name = command.name;
    parentDomain.address = command.address;
    parentDomain.phoneNumber = command.phoneNumber;
    parentDomain.gender = command.gender;
    parentDomain.address = command.address;
    parentDomain.status = command.status;
    if (parentDomain.lat !== command.lat || parentDomain.lng !== command.lng) {
      const kids = await this.parentQuery.getKidsWithParentId(
        command.id,
        new CollectionQuery(),
      );
      for (const kidd of kids.data) {
        const existingKidDomain = await this.parentRepository.getByKidId(
          kidd.id,
        );
        const school = await this.schoolQuery.getSchool(
          existingKidDomain.schoolId,
        );
        const origin = `${command.lat},${command.lng}`;
        const destination = `${school.lat},${school.lng}`;
        // const distance = await this.getDistance(origin, destination);
        const distance = await this.appService.getDistance(origin, destination);
        existingKidDomain.distanceFromSchool = distance / 1000;
        parentDomain.updateKid(existingKidDomain);
      }
    }
    parentDomain.lat = command.lat;
    parentDomain.lng = command.lng;
    parentDomain.totalPayment = command.totalPayment;
    const parent = await this.parentRepository.update(parentDomain);
    if (parent) {
      this.eventEmitter.emit('update.account', {
        accountId: parent.id,
        name: parent.name,
        email: parent.email,
        type: CredentialType.Parent,
        phoneNumber: parent.phoneNumber,
        gender: parent.gender,
        profileImage: parent.profileImage,
        address: parent.address,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: parent.id,
        modelName: MODELS.PARENT,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: { ...oldPayload },
        payload: { ...parent },
      });
    }
    return ParentResponse.fromDomain(parent);
  }
  async archiveParent(
    archiveCommand: ArchiveParentCommand,
  ): Promise<ParentResponse> {
    const parentDomain = await this.parentRepository.getById(archiveCommand.id);
    if (!parentDomain) {
      throw new NotFoundException(
        `Parent not found with id ${archiveCommand.id}`,
      );
    }
    parentDomain.deletedAt = new Date();
    parentDomain.deletedBy = archiveCommand.currentUser.id;
    parentDomain.archiveReason = archiveCommand.reason;
    const result = await this.parentRepository.update(parentDomain);
    if (result) {
      const kids = await this.parentRepository.getAllKidsByParentId(
        archiveCommand.id,
        false,
      );
      if (kids && kids.length > 0) {
        for (const kid of kids) {
          kid.deletedAt = new Date();
          kid.deletedBy = archiveCommand.currentUser.id;
          kid.archiveReason = archiveCommand.reason;
          parentDomain.updateKid(kid);
          const result = await this.parentRepository.update(parentDomain);
        }
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: archiveCommand.id,
        modelName: MODELS.PARENT,
        action: ACTIONS.ARCHIVE,
        userId: archiveCommand.currentUser.id,
        user: archiveCommand.currentUser,
      });
      this.eventEmitter.emit('account.archived', {
        phoneNumber: parentDomain.phoneNumber,
        id: parentDomain.id,
      });
    }
    return ParentResponse.fromDomain(result);
  }
  async restoreParent(
    id: string,
    currentUser: UserInfo,
  ): Promise<ParentResponse> {
    const parentDomain = await this.parentRepository.getById(id, true);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${id}`);
    }
    parentDomain.archiveReason = null;
    parentDomain.deletedBy = null;
    parentDomain.deletedAt = null;
    const parent = await this.parentRepository.update(parentDomain);
    const r = await this.parentRepository.restore(id);
    if (r) {
      const kids = await this.parentRepository.getAllKidsByParentId(id, true);
      if (kids && kids.length > 0) {
        for (const kid of kids) {
          kid.deletedAt = null;
          kid.deletedBy = null;
          kid.archiveReason = null;
          parentDomain.updateKid(kid);
          const result = await this.parentRepository.update(parentDomain);
        }
      }
      this.eventEmitter.emit('account.restored', {
        phoneNumber: parentDomain.phoneNumber,
        id: parentDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PARENT,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return ParentResponse.fromDomain(parentDomain);
  }
  async deleteParent(id: string, currentUser: UserInfo): Promise<boolean> {
    const parentDomain = await this.parentRepository.getById(id, true);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${id}`);
    }

    const result = await this.parentRepository.delete(id);
    if (result) {
      if (parentDomain.profileImage) {
        await this.fileManagerService.removeFile(
          parentDomain.profileImage,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      this.eventEmitter.emit('account.deleted', {
        phoneNumber: parentDomain.phoneNumber,
        id: parentDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PARENT,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
  async activateOrBlockParent(
    id: string,
    currentUser: UserInfo,
  ): Promise<ParentResponse> {
    const parentDomain = await this.parentRepository.getById(id);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${id}`);
    }
    parentDomain.enabled = !parentDomain.enabled;
    const result = await this.parentRepository.update(parentDomain);
    if (result) {
      this.eventEmitter.emit('account.activate-or-block', {
        phoneNumber: parentDomain.phoneNumber,
        id: parentDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PARENT,
        action: parentDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return ParentResponse.fromDomain(result);
  }
  async updateParentProfileImage(id: string, fileDto: FileResponseDto) {
    const parentDomain = await this.parentRepository.getById(id);
    if (!parentDomain) {
      throw new NotFoundException(`Kid not found with id ${id}`);
    }
    if (parentDomain.profileImage && fileDto) {
      await this.fileManagerService.removeFile(
        parentDomain.profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    parentDomain.profileImage = fileDto as FileDto;
    const result = await this.parentRepository.update(parentDomain);
    if (result) {
      this.eventEmitter.emit('update-account-profile', {
        id: result.id,
        profileImage: result.profileImage,
      });
    }
    return ParentResponse.fromDomain(result);
  }

  //Kids
  async addKid(command: CreateKidCommand): Promise<KidResponse> {
    const schoolId = command.schoolId;
    const parentDomain = await this.parentRepository.getById(command.parentId);
    const schoolDomain = await this.schoolQuery.getSchool(schoolId);
    if (!parentDomain) {
      throw new NotFoundException(
        `Parent not found with id ${command.parentId}`,
      );
    }
    const existingKid = await this.kidRepository.findOneBy({
      name: command.name,
      parentId: command.parentId,
    });
    if (existingKid) {
      throw new BadRequestException(
        `Kid already exist with name ${command.name}`,
      );
    }
    const category = await this.categoryRepository.getById(command.categoryId);
    if (command.age <= 6) {
      if (category.name !== 'Auto' && category.name !== 'Automobile') {
        throw new BadRequestException(
          `Kids younger than six must be assigned to an Auto vehicle.`,
        );
      }
    }
    if (
      command.kidTravelStatus === KidTravelStatus.Alone ||
      command.kidTravelStatus === KidTravelStatus.WithSiblings
    ) {
      if (category.name === 'Minibus' || category.name === 'minibus') {
        throw new BadRequestException(
          `Kids can not travel alone or with siblings in a minibus`,
        );
      }
    }
    //Distance Between Kid And School In KMs
    const origin = `${parentDomain.lat},${parentDomain.lng}`;
    const destination = `${schoolDomain.lat},${schoolDomain.lng}`;
    const dist = await this.appService.getDistance(origin, destination);
    const newDistance = dist / 1000;

    command.distanceFromSchool = Number(newDistance.toFixed(2));
    command.categoryName = category.name;
    const parentGroupAssignments = await this.groupQuery.getAssignedByParentId(
      command.parentId,
      new CollectionQuery(),
    );
    console.log(
      '🚀 ~ file: parent.usecase.commands.ts:445 ~ ParentCommands ~ addKid ~ parentGroupAssignments:',
      parentGroupAssignments,
    );
    if (parentGroupAssignments.data && parentGroupAssignments.data.length > 0) {
      parentDomain.status = ParentStatus.PartiallyAssigned;
    }
    let kidResponse = new KidResponse();
    const newKid = CreateKidCommand.fromCommand(command);
    parentDomain.addKid(newKid);
    const result = await this.parentRepository.update(parentDomain);
    if (!result) return null;
    kidResponse = KidResponse.fromDomain(result.kids[result.kids.length - 1]);
    const kidInfo = result.kids[result.kids.length - 1];

    const kid = await this.parentQuery.getKid(kidInfo.id);
    const parentId = kid.parentId;
    const distance = kid.distanceFromSchool;
    const initialFee = kid.category.kidInitialFee;
    const config = await this.configQuery.getConfigurations(
      new CollectionQuery(),
    );
    const numOfDays = config.globalConfigurations.kidsNumberOfSchoolDays;
    const perKiloMeterCost = kid.category.kidsPerKilometerCost;
    const roadDifficultyCost = 0;
    const capacity = kid.category.capacity;

    //Calculate Amount
    const paymentCalculationCommand =
      new CalculateGroupAssignmentPaymentCommand();
    paymentCalculationCommand.capacity = capacity;
    paymentCalculationCommand.roadDifficultyCost = roadDifficultyCost;
    paymentCalculationCommand.perKiloMeterCost = perKiloMeterCost;
    paymentCalculationCommand.numOfDays = numOfDays;
    paymentCalculationCommand.transportationTime = kid.transportationTime;
    paymentCalculationCommand.kidTravelStatus = kid.kidTravelStatus;
    paymentCalculationCommand.initialFee = initialFee;
    paymentCalculationCommand.distance = distance > 8 ? distance : 8;
    //Calculated Amount
    const paymentAmount = await this.appService.calculateGroupAssignmentAmount(
      paymentCalculationCommand,
    );

    if (command.kidTravelStatus !== KidTravelStatus.WithOthers) {
      if (command.kidTravelStatus === KidTravelStatus.WithSiblings) {
        const gac =
          await this.groupQuery.getGroupAssignmentByParentCategorySchoolId(
            command.parentId,
            command.categoryId,
            command.schoolId,
            command.transportationTime,
            new CollectionQuery(),
          );
        if (gac != null && gac.group.availableSeats > 0) {
          kidResponse = await this.updateKidFee(kid.id, 0);
        } else {
          kidResponse = await this.updateKidFee(kid.id, paymentAmount);
        }
      } else {
        kidResponse = await this.updateKidFee(kid.id, paymentAmount);
      }
      //Fire event to create Group
      this.eventEmitter.emit('create.kid.group', {
        kidId: kidInfo.id,
        parentId: command.parentId,
        categoryId: command.categoryId,
        schoolId: command.schoolId,
        schoolName: command.schoolName,
        kidTravelStatus: command.kidTravelStatus,
        transportationTime: command.transportationTime,
        categoryCapacity: category.capacity,
        currentUser: command.currentUser,
      });
    } else {
      kidResponse = await this.updateKidFee(kid.id, paymentAmount);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: kidResponse.id,
      modelName: MODELS.KID,
      action: ACTIONS.CREATE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return kidResponse;
  }
  async changeParentStatus(
    command: ParentStatusCommand,
  ): Promise<ParentResponse> {
    const parentDomain = await this.parentRepository.getById(command.id);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${command.id}`);
    }
    parentDomain.status = command.status;
    if (
      parentDomain.status !== ParentStatus.WaitingToBeAssigned &&
      command.status === ParentStatus.WaitingToBeAssigned
    ) {
      const paymentCommand = new PayWithChapaCommand();
      const parent = await this.parentRepository.getById(parentDomain.id);
      paymentCommand.parentId = parent.id;
      paymentCommand.depositedBy = {
        id: parent.id,
        name: parent.name,
        email: parent.email,
        phoneNumber: parent.phoneNumber,
      };
      paymentCommand.status = PaymentStatus.Pending;
      paymentCommand.amount = parentDomain.totalPayment;
      paymentCommand.method = PaymentMethod.Manual;
      paymentCommand.reason = 'Pay for kids manually';
      this.eventEmitter.emit('create.payment', paymentCommand);
    }
    const result = await this.parentRepository.update(parentDomain);
    if (result) {
      if (command.currentUser) {
        this.eventEmitter.emit('activity-logger.store', {
          modelId: command.id,
          modelName: MODELS.PARENT,
          action: ACTIONS.UPDATE,
          userId: command.currentUser.id,
          user: command.currentUser,
        });
      }
    }
    return ParentResponse.fromDomain(result);
  }
  @OnEvent('update.parent.status')
  async changeParentStatusOnly(
    command: ParentStatusCommand,
  ): Promise<ParentResponse> {
    const parentDomain = await this.parentRepository.getById(command.id);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${command.id}`);
    }
    parentDomain.status = command.status;
    const result = await this.parentRepository.update(parentDomain);
    if (result) {
      if (command.currentUser) {
        this.eventEmitter.emit('activity-logger.store', {
          modelId: command.id,
          modelName: MODELS.PARENT,
          action: ACTIONS.UPDATE,
          userId: command.currentUser.id,
          user: command.currentUser,
        });
      }
    }
    return ParentResponse.fromDomain(result);
  }
  @OnEvent('update-total-parent-payment')
  async updateTotalParentPayment(
    command: UpdateParentPaymentCommand,
  ): Promise<void> {
    const parentDomain = await this.parentRepository.getById(command.id);
    if (!parentDomain) {
      throw new NotFoundException(`Parent not found with id ${command.id}`);
    }
    parentDomain.totalPayment = command.amount;
    const result = await this.parentRepository.update(parentDomain);
  }
  // async getDistance(origin: string, destination: string): Promise<number> {
  //   const apiKey = this.apiKey;
  //   const apiUrl = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${destination}&key=${apiKey}`;

  //   try {
  //     const response = await axios.get(apiUrl);
  //     const distance = response.data.rows[0].elements[0].distance.value;
  //     return distance;
  //   } catch (error) {
  //     throw new Error('Failed to get distance from API');
  //   }
  // }
  async updateKid(command: UpdateKidCommand): Promise<KidResponse> {
    const parentDomain = await this.parentRepository.getById(command.parentId);
    const schoolDomain = await this.schoolQuery.getSchool(command.schoolId);

    if (!parentDomain) {
      throw new NotFoundException(
        `Parent not found with id ${command.parentId}`,
      );
    }
    const existingKid = await this.kidRepository.findOneBy({
      name: command.name,
      parentId: command.parentId,
    });
    if (existingKid && existingKid.id !== command.id) {
      throw new BadRequestException(
        `Kid already exist with name${command.name}`,
      );
    }
    const existingKidDomain = await this.parentRepository.getByKidId(
      command.id,
    );
    const category = await this.categoryRepository.getById(command.categoryId);
    if (command.age <= 6) {
      if (category.name !== 'Auto' && category.name !== 'Automobile') {
        throw new BadRequestException(
          `Kids younger than six must be assigned to an Auto vehicle.`,
        );
      }
    }
    if (
      command.kidTravelStatus === KidTravelStatus.Alone ||
      command.kidTravelStatus === KidTravelStatus.WithSiblings
    ) {
      if (category.name === 'Minibus' || category.name === 'minibus') {
        throw new BadRequestException(
          `Kids can not travel alone or with siblings in a minibus`,
        );
      }
    }
    const groupAss = await this.groupRepository.getByKidId(command.id);
    if (
      groupAss &&
      groupAss.status === GroupAssignmentStatus.Active &&
      (command.kidTravelStatus !== existingKidDomain.kidTravelStatus ||
        command.categoryId !== existingKidDomain.categoryId ||
        command.schoolId !== existingKidDomain.schoolId ||
        command.transportationTime !== existingKidDomain.transportationTime)
    ) {
      throw new BadRequestException(
        'You can only update name, age, grade and gender of kids after payment',
      );
    }
    //Distance Between Kid And School In KMs
    const origin = `${parentDomain.lat},${parentDomain.lng}`;
    const destination = `${schoolDomain.lat},${schoolDomain.lng}`;
    const dist = await this.appService.getDistance(origin, destination);
    const newDistance = dist / 1000;

    command.distanceFromSchool = Number(newDistance.toFixed(2));
    command.categoryName = category.name;
    let res = new KidResponse();
    const newKid = UpdateKidCommand.fromCommand(command);
    parentDomain.updateKid(newKid);
    const result = await this.parentRepository.update(parentDomain);
    if (!result) return null;
    res = KidResponse.fromDomain(
      result.kids.find((kid) => kid.id === command.id),
    );

    const kid = await this.parentQuery.getKid(command.id);
    const parentId = kid.parentId;
    const distance = kid.distanceFromSchool;
    const initialFee = kid.category.kidInitialFee;
    const config = await this.configQuery.getConfigurations(
      new CollectionQuery(),
    );
    const numOfDays = config.globalConfigurations.kidsNumberOfSchoolDays;
    const perKiloMeterCost = kid.category.kidsPerKilometerCost;
    const roadDifficultyCost = 0;
    const capacity = kid.category.capacity;

    //Calculate Amount
    const paymentCalculationCommand =
      new CalculateGroupAssignmentPaymentCommand();
    paymentCalculationCommand.capacity = capacity;
    paymentCalculationCommand.roadDifficultyCost = roadDifficultyCost;
    paymentCalculationCommand.perKiloMeterCost = perKiloMeterCost;
    paymentCalculationCommand.numOfDays = numOfDays;
    paymentCalculationCommand.transportationTime = kid.transportationTime;
    paymentCalculationCommand.kidTravelStatus = kid.kidTravelStatus;
    paymentCalculationCommand.initialFee = initialFee;
    paymentCalculationCommand.distance = distance > 8 ? distance : 8;
    //Calculated Amount
    const paymentAmount = await this.appService.calculateGroupAssignmentAmount(
      paymentCalculationCommand,
    );
    res = await this.updateKidFee(kid.id, paymentAmount);
    if (command.kidTravelStatus !== existingKidDomain.kidTravelStatus) {
      const category = await this.categoryRepository.getById(
        command.categoryId,
      );
      const groupAss = await this.groupRepository.getByKidId(command.id);
      if (
        command.kidTravelStatus !== KidTravelStatus.WithOthers &&
        existingKidDomain.kidTravelStatus === KidTravelStatus.WithOthers
      ) {
        if (groupAss) {
          //Fire event to create Group
          this.eventEmitter.emit('delete.kid.group', {
            id: command.id,
            currentUser: command.currentUser,
          });
        }
        //Fire event to create Group
        this.eventEmitter.emit('create.kid.group', {
          kidId: command.id,
          parentId: command.parentId,
          categoryId: command.categoryId,
          schoolId: command.schoolId,
          schoolName: command.schoolName,
          kidTravelStatus: command.kidTravelStatus,
          transportationTime: command.transportationTime,
          categoryCapacity: category.capacity,
          currentUser: command.currentUser,
        });
      } else if (
        command.kidTravelStatus === KidTravelStatus.WithOthers &&
        existingKidDomain.kidTravelStatus !== KidTravelStatus.WithOthers
      ) {
        if (groupAss) {
          const groupAssi = await this.groupRepository.getByGroupIdForUpdate(
            groupAss.groupId,
          );
          if (groupAssi) {
            await this.updateKidFee(groupAss.kidId, paymentAmount);
          }
          //Fire event to delete Group
          this.eventEmitter.emit('delete.kid.group', {
            id: command.id,
            currentUser: command.currentUser,
          });
        }
      } else if (
        (command.kidTravelStatus === KidTravelStatus.Alone &&
          existingKidDomain.kidTravelStatus === KidTravelStatus.WithSiblings) ||
        (command.kidTravelStatus === KidTravelStatus.WithSiblings &&
          existingKidDomain.kidTravelStatus === KidTravelStatus.Alone)
      ) {
        const category = await this.categoryRepository.getById(
          command.categoryId,
        );
        if (groupAss) {
          if (
            command.kidTravelStatus !== KidTravelStatus.WithSiblings &&
            existingKidDomain.kidFee > 0
          ) {
            const groupAssi = await this.groupRepository.getByGroupIdForUpdate(
              groupAss.groupId,
            );
            if (groupAssi) {
              await this.updateKidFee(groupAss.kidId, paymentAmount);
              this.eventEmitter.emit('update.group.assignment.amount', {
                id: groupAss.id,
                groupId: groupAss.groupId,
                amount: paymentAmount,
              });
            }
          }
          //Fire event to delete or update group
          this.eventEmitter.emit('delete.kid.group', {
            id: command.id,
            currentUser: command.currentUser,
          });
          //Fire event to create Group
          this.eventEmitter.emit('create.kid.group', {
            kidId: command.id,
            parentId: command.parentId,
            categoryId: command.categoryId,
            schoolId: command.schoolId,
            schoolName: command.schoolName,
            kidTravelStatus: command.kidTravelStatus,
            transportationTime: command.transportationTime,
            categoryCapacity: category.capacity,
            currentUser: command.currentUser,
          });
        }
      }
    }
    if (
      command.categoryId !== existingKidDomain.categoryId ||
      command.schoolId !== existingKidDomain.schoolId ||
      command.transportationTime !== existingKidDomain.transportationTime
    ) {
      // res = await this.updateKidFee(kid.id, paymentAmount);
      const category = await this.categoryRepository.getById(
        command.categoryId,
      );
      const groupAss = await this.groupRepository.getByKidId(command.id);
      if (groupAss) {
        //Fire event to delete or update group
        this.eventEmitter.emit('delete.kid.group', {
          id: command.id,
          currentUser: command.currentUser,
        });
        //Fire event to create Group
        this.eventEmitter.emit('create.kid.group', {
          kidId: command.id,
          parentId: command.parentId,
          categoryId: command.categoryId,
          schoolId: command.schoolId,
          schoolName: command.schoolName,
          kidTravelStatus: command.kidTravelStatus,
          transportationTime: command.transportationTime,
          categoryCapacity: category.capacity,
          currentUser: command.currentUser,
        });
      }
    }
    if (
      command.kidTravelStatus !== existingKidDomain.kidTravelStatus &&
      command.kidTravelStatus === KidTravelStatus.WithSiblings
    ) {
      const gac =
        await this.groupQuery.getGroupAssignmentByParentCategorySchoolId(
          command.parentId,
          command.categoryId,
          command.schoolId,
          command.transportationTime,
          new CollectionQuery(),
        );
      if (gac != null && gac.group.availableSeats > 0) {
        res = await this.updateKidFee(kid.id, 0);
      }
    }
    // const response = KidResponse.fromDomain(res);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.KID,
      action: ACTIONS.UPDATE,
      userId: command.currentUser.id,
      user: command.currentUser,
      payload: kid,
      existingKidDomain,
    });
    return res;
  }
  async deleteKid(command: DeleteKidCommand): Promise<boolean> {
    const kid = await this.kidRepository.find({
      where: { id: command.id, parentId: command.parentId },
      relations: ['groupAssignment'],
      withDeleted: true,
    });
    if (!kid[0]) {
      throw new NotFoundException(
        `Kid not found with id ${command.id} and parent id ${command.parentId}`,
      );
    }
    if (
      kid[0].groupAssignment &&
      kid[0].groupAssignment.status === GroupAssignmentStatus.Active
    ) {
      throw new BadRequestException(
        `Kid has already paid and can not be deleted`,
      );
    }
    if (
      kid[0].kidTravelStatus === KidTravelStatus.WithSiblings &&
      kid[0].kidFee > 0
    ) {
      const paymentAmount = kid[0].kidFee;
      const groupAssi = await this.groupRepository.getByGroupIdForUpdate(
        kid[0].groupAssignment.groupId,
      );
      if (groupAssi) {
        await this.updateKidFee(groupAssi.kidId, paymentAmount);
        this.eventEmitter.emit('update.group.assignment.amount', {
          id: groupAssi.id,
          groupId: groupAssi.groupId,
          amount: paymentAmount,
        });
      }
    }
    const result = await this.kidRepository.delete({ id: command.id });
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.KID,
        action: ACTIONS.DELETE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  async archiveKid(command: ArchiveKidCommand): Promise<boolean> {
    const kid = await this.kidRepository.find({
      where: { id: command.id, parentId: command.parentId },
      relations: ['groupAssignment'],
      withDeleted: true,
    });
    if (!kid[0]) {
      throw new NotFoundException(
        `Kid not found with id ${command.id} and parent id ${command.parentId}`,
      );
    }

    if (
      kid[0].groupAssignment &&
      kid[0].groupAssignment.status === GroupAssignmentStatus.Active
    ) {
      throw new BadRequestException(
        `Kid has already paid and can not be deleted`,
      );
    }
    kid[0].deletedAt = new Date();
    kid[0].deletedBy = command.currentUser.id;
    kid[0].archiveReason = command.reason;
    const result = await this.kidRepository.save(kid[0]);

    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.KID,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  async activateOrBlockKid(
    id: string,
    currentUser: UserInfo,
  ): Promise<KidResponse> {
    const kid = await this.kidRepository.find({
      where: { id: id },
      withDeleted: true,
    });
    if (!kid[0]) {
      throw new NotFoundException(`Kid not found with id ${id}`);
    }
    kid[0].enabled = !kid[0].enabled;
    const result = await this.kidRepository.save(kid[0]);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.KID,
        action: ACTIONS.UPDATE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return KidResponse.fromEntity(result);
  }
  @OnEvent('update.kid.fee')
  async updateKidFee(id: string, amount: number): Promise<KidResponse> {
    const kid = await this.kidRepository.find({
      where: { id: id },
      withDeleted: true,
    });
    if (!kid[0]) {
      throw new NotFoundException(`Kid not found with id ${id}`);
    }
    kid[0].kidFee = amount;
    const result = await this.kidRepository.save(kid[0]);
    return KidResponse.fromEntity(result);
  }
  async assignOrUnassignKid(
    id: string,
    status: boolean,
    currentUser: UserInfo,
  ): Promise<KidResponse> {
    const kid = await this.kidRepository.find({
      where: { id: id },
      withDeleted: true,
    });
    if (!kid[0]) {
      throw new NotFoundException(`Kid not found with id ${id}`);
    }
    kid[0].assigned = status;
    const result = await this.kidRepository.save(kid[0]);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.KID,
        action: ACTIONS.ASSIGNKID,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return KidResponse.fromEntity(result);
  }
  async restoreKid(command: DeleteKidCommand): Promise<KidResponse> {
    const kid = await this.kidRepository.find({
      where: { id: command.id },
      withDeleted: true,
    });
    if (!kid[0]) {
      throw new NotFoundException(`Kid not found with id ${command.id}`);
    }
    kid[0].deletedAt = null;
    const result = await this.kidRepository.save(kid[0]);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.KID,
      action: ACTIONS.RESTORE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });

    return KidResponse.fromEntity(result);
  }
  async updateKidProfileImage(id: string, fileDto: FileResponseDto) {
    const kidDomain = await this.kidRepository.find({
      where: { id: id },
      withDeleted: true,
    });
    if (!kidDomain) {
      throw new NotFoundException(`Kid not found with id ${id}`);
    }
    if (kidDomain[0].profileImage && fileDto) {
      await this.fileManagerService.removeFile(
        kidDomain[0].profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    kidDomain[0].profileImage = fileDto as FileDto;
    const result = await this.kidRepository.save(kidDomain[0]);

    return KidResponse.fromEntity(result);
  }
}
