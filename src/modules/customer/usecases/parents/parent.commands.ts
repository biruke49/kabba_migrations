import { UserInfo } from '@account/dtos/user-info.dto';
import { Parent } from '@customer/domains/parents/parent';
import { Address } from '@libs/common/address';
import { Gender, ParentStatus } from '@libs/common/enums';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
} from 'class-validator';

export class ParentAddressCommand {
  @ApiProperty()
  @IsNotEmpty()
  country: string;
  @ApiProperty()
  @IsNotEmpty()
  city: string;
  @ApiProperty()
  @IsNotEmpty()
  subCity: string;
  @ApiProperty()
  @IsNotEmpty()
  woreda: string;
  @ApiProperty()
  houseNumber: string;
}
export class CreateParentCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  email: string;
  @ApiProperty({
    example: '+251911111111',
  })
  phoneNumber: string;
  @ApiProperty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: Gender;
  @ApiProperty({
    example: {
      country: 'Ethiopia',
      city: 'Addis Ababa',
      subCity: 'Kolfe',
      woreda: '01',
      houseNumber: '1000002',
    },
  })
  @IsNotEmpty()
  address: ParentAddressCommand;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  @IsNotEmpty()
  lng: number;
  @ApiProperty()
  @IsNotEmpty()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  status: string;
  currentUser: UserInfo;
  static fromCommand(command: CreateParentCommand): Parent {
    const parentDomain = new Parent();
    parentDomain.name = command.name;
    parentDomain.email = command.email;
    parentDomain.phoneNumber = command.phoneNumber;
    parentDomain.gender = command.gender;
    parentDomain.address = { ...command.address };
    parentDomain.enabled = command.enabled;
    parentDomain.status = command.status;
    parentDomain.lng = command.lng;
    parentDomain.lat = command.lat;
    return parentDomain;
  }
}
export class RegisterParentCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  password: string;
  @ApiProperty()
  //@IsNotEmpty()
  gender: Gender;
  @ApiProperty({
    example: {
      country: 'Ethiopia',
      city: 'Addis Ababa',
      subCity: 'Kolfe',
      woreda: '01',
      houseNumber: '1000002',
    },
  })
  address: Address;
  @ApiProperty()
  @IsNotEmpty()
  lng: number;
  @ApiProperty()
  @IsNotEmpty()
  lat: number;
  static fromCommand(command: RegisterParentCommand): Parent {
    const passenger = new Parent();
    passenger.name = command.name;
    passenger.email = command.email;
    passenger.phoneNumber = command.phoneNumber;
    passenger.gender = command.gender;
    passenger.address = command.address;
    passenger.lat = command.lat;
    passenger.lng = command.lng;
    return passenger;
  }
}
export class UpdateParentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  email: string;
  @ApiProperty({
    example: '+251911111111',
  })
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: Gender;
  @ApiProperty({
    example: {
      country: 'Ethiopia',
      city: 'Addis Ababa',
      subCity: 'Kolfe',
      woreda: '01',
      houseNumber: '1000002',
    },
  })
  @IsNotEmpty()
  address: ParentAddressCommand;
  @ApiProperty()
  @IsBoolean()
  enabled: boolean;
  @ApiProperty()
  @IsNotEmpty()
  lng: number;
  @ApiProperty()
  @IsNotEmpty()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  status: string;
  totalPayment: number;
  currentUser: UserInfo;
}
export class UpdateParentPaymentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  amount: number;
}
export class ArchiveParentCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class ParentStatusCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  status: string;
  currentUser: UserInfo;
}
export class GetNearestParentQuery {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  lat: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  lng: number;
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty()
  @IsNotEmpty()
  radius: number = 1;
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty()
  @IsNotEmpty()
  top: number = 5;
}
