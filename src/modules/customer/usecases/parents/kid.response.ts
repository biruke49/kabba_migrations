import { Kid } from '@customer/domains/parents/kid';
import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { SchoolResponse } from '../schools/school.response';
import { ParentResponse } from './parent.response';
import { CategoryResponse } from '@classification/usecases/categories/category.response';
import { GroupAssignmentResponse } from '@assignment/usecases/groups/group-assignment.response';

export class KidResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  parentId: string;
  @ApiProperty()
  schoolId: string;
  @ApiProperty()
  schoolName: string;
  @ApiProperty()
  categoryName: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  age: number;
  @ApiProperty()
  grade: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  remark: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  kidTravelStatus: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  startDate: Date;
  @ApiProperty()
  transportationTime: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  assigned: boolean;
  @ApiProperty()
  distanceFromSchool: number;
  @ApiProperty()
  kidFee: number;
  @ApiProperty()
  profileImage: FileDto;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  parent?: ParentResponse;
  school?: SchoolResponse;
  groupAssignment?: GroupAssignmentResponse;
  category?: CategoryResponse;
  static fromEntity(kidEntity: KidEntity): KidResponse {
    const kidResponse = new KidResponse();
    kidResponse.id = kidEntity.id;
    kidResponse.parentId = kidEntity.parentId;
    kidResponse.schoolId = kidEntity.schoolId;
    kidResponse.schoolName = kidEntity.schoolName;
    kidResponse.categoryName = kidEntity.categoryName;
    kidResponse.name = kidEntity.name;
    kidResponse.age = kidEntity.age;
    kidResponse.grade = kidEntity.grade;
    kidResponse.gender = kidEntity.gender;
    kidResponse.status = kidEntity.status;
    kidResponse.kidTravelStatus = kidEntity.kidTravelStatus;
    kidResponse.categoryId = kidEntity.categoryId;
    kidResponse.startDate = kidEntity.startDate;
    kidResponse.transportationTime = kidEntity.transportationTime;
    kidResponse.enabled = kidEntity.enabled;
    kidResponse.assigned = kidEntity.assigned;
    kidResponse.remark = kidEntity.remark;
    kidResponse.distanceFromSchool = kidEntity.distanceFromSchool;
    kidResponse.kidFee = kidEntity.kidFee;
    kidResponse.archiveReason = kidEntity.archiveReason;
    kidResponse.createdBy = kidEntity.createdBy;
    kidResponse.updatedBy = kidEntity.updatedBy;
    kidResponse.deletedBy = kidEntity.deletedBy;
    kidResponse.createdAt = kidEntity.createdAt;
    kidResponse.updatedAt = kidEntity.updatedAt;
    kidResponse.deletedAt = kidEntity.deletedAt;
    if (kidEntity.profileImage) {
      kidResponse.profileImage = kidEntity.profileImage;
    }
    if (kidEntity.school) {
      kidResponse.school = SchoolResponse.fromEntity(kidEntity.school);
    }
    if (kidEntity.parent) {
      kidResponse.parent = ParentResponse.fromEntity(kidEntity.parent);
    }
    if (kidEntity.category) {
      kidResponse.category = CategoryResponse.fromEntity(kidEntity.category);
    }
    if (kidEntity.groupAssignment) {
      kidResponse.groupAssignment = GroupAssignmentResponse.fromEntity(
        kidEntity.groupAssignment,
      );
    }

    return kidResponse;
  }
  static fromDomain(kid: Kid): KidResponse {
    const kidResponse = new KidResponse();
    kidResponse.id = kid.id;
    kidResponse.parentId = kid.parentId;
    kidResponse.schoolId = kid.schoolId;
    kidResponse.schoolName = kid.schoolName;
    kidResponse.categoryName = kid.categoryName;
    kidResponse.name = kid.name;
    kidResponse.age = kid.age;
    kidResponse.grade = kid.grade;
    kidResponse.gender = kid.gender;
    kidResponse.status = kid.status;
    kidResponse.kidTravelStatus = kid.kidTravelStatus;
    kidResponse.categoryId = kid.categoryId;
    kidResponse.startDate = kid.startDate;
    kidResponse.transportationTime = kid.transportationTime;
    kidResponse.enabled = kid.enabled;
    kidResponse.assigned = kid.assigned;
    kidResponse.remark = kid.remark;
    kidResponse.distanceFromSchool = kid.distanceFromSchool;
    kidResponse.kidFee = kid.kidFee;
    kidResponse.archiveReason = kid.archiveReason;
    kidResponse.createdBy = kid.createdBy;
    kidResponse.updatedBy = kid.updatedBy;
    kidResponse.deletedBy = kid.deletedBy;
    kidResponse.createdAt = kid.createdAt;
    kidResponse.updatedAt = kid.updatedAt;
    kidResponse.deletedAt = kid.deletedAt;
    if (kid.profileImage) {
      kidResponse.profileImage = kid.profileImage;
    }
    if (kid.school) {
      kidResponse.school = SchoolResponse.fromDomain(kid.school);
    }
    if (kid.parent) {
      kidResponse.parent = ParentResponse.fromDomain(kid.parent);
    }
    if (kid.category) {
      kidResponse.category = CategoryResponse.fromDomain(kid.category);
    }
    if (kid.groupAssignment) {
      kidResponse.groupAssignment = GroupAssignmentResponse.fromDomain(
        kid.groupAssignment,
      );
    }

    return kidResponse;
  }
}
