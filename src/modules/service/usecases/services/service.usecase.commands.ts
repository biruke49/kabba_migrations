import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { UserInfo } from '@account/dtos/user-info.dto';
import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { ServiceRepository } from '@service/persistence/services/service.repository';
import {
  ArchiveServiceCommand,
  CreateServiceCommand,
  UpdateServiceCommand,
} from './service.commands';
import { ServiceResponse } from './service.response';

@Injectable()
export class ServiceCommands {
  constructor(
    private serviceRepository: ServiceRepository,
    private readonly eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createService(command: CreateServiceCommand): Promise<ServiceResponse> {
    const serviceDomain = CreateServiceCommand.fromCommand(command);
    const service = await this.serviceRepository.insert(serviceDomain);
    if (service) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: service.id,
        modelName: MODELS.SERVICE,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
    }
    return ServiceResponse.fromDomain(service);
  }
  async updateService(command: UpdateServiceCommand): Promise<ServiceResponse> {
    const serviceDomain = await this.serviceRepository.getServiceById(
      command.id,
    );
    if (!serviceDomain) {
      throw new NotFoundException(`Service not found with id ${command.id}`);
    }
    if (serviceDomain.coverImage && command.coverImage) {
      if (serviceDomain.coverImage.filename !== command.coverImage?.filename) {
        await this.fileManagerService.removeFile(
          serviceDomain.coverImage,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
    }
    const oldPayload = { ...serviceDomain };
    serviceDomain.title = command.title;
    serviceDomain.description = command.description;
    if (
      command.coverImage &&
      command.coverImage?.filename !== serviceDomain.coverImage?.filename
    ) {
      serviceDomain.coverImage = command.coverImage;
    }
    const service = await this.serviceRepository.update(serviceDomain);
    if (service) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: service.id,
        modelName: MODELS.SERVICE,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: { ...service },
        oldPayload: { ...oldPayload },
      });
    }
    return ServiceResponse.fromDomain(service);
  }
  async deleteService(id: string, currentUser: UserInfo): Promise<boolean> {
    const serviceDomain = await this.serviceRepository.getServiceById(id, true);
    if (!serviceDomain) {
      throw new NotFoundException(`Service not found with id ${id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.SERVICE,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    if (serviceDomain.coverImage) {
      await this.fileManagerService.removeFile(
        serviceDomain.coverImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    return await this.serviceRepository.delete(id);
  }
  async archiveService(
    command: ArchiveServiceCommand,
  ): Promise<ServiceResponse> {
    const serviceDomain = await this.serviceRepository.getServiceById(
      command.id,
    );
    if (!serviceDomain) {
      throw new NotFoundException(`Service not found with id ${command.id}`);
    }
    serviceDomain.archiveReason = command.reason;
    serviceDomain.deletedAt = new Date();
    serviceDomain.deletedBy = command.currentUser.id;
    const result = await this.serviceRepository.update(serviceDomain);

    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.SERVICE,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return ServiceResponse.fromDomain(result);
  }
  async restoreService(
    id: string,
    currentUser: UserInfo,
  ): Promise<ServiceResponse> {
    const serviceDomain = await this.serviceRepository.getServiceById(id, true);
    if (!serviceDomain) {
      throw new NotFoundException(`Service not found with id ${id}`);
    }
    const r = await this.serviceRepository.restore(id);
    if (r) {
      serviceDomain.deletedAt = null;
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.SERVICE,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    return ServiceResponse.fromDomain(serviceDomain);
  }
}
