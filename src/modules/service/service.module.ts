import { FileManagerService } from '@libs/common/file-manager';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServicesController } from './controllers/service.controller';
import { ServiceEntity } from './persistence/services/service.entity';
import { ServiceRepository } from './persistence/services/service.repository';
import { ServiceCommands } from './usecases/services/service.usecase.commands';
import { ServiceQuery } from './usecases/services/service.usecase.queries';

@Module({
  controllers: [ServicesController],
  imports: [TypeOrmModule.forFeature([ServiceEntity])],
  providers: [
    ServiceQuery,
    ServiceCommands,
    ServiceRepository,
    FileManagerService,
  ],
})
export class ServiceModule {}
