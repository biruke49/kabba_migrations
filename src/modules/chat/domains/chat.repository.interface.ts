import { Chat } from './chat';

export interface IChatRepository {
  insert(chat: Chat): Promise<Chat>;
  update(chat: Chat): Promise<Chat>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Chat[]>;
  getById(id: string, withDeleted: boolean): Promise<Chat>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
