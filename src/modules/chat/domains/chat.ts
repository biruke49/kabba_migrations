import { Account } from '@account/domains/accounts/account';
import { ChatEntity } from '@chat/persistence/chat.entity';
import { FileDto } from '@libs/common/file-dto';

export class Chat {
  id: string;
  body: string;
  senderId: string;
  senderName: string;
  receiverId: string;
  receiverName: string;
  groupId: string;
  attachments: FileDto[];
  isSeen: boolean;
  createdBy?: string;
  createdAt: Date;
  accountSender?: Account;
  accountReceiver?: Account;
  static toChat(chatEntity: ChatEntity): Chat {
    const chat = new Chat();
    chat.id = chatEntity.id;
    chat.body = chatEntity.body;
    chat.senderId = chatEntity.senderId;
    chat.senderName = chatEntity.senderName;
    chat.receiverId = chatEntity.receiverId;
    chat.receiverName = chatEntity.receiverName;
    chat.isSeen = chatEntity.isSeen;
    chat.groupId = chatEntity.groupId;
    chat.attachments = chatEntity.attachments;
    chat.createdBy = chatEntity.createdBy;
    chat.createdAt = chatEntity.createdAt;
    return chat;
  }
}
