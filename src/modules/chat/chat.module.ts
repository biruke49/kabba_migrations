import { FileManagerService } from '@libs/common/file-manager';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatController } from './controllers/chat.controller';
import { ChatEntity } from './persistence/chat.entity';
import { ChatRepository } from './persistence/chat.repository';
import { ChatCommands } from './usecases/chat.usecase.commands';
import { ChatQuery } from './usecases/chat.usecase.queries';
import { AccountEntity } from '@account/persistence/accounts/account.entity';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import { ParentEntity } from '@customer/persistence/parents/parent.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { KidEntity } from '@customer/persistence/parents/kid.entity';
import { ParentPreferenceEntity } from '@interaction/persistence/parent-preference/parent-preference.entity';
import { AppService } from 'app.service';

@Module({
  controllers: [ChatController],
  imports: [
    TypeOrmModule.forFeature([
      ChatEntity,
      AccountEntity,
      DriverEntity,
      ParentEntity,
      KidEntity,
      ParentPreferenceEntity,
    ]),
  ],
  providers: [
    AppService,
    ChatCommands,
    ChatQuery,
    ChatRepository,
    FileManagerService,
    AccountRepository,
    DriverRepository,
    ParentRepository,
  ],
})
export class ChatModule {}
