import { FileDto } from '@libs/common/file-dto';
import { FileResponseDto } from '@libs/common/file-manager';
import { ApiProperty } from '@nestjs/swagger';
import { Chat } from '../domains/chat';
import { ChatEntity } from '../persistence/chat.entity';
import { AccountResponse } from '@account/usecases/accounts/account.response';

export class ChatResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  body: string;
  @ApiProperty()
  senderId: string;
  @ApiProperty()
  senderName: string;
  @ApiProperty()
  receiverId: string;
  @ApiProperty()
  receiverName: string;
  @ApiProperty()
  groupId: string;
  @ApiProperty()
  attachments: FileResponseDto[];
  @ApiProperty()
  isSeen: boolean;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  createdAt: Date;
  accountSender?: AccountResponse;
  accountReceiver?: AccountResponse;
  static fromEntity(chatEntity: ChatEntity): ChatResponse {
    const chatResponse = new ChatResponse();
    chatResponse.id = chatEntity.id;
    chatResponse.body = chatEntity.body;
    chatResponse.senderId = chatEntity.senderId;
    chatResponse.senderName = chatEntity.senderName;
    chatResponse.receiverId = chatEntity.receiverId;
    chatResponse.receiverName = chatEntity.receiverName;
    chatResponse.isSeen = chatEntity.isSeen;
    chatResponse.groupId = chatEntity.groupId;
    chatResponse.attachments = chatEntity.attachments;
    chatResponse.createdBy = chatEntity.createdBy;
    chatResponse.createdAt = chatEntity.createdAt;
    if (chatEntity.accountSender) {
      chatResponse.accountSender = AccountResponse.fromEntity(
        chatEntity.accountSender,
      );
    }
    if (chatEntity.accountReceiver) {
      chatResponse.accountReceiver = AccountResponse.fromEntity(
        chatEntity.accountReceiver,
      );
    }
    return chatResponse;
  }
  static fromDomain(chat: Chat): ChatResponse {
    const chatResponse = new ChatResponse();
    chatResponse.id = chat.id;
    chatResponse.body = chat.body;
    chatResponse.senderId = chat.senderId;
    chatResponse.senderName = chat.senderName;
    chatResponse.receiverId = chat.receiverId;
    chatResponse.receiverName = chat.receiverName;
    chatResponse.isSeen = chat.isSeen;
    chatResponse.groupId = chat.groupId;
    chatResponse.attachments = chat.attachments;
    chatResponse.createdBy = chat.createdBy;
    chatResponse.createdAt = chat.createdAt;
    if (chat.accountSender) {
      chatResponse.accountSender = AccountResponse.fromDomain(
        chat.accountSender,
      );
    }
    if (chat.accountReceiver) {
      chatResponse.accountReceiver = AccountResponse.fromDomain(
        chat.accountReceiver,
      );
    }

    return chatResponse;
  }
}
export class TotalUnseenChatResponse {
  @ApiProperty()
  total: number;
}
