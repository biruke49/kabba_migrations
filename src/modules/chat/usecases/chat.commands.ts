import { UserInfo } from '@account/dtos/user-info.dto';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Chat } from '../domains/chat';

export class CreateChatCommand {
  @ApiProperty()
  @IsNotEmpty()
  body: string;
  @ApiProperty()
  senderId: string;
  @ApiProperty()
  receiverId: string;
  @ApiProperty()
  groupId: string;
  isSeen: boolean;
  senderName: string;
  receiverName: string;
  attachments: FileDto[];
  currentUser: UserInfo;
  static fromCommand(command: CreateChatCommand): Chat {
    const categoryDomain = new Chat();
    categoryDomain.body = command.body;
    categoryDomain.senderId = command.senderId;
    categoryDomain.receiverId = command.receiverId;
    categoryDomain.isSeen = command.isSeen;
    categoryDomain.groupId = command.groupId;
    categoryDomain.senderName = command.senderName;
    categoryDomain.receiverName = command.receiverName;
    return categoryDomain;
  }
}
export class CreateChatCommands {
  @ApiProperty()
  @IsNotEmpty()
  body: string;
  @ApiProperty()
  @IsNotEmpty()
  senderId: string;
  @ApiProperty()
  receiverId: string;
  @ApiProperty()
  groupId: string;
  senderName: string;
  receiverName: string;
  createdAt: Date;
}
export class BulkChatCommand {
  @ApiProperty()
  @IsNotEmpty()
  body: string;
  @ApiProperty()
  @IsNotEmpty()
  senderId: string;
  @ApiProperty()
  type: string;
  createdAt: Date;
}
export class GetIdsCommand {
  @ApiProperty()
  @IsNotEmpty()
  senderId: string;
  receiverId: string;
}
