import { CollectionQuery } from '@libs/collection-query/collection-query';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChatEntity } from '../persistence/chat.entity';
import { ChatResponse, TotalUnseenChatResponse } from './chat.response';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { GetIdsCommand } from './chat.commands';
import { AccountEntity } from '@account/persistence/accounts/account.entity';
import { Chat } from '@chat/domains/chat';

@Injectable()
export class ChatQuery {
  constructor(
    @InjectRepository(ChatEntity)
    private chatRepository: Repository<ChatEntity>,
    @InjectRepository(AccountEntity)
    private accountRepository: Repository<AccountEntity>,
  ) {}
  async getChat(id: string, withDeleted = false): Promise<ChatResponse> {
    const category = await this.chatRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!category[0]) {
      throw new NotFoundException(`Chat not found with id ${id}`);
    }
    return ChatResponse.fromEntity(category[0]);
  }
  async getChats(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ChatResponse>> {
    const dataQuery = QueryConstructor.constructQuery<ChatEntity>(
      this.chatRepository,
      query,
    );
    const d = new DataResponseFormat<ChatResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => ChatResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getChatBySenderIdAndReceiverId(
    command: GetIdsCommand,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ChatResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'senderId',
          operator: FilterOperators.EqualTo,
          value: command.senderId,
        },
      ],
      [
        {
          field: 'receiverId',
          operator: FilterOperators.EqualTo,
          value: command.receiverId,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<ChatEntity>(
      this.chatRepository,
      query,
    );
    const d = new DataResponseFormat<ChatResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => ChatResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getChatDomain(
    command: GetIdsCommand,
    query: CollectionQuery,
  ): Promise<Chat[]> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'senderId',
          operator: FilterOperators.EqualTo,
          value: command.senderId,
        },
      ],
      [
        {
          field: 'receiverId',
          operator: FilterOperators.EqualTo,
          value: command.receiverId,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<ChatEntity>(
      this.chatRepository,
      query,
    );
    const d = new DataResponseFormat<ChatResponse>();
    const result = await dataQuery.getMany();
    return result.map((entity) => Chat.toChat(entity));
  }
  async getPeopleInChat(id: string, query: CollectionQuery): Promise<any> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'sender.sender_id',
          operator: FilterOperators.EqualTo,
          value: id,
        },
        {
          field: 'sender.receiver_id',
          operator: FilterOperators.EqualTo,
          value: id,
        },
      ],
      [
        {
          field: 'id',
          operator: FilterOperators.NotEqualTo,
          value: id,
        },
      ],
      [
        {
          field: 'type',
          operator: FilterOperators.In,
          value: ['parent', 'driver', 'passenger'],
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<AccountEntity>(
      this.accountRepository,
      query,
    );
    const resultSender = await dataQuery
      .leftJoin('accounts.chatSender', 'sender')
      .getMany();
    const chatResponses = [];
    for (const result of resultSender) {
      const chatResponse = {
        id: result.id,
        name: result.name,
      };
      chatResponses.push(chatResponse);
    }
    return chatResponses;
  }
  async getTotalUnseenChatByReceiverId(
    receiverId: string,
    query: CollectionQuery,
  ): Promise<TotalUnseenChatResponse> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'isSeen',
          operator: FilterOperators.EqualTo,
          value: false,
        },
      ],
      [
        {
          field: 'receiverId',
          operator: FilterOperators.EqualTo,
          value: receiverId,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<ChatEntity>(
      this.chatRepository,
      query,
    );
    const total = await dataQuery.getCount();
    return { total: total };
  }
  async getTotalUnseenChatBySenderIdAndReceiverId(
    command: GetIdsCommand,
    query: CollectionQuery,
  ): Promise<TotalUnseenChatResponse> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'isSeen',
          operator: FilterOperators.EqualTo,
          value: false,
        },
      ],
      [
        {
          field: 'receiverId',
          operator: FilterOperators.EqualTo,
          value: command.receiverId,
        },
      ],
      [
        {
          field: 'senderId',
          operator: FilterOperators.EqualTo,
          value: command.senderId,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<ChatEntity>(
      this.chatRepository,
      query,
    );
    const total = await dataQuery.getCount();
    return { total: total };
  }
}
