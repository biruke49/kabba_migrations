import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { UserInfo } from '@account/dtos/user-info.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { ChatRepository } from '../persistence/chat.repository';
import { CreateChatCommand, GetIdsCommand } from './chat.commands';
import { ChatResponse } from './chat.response';
import { ChatQuery } from './chat.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import axios from 'axios';
import { AppService } from 'app.service';

@Injectable()
export class ChatCommands {
  constructor(
    private chatRepository: ChatRepository,
    private chatQuery: ChatQuery,
    private readonly eventEmitter: EventEmitter2,
    private accountRepository: AccountRepository,
    private driverRepository: DriverRepository,
    private parentRepository: ParentRepository,
    private appService: AppService,
  ) {}
  @OnEvent('create.new.chat')
  async createChat(command: CreateChatCommand) {
    const chatDomain = CreateChatCommand.fromCommand(command);
    chatDomain.createdBy = command.senderId;
    chatDomain.attachments = command.attachments ? command.attachments : [];
    const chat = await this.chatRepository.insert(chatDomain);
    return ChatResponse.fromDomain(chat);
  }
  async deleteChat(id: string, currentUser: UserInfo): Promise<boolean> {
    const chatDomain = await this.chatRepository.getById(id, true);
    if (!chatDomain) {
      throw new NotFoundException(`Chat not found with id ${id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.CHAT,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    return await this.chatRepository.delete(id);
  }
  async changeChatSeenStatus(
    command: GetIdsCommand,
    query: CollectionQuery,
  ): Promise<boolean> {
    const chatDomains = await this.chatQuery.getChatDomain(command, query);
    if (!chatDomains) {
      throw new NotFoundException(
        `Chat not found with sender Id ${command.senderId} and receiver Id ${command.receiverId}`,
      );
    }
    for (const chat of chatDomains) {
      chat.isSeen = true;
      const result = await this.chatRepository.update(chat);
      if (!result) {
        return false;
      }
    }
    return true;
  }
}
