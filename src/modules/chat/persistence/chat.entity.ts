import { AccountEntity } from '@account/persistence/accounts/account.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { FileDto } from '@libs/common/file-dto';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('chats')
export class ChatEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  body: string;
  @Column({ name: 'sender_id', type: 'uuid' })
  senderId: string;
  @Column({ name: 'sender_name', nullable: true })
  senderName: string;
  @Column({ name: 'receiver_id', type: 'uuid', nullable: true })
  receiverId: string;
  @Column({ name: 'receiver_name', nullable: true })
  receiverName: string;
  @Column({ name: 'group_id', nullable: true })
  groupId: string;
  @Column({ nullable: true, type: 'jsonb', default: [] })
  attachments: FileDto[];
  @Column({ name: 'is_seen', nullable: true, default: false })
  isSeen: boolean;
  @ManyToOne(() => AccountEntity, (accountSender) => accountSender.chatSender, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'sender_id' })
  accountSender: AccountEntity;
  @ManyToOne(
    () => AccountEntity,
    (accountReceiver) => accountReceiver.chatReceiver,
    {
      orphanedRowAction: 'delete',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'receiver_id' })
  accountReceiver: AccountEntity;
}
