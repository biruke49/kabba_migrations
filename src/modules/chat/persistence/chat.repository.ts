import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Chat } from '../domains/chat';
import { IChatRepository } from '../domains/chat.repository.interface';
import { ChatEntity } from './chat.entity';

@Injectable()
export class ChatRepository implements IChatRepository {
  constructor(
    @InjectRepository(ChatEntity)
    private chatRepository: Repository<ChatEntity>,
  ) {}
  async insert(chat: Chat): Promise<Chat> {
    const chatEntity = this.toChatEntity(chat);
    const result = await this.chatRepository.save(chatEntity);
    return result ? this.toChat(result) : null;
  }
  async update(chat: Chat): Promise<Chat> {
    const chatEntity = this.toChatEntity(chat);
    const result = await this.chatRepository.save(chatEntity);
    return result ? this.toChat(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.chatRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Chat[]> {
    const categories = await this.chatRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!categories.length) {
      return null;
    }
    return categories.map((chat) => this.toChat(chat));
  }
  async getById(id: string, withDeleted = false): Promise<Chat> {
    const chat = await this.chatRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!chat[0]) {
      return null;
    }
    return this.toChat(chat[0]);
  }
  async getBySenderIdAndRecieverId(
    senderId: string,
    receiverId: string,
    withDeleted = false,
  ): Promise<Chat> {
    const chat = await this.chatRepository.find({
      where: { senderId: senderId, receiverId: receiverId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!chat[0]) {
      return null;
    }
    return this.toChat(chat[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.chatRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.chatRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toChat(chatEntity: ChatEntity): Chat {
    const chat = new Chat();
    chat.id = chatEntity.id;
    chat.body = chatEntity.body;
    chat.senderId = chatEntity.senderId;
    chat.senderName = chatEntity.senderName;
    chat.receiverId = chatEntity.receiverId;
    chat.receiverName = chatEntity.receiverName;
    chat.isSeen = chatEntity.isSeen;
    chat.groupId = chatEntity.groupId;
    chat.attachments = chatEntity.attachments;
    chat.createdBy = chatEntity.createdBy;
    chat.createdAt = chatEntity.createdAt;
    return chat;
  }
  toChatEntity(chat: Chat): ChatEntity {
    const chatEntity = new ChatEntity();
    chatEntity.id = chat.id;
    chatEntity.body = chat.body;
    chatEntity.senderId = chat.senderId;
    chatEntity.senderName = chat.senderName;
    chatEntity.receiverId = chat.receiverId;
    chatEntity.receiverName = chat.receiverName;
    chatEntity.isSeen = chat.isSeen;
    chatEntity.groupId = chat.groupId;
    chatEntity.attachments = chat.attachments;
    chatEntity.createdBy = chat.createdBy;
    chatEntity.createdAt = chat.createdAt;
    return chatEntity;
  }
}
