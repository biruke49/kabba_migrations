import { NotFoundException, OnModuleInit } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'socket.io';
import { BulkChatCommand, CreateChatCommands } from './usecases/chat.commands';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { ParentRepository } from '@customer/persistence/parents/parent.repository';
import { AppService } from 'app.service';

@WebSocketGateway()
export class MyGateway implements OnModuleInit {
  constructor(
    private eventEmitter: EventEmitter2,
    private accountRepository: AccountRepository,
    private driverRepository: DriverRepository,
    private parentRepository: ParentRepository,
    private appService: AppService,
  ) {}
  @WebSocketServer()
  server: Server;
  onModuleInit() {
    this.server.on('connection', (socket) => {
      // console.log('connected');
    });
  }
  @SubscribeMessage('newMessage')
  async onNewMessage(@MessageBody() body: CreateChatCommands) {
    if (!body.senderId) {
      throw new NotFoundException(`Sender ID can not be empty`);
    }
    if (!body.receiverId) {
      throw new NotFoundException(`Receiver ID can not be empty`);
    }
    body.createdAt = new Date();
    const type = 'chat';
    const receiver = await this.accountRepository.getByAccountId(
      body.receiverId,
    );
    const sender = (await this.driverRepository.getByIdNotification(
      body.senderId,
    ))
      ? await this.driverRepository.getByIdNotification(body.senderId)
      : await this.parentRepository.getByIdNotification(body.senderId);

    this.server.emit(`${body.receiverId}`, {
      msg: 'New Private Message',
      content: body,
    });
    this.server.emit(`${body.senderId}`, {
      msg: 'New Private Message',
      content: body,
    });
    this.appService.sendNotification(receiver.fcmId, body, sender, type);
    this.eventEmitter.emit('create.new.chat', {
      body: body.body,
      senderId: body.senderId,
      senderName: sender.name,
      receiverId: body.receiverId,
      receiverName: receiver.name,
    });
  }
  //   @SubscribeMessage('newGroupMessage')
  //   onNewGroupMessage(@MessageBody() body: CreateChatCommands) {
  //     if (!body.groupId) {
  //       throw new NotFoundException(`Group ID can not be empty`);
  //     }
  //     if (!body.senderId) {
  //       throw new NotFoundException(`Sender ID can not be empty`);
  //     }
  //     body.createdAt = new Date();
  //     this.server.emit(`${body.groupId}`, {
  //       msg: 'New Group Message',
  //       content: body,
  //     });
  //     this.server.emit(`${body.senderId}`, {
  //       msg: 'New Group Message',
  //       content: body,
  //     });
  //     this.eventEmitter.emit('create.new.chat', {
  //       body: body.body,
  //       senderId: body.senderId,
  //       groupid: body.groupId,
  //     });
  //   }
}
