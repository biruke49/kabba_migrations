import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { RolesGuard } from '@account/guards/role.quards';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { CreateChatCommand, GetIdsCommand } from '../usecases/chat.commands';
import {
  ChatResponse,
  TotalUnseenChatResponse,
} from '../usecases/chat.response';
import { ChatCommands } from '../usecases/chat.usecase.commands';
import { ChatQuery } from '../usecases/chat.usecase.queries';

@Controller('chat')
@ApiTags('chat')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class ChatController {
  constructor(
    private command: ChatCommands,
    private chatQuery: ChatQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-chat/:id')
  @UseGuards(RolesGuard('parent|driver|school'))
  @ApiOkResponse({ type: ChatResponse })
  async getChat(@Param('id') id: string) {
    return this.chatQuery.getChat(id);
  }
  @Get('get-chats')
  @UseGuards(RolesGuard('parent|driver|school'))
  @ApiPaginatedResponse(ChatResponse)
  async getChats(@Query() query: CollectionQuery) {
    return this.chatQuery.getChats(query);
  }
  @Get('get-people-in-chat/:id')
  // @UseGuards(RolesGuard('parent|driver|school'))
  @ApiPaginatedResponse(ChatResponse)
  async getPeopleInChat(
    @Param('id') id: string,
    @Query() query: CollectionQuery,
  ) {
    return this.chatQuery.getPeopleInChat(id, query);
  }
  @Get('get-total-unseen-chat-by-receiver-id')
  @ApiOkResponse({ type: TotalUnseenChatResponse })
  async getTotalUnseenChatByReceiverId(
    @Query() query: CollectionQuery,
    @CurrentUser() user: UserInfo,
  ) {
    const receiverId = user.id;
    return this.chatQuery.getTotalUnseenChatByReceiverId(receiverId, query);
  }
  @Get('get-total-unseen-chat-by-sender-id-and-receiver-id')
  @ApiOkResponse({ type: TotalUnseenChatResponse })
  async getTotalUnseenChatBySenderIdAndReceiverId(
    @Query() query: CollectionQuery,
    @CurrentUser() user: UserInfo,
    @Body() command: GetIdsCommand,
  ) {
    command.receiverId = user.id;
    return this.chatQuery.getTotalUnseenChatBySenderIdAndReceiverId(
      command,
      query,
    );
  }
  @Post('create-chat')
  @UseGuards(RolesGuard('parent|driver|school'))
  @UseInterceptors(
    FilesInterceptor('attachments', 10, {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (
          !file.mimetype.includes('pdf') ||
          !file.mimetype.includes('image')
        ) {
          return callback(
            new BadRequestException('Provide a valid attachment'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: ChatResponse, isArray: true })
  async createChat(
    @CurrentUser() user: UserInfo,
    @Body() createChatCommand: CreateChatCommand,
    @UploadedFiles() attachments: Express.Multer.File[],
  ) {
    if (attachments) {
      const result = await this.fileManagerService.uploadFiles(
        attachments,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        createChatCommand.attachments = result;
      }
    }
    createChatCommand.currentUser = user;
    return this.command.createChat(createChatCommand);
  }
  @Delete('delete-chat/:id')
  @UseGuards(RolesGuard('parent|driver|school'))
  @ApiOkResponse({ type: Boolean })
  async deleteChat(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteChat(id, user);
  }
  @Post('change-chat-status')
  @ApiOkResponse({ type: Boolean })
  async changeChatStatus(
    @Query() query: CollectionQuery,
    @CurrentUser() user: UserInfo,
    @Body() command: GetIdsCommand,
  ) {
    command.receiverId = user.id;
    return this.command.changeChatSeenStatus(command, query);
  }
}
