import { Owner } from './owner';
export interface IOwnerRepository {
  insert(owner: Owner): Promise<Owner>;
  update(owner: Owner): Promise<Owner>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Owner[]>;
  getById(id: string, withDeleted: boolean): Promise<Owner>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
  getByPhoneNumber(phoneNumber: string, withDeleted: boolean): Promise<Owner>;
  getByTinNumber(tinNumber: string, withDeleted: boolean): Promise<Owner>;
  getByEmail(email: string, withDeleted: boolean): Promise<Owner>;
}
