import { Vehicle } from './vehicle';
import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';

export class Owner {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  gender: string;
  logo: FileDto;
  address: Address;
  tinNumber: string;
  website: string;
  license: FileDto;
  licenseDueDate: Date;
  isCompany: boolean;
  enabled: boolean;
  archiveReason: string;
  vehicles: Vehicle[];
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  async addVehicle(vehicle: Vehicle) {
    this.vehicles.push(vehicle);
  }
  async updateVehicle(vehicle: Vehicle) {
    const existIndex = this.vehicles.findIndex(
      (element) => element.id == vehicle.id,
    );
    this.vehicles[existIndex] = vehicle;
  }
  async removeVehicle(id: string) {
    this.vehicles = this.vehicles.filter((element) => element.id != id);
  }
  async updateVehicles(vehicles: Vehicle[]) {
    this.vehicles = vehicles;
  }
}
