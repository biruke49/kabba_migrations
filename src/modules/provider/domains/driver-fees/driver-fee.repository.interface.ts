import { DriverFee } from './driver-fee';
export interface IDriverFeeRepository {
  insert(driverFee: DriverFee): Promise<DriverFee>;
  update(driverFee: DriverFee): Promise<DriverFee>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<DriverFee[]>;
  getById(id: string, withDeleted: boolean): Promise<DriverFee>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
