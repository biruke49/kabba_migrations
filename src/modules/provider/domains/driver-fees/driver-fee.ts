import { User } from '@user/domains/users/user';
import { Driver } from '../drivers/driver';

export class DriverFee {
  id: string;
  driverId: string;
  paymentMethod: string;
  fee: number;
  assignmentIds: string[];
  paymentStatus: string;
  accountNumber?: string;
  transactionNumber?: string;
  paidById?: string;
  bankName?: string;
  driver: Driver;
  createdBy?: string;
  createdAt: Date;
  deletedAt: Date;
  deletedBy: string;
  paidBy?: User;
}
