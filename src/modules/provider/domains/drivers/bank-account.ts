export class BankAccount {
  id: string;
  driverId: string;
  accountNumber: string;
  bankName: string;
  isPreferred: boolean;
  archiveReason: string;
  isActive: boolean;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
