import { Preference } from '@interaction/domains/preferences/preference';
import { Vehicle } from '@provider/domains/owners/vehicle';
import { BankAccount } from '@provider/domains/drivers/bank-account';
import { FileDto } from '@libs/common/file-dto';
import { Address } from '@libs/common/address';
import { AverageRate } from '@libs/common/average-rate';
import { Owner } from '../owners/owner';
import { EmergencyContact } from '@libs/common/emergency-contact';
export class Driver {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  gender: string;
  profileImage: FileDto;
  emergencyContact: EmergencyContact;
  address: Address;
  isActive: boolean;
  vehicleId: string;
  enabled: boolean;
  passport: FileDto;
  license: FileDto;
  licenseDueDate: Date;
  fcmId: string;
  averageRate: AverageRate;
  lat: number;
  lng: number;
  isApproved: boolean;
  datePreferences: string[];
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  bankAccounts: BankAccount[];
  vehicle: Vehicle;
  owner: Owner;
  preferences: Preference[];
  async addBankAccount(bankAccount: BankAccount) {
    this.bankAccounts.push(bankAccount);
  }
  async updateBankAccount(bankAccount: BankAccount) {
    const existIndex = this.bankAccounts.findIndex(
      (element) => element.id === bankAccount.id,
    );
    this.bankAccounts[existIndex] = bankAccount;
  }
  async removeBankAccount(id: string) {
    this.bankAccounts = this.bankAccounts.filter(
      (element) => element.id !== id,
    );
  }
  async updateBankAccounts(bankAccounts: BankAccount[]) {
    this.bankAccounts = bankAccounts;
  }
}
