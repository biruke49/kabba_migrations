import { Driver } from './driver';
export interface IDriverRepository {
  insert(user: Driver): Promise<Driver>;
  update(user: Driver): Promise<Driver>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Driver[]>;
  getById(id: string, withDeleted: boolean): Promise<Driver>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
  getByPhoneNumber(phoneNumber: string, withDeleted: boolean): Promise<Driver>;
}
