import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { DriverFeesController } from './controllers/driver-fee.controller';
import { DriverFeeEntity } from './persistence/driver-fees/driver-fee.entity';
import { DriverFeeCommands } from './usecases/driver-fees/driver-fee.usecase.commands';
import { DriverFeeRepository } from './persistence/driver-fees/driver-fee.repository';
import { DriverQuery } from './usecases/drivers/driver.usecase.queries';
import { DriverCommands } from '@provider/usecases/drivers/driver.usecase.commands';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { BankAccountEntity } from '@provider/persistence/drivers/bank-account.entity';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { OwnerCommands } from './usecases/owners/owner.usecase.commands';
import { OwnerRepository } from './persistence/owners/owner.repository';
import { OwnerEntity } from './persistence/owners/owner.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OwnersController } from './controllers/owner.controller';
import { Module } from '@nestjs/common';
import { OwnerQuery } from './usecases/owners/owner.usecase.queries';
import { VehicleEntity } from './persistence/owners/vehicle.entity';
import { AccountModule } from '@account/account.module';
import { DriversController } from './controllers/driver.controller';
import { FileManagerService } from '@libs/common/file-manager';
import { DriverFeeQuery } from './usecases/driver-fees/driver-fee.usecase.queries';
import { AssignmentRepository } from '@router/persistence/assignments/assignment.repository';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';
import { DriverAssignmentRepository } from '@assignment/persistence/driver-assignments/driver-assignment.repository';
@Module({
  imports: [
    TypeOrmModule.forFeature([
      OwnerEntity,
      VehicleEntity,
      DriverEntity,
      BankAccountEntity,
      DriverFeeEntity,
      AssignmentEntity,
      GroupEntity,
      GroupAssignmentEntity,
      DriverAssignmentEntity,
    ]),
    AccountModule,
  ],
  providers: [
    OwnerRepository,
    OwnerCommands,
    OwnerQuery,
    DriverRepository,
    DriverCommands,
    DriverQuery,
    FileManagerService,
    DriverFeeRepository,
    DriverFeeQuery,
    DriverFeeCommands,
    AssignmentRepository,
    DriverAssignmentRepository,
  ],
  exports: [DriverFeeCommands, DriverFeeRepository, DriverRepository],
  controllers: [OwnersController, DriversController, DriverFeesController],
})
export class ProviderModule {}
