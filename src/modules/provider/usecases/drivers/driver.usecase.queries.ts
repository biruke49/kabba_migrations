import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { BankAccountEntity } from '@provider/persistence/drivers/bank-account.entity';
import { Repository } from 'typeorm';
import { DriverResponse } from './driver.response';
import { BankAccountResponse } from './bank-account.response';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
@Injectable()
export class DriverQuery {
  constructor(
    @InjectRepository(DriverEntity)
    private driverRepository: Repository<DriverEntity>,
    @InjectRepository(BankAccountEntity)
    private bankAccountRepository: Repository<BankAccountEntity>,
  ) {}
  async getDriver(id: string, withDeleted = false): Promise<DriverResponse> {
    const driver = await this.driverRepository.find({
      where: { id: id },
      relations: ['bankAccounts', 'vehicle'],
      withDeleted: withDeleted,
    });
    if (!driver[0]) {
      throw new NotFoundException(`Driver not found with id ${id}`);
    }
    return DriverResponse.fromEntity(driver[0]);
  }
  async getDrivers(
    query: CollectionQuery,
    routeId = null,
    datePreference = null,
    categoryId = null,
  ): Promise<DataResponseFormat<DriverResponse>> {
    const dataQuery = QueryConstructor.constructQuery<DriverEntity>(
      this.driverRepository,
      query,
    );
    if (routeId) {
      dataQuery.andWhere(
        `drivers.id in ${dataQuery
          .subQuery()
          .select('driver_id')
          .from(PreferenceEntity, 'preferences')
          .where(`route_id= :routeId`, {
            routeId: routeId,
          })
          .getQuery()}`,
      );
    }
    if (datePreference) {
      dataQuery.andWhere(`'${datePreference}'=any(date_preferences)`);
    }
    if (categoryId) {
      dataQuery.andWhere(
        `category.id = ${dataQuery
          .subQuery()
          .select('id')
          .from(CategoryEntity, 'category')
          .where(`category.id= :categoryId`, {
            categoryId: categoryId,
          })
          .getQuery()}`,
      );
    }
    const d = new DataResponseFormat<DriverResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery
        .leftJoinAndSelect('drivers.vehicle', 'vehicle')
        .leftJoinAndSelect('vehicle.category', 'category')
        .getManyAndCount();
      d.data = result.map((entity) => DriverResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getDriversByCarType(
    categoryId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverResponse>> {
    const dataQuery = QueryConstructor.constructQuery<DriverEntity>(
      this.driverRepository,
      query,
    );
    dataQuery
      .leftJoinAndSelect('drivers.vehicle', 'vehicle')
      .andWhere('vehicle.category_id= :categoryId', { categoryId: categoryId });
    const d = new DataResponseFormat<DriverResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => DriverResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async groupDriversByGender(
    query: CollectionQuery,
  ): Promise<CountByGenderResponse[]> {
    query.select = [];
    query.select.push('gender', 'COUNT(drivers.id)');
    const dataQuery = QueryConstructor.constructQuery<DriverEntity>(
      this.driverRepository,
      query,
    );
    dataQuery.groupBy('drivers.gender');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByGenderResponse();
      countResponse.gender = d.drivers_gender;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupDriversByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('is_active as isActive', 'COUNT(drivers.id)');
    const dataQuery = QueryConstructor.constructQuery<DriverEntity>(
      this.driverRepository,
      query,
    );
    dataQuery.groupBy('drivers.isActive');
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.isactive ? 'Active' : 'InActive';
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupDriversByAddress(
    field: string,
    query: CollectionQuery,
  ): Promise<GroupByAddressResponse[]> {
    query.select = [];
    query.select.push(
      `drivers.address->>'${field}' as ${field}`,
      `COUNT(drivers.id)`,
    );
    const dataQuery = QueryConstructor.constructQuery<DriverEntity>(
      this.driverRepository,
      query,
    );

    dataQuery.groupBy(`drivers.address->>'${field}'`);
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByAddressResponse();
      countResponse[field] = d[field];
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getArchivedDrivers(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverEntity>(
      this.driverRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<DriverResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => DriverResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }

  async getBankAccounts(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BankAccountResponse>> {
    const dataQuery = QueryConstructor.constructQuery<BankAccountEntity>(
      this.bankAccountRepository,
      query,
    );
    const d = new DataResponseFormat<BankAccountResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BankAccountResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getBankAccount(id: string): Promise<BankAccountResponse> {
    const bankAccount = await this.bankAccountRepository.find({
      where: { id: id },
    });
    if (!bankAccount[0]) {
      throw new NotFoundException(`Bank Account not found with id ${id}`);
    }
    return BankAccountResponse.fromEntity(bankAccount[0]);
  }
  async getArchivedBankAccounts(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<BankAccountResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<BankAccountEntity>(
      this.bankAccountRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<BankAccountResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => BankAccountResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
