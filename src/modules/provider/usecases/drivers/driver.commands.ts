import { FileDto } from '@libs/common/file-dto';
import { UserInfo } from '@account/dtos/user-info.dto';
import { Address } from '@libs/common/address';
import { ApiProperty } from '@nestjs/swagger';
import { Driver } from '@provider/domains/drivers/driver';
import {
  IsArray,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsUUID,
} from 'class-validator';
import { EmergencyContact } from '@libs/common/emergency-contact';
import { DateOfWeek, Gender } from '@libs/common/enums';
export class CreateDriverCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  vehicleId: string;
  currentUser: UserInfo;
  static fromCommand(command: CreateDriverCommand): Driver {
    const driver = new Driver();
    driver.name = command.name;
    driver.email = command.email;
    driver.phoneNumber = command.phoneNumber;
    driver.gender = command.gender;
    driver.address = command.address;
    driver.vehicleId = command.vehicleId;
    driver.emergencyContact = command.emergencyContact;
    return driver;
  }
}
export class UpdateDriverCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  vehicleId: string;
  currentUser: UserInfo;
  static fromCommand(command: UpdateDriverCommand): Driver {
    const driver = new Driver();
    driver.id = command.id;
    driver.name = command.name;
    driver.email = command.email;
    driver.phoneNumber = command.phoneNumber;
    driver.gender = command.gender;
    driver.address = command.address;
    driver.emergencyContact = command.emergencyContact;
    return driver;
  }
}
export class UpdateMyProfileCommand {
  // @ApiProperty()
  // @IsNotEmpty()
  // @IsUUID()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  @ApiProperty()
  address: Address;
  // @ApiProperty()
  // @IsNotEmpty()
  // @IsUUID()
  // vehicleId: string;
  currentUser: UserInfo;
  static fromCommand(command: UpdateDriverCommand): Driver {
    const driver = new Driver();
    driver.id = command.id;
    driver.name = command.name;
    driver.email = command.email;
    driver.phoneNumber = command.phoneNumber;
    driver.gender = command.gender;
    driver.address = command.address;
    driver.emergencyContact = command.emergencyContact;
    return driver;
  }
}
export class RegisterDriverCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  // @ApiProperty()
  // email: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  @IsNotEmpty()
  password: string;
  // @ApiProperty()
  // emergencyContact: EmergencyContact;
  // @ApiProperty()
  // address: Address;
  // @ApiProperty()
  // @IsNotEmpty()
  // @IsUUID()
  // vehicleId: string;
  currentUser: UserInfo;
  static fromCommand(command: RegisterDriverCommand): Driver {
    const driver = new Driver();
    driver.name = command.name;
    // driver.email = command.email;
    driver.phoneNumber = command.phoneNumber;
    driver.gender = command.gender;
    // driver.address = command.address;
    // driver.vehicleId = command.vehicleId;
    // driver.emergencyContact = command.emergencyContact;
    return driver;
  }
}
export class UpdateDocument {
  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsDateString()
  licenseDueDate: Date;
  profileImage: FileDto;
  license: FileDto;
  passport: FileDto;
  currentUser: UserInfo;
}

export class ReAssignVehicleCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  vehicleId: string;
  currentUser: UserInfo;
}
export class ArchiveDriverCommand {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class UpdateDriverDatePreferenceCommand {
  id: string;
  @ApiProperty({
    isArray: true,
    enum: DateOfWeek,
  })
  @IsNotEmpty()
  @IsArray()
  @IsEnum(DateOfWeek, {
    each: true,
    message: `Date Preferences must be in [ ${Object.values(DateOfWeek).join(
      ', ',
    )} ]`,
  })
  datePreferences: string[];
}
