import { ApiProperty } from '@nestjs/swagger';
import { BankAccount } from '@provider/domains/drivers/bank-account';
import { BankAccountEntity } from '@provider/persistence/drivers/bank-account.entity';

export class BankAccountResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  accountNumber: string;
  @ApiProperty()
  bankName: string;
  @ApiProperty()
  isPreferred: boolean;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  static fromEntity(bankAccountEntity: BankAccountEntity): BankAccountResponse {
    const bankAccountResponse = new BankAccountResponse();
    bankAccountResponse.id = bankAccountEntity.id;
    bankAccountResponse.accountNumber = bankAccountEntity.accountNumber;
    bankAccountResponse.bankName = bankAccountEntity.bankName;
    bankAccountResponse.isActive = bankAccountEntity.isActive;
    bankAccountResponse.isPreferred = bankAccountEntity.isPreferred;
    bankAccountResponse.driverId = bankAccountEntity.driverId;
    bankAccountResponse.archiveReason = bankAccountEntity.archiveReason;
    bankAccountResponse.createdBy = bankAccountEntity.createdBy;
    bankAccountResponse.updatedBy = bankAccountEntity.updatedBy;
    bankAccountResponse.deletedBy = bankAccountEntity.deletedBy;
    bankAccountResponse.createdAt = bankAccountEntity.createdAt;
    bankAccountResponse.updatedAt = bankAccountEntity.updatedAt;
    bankAccountResponse.deletedAt = bankAccountEntity.deletedAt;
    return bankAccountResponse;
  }
  static fromDomain(bankAccount: BankAccount): BankAccountResponse {
    const bankAccountResponse = new BankAccountResponse();
    bankAccountResponse.id = bankAccount.id;
    bankAccountResponse.accountNumber = bankAccount.accountNumber;
    bankAccountResponse.bankName = bankAccount.bankName;
    bankAccountResponse.isActive = bankAccount.isActive;
    bankAccountResponse.isPreferred = bankAccount.isPreferred;
    bankAccountResponse.driverId = bankAccount.driverId;
    bankAccountResponse.archiveReason = bankAccount.archiveReason;
    bankAccountResponse.createdBy = bankAccount.createdBy;
    bankAccountResponse.updatedBy = bankAccount.updatedBy;
    bankAccountResponse.deletedBy = bankAccount.deletedBy;
    bankAccountResponse.createdAt = bankAccount.createdAt;
    bankAccountResponse.updatedAt = bankAccount.updatedAt;
    bankAccountResponse.deletedAt = bankAccount.deletedAt;
    return bankAccountResponse;
  }
}
