import { BankAccountEntity } from '@provider/persistence/drivers/bank-account.entity';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { AverageRate } from '@libs/common/average-rate';
import {
  ArchiveDriverCommand,
  CreateDriverCommand,
  ReAssignVehicleCommand,
  RegisterDriverCommand,
  UpdateDocument,
  UpdateDriverCommand,
  UpdateDriverDatePreferenceCommand,
  UpdateMyProfileCommand,
} from './driver.commands';
import { DriverRepository } from '@provider/persistence/drivers/driver.repository';
import { DriverResponse } from './driver.response';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import {
  ArchiveBankAccountCommand,
  CreateBankAccountCommand,
  DeleteBankAccountCommand,
  UpdateBankAccountCommand,
} from './bank-account.commands';
import { BankAccountResponse } from './bank-account.response';
import { AccountCommands } from '@account/usecases/accounts/account.usecase.commands';
import { CreateAccountCommand } from '@account/usecases/accounts/account.commands';
import { CredentialType } from '@libs/common/enums';
import { OnEvent, EventEmitter2 } from '@nestjs/event-emitter';
import { UserInfo } from '@account/dtos/user-info.dto';
import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Util } from '@libs/common/util';
import { AssignmentRepository } from '@router/persistence/assignments/assignment.repository';
import { GroupRepository } from '@assignment/persistence/groups/group.repository';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { DriverAssignmentRepository } from '@assignment/persistence/driver-assignments/driver-assignment.repository';
import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';
@Injectable()
export class DriverCommands {
  constructor(
    private driverRepository: DriverRepository,
    @InjectRepository(GroupEntity)
    private groupRepository: Repository<GroupEntity>,
    @InjectRepository(GroupAssignmentEntity)
    private groupAssRepository: Repository<GroupAssignmentEntity>,
    @InjectRepository(DriverAssignmentEntity)
    private driverAssRepository: Repository<DriverAssignmentEntity>,
    private assignmentRepository: AssignmentRepository,
    private driverAssignmentRepository: DriverAssignmentRepository,
    private readonly accountCommands: AccountCommands,
    private readonly eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
    @InjectRepository(BankAccountEntity)
    private bankAccountRepository: Repository<BankAccountEntity>,
  ) {}
  async createDriver(command: CreateDriverCommand): Promise<DriverResponse> {
    const existingDriver = await this.driverRepository.getByPhoneNumber(
      command.phoneNumber,
    );
    if (existingDriver) {
      throw new BadRequestException(
        'Driver already exists with this phone number.',
      );
    }
    const existingVehicle = await this.driverRepository.getByVehicleId(
      command.vehicleId,
    );
    if (existingVehicle) {
      throw new BadRequestException(
        'Vehicle has already been assigned to another driver',
      );
    }
    const driverDomain = CreateDriverCommand.fromCommand(command);
    driverDomain.isApproved = true;
    const driver = await this.driverRepository.insert(driverDomain);
    if (driver) {
      this.eventEmitter.emit('update.driver.rate', {
        driverId: driver.id,
        score: 5.0,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: driver.id,
        modelName: MODELS.DRIVER,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
      const password = '123456';
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = driver.id;
      createAccountCommand.type = CredentialType.Driver;
      createAccountCommand.role = ['driver'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(password);
      createAccountCommand.address = command.address;
      createAccountCommand.gender = command.gender;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      // if (account && account.email) {
      //   this.eventEmitter.emit('send.email.credential', {
      //     name: account.name,
      //     email: account.email,
      //     phoneNumber: account.phoneNumber,
      //     password: password,
      //   });
      // }
    }
    return DriverResponse.fromDomain(driver);
  }
  async registerDriver(
    command: RegisterDriverCommand,
  ): Promise<DriverResponse> {
    const driverDomain = RegisterDriverCommand.fromCommand(command);
    driverDomain.isApproved = false;
    const driver = await this.driverRepository.insert(driverDomain);
    if (driver) {
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = driver.id;
      createAccountCommand.type = CredentialType.Driver;
      createAccountCommand.role = ['driver'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(command.password);
      createAccountCommand.gender = command.gender;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
    }
    return DriverResponse.fromDomain(driver);
  }
  async updateDriver(command: UpdateDriverCommand): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(command.id);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${command.id}`);
    }
    if (
      driverDomain.name !== command.name ||
      driverDomain.phoneNumber !== command.phoneNumber
    ) {
      const updatedGroup = await this.groupRepository.update(
        { driverId: command.id },
        { driverName: command.name, driverPhone: command.phoneNumber },
      );
      const updatedGroupAss = await this.groupAssRepository.update(
        { driverId: command.id },
        { driverName: command.name, driverPhone: command.phoneNumber },
      );
      const updatedDriverAss = await this.driverAssRepository.update(
        { driverId: command.id },
        { driverName: command.name, driverPhone: command.phoneNumber },
      );
    }
    const oldPayload = driverDomain;
    driverDomain.email = command.email;
    driverDomain.name = command.name;
    driverDomain.address = command.address;
    driverDomain.phoneNumber = command.phoneNumber;
    driverDomain.gender = command.gender;
    driverDomain.emergencyContact = command.emergencyContact;
    driverDomain.vehicleId = command.vehicleId;
    const driver = await this.driverRepository.update(driverDomain);
    if (driver) {
      this.eventEmitter.emit('update.account', {
        accountId: driverDomain.id,
        name: driver.name,
        email: driver.email,
        type: CredentialType.Driver,
        phoneNumber: driver.phoneNumber,
        address: driver.address,
        gender: driver.gender,
        profileImage: driver.profileImage,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.DRIVER,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: driverDomain,
        oldPayload,
      });
    }
    return DriverResponse.fromDomain(driver);
  }
  async updateDriverDatePreference(
    command: UpdateDriverDatePreferenceCommand,
  ): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(command.id);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${command.id}`);
    }
    driverDomain.datePreferences = command.datePreferences;
    const driver = await this.driverRepository.update(driverDomain);
    return DriverResponse.fromDomain(driver);
  }

  async updateDriverProfile(
    command: UpdateMyProfileCommand,
  ): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(command.id);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${command.id}`);
    }
    const oldPayload = driverDomain;
    driverDomain.email = command.email;
    driverDomain.name = command.name;
    driverDomain.address = command.address;
    driverDomain.phoneNumber = command.phoneNumber;
    driverDomain.gender = command.gender;
    driverDomain.emergencyContact = command.emergencyContact;
    const driver = await this.driverRepository.update(driverDomain);
    if (driver) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.DRIVER,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: driverDomain,
        oldPayload,
      });
    }
    return DriverResponse.fromDomain(driver);
  }

  async reAssignVehicle(
    command: ReAssignVehicleCommand,
  ): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(command.id);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${command.id}`);
    }
    const oldPayload = driverDomain;
    driverDomain.vehicleId = command.vehicleId;
    const driver = await this.driverRepository.update(driverDomain);
    if (driver) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.DRIVER,
        action: ACTIONS.ReAssignVehicle,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: driverDomain,
        oldPayload,
      });
    }
    return DriverResponse.fromDomain(driver);
  }
  async archiveDriver(
    archiveCommand: ArchiveDriverCommand,
  ): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(archiveCommand.id);
    if (!driverDomain) {
      throw new NotFoundException(
        `Driver not found with id ${archiveCommand.id}`,
      );
    }
    const assignments =
      await this.assignmentRepository.getActiveAssignmentsByDriverId(
        archiveCommand.id,
      );
    const driverAssignments =
      await this.driverAssignmentRepository.checkIfDriverIsAssigned(
        archiveCommand.id,
      );
    if (assignments || driverAssignments) {
      throw new BadRequestException(
        'Driver can not be archived because it has active assignments',
      );
    }
    driverDomain.deletedAt = new Date();
    driverDomain.vehicleId = null;
    driverDomain.vehicle = null;
    driverDomain.deletedBy = archiveCommand.currentUser.id;
    driverDomain.archiveReason = archiveCommand.reason;
    const result = await this.driverRepository.update(driverDomain);
    if (result) {
      this.eventEmitter.emit('account.archived', {
        phoneNumber: driverDomain.phoneNumber,
        id: driverDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: archiveCommand.id,
        modelName: MODELS.DRIVER,
        action: ACTIONS.ARCHIVE,
        userId: archiveCommand.currentUser.id,
        user: archiveCommand.currentUser,
      });
    }
    return DriverResponse.fromDomain(result);
  }
  async restoreDriver(
    id: string,
    currentUser: UserInfo,
  ): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(id, true);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${id}`);
    }
    const r = await this.driverRepository.restore(id);
    if (r) {
      this.eventEmitter.emit('account.restored', {
        phoneNumber: driverDomain.phoneNumber,
        id: driverDomain.id,
      });
      driverDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.DRIVER,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return DriverResponse.fromDomain(driverDomain);
  }
  async deleteDriver(id: string, currentUser: UserInfo): Promise<boolean> {
    const driverDomain = await this.driverRepository.getById(id, true);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${id}`);
    }
    const assignments =
      await this.assignmentRepository.getActiveAssignmentsByDriverId(id);
    const driverAssignments =
      await this.driverAssignmentRepository.checkIfDriverIsAssigned(id);
    if (assignments || driverAssignments) {
      throw new BadRequestException(
        'Driver can not be deleted because it has active assignments',
      );
    }
    const result = await this.driverRepository.delete(id);
    if (result) {
      this.eventEmitter.emit('account.deleted', {
        phoneNumber: driverDomain.phoneNumber,
        id: driverDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.DRIVER,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
  async addBankAccount(
    command: CreateBankAccountCommand,
  ): Promise<BankAccountResponse> {
    const driverDomain = await this.driverRepository.getById(command.driverId);
    if (!driverDomain) {
      throw new NotFoundException(
        `Driver does not found with id ${command.driverId}`,
      );
    }
    const existingBankAccount = await this.bankAccountRepository.findOneBy({
      accountNumber: command.accountNumber,
    });
    if (existingBankAccount) {
      throw new BadRequestException(`Bank Account already taken by someone`);
    }
    const isExist = driverDomain.bankAccounts.find(
      (bankAccount) =>
        bankAccount.accountNumber === command.accountNumber ||
        bankAccount.bankName === command.bankName,
    );
    if (isExist) {
      throw new BadRequestException(
        `Bank account already exist with this bank account ${command.accountNumber} or with bank name ${command.bankName}`,
      );
    }
    const bankAccount = CreateBankAccountCommand.fromCommand(command);
    driverDomain.addBankAccount(bankAccount);
    const result = await this.driverRepository.update(driverDomain);
    if (!result) return null;

    const response = BankAccountResponse.fromDomain(
      result.bankAccounts[result.bankAccounts.length - 1],
    );
    this.eventEmitter.emit('activity-logger.store', {
      modelId: response.id,
      modelName: MODELS.BANKACCOUNT,
      action: ACTIONS.CREATE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return response;
  }
  async updateBankAccount(
    command: UpdateBankAccountCommand,
  ): Promise<BankAccountResponse> {
    const driverDomain = await this.driverRepository.getById(command.driverId);
    if (!driverDomain) {
      throw new NotFoundException(
        `Driver does not found with id ${command.driverId}`,
      );
    }
    const oldPayload = driverDomain.bankAccounts.find(
      (b) => b.id === command.id,
    );
    if (oldPayload.accountNumber !== command.accountNumber) {
      const existingBankAccount = await this.bankAccountRepository.findOneBy({
        accountNumber: command.accountNumber,
      });
      if (existingBankAccount) {
        throw new BadRequestException(`Bank Account already taken by someone`);
      }
      // const isExist = driverDomain.bankAccounts.find(
      //   (bankAccount) =>
      //     bankAccount.accountNumber === command.accountNumber ||
      //     bankAccount.bankName === command.bankName,
      // );
      // if (isExist) {
      //   throw new BadRequestException(
      //     `Bank account already exist with this bank account ${command.accountNumber} or with bank name ${command.bankName}`,
      //   );
      // }
    }

    const bankAccount = UpdateBankAccountCommand.fromCommand(command);
    driverDomain.updateBankAccount(bankAccount);
    const result = await this.driverRepository.update(driverDomain);
    if (!result) return null;

    const response = BankAccountResponse.fromDomain(
      result.bankAccounts.find((bankAccount) => bankAccount.id === command.id),
    );
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.BANKACCOUNT,
      action: ACTIONS.UPDATE,
      userId: command.currentUser.id,
      user: command.currentUser,
      payload: bankAccount,
      oldPayload,
    });
    return response;
  }
  async deleteBankAccount(command: DeleteBankAccountCommand): Promise<boolean> {
    const bankAccount = await this.bankAccountRepository.find({
      where: { id: command.id },
      withDeleted: true,
    });
    if (!bankAccount[0]) {
      throw new NotFoundException(
        `Bank Account not found with id ${command.id}`,
      );
    }
    const result = await this.bankAccountRepository.delete({ id: command.id });
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.BANKACCOUNT,
        action: ACTIONS.DELETE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  async archiveBankAccount(
    command: ArchiveBankAccountCommand,
  ): Promise<boolean> {
    const driverDomain = await this.driverRepository.getById(command.driverId);
    if (!driverDomain) {
      throw new NotFoundException(
        `Driver does not found with id ${command.driverId}`,
      );
    }
    const bankAccount = driverDomain.bankAccounts.find(
      (bankAccount) => bankAccount.id === command.id,
    );
    bankAccount.deletedAt = new Date();
    bankAccount.deletedBy = command.currentUser.id;
    bankAccount.archiveReason = command.reason;
    driverDomain.updateBankAccount(bankAccount);
    const result = await this.driverRepository.update(driverDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.BANKACCOUNT,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  @OnEvent('update.driver.rate')
  async updateDriverAvgRate(data: { driverId: string; score: number }) {
    const driverDomain = await this.driverRepository.getById(data.driverId);
    if (driverDomain) {
      const currentAvgRate = driverDomain?.averageRate?.rate
        ? driverDomain.averageRate.rate
        : 0;
      const currentTotalReview = driverDomain?.averageRate?.totalReviews
        ? driverDomain.averageRate.totalReviews
        : 0;
      const newTotalReviews = currentTotalReview + 1;
      const newScore =
        (currentTotalReview * currentAvgRate + data.score) / newTotalReviews;
      const newAvgRate: AverageRate = {
        rate: newScore,
        totalReviews: newTotalReviews,
      };
      driverDomain.averageRate = newAvgRate;
      await this.driverRepository.update(driverDomain);
    }
  }
  async activateOrBlockDriver(
    id: string,
    currentUser: UserInfo,
  ): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(id);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${id}`);
    }
    const accountStatus = this.eventEmitter.emit('account.activate-or-block', {
      phoneNumber: driverDomain.phoneNumber,
      id: driverDomain.id,
    });
    if (accountStatus) {
      driverDomain.enabled = !driverDomain.enabled;
      const result = await this.driverRepository.update(driverDomain);
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.DRIVER,
        action: driverDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
      return DriverResponse.fromDomain(result);
    }
  }
  async updateDocument(command: UpdateDocument): Promise<DriverResponse> {
    const driverDomain = await this.driverRepository.getById(command.id);
    if (!driverDomain) {
      throw new NotFoundException(`Driver not found with id ${command.id}`);
    }
    if (command.license) {
      if (driverDomain.license) {
        await this.fileManagerService.removeFile(
          driverDomain.license,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      driverDomain.license = command.license;
    }
    if (command.profileImage) {
      if (driverDomain.profileImage) {
        await this.fileManagerService.removeFile(
          driverDomain.profileImage,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      driverDomain.profileImage = command.profileImage;
      this.eventEmitter.emit('update-account-profile', {
        id: driverDomain.id,
        profileImage: driverDomain.profileImage,
      });
    }
    if (command.passport) {
      if (driverDomain.passport) {
        await this.fileManagerService.removeFile(
          driverDomain.passport,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      driverDomain.passport = command.passport;
    }
    driverDomain.licenseDueDate = command.licenseDueDate;
    driverDomain.updatedBy = command.currentUser.id;
    const driver = await this.driverRepository.update(driverDomain);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.DRIVER,
      action: ACTIONS.UPDATEDOCUMENT,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return DriverResponse.fromDomain(driver);
  }
  async restoreDriverBankAccount(
    command: DeleteBankAccountCommand,
  ): Promise<BankAccountResponse> {
    const bankAccount = await this.bankAccountRepository.find({
      where: { id: command.id },
      withDeleted: true,
    });
    if (!bankAccount[0]) {
      throw new NotFoundException(
        `Bank Account not found with id ${command.id}`,
      );
    }
    bankAccount[0].deletedAt = null;
    const result = await this.bankAccountRepository.save(bankAccount[0]);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.BANKACCOUNT,
      action: ACTIONS.RESTORE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });

    return BankAccountResponse.fromEntity(result);
  }
  async isDriverExist(phoneNumber: string) {
    const driver = await this.driverRepository.getByPhoneNumber(phoneNumber);
    return driver ? true : false;
  }
}
