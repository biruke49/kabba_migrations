import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { BankAccount } from '@provider/domains/drivers/bank-account';
import { IsNotEmpty } from 'class-validator';
export class CreateBankAccountCommand {
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  accountNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  bankName: string;
  @ApiProperty()
  isPreferred: boolean;
  @ApiProperty()
  isActive: boolean;
  currentUser: UserInfo;
  static fromCommand(createBankAccount: CreateBankAccountCommand): BankAccount {
    const bankAccount = new BankAccount();
    bankAccount.driverId = createBankAccount.driverId;
    bankAccount.accountNumber = createBankAccount.accountNumber;
    bankAccount.bankName = createBankAccount.bankName;
    bankAccount.isPreferred = createBankAccount.isPreferred;
    bankAccount.isActive = createBankAccount.isActive;
    return bankAccount;
  }
}
export class UpdateBankAccountCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  accountNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  bankName: string;
  @ApiProperty()
  isPreferred: boolean;
  @ApiProperty()
  @IsNotEmpty()
  isActive: boolean;
  currentUser: UserInfo;
  static fromCommand(updateBankAccount: UpdateBankAccountCommand): BankAccount {
    const bankAccount = new BankAccount();
    bankAccount.id = updateBankAccount.id;
    bankAccount.driverId = updateBankAccount.driverId;
    bankAccount.accountNumber = updateBankAccount.accountNumber;
    bankAccount.bankName = updateBankAccount.bankName;
    bankAccount.isPreferred = updateBankAccount.isPreferred;
    bankAccount.isActive = updateBankAccount.isActive;
    return bankAccount;
  }
}
export class DeleteBankAccountCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  currentUser: UserInfo;
}
export class ArchiveBankAccountCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
