import { PreferenceResponse } from '@interaction/usecases/preferences/preference.response';
import { BankAccountResponse } from './bank-account.response';
import { FileDto } from '@libs/common/file-dto';
import { Address } from '@libs/common/address';
import { AverageRate } from '@libs/common/average-rate';
import { ApiProperty } from '@nestjs/swagger';
import { Driver } from '@provider/domains/drivers/driver';
import { VehicleResponse } from '../owners/vehicle.response';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { EmergencyContact } from '@libs/common/emergency-contact';
export class DriverResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  profileImage: FileDto;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  isActive: boolean;
  @ApiProperty()
  vehicleId: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  passport: FileDto;
  @ApiProperty()
  license: FileDto;
  @ApiProperty()
  licenseDueDate: Date;
  @ApiProperty()
  fcmId: string;
  @ApiProperty()
  averageRate: AverageRate;
  @ApiProperty()
  lat: number;
  @ApiProperty()
  lng: number;
  @ApiProperty()
  isApproved: boolean;
  @ApiProperty()
  datePreferences: string[];
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty({ isArray: true, type: BankAccountResponse })
  bankAccounts: BankAccountResponse[];
  @ApiProperty()
  vehicle: VehicleResponse;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  preferences: PreferenceResponse[];
  static fromEntity(driverEntity: DriverEntity): DriverResponse {
    const driverResponse = new DriverResponse();
    driverResponse.id = driverEntity.id;
    driverResponse.name = driverEntity.name;
    driverResponse.email = driverEntity.email;
    driverResponse.phoneNumber = driverEntity.phoneNumber;
    driverResponse.gender = driverEntity.gender;
    driverResponse.enabled = driverEntity.enabled;
    driverResponse.profileImage = driverEntity.profileImage;
    driverResponse.address = driverEntity.address;
    driverResponse.license = driverEntity.license;
    driverResponse.passport = driverEntity.passport;
    driverResponse.isActive = driverEntity.isActive;
    driverResponse.licenseDueDate = driverEntity.licenseDueDate;
    driverResponse.emergencyContact = driverEntity.emergencyContact;
    driverResponse.lat = driverEntity.lat;
    driverResponse.lng = driverEntity.lng;
    driverResponse.vehicleId = driverEntity.vehicleId;
    driverResponse.isApproved = driverEntity.isApproved;
    driverResponse.archiveReason = driverEntity.archiveReason;
    driverResponse.datePreferences = driverEntity.datePreferences;
    driverResponse.createdBy = driverEntity.createdBy;
    driverResponse.updatedBy = driverEntity.updatedBy;
    driverResponse.deletedBy = driverEntity.deletedBy;
    driverResponse.createdAt = driverEntity.createdAt;
    driverResponse.updatedAt = driverEntity.updatedAt;
    driverResponse.deletedAt = driverEntity.deletedAt;
    driverResponse.bankAccounts = driverEntity.bankAccounts
      ? driverEntity.bankAccounts.map((bankAccount) => {
          return BankAccountResponse.fromEntity(bankAccount);
        })
      : [];
    if (driverEntity.preferences) {
      driverResponse.preferences = driverEntity.preferences.map((preference) =>
        PreferenceResponse.fromEntity(preference),
      );
    }
    if (driverEntity.averageRate) {
      driverResponse.averageRate = driverEntity.averageRate;
    }
    if (driverEntity.vehicle) {
      driverResponse.vehicle = VehicleResponse.fromEntity(driverEntity.vehicle);
    }
    return driverResponse;
  }
  static fromDomain(driver: Driver): DriverResponse {
    const driverResponse = new DriverResponse();
    driverResponse.id = driver.id;
    driverResponse.name = driver.name;
    driverResponse.email = driver.email;
    driverResponse.phoneNumber = driver.phoneNumber;
    driverResponse.gender = driver.gender;
    driverResponse.enabled = driver.enabled;
    driverResponse.profileImage = driver.profileImage;
    driverResponse.address = driver.address;
    driverResponse.license = driver.license;
    driverResponse.passport = driver.passport;
    driverResponse.isActive = driver.isActive;
    driverResponse.licenseDueDate = driver.licenseDueDate;
    driverResponse.emergencyContact = driver.emergencyContact;
    driverResponse.lat = driver.lat;
    driverResponse.lng = driver.lng;
    driverResponse.vehicleId = driver.vehicleId;
    driverResponse.license = driver.license;
    driverResponse.isApproved = driver.isApproved;
    driverResponse.datePreferences = driver.datePreferences;
    driverResponse.archiveReason = driver.archiveReason;
    driverResponse.createdBy = driver.createdBy;
    driverResponse.updatedBy = driver.updatedBy;
    driverResponse.deletedBy = driver.deletedBy;
    driverResponse.createdAt = driver.createdAt;
    driverResponse.updatedAt = driver.updatedAt;
    driverResponse.deletedAt = driver.deletedAt;
    driverResponse.bankAccounts = driver.bankAccounts
      ? driver.bankAccounts.map((bankAccount) => {
          return BankAccountResponse.fromDomain(bankAccount);
        })
      : [];
    if (driver.vehicle) {
      driverResponse.vehicle = VehicleResponse.fromDomain(driver.vehicle);
    }
    if (driver.averageRate) {
      driverResponse.averageRate = driver.averageRate;
    }
    if (driver.preferences) {
      driverResponse.preferences = driver.preferences.map((preference) =>
        PreferenceResponse.fromDomain(preference),
      );
    }
    return driverResponse;
  }
}
