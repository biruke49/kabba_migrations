import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { UserInfo } from '@account/dtos/user-info.dto';
import { DriverFeeRepository } from '@provider/persistence/driver-fees/driver-fee.repository';
import {
  ArchiveDriverFeeCommand,
  ChangeDriverFeeStatusCommand,
  CreateDriverFeeCommand,
} from './driver-fee.commands';
import { DriverFeeResponse } from './driver-fee.response';
@Injectable()
export class DriverFeeCommands {
  constructor(
    private driverFeeRepository: DriverFeeRepository,
    private readonly eventEmitter: EventEmitter2,
  ) {}
  async createDriverFee(
    command: CreateDriverFeeCommand,
  ): Promise<DriverFeeResponse> {
    command.fee = Number(command.fee.toFixed(2));
    const driverFeeDomain = CreateDriverFeeCommand.fromCommand(command);
    const driverFee = await this.driverFeeRepository.insert(driverFeeDomain);
    if (driverFee) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: driverFee.id,
        modelName: MODELS.DRIVERFEE,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return DriverFeeResponse.fromDomain(driverFee);
  }
  @OnEvent('change.driver.fee.status')
  async changeDriverFeeStatus(
    command: ChangeDriverFeeStatusCommand,
  ): Promise<DriverFeeResponse[]> {
    const results = [];
    for (const driverFeeId of command.driverFeeIds) {
      const driverFeeDomain = await this.driverFeeRepository.getByDriverId(
        driverFeeId,
      );
      if (!driverFeeDomain) {
        throw new NotFoundException(
          `Driver fee not found with ID ${driverFeeId}`,
        );
      }
      driverFeeDomain.paymentStatus = command.paymentStatus;
      driverFeeDomain.transactionNumber = command?.transactionNumber;
      driverFeeDomain.paidById = command.currentUser.id;
      const result = await this.driverFeeRepository.update(driverFeeDomain);
      if (result) {
        this.eventEmitter.emit('change.driver.fee.assignment.status', {
          assignmentIds: driverFeeDomain.assignmentIds,
          status: command.paymentStatus,
          currentUser: command.currentUser,
        });
        this.eventEmitter.emit('activity-logger.store', {
          modelId: driverFeeId,
          modelName: MODELS.DRIVERFEE,
          action: ACTIONS.UPDATE,
          userId: command.currentUser.id,
          user: command.currentUser,
        });
      }
      results.push(DriverFeeResponse.fromDomain(result));
    }
    return results;
  }
  async archiveDriverFee(
    archiveCommand: ArchiveDriverFeeCommand,
  ): Promise<boolean> {
    const driverFeeDomain = await this.driverFeeRepository.getById(
      archiveCommand.id,
    );
    if (!driverFeeDomain) {
      throw new NotFoundException(
        `DriverFee not found with id ${archiveCommand.id}`,
      );
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: archiveCommand.id,
      modelName: MODELS.DRIVERFEE,
      action: ACTIONS.ARCHIVE,
      userId: archiveCommand.currentUser.id,
      user: archiveCommand.currentUser,
      reason: archiveCommand.reason,
    });
    return await this.driverFeeRepository.archive(archiveCommand.id);
  }
  async restoreDriverFee(
    id: string,
    currentUser: UserInfo,
  ): Promise<DriverFeeResponse> {
    const driverFeeDomain = await this.driverFeeRepository.getById(id, true);
    if (!driverFeeDomain) {
      throw new NotFoundException(`DriverFee not found with id ${id}`);
    }
    const r = await this.driverFeeRepository.restore(id);
    if (r) {
      driverFeeDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.DRIVERFEE,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return DriverFeeResponse.fromDomain(driverFeeDomain);
  }
  async deleteDriverFee(id: string, currentUser: UserInfo): Promise<boolean> {
    const driverFeeDomain = await this.driverFeeRepository.getById(id, true);
    if (!driverFeeDomain) {
      throw new NotFoundException(`DriverFee not found with id ${id}`);
    }
    const result = await this.driverFeeRepository.delete(id);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.DRIVERFEE,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
}
