import { AssignmentResponse } from '@router/usecases/assignments/assignment.response';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DriverFeeEntity } from '@provider/persistence/driver-fees/driver-fee.entity';
import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { Repository } from 'typeorm';
import { DriverFeeResponse } from './driver-fee.response';
@Injectable()
export class DriverFeeQuery {
  constructor(
    @InjectRepository(DriverFeeEntity)
    private readonly driverFeeRepository: Repository<DriverFeeEntity>,
    @InjectRepository(AssignmentEntity)
    private readonly assignmentRepository: Repository<AssignmentEntity>,
  ) {}
  async getDriverFee(id: string): Promise<DriverFeeResponse> {
    const driverFee = await this.driverFeeRepository.find({
      where: { id: id },
      relations: ['driver', 'driver.vehicle'],
    });
    if (!driverFee[0]) {
      throw new NotFoundException(`DriverFee not found with id ${id}`);
    }
    const item = driverFee[0];
    const response = DriverFeeResponse.fromEntity(item);
    if (item.assignmentIds) {
      const query = new CollectionQuery();

      query.filter = [
        [
          {
            field: 'id',
            operator: FilterOperators.In,
            value: item.assignmentIds,
          },
        ],
      ];
      const dataQuery = QueryConstructor.constructQuery<AssignmentEntity>(
        this.assignmentRepository,
        query,
      );
      const result = await dataQuery.getMany();
      const categories = result.map((entity) =>
        AssignmentResponse.fromEntity(entity),
      );
      response.assignments = categories;
    }
    return response;
  }
  async getDriverFees(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverFeeResponse>> {
    const dataQuery = QueryConstructor.constructQuery<DriverFeeEntity>(
      this.driverFeeRepository,
      query,
    );
    const d = new DataResponseFormat<DriverFeeResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => DriverFeeResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getDriverEarning(
    driverId: string,
    query: CollectionQuery,
  ): Promise<any> {
    const today = new Date('Y-m-d');
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        value: driverId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverFeeEntity>(
      this.driverFeeRepository,
      query,
    );
    const data = await dataQuery.select('SUM(fee)', 'total').getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async getDriverFeesByDriverId(
    driverId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverFeeResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'driverId',
        operator: FilterOperators.EqualTo,
        value: driverId,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverFeeEntity>(
      this.driverFeeRepository,
      query,
    );
    const d = new DataResponseFormat<DriverFeeResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => DriverFeeResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getTotalDriverFees(query: CollectionQuery): Promise<any> {
    const dataQuery = QueryConstructor.constructQuery<DriverFeeEntity>(
      this.driverFeeRepository,
      query,
    );
    const data = await dataQuery.select('SUM(fee)', 'total').getRawOne();
    const total = data.total != null ? data.total : 0;
    return { total: total };
  }
  async getArchivedDriverFees(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<DriverFeeResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<DriverFeeEntity>(
      this.driverFeeRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<DriverFeeResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => DriverFeeResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
