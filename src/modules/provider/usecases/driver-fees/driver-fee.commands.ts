import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { DriverFee } from '@provider/domains/driver-fees/driver-fee';
import { IsNotEmpty, IsNumber, IsPositive, IsArray } from 'class-validator';

export class CreateDriverFeeCommand {
  @ApiProperty()
  @IsNotEmpty()
  driverId: string;
  @ApiProperty()
  paymentMethod: string;
  @ApiProperty({
    format: 'double',
  })
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  fee: number;
  // @ApiProperty()
  // @IsNotEmpty()
  accountNumber?: string;
  // @ApiProperty()
  // @IsNotEmpty()
  // toDate: Date;
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  assignmentIds: string[];
  @ApiProperty()
  paymentStatus: string;
  bankName?: string;
  currentUser: UserInfo;
  static fromCommand(command: CreateDriverFeeCommand): DriverFee {
    const driverFee = new DriverFee();
    driverFee.driverId = command.driverId;
    driverFee.paymentMethod = command.paymentMethod;
    driverFee.fee = command.fee;
    driverFee.accountNumber = command?.accountNumber;
    driverFee.bankName = command?.bankName;
    // driverFee.accountNumber = command.accountNumber;
    // driverFee.toDate = command.toDate;
    driverFee.assignmentIds = command.assignmentIds;
    driverFee.paymentStatus = command.paymentStatus;
    driverFee.createdBy = command.currentUser.id;
    return driverFee;
  }
}
export class DeleteDriverFeeCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  currentUser: UserInfo;
}
export class ArchiveDriverFeeCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class RestoreDriverFeeCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  currentUser: UserInfo;
}
export class ChangeDriverFeeStatusCommand {
  @ApiProperty()
  @IsNotEmpty()
  driverFeeIds: string[];
  @ApiProperty()
  @IsNotEmpty()
  paymentStatus: string;
  @ApiProperty()
  transactionNumber?: string;
  currentUser: UserInfo;
}
