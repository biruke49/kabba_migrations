import { AssignmentResponse } from '@router/usecases/assignments/assignment.response';
import { ApiProperty } from '@nestjs/swagger';
import { DriverFee } from '@provider/domains/driver-fees/driver-fee';
import { DriverFeeEntity } from '@provider/persistence/driver-fees/driver-fee.entity';
import { DriverResponse } from '../drivers/driver.response';
import { UserResponse } from '@user/usecases/users/user.response';

export class DriverFeeResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  paymentMethod: string;
  @ApiProperty()
  fee: number;
  // @ApiProperty()
  // fromDate: Date;
  // @ApiProperty()
  // toDate: Date;
  @ApiProperty()
  assignmentIds: string[];
  @ApiProperty()
  paymentStatus: string;
  @ApiProperty()
  accountNumber?: string;
  @ApiProperty()
  transactionNumber?: string;
  @ApiProperty()
  paidById?: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  deletedAt: Date;
  @ApiProperty()
  deletedBy: string;
  @ApiProperty()
  assignments: AssignmentResponse[];
  driver: DriverResponse;
  paidBy: UserResponse;

  static fromEntity(driverFeeEntity: DriverFeeEntity): DriverFeeResponse {
    const driverFeeResponse = new DriverFeeResponse();
    driverFeeResponse.id = driverFeeEntity.id;
    driverFeeResponse.driverId = driverFeeEntity.driverId;
    driverFeeResponse.paymentMethod = driverFeeEntity.paymentMethod;
    driverFeeResponse.fee = driverFeeEntity.fee;
    if (driverFeeEntity.driver) {
      driverFeeResponse.driver = DriverResponse.fromEntity(
        driverFeeEntity.driver,
      );
    }
    if (driverFeeEntity.paidBy) {
      driverFeeResponse.paidBy = UserResponse.fromEntity(
        driverFeeEntity.paidBy,
      );
    }
    driverFeeResponse.assignmentIds = driverFeeEntity.assignmentIds;
    driverFeeResponse.paymentStatus = driverFeeEntity.paymentStatus;
    driverFeeResponse.accountNumber = driverFeeEntity?.accountNumber;
    driverFeeResponse.transactionNumber = driverFeeEntity?.transactionNumber;
    driverFeeResponse.paidById = driverFeeEntity?.paidById;
    driverFeeResponse.createdBy = driverFeeEntity.createdBy;
    driverFeeResponse.deletedBy = driverFeeEntity.deletedBy;
    driverFeeResponse.createdAt = driverFeeEntity.createdAt;
    driverFeeResponse.deletedAt = driverFeeEntity.deletedAt;
    return driverFeeResponse;
  }
  static fromDomain(driverFee: DriverFee): DriverFeeResponse {
    const driverFeeResponse = new DriverFeeResponse();
    driverFeeResponse.id = driverFee.id;
    driverFeeResponse.driverId = driverFee.driverId;
    driverFeeResponse.paymentMethod = driverFee.paymentMethod;
    driverFeeResponse.fee = driverFee.fee;
    if (driverFee.driver) {
      driverFeeResponse.driver = DriverResponse.fromDomain(driverFee.driver);
    }
    if (driverFee.paidBy) {
      driverFeeResponse.paidBy = UserResponse.fromDomain(driverFee.paidBy);
    }
    driverFeeResponse.assignmentIds = driverFee.assignmentIds;
    driverFeeResponse.paymentStatus = driverFee.paymentStatus;
    driverFeeResponse.accountNumber = driverFee?.accountNumber;
    driverFeeResponse.transactionNumber = driverFee?.transactionNumber;
    driverFeeResponse.paidById = driverFee?.paidById;
    driverFeeResponse.createdBy = driverFee.createdBy;
    driverFeeResponse.deletedBy = driverFee.deletedBy;
    driverFeeResponse.createdAt = driverFee.createdAt;
    driverFeeResponse.deletedAt = driverFee.deletedAt;
    return driverFeeResponse;
  }
}

export class DriverPaymentExcelExport {
  driverName: string;
  accountNumber: string;
  totalPayment: number;
}
