import { FileDto } from '@libs/common/file-dto';
import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsUUID,
} from 'class-validator';
import { Owner } from '@provider/domains/owners/owner';
export class OwnerAddressCommand {
  @ApiProperty()
  @IsNotEmpty()
  country: string;
  @ApiProperty()
  @IsNotEmpty()
  city: string;
  @ApiProperty()
  subCity: string;
  @ApiProperty()
  woreda: string;
  @ApiProperty()
  @IsNotEmpty()
  houseNumber: string;
}
export class CreateOwnerCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  @IsEmail()
  email: string;
  @ApiProperty({
    example: '+251911111111',
  })
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  @IsNotEmpty()
  tinNumber: string;
  @ApiProperty({
    example: {
      country: 'Ethiopia',
      city: 'Addis Ababa',
      subCity: 'Kolfe',
      woreda: '01',
      houseNumber: '1000002',
    },
  })
  @IsNotEmpty()
  address: OwnerAddressCommand;
  @ApiProperty()
  website: string;
  @ApiProperty()
  @IsBoolean()
  isCompany: boolean;
  currentUser: UserInfo;
  static fromCommand(command: CreateOwnerCommand): Owner {
    const ownerDomain = new Owner();
    ownerDomain.name = command.name;
    ownerDomain.email = command.email;
    ownerDomain.phoneNumber = command.phoneNumber;
    ownerDomain.gender = command.gender;
    ownerDomain.tinNumber = command.tinNumber;
    ownerDomain.address = { ...command.address };
    ownerDomain.website = command.website;
    ownerDomain.isCompany = command.isCompany;
    return ownerDomain;
  }
}
export class UpdateOwnerCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  @IsEmail()
  email: string;
  @ApiProperty({
    example: '+251911111111',
  })
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty({
    example: {
      country: 'Ethiopia',
      city: 'Addis Ababa',
      subCity: 'Kolfe',
      woreda: '01',
      houseNumber: '1000002',
    },
  })
  @IsNotEmpty()
  address: OwnerAddressCommand;
  @ApiProperty()
  @IsNotEmpty()
  tinNumber: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  website: string;
  @ApiProperty()
  @IsBoolean()
  isCompany: boolean;
  currentUser: UserInfo;
}
export class UpdateDocument {
  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  licenseDueDate: Date;
  logo: FileDto;
  license: FileDto;
  currentUser: UserInfo;
}
export class ArchiveOwnerCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
