import { OwnerResponse } from '@provider/usecases/owners/owner.response';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import { Vehicle } from '@provider/domains/owners/vehicle';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import { CategoryResponse } from '@classification/usecases/categories/category.response';
export class VehicleResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  code: string;
  @ApiProperty()
  plateNumber: string;
  @ApiProperty()
  model: string;
  @ApiProperty()
  categoryId: string;
  @ApiProperty()
  color: string;
  @ApiProperty()
  bolo: FileDto;
  @ApiProperty()
  boloDueDate: Date;
  @ApiProperty()
  insurance: FileDto;
  @ApiProperty()
  insuranceCertificateDueDate: Date;
  @ApiProperty()
  ownerId: string;
  @ApiProperty()
  ownerName: string;
  @ApiProperty()
  ownerPhoneNumber: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  //@ApiProperty() do not uncomment this,  it will throw swagger circular dependency  error
  owner: OwnerResponse;
  //@ApiProperty() do not uncomment this,  it will throw swagger circular dependency  error
  driver: DriverResponse;
  category: CategoryResponse;
  static fromEntity(vehicleEntity: VehicleEntity): VehicleResponse {
    const vehicleResponse = new VehicleResponse();
    vehicleResponse.id = vehicleEntity.id;
    vehicleResponse.code = vehicleEntity.code;
    vehicleResponse.model = vehicleEntity.model;
    vehicleResponse.ownerId = vehicleEntity.ownerId;
    vehicleResponse.plateNumber = vehicleEntity.plateNumber;
    vehicleResponse.enabled = vehicleEntity.enabled;
    vehicleResponse.categoryId = vehicleEntity.categoryId;
    vehicleResponse.color = vehicleEntity.color;
    vehicleResponse.ownerName = vehicleEntity.ownerName;
    vehicleResponse.ownerPhoneNumber = vehicleEntity.ownerPhoneNumber;
    vehicleResponse.bolo = vehicleEntity.bolo;
    vehicleResponse.boloDueDate = vehicleEntity.boloDueDate;
    vehicleResponse.insurance = vehicleEntity.insurance;
    vehicleResponse.insuranceCertificateDueDate =
      vehicleEntity.insuranceCertificateDueDate;
    vehicleResponse.archiveReason = vehicleEntity.archiveReason;
    vehicleResponse.createdBy = vehicleEntity.createdBy;
    vehicleResponse.updatedBy = vehicleEntity.updatedBy;
    vehicleResponse.deletedBy = vehicleEntity.deletedBy;
    vehicleResponse.createdAt = vehicleEntity.createdAt;
    vehicleResponse.updatedAt = vehicleEntity.updatedAt;
    vehicleResponse.deletedAt = vehicleEntity.deletedAt;
    if (vehicleEntity.driver) {
      vehicleResponse.driver = DriverResponse.fromEntity(vehicleEntity.driver);
    }
    if (vehicleEntity.owner) {
      vehicleResponse.owner = OwnerResponse.fromEntity(vehicleEntity.owner);
    }
    if (vehicleEntity.category) {
      vehicleResponse.category = CategoryResponse.fromEntity(
        vehicleEntity.category,
      );
    }
    return vehicleResponse;
  }
  static fromDomain(vehicle: Vehicle): VehicleResponse {
    const vehicleResponse = new VehicleResponse();
    vehicleResponse.id = vehicle.id;
    vehicleResponse.code = vehicle.code;
    vehicleResponse.model = vehicle.model;
    vehicleResponse.ownerId = vehicle.ownerId;
    vehicleResponse.plateNumber = vehicle.plateNumber;
    vehicleResponse.enabled = vehicle.enabled;
    vehicleResponse.categoryId = vehicle.categoryId;
    vehicleResponse.color = vehicle.color;
    vehicleResponse.ownerName = vehicle.ownerName;
    vehicleResponse.ownerPhoneNumber = vehicle.ownerPhoneNumber;
    vehicleResponse.bolo = vehicle.bolo;
    vehicleResponse.boloDueDate = vehicle.boloDueDate;
    vehicleResponse.insurance = vehicle.insurance;
    vehicleResponse.insuranceCertificateDueDate =
      vehicle.insuranceCertificateDueDate;
    vehicleResponse.archiveReason = vehicle.archiveReason;
    vehicleResponse.createdBy = vehicle.createdBy;
    vehicleResponse.updatedBy = vehicle.updatedBy;
    vehicleResponse.deletedBy = vehicle.deletedBy;
    vehicleResponse.createdAt = vehicle.createdAt;
    vehicleResponse.updatedAt = vehicle.updatedAt;
    vehicleResponse.deletedAt = vehicle.deletedAt;
    if (vehicle.driver) {
      vehicleResponse.driver = DriverResponse.fromDomain(vehicle.driver);
    }
    if (vehicle.owner) {
      vehicleResponse.owner = OwnerResponse.fromDomain(vehicle.owner);
    }
    if (vehicle.category) {
      vehicleResponse.category = CategoryResponse.fromDomain(vehicle.category);
    }
    return vehicleResponse;
  }
}
