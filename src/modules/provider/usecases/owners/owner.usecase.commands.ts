import {
  FileManagerService,
  FileManagerHelper,
} from '@libs/common/file-manager';
import { UserInfo } from '@account/dtos/user-info.dto';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import {
  CreateOwnerCommand,
  UpdateOwnerCommand,
  UpdateDocument,
  ArchiveOwnerCommand,
} from './owner.commands';
import { OwnerRepository } from '@provider/persistence/owners/owner.repository';
import { OwnerResponse } from './owner.response';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import {
  ArchiveVehicleCommand,
  CreateVehicleCommand,
  DeleteVehicleCommand,
  UpdateVehicleCommand,
  UpdateVehicleDocumentCommand,
} from './vechile.commands';
import { VehicleResponse } from './vehicle.response';
import { AccountCommands } from '@account/usecases/accounts/account.usecase.commands';
import { CreateAccountCommand } from '@account/usecases/accounts/account.commands';
import { CredentialType } from '@libs/common/enums';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Util } from '@libs/common/util';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
@Injectable()
export class OwnerCommands {
  constructor(
    private ownerRepository: OwnerRepository,
    private readonly accountCommands: AccountCommands,
    @InjectRepository(VehicleEntity)
    private vehicleRepository: Repository<VehicleEntity>,
    @InjectRepository(DriverEntity)
    private driverRepository: Repository<DriverEntity>,
    private eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createOwner(command: CreateOwnerCommand): Promise<OwnerResponse> {
    if (
      await this.ownerRepository.getByPhoneNumber(command.phoneNumber, true)
    ) {
      throw new BadRequestException(
        `Owner already exist with this phone number`,
      );
    }
    if (
      command.email &&
      (await this.ownerRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Owner already exist with this email Address`,
      );
    }
    if (
      command.tinNumber &&
      (await this.ownerRepository.getByTinNumber(command.tinNumber, true))
    ) {
      throw new BadRequestException(`Owner already exist with this tin number`);
    }
    const ownerDomain = CreateOwnerCommand.fromCommand(command);
    const owner = await this.ownerRepository.insert(ownerDomain);
    if (owner) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: owner.id,
        modelName: MODELS.OWNER,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
      const password = Util.generatePassword(6);
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = owner.id;
      createAccountCommand.type = CredentialType.Owner;
      createAccountCommand.role = ['owner'];
      createAccountCommand.isActive = true;
      createAccountCommand.password = Util.hashPassword(password);
      createAccountCommand.address = command.address;
      createAccountCommand.gender = command.gender;
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      // if (account && account.email) {
      //   this.eventEmitter.emit('send.email.credential', {
      //     name: account.name,
      //     email: account.email,
      //     phoneNumber: account.phoneNumber,
      //     password: password,
      //   });
      // }
    }
    return OwnerResponse.fromDomain(owner);
  }
  async updateOwner(command: UpdateOwnerCommand): Promise<OwnerResponse> {
    const ownerDomain = await this.ownerRepository.getById(command.id);
    if (!ownerDomain) {
      throw new NotFoundException(`Owner not found with id ${command.id}`);
    }
    if (
      ownerDomain.phoneNumber !== command.phoneNumber &&
      (await this.ownerRepository.getByPhoneNumber(command.phoneNumber, true))
    ) {
      throw new BadRequestException(
        `Owner already exist with this phone number`,
      );
    }
    if (
      command.email &&
      ownerDomain.email !== command.email &&
      (await this.ownerRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Owner already exist with this email Address`,
      );
    }
    if (
      command.tinNumber &&
      ownerDomain.tinNumber !== command.tinNumber &&
      (await this.ownerRepository.getByTinNumber(command.tinNumber, true))
    ) {
      throw new BadRequestException(`Owner already exist with this tin number`);
    }
    const oldPayload = ownerDomain;
    ownerDomain.email = command.email;
    ownerDomain.name = command.name;
    ownerDomain.address = { ...command.address };
    ownerDomain.phoneNumber = command.phoneNumber;
    ownerDomain.gender = ownerDomain.isCompany === true ? null : command.gender;
    ownerDomain.tinNumber = command.tinNumber;
    ownerDomain.enabled = command.enabled;
    ownerDomain.website = command.website;
    ownerDomain.isCompany = command.isCompany;
    const owner = await this.ownerRepository.update(ownerDomain);
    if (owner) {
      this.eventEmitter.emit('update.account', {
        accountId: owner.id,
        name: owner.name,
        email: owner.email,
        type: CredentialType.Owner,
        phoneNumber: owner.phoneNumber,
        address: owner.address,
        gender: ownerDomain.isCompany === true ? null : owner.gender,
        profileImage: owner.logo,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.OWNER,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: ownerDomain,
        oldPayload,
      });
    }
    return OwnerResponse.fromDomain(owner);
  }

  async updateOwnerDocument(command: UpdateDocument): Promise<OwnerResponse> {
    const ownerDomain = await this.ownerRepository.getById(command.id);
    if (!ownerDomain) {
      throw new NotFoundException(`Owner not found with id ${command.id}`);
    }
    if (command.license) {
      if (ownerDomain.license) {
        await this.fileManagerService.removeFile(
          ownerDomain.license,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      ownerDomain.license = command.license;
    }
    if (command.logo) {
      if (ownerDomain.logo) {
        await this.fileManagerService.removeFile(
          ownerDomain.logo,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      ownerDomain.logo = command.logo;
    }
    ownerDomain.updatedBy = command.currentUser.id;
    ownerDomain.licenseDueDate = command.licenseDueDate;
    const owner = await this.ownerRepository.update(ownerDomain);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.OWNER,
      action: ACTIONS.UPDATEDOCUMENT,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return OwnerResponse.fromDomain(owner);
  }
  async archiveOwner(
    archiveCommand: ArchiveOwnerCommand,
  ): Promise<OwnerResponse> {
    const ownerDomain = await this.ownerRepository.getById(archiveCommand.id);
    if (!ownerDomain) {
      throw new NotFoundException(
        `Owner not found with id ${archiveCommand.id}`,
      );
    }
    ownerDomain.archiveReason = archiveCommand.reason;
    ownerDomain.deletedAt = new Date();
    ownerDomain.deletedBy = archiveCommand.currentUser.id;
    const result = await this.ownerRepository.update(ownerDomain);
    if (result) {
      this.archiveVehiclesByOwnerId(archiveCommand.id);
      this.eventEmitter.emit('account.archived', {
        phoneNumber: ownerDomain.phoneNumber,
        id: ownerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: archiveCommand.id,
        modelName: MODELS.OWNER,
        action: ACTIONS.ARCHIVE,
        userId: archiveCommand.currentUser.id,
        user: archiveCommand.currentUser,
      });
    }
    return OwnerResponse.fromDomain(result);
  }
  async restoreOwner(
    id: string,
    currentUser: UserInfo,
  ): Promise<OwnerResponse> {
    const ownerDomain = await this.ownerRepository.getById(id, true);
    if (!ownerDomain) {
      throw new NotFoundException(`Owner not found with id ${id}`);
    }
    const r = await this.ownerRepository.restore(id);
    if (r) {
      ownerDomain.deletedAt = null;
      this.restoreVehiclesByOwnerId(id);
      this.eventEmitter.emit('account.restored', {
        phoneNumber: ownerDomain.phoneNumber,
        id: ownerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.OWNER,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return OwnerResponse.fromDomain(ownerDomain);
  }
  async deleteOwner(id: string, currentUser: UserInfo): Promise<boolean> {
    const ownerDomain = await this.ownerRepository.getById(id, true);
    if (!ownerDomain) {
      throw new NotFoundException(`Owner not found with id ${id}`);
    }
    const isOwnerHasVehicle = await this.vehicleRepository.find({
      where: { ownerId: id },
      take: 1,
      withDeleted: true,
    });
    if (isOwnerHasVehicle) {
      throw new NotFoundException(`This owner has vehicle so you can't delete`);
    }
    const result = await this.ownerRepository.delete(id);
    if (result) {
      this.eventEmitter.emit('account.deleted', {
        phoneNumber: ownerDomain.phoneNumber,
        id: ownerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.OWNER,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
      if (ownerDomain.logo) {
        await this.fileManagerService.removeFile(
          ownerDomain.logo,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      if (ownerDomain.license) {
        await this.fileManagerService.removeFile(
          ownerDomain.license,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
    }
    return result;
  }
  async addVehicle(command: CreateVehicleCommand): Promise<VehicleResponse> {
    const ownerDomain = await this.ownerRepository.getById(command.ownerId);
    if (!ownerDomain) {
      throw new NotFoundException(
        `Owner does not found with id ${command.ownerId}`,
      );
    }
    const existingVehicle = await this.vehicleRepository.findOneBy({
      plateNumber: command.plateNumber,
      code: command.code,
    });
    if (existingVehicle) {
      throw new BadRequestException(
        `Vehicle already exist with plate number ${command.plateNumber} and code ${command.code}`,
      );
    }
    const vehicle = CreateVehicleCommand.fromCommand(command);
    ownerDomain.addVehicle(vehicle);
    const result = await this.ownerRepository.update(ownerDomain);
    if (!result) return null;

    const vehicleResponse = VehicleResponse.fromDomain(
      result.vehicles[result.vehicles.length - 1],
    );
    this.eventEmitter.emit('activity-logger.store', {
      modelId: vehicleResponse.id,
      modelName: MODELS.VEHICLE,
      action: ACTIONS.CREATE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return vehicleResponse;
  }
  async updateVehicle(command: UpdateVehicleCommand): Promise<VehicleResponse> {
    const ownerDomain = await this.ownerRepository.getById(command.ownerId);
    if (!ownerDomain) {
      throw new NotFoundException(
        `Owner does not found with id ${command.ownerId}`,
      );
    }
    const existingVehicle = await this.vehicleRepository.findOneBy({
      plateNumber: command.plateNumber,
      code: command.code,
    });
    if (existingVehicle && existingVehicle.id !== command.id) {
      throw new BadRequestException(
        `Vehicle already exist with plate number ${command.plateNumber} and code ${command.code}`,
      );
    }
    const oldPayload = ownerDomain.vehicles.find((v) => v.id === command.id);
    const vehicle = UpdateVehicleCommand.fromCommand(command);
    ownerDomain.updateVehicle(vehicle);
    const result = await this.ownerRepository.update(ownerDomain);
    if (!result) return null;
    this.eventEmitter.emit('activity-logger.store', {
      modelId: vehicle.id,
      modelName: MODELS.VEHICLE,
      action: ACTIONS.UPDATE,
      payload: vehicle,
      oldPayload,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return VehicleResponse.fromDomain(
      result.vehicles.find((vehicle) => vehicle.id === command.id),
    );
  }
  async deleteVehicle(command: DeleteVehicleCommand): Promise<boolean> {
    const vehicles = await this.vehicleRepository.find({
      where: { id: command.id },
      relations: [],
      withDeleted: true,
    });
    if (!vehicles[0]) {
      throw new NotFoundException(
        `Vehicle does not found with id ${command.id}`,
      );
    }
    const vehicle = vehicles[0];
    const driver = await this.driverRepository.find({
      where: { vehicleId: command.id },
      withDeleted: true,
      take: 1,
    });

    if (driver[0]) {
      throw new BadRequestException(
        `You can't delete the vehicle because it is already assigned for a driver`,
      );
    }
    const result = await this.vehicleRepository.delete({ id: command.id });
    if (result) {
      if (vehicle.bolo) {
        await this.fileManagerService.removeFile(
          vehicle.bolo,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      if (vehicle.insurance) {
        await this.fileManagerService.removeFile(
          vehicle.insurance,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.VEHICLE,
        action: ACTIONS.DELETE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return result ? true : false;
  }
  async archiveVehicle(
    command: ArchiveVehicleCommand,
  ): Promise<VehicleResponse> {
    const ownerDomain = await this.ownerRepository.getById(command.ownerId);
    if (!ownerDomain) {
      throw new NotFoundException(
        `Owner does not found with id ${command.ownerId}`,
      );
    }
    const vehicle = ownerDomain.vehicles.find((v) => v.id === command.id);
    vehicle.deletedAt = new Date();
    vehicle.deletedBy = command.currentUser.id;
    vehicle.archiveReason = command.reason;
    ownerDomain.updateVehicle(vehicle);
    const result = await this.ownerRepository.update(ownerDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: vehicle.id,
        modelName: MODELS.VEHICLE,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return VehicleResponse.fromDomain(vehicle);
  }
  async restoreVehicle(
    command: DeleteVehicleCommand,
  ): Promise<VehicleResponse> {
    const vehicle = await this.vehicleRepository.find({
      where: { id: command.id },
      relations: [],
      withDeleted: true,
    });
    if (!vehicle[0]) {
      throw new NotFoundException(`Vehicle not found with id ${command.id}`);
    }
    vehicle[0].deletedAt = null;
    vehicle[0].deletedBy = null;
    vehicle[0].archiveReason = null;
    const result = await this.vehicleRepository.save(vehicle[0]);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.VEHICLE,
      action: ACTIONS.RESTORE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });

    return VehicleResponse.fromEntity(result);
  }
  async archiveVehiclesByOwnerId(ownerId: string): Promise<boolean> {
    const result = await this.vehicleRepository.update(
      { ownerId: ownerId },
      { deletedAt: new Date(), archiveReason: 'Owner is archived' },
    );
    return result ? true : false;
  }
  async restoreVehiclesByOwnerId(ownerId: string): Promise<boolean> {
    const result = await this.vehicleRepository.update(
      { ownerId: ownerId },
      { deletedAt: null, archiveReason: null },
    );
    return result ? true : false;
  }
  async activateOrBlockOwner(
    id: string,
    currentUser: UserInfo,
  ): Promise<OwnerResponse> {
    const ownerDomain = await this.ownerRepository.getById(id);
    if (!ownerDomain) {
      throw new NotFoundException(`Owner not found with id ${id}`);
    }
    ownerDomain.enabled = !ownerDomain.enabled;
    const result = await this.ownerRepository.update(ownerDomain);
    if (result) {
      this.eventEmitter.emit('account.activate-or-block', {
        phoneNumber: ownerDomain.phoneNumber,
        id: ownerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.OWNER,
        action: ownerDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return OwnerResponse.fromDomain(result);
  }
  async activateOrBlockVehicles(
    id: string,
    currentUser: UserInfo,
  ): Promise<VehicleResponse> {
    const vehicle = await this.vehicleRepository.find({
      where: { id: id },
      withDeleted: true,
    });
    if (!vehicle[0]) {
      throw new NotFoundException(`Vehicle not found with id ${id}`);
    }
    vehicle[0].enabled = !vehicle[0].enabled;
    const result = await this.vehicleRepository.save(vehicle[0]);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.VEHICLE,
        action: vehicle[0].enabled === true ? ACTIONS.BLOCK : ACTIONS.ACTIVATE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return VehicleResponse.fromEntity(result);
  }
  async updateVehicleDocument(
    command: UpdateVehicleDocumentCommand,
  ): Promise<VehicleResponse> {
    const ownerDomain = await this.ownerRepository.getById(command.ownerId);
    if (!ownerDomain) {
      throw new NotFoundException(`Owner not found with id ${command.ownerId}`);
    }
    const vehicle = ownerDomain.vehicles.find((v) => v.id === command.id);
    if (!vehicle) {
      throw new NotFoundException(`Vehicle not found with id ${command.id}`);
    }
    if (command.bolo) {
      if (vehicle.bolo) {
        await this.fileManagerService.removeFile(
          vehicle.bolo,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      vehicle.bolo = command.bolo;
    }
    if (command.insurance) {
      if (vehicle.insurance) {
        await this.fileManagerService.removeFile(
          vehicle.insurance,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      vehicle.insurance = command.insurance;
    }
    vehicle.boloDueDate = command.boloDueDate;
    vehicle.insuranceCertificateDueDate = command.insuranceCertificateDueDate;
    vehicle.updatedBy = command.currentUser.id;
    ownerDomain.updateVehicle(vehicle);
    this.eventEmitter.emit('activity-logger.store', {
      modelId: vehicle.id,
      modelName: MODELS.VEHICLE,
      action: ACTIONS.UPDATEDOCUMENT,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    await this.ownerRepository.update(ownerDomain);
    return VehicleResponse.fromDomain(vehicle);
  }
}
