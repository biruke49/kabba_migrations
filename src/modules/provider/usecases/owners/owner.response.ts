import { VehicleResponse } from './vehicle.response';
import { OwnerEntity } from '@provider/persistence/owners/owner.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Address } from '@libs/common/address';
import { Owner } from '@provider/domains/owners/owner';
import { FileDto } from '@libs/common/file-dto';

export class OwnerResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  logo: FileDto;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  tinNumber: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  license: FileDto;
  @ApiProperty()
  licenseDueDate: Date;
  @ApiProperty()
  isCompany: boolean;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  @ApiProperty({ isArray: true, type: VehicleResponse })
  vehicles: VehicleResponse[];
  static fromEntity(ownerEntity: OwnerEntity): OwnerResponse {
    const ownerResponse = new OwnerResponse();
    ownerResponse.id = ownerEntity.id;
    ownerResponse.name = ownerEntity.name;
    ownerResponse.email = ownerEntity.email;
    ownerResponse.phoneNumber = ownerEntity.phoneNumber;
    ownerResponse.gender = ownerEntity.gender;
    ownerResponse.enabled = ownerEntity.enabled;
    ownerResponse.logo = ownerEntity.logo;
    ownerResponse.address = ownerEntity.address;
    ownerResponse.tinNumber = ownerEntity.tinNumber;
    ownerResponse.website = ownerEntity.website;
    ownerResponse.isCompany = ownerEntity.isCompany;
    ownerResponse.license = ownerEntity.license;
    ownerResponse.licenseDueDate = ownerEntity.licenseDueDate;
    ownerResponse.archiveReason = ownerEntity.archiveReason;
    ownerResponse.createdBy = ownerEntity.createdBy;
    ownerResponse.updatedBy = ownerEntity.updatedBy;
    ownerResponse.deletedBy = ownerEntity.deletedBy;
    ownerResponse.createdAt = ownerEntity.createdAt;
    ownerResponse.updatedAt = ownerEntity.updatedAt;
    ownerResponse.deletedAt = ownerEntity.deletedAt;
    ownerResponse.vehicles = ownerEntity.vehicles
      ? ownerEntity.vehicles.map((vehicle) => {
          return VehicleResponse.fromEntity(vehicle);
        })
      : null;
    return ownerResponse;
  }
  static fromDomain(owner: Owner): OwnerResponse {
    const ownerResponse = new OwnerResponse();
    ownerResponse.id = owner.id;
    ownerResponse.name = owner.name;
    ownerResponse.email = owner.email;
    ownerResponse.phoneNumber = owner.phoneNumber;
    ownerResponse.gender = owner.gender;
    ownerResponse.enabled = owner.enabled;
    ownerResponse.logo = owner.logo;
    ownerResponse.address = owner.address;
    ownerResponse.tinNumber = owner.tinNumber;
    ownerResponse.website = owner.website;
    ownerResponse.isCompany = owner.isCompany;
    ownerResponse.license = owner.license;
    ownerResponse.licenseDueDate = owner.licenseDueDate;
    ownerResponse.archiveReason = owner.archiveReason;
    ownerResponse.createdBy = owner.createdBy;
    ownerResponse.updatedBy = owner.updatedBy;
    ownerResponse.deletedBy = owner.deletedBy;
    ownerResponse.createdAt = owner.createdAt;
    ownerResponse.updatedAt = owner.updatedAt;
    ownerResponse.deletedAt = owner.deletedAt;
    ownerResponse.vehicles = owner.vehicles
      ? owner.vehicles.map((vehicle) => {
          return VehicleResponse.fromDomain(vehicle);
        })
      : null;
    return ownerResponse;
  }
}
