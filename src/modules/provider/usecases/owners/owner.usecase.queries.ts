import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import { CountByCategoryResponse } from '@libs/common/count-by-category.response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { OwnerEntity } from '@provider/persistence/owners/owner.entity';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import { DataSource, Repository } from 'typeorm';
import { OwnerResponse } from './owner.response';
import { VehicleResponse } from './vehicle.response';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
@Injectable()
export class OwnerQuery {
  constructor(
    @InjectRepository(OwnerEntity)
    private ownerRepository: Repository<OwnerEntity>,
    @InjectRepository(VehicleEntity)
    private vehicleRepository: Repository<VehicleEntity>,
    private dataSource: DataSource,
  ) {}
  async getOwner(id: string, withDeleted = false): Promise<OwnerResponse> {
    const owner = await this.ownerRepository.find({
      where: { id: id },
      relations: ['vehicles'],
      withDeleted: withDeleted,
    });
    if (!owner[0]) {
      throw new NotFoundException(`Owner not found with id ${id}`);
    }
    return OwnerResponse.fromEntity(owner[0]);
  }
  async getOwners(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<OwnerResponse>> {
    const dataQuery = QueryConstructor.constructQuery<OwnerEntity>(
      this.ownerRepository,
      query,
    );
    const d = new DataResponseFormat<OwnerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => OwnerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async countOwnersByCreatedMonth(
    query: CollectionQuery,
  ): Promise<CountByCreatedAtResponse[]> {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 1);
    const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    query.select = [];
    query.select.push(
      `to_char(owners.created_at,'MM') as created_date`,
      'COUNT(owners.id)',
    );
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'owners.created_at',
        value: [startOfYear.toISOString(), endOfYear.toISOString()],
        operator: FilterOperators.Between,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<OwnerEntity>(
      this.ownerRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupOwnersByGender(
    query: CollectionQuery,
  ): Promise<CountByGenderResponse[]> {
    query.select = [];
    query.select.push('gender', 'COUNT(owners.id)');
    const dataQuery = QueryConstructor.constructQuery<OwnerEntity>(
      this.ownerRepository,
      query,
    );
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByGenderResponse();
      countResponse.gender = d.owners_gender;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupOwnersByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('enabled as enabled', 'COUNT(owners.id)');
    const dataQuery = QueryConstructor.constructQuery<OwnerEntity>(
      this.ownerRepository,
      query,
    );
    const data = await dataQuery.groupBy('enabled').getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.enabled;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupOwnersByAddress(
    field: string,
    query: CollectionQuery,
  ): Promise<GroupByAddressResponse[]> {
    query.select = [];
    query.select.push(
      `owners.address->>'${field}' as ${field}`,
      `COUNT(owners.id)`,
    );
    const dataQuery = QueryConstructor.constructQuery<OwnerEntity>(
      this.ownerRepository,
      query,
    );
    dataQuery.groupBy(`owners.address->>'${field}'`);
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByAddressResponse();
      countResponse.address = d.field;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getArchivedOwners(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<OwnerResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<OwnerEntity>(
      this.ownerRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<OwnerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => OwnerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getVehicle(id: string, withDeleted = false): Promise<VehicleResponse> {
    const vehicle = await this.vehicleRepository.find({
      where: { id: id },
      relations: ['driver', 'owner', 'category'],
      withDeleted: withDeleted,
    });
    if (!vehicle[0]) {
      throw new NotFoundException(`Vehicle not found with id ${id}`);
    }
    return VehicleResponse.fromEntity(vehicle[0]);
  }
  async getVehicles(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<VehicleResponse>> {
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    const d = new DataResponseFormat<VehicleResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => VehicleResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getUnAssignedVehicles(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<VehicleResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push(
      [
        {
          field: 'deleted_at',
          operator: FilterOperators.IsNull,
        },
      ],
      [
        {
          field: 'enabled',
          operator: FilterOperators.EqualTo,
          value: true,
        },
      ],
    );
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    dataQuery.andWhere(
      `id not in ${dataQuery
        .subQuery()
        .select('vehicle_id')
        .andWhere('drivers.vehicle_id IS NOT NULL')
        .from(DriverEntity, 'drivers')
        .getQuery()}`,
    );
    const d = new DataResponseFormat<VehicleResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => VehicleResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getOwnerVehicles(
    ownerId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<VehicleResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'owner_id',
        value: ownerId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    const d = new DataResponseFormat<VehicleResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => VehicleResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getVehiclesByCategoryId(
    categoryId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<VehicleResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'category_id',
        value: categoryId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    const d = new DataResponseFormat<VehicleResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => VehicleResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async countVehiclesByCategory(query: CollectionQuery) {
    query.select = [];
    query.select.push(`category.name as Category`, `COUNT(vehicles.id)`);
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    dataQuery
      .addGroupBy(`category.name`)
      .leftJoin('vehicles.category', 'category');

    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCategoryResponse();
      countResponse.category = d.category;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async groupVehiclesByStatus(
    query: CollectionQuery,
  ): Promise<GroupByStatusResponse[]> {
    query.select = [];
    query.select.push('enabled as enabled', 'COUNT(vehicles.id)');
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    const data = await dataQuery.getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new GroupByStatusResponse();
      countResponse.status = d.enabled;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async countVehiclesByCreatedMonth(
    query: CollectionQuery,
  ): Promise<CountByCreatedAtResponse[]> {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 1);
    const endOfYear = new Date(now.getFullYear() + 1, 0, 1);
    query.select = [];
    query.select.push(
      `to_char(vehicles.created_at,'MM') as created_date`,
      'COUNT(vehicles.id)',
    );
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'vehicles.created_at',
        value: [startOfYear.toISOString(), endOfYear.toISOString()],
        operator: FilterOperators.Between,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    const data = await dataQuery
      .groupBy(`created_date`)
      .orderBy('created_date')
      .getRawMany();
    const countResponses = [];
    data.map((d) => {
      const countResponse = new CountByCreatedAtResponse();
      countResponse.createdAt = d.created_date;
      countResponse.count = d.count;
      countResponses.push(countResponse);
    });
    return countResponses;
  }
  async getArchivedVehicles(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<VehicleResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<VehicleResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => VehicleResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }

  async getArchivedOwnerVehicles(
    ownerId: string,
    query: CollectionQuery,
  ): Promise<DataResponseFormat<VehicleResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    query.filter.push([
      {
        field: 'owner_id',
        value: ownerId,
        operator: FilterOperators.EqualTo,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<VehicleEntity>(
      this.vehicleRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<VehicleResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => VehicleResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
