import { UserInfo } from '@account/dtos/user-info.dto';
import { VehicleColor } from '@libs/common/enums';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { Vehicle } from '@provider/domains/owners/vehicle';
import { IsDateString, IsEnum, IsNotEmpty, IsUUID } from 'class-validator';
export class CreateVehicleCommand {
  @ApiProperty()
  @IsNotEmpty()
  ownerId: string;
  @ApiProperty()
  @IsNotEmpty()
  code: string;
  @ApiProperty()
  @IsNotEmpty()
  plateNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  model: string;
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(VehicleColor, {
    message: `Vehicle Color must be one of  ${Object.values(VehicleColor).join(
      ',',
    )}`,
  })
  color: string;
  ownerName: string;
  ownerPhoneNumber: string;
  currentUser: UserInfo;
  static fromCommand(createVehicle: CreateVehicleCommand): Vehicle {
    const vehicle = new Vehicle();
    vehicle.ownerId = createVehicle.ownerId;
    vehicle.code = createVehicle.code;
    vehicle.plateNumber = createVehicle.plateNumber;
    vehicle.model = createVehicle.model;
    vehicle.categoryId = createVehicle.categoryId;
    vehicle.color = createVehicle.color;
    vehicle.ownerName = createVehicle.ownerName;
    vehicle.ownerPhoneNumber = createVehicle.ownerPhoneNumber;
    return vehicle;
  }
}
export class UpdateVehicleCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  ownerId: string;
  @ApiProperty()
  @IsNotEmpty()
  code: string;
  @ApiProperty()
  @IsNotEmpty()
  plateNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  model: string;
  @ApiProperty()
  @IsNotEmpty()
  categoryId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(VehicleColor, {
    message: `Vehicle Color must be one of  ${Object.values(VehicleColor).join(
      ',',
    )}`,
  })
  color: string;
  ownerName: string;
  ownerPhoneNumber: string;
  currentUser: UserInfo;
  static fromCommand(updateVehicle: UpdateVehicleCommand): Vehicle {
    const vehicle = new Vehicle();
    vehicle.id = updateVehicle.id;
    vehicle.ownerId = updateVehicle.ownerId;
    vehicle.code = updateVehicle.code;
    vehicle.plateNumber = updateVehicle.plateNumber;
    vehicle.model = updateVehicle.model;
    vehicle.categoryId = updateVehicle.categoryId;
    vehicle.color = updateVehicle.color;
    vehicle.ownerName = updateVehicle.ownerName;
    vehicle.ownerPhoneNumber = updateVehicle.ownerPhoneNumber;
    return vehicle;
  }
}
export class DeleteVehicleCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  ownerId: string;
  currentUser: UserInfo;
}
export class UpdateVehicleDocumentCommand {
  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  ownerId: string;
  @ApiProperty()
  @IsDateString()
  boloDueDate: Date;
  @ApiProperty()
  @IsDateString()
  insuranceCertificateDueDate: Date;
  bolo: FileDto;
  insurance: FileDto;
  currentUser: UserInfo;
}
export class ArchiveVehicleCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  ownerId: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
