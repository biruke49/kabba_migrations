import { diskStorage } from 'multer';

import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { CountByCategoryResponse } from '@libs/common/count-by-category.response';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  StreamableFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import * as XLSX from 'xlsx';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ArchiveOwnerCommand,
  CreateOwnerCommand,
  UpdateDocument,
  UpdateOwnerCommand,
} from '@provider/usecases/owners/owner.commands';
import { OwnerResponse } from '@provider/usecases/owners/owner.response';
import { OwnerCommands } from '@provider/usecases/owners/owner.usecase.commands';
import { OwnerQuery } from '@provider/usecases/owners/owner.usecase.queries';
import {
  ArchiveVehicleCommand,
  CreateVehicleCommand,
  DeleteVehicleCommand,
  UpdateVehicleCommand,
  UpdateVehicleDocumentCommand,
} from '@provider/usecases/owners/vechile.commands';
import { VehicleResponse } from '@provider/usecases/owners/vehicle.response';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';
import { CountByCreatedAtResponse } from '@order/usecases/bookings/booking.response';
import { FileFieldsInterceptor } from '@nestjs/platform-express';

@Controller('owners')
@ApiTags('owners')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class OwnersController {
  constructor(
    private command: OwnerCommands,
    private ownerQuery: OwnerQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-owner/:id')
  @ApiOkResponse({ type: OwnerResponse })
  async getOwner(@Param('id') id: string) {
    return this.ownerQuery.getOwner(id);
  }
  @Get('get-archived-owner/:id')
  @ApiOkResponse({ type: OwnerResponse })
  async getArchivedOwner(@Param('id') id: string) {
    return this.ownerQuery.getOwner(id, true);
  }
  @Get('get-owners')
  @ApiPaginatedResponse(OwnerResponse)
  async getOwners(@Query() query: CollectionQuery) {
    return this.ownerQuery.getOwners(query);
  }
  @Get('count-owners-by-created-month')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async countOwnersByCreatedMonth(@Query() query: CollectionQuery) {
    return this.ownerQuery.countOwnersByCreatedMonth(query);
  }
  @Get('group-owners-by-gender')
  @ApiOkResponse({ type: CountByGenderResponse, isArray: true })
  async groupOwnersByGender(@Query() query: CollectionQuery) {
    return this.ownerQuery.groupOwnersByGender(query);
  }
  @Get('group-owners-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupOwnersByStatus(@Query() query: CollectionQuery) {
    return this.ownerQuery.groupOwnersByStatus(query);
  }
  @Get('group-owners-by-address/:address')
  @ApiOkResponse({ type: GroupByAddressResponse, isArray: true })
  async groupOwnersByAddress(
    @Param('address') address: string,
    @Query() query: CollectionQuery,
  ) {
    return this.ownerQuery.groupOwnersByAddress(address, query);
  }
  @Post('create-owner')
  @UseGuards(PermissionsGuard('manage-owners'))
  @ApiOkResponse({ type: OwnerResponse })
  async createOwner(
    @CurrentUser() user: UserInfo,
    @Body() createOwnerCommand: CreateOwnerCommand,
  ) {
    createOwnerCommand.currentUser = user;
    return this.command.createOwner(createOwnerCommand);
  }
  @Put('update-owner')
  @UseGuards(PermissionsGuard('manage-owners'))
  @ApiOkResponse({ type: OwnerResponse })
  async updateOwner(
    @CurrentUser() user: UserInfo,
    @Body() updateOwnerCommand: UpdateOwnerCommand,
  ) {
    updateOwnerCommand.currentUser = user;
    return this.command.updateOwner(updateOwnerCommand);
  }
  @Delete('archive-owner')
  @UseGuards(PermissionsGuard('manage-owners'))
  @ApiOkResponse({ type: OwnerResponse })
  async archiveOwner(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveOwnerCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveOwner(archiveCommand);
  }
  @Delete('delete-owner/:id')
  @UseGuards(PermissionsGuard('manage-owners'))
  @ApiOkResponse({ type: Boolean })
  async deleteOwner(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteOwner(id, user);
  }
  @Post('restore-owner/:id')
  @UseGuards(PermissionsGuard('manage-owners'))
  @ApiOkResponse({ type: OwnerResponse })
  async restoreOwner(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreOwner(id, user);
  }
  @Get('get-archived-owners')
  @ApiPaginatedResponse(OwnerResponse)
  async getArchivedOwners(@Query() query: CollectionQuery) {
    return this.ownerQuery.getArchivedOwners(query);
  }
  @Post('add-owner-vehicle')
  @UseGuards(PermissionsGuard('manage-owners|manage-vehicles'))
  @ApiOkResponse({ type: VehicleResponse })
  async addOwnerVehicle(
    @CurrentUser() user: UserInfo,
    @Body() createVehicleCommand: CreateVehicleCommand,
  ) {
    createVehicleCommand.currentUser = user;
    return this.command.addVehicle(createVehicleCommand);
  }
  @Put('update-owner-vehicle')
  @UseGuards(PermissionsGuard('manage-owners|manage-vehicles'))
  @ApiOkResponse({ type: VehicleResponse })
  async updateOwnerVehicle(
    @CurrentUser() user: UserInfo,
    @Body() updateVehicleCommand: UpdateVehicleCommand,
  ) {
    updateVehicleCommand.currentUser = user;
    return this.command.updateVehicle(updateVehicleCommand);
  }
  @Delete('remove-owner-vehicle')
  @UseGuards(PermissionsGuard('manage-owners|manage-vehicles'))
  @ApiOkResponse({ type: Boolean })
  async removeOwnerVehicle(
    @CurrentUser() user: UserInfo,
    @Body() removeVehicleCommand: DeleteVehicleCommand,
  ) {
    removeVehicleCommand.currentUser = user;
    return this.command.deleteVehicle(removeVehicleCommand);
  }
  @Delete('archive-owner-vehicle')
  @UseGuards(PermissionsGuard('manage-owners|manage-vehicles'))
  @ApiOkResponse({ type: Boolean })
  async archiveOwnerVehicle(
    @CurrentUser() user: UserInfo,
    @Body() archiveVehicleCommand: ArchiveVehicleCommand,
  ) {
    archiveVehicleCommand.currentUser = user;
    return this.command.archiveVehicle(archiveVehicleCommand);
  }
  @Post('restore-owner-vehicle')
  @UseGuards(PermissionsGuard('manage-owners|manage-vehicles'))
  @ApiOkResponse({ type: VehicleResponse })
  async restoreOwnerVehicle(
    @CurrentUser() user: UserInfo,
    @Body() restoreVehicleCommand: DeleteVehicleCommand,
  ) {
    restoreVehicleCommand.currentUser = user;
    return this.command.restoreVehicle(restoreVehicleCommand);
  }
  @Get('get-vehicle/:id')
  @ApiOkResponse({ type: VehicleResponse })
  async getVehicle(@Param('id') id: string) {
    return this.ownerQuery.getVehicle(id);
  }
  @Get('get-archiver-vehicle/:id')
  @ApiOkResponse({ type: VehicleResponse })
  async getArchivedVehicle(@Param('id') id: string) {
    return this.ownerQuery.getVehicle(id, true);
  }
  @Get('get-vehicles')
  @ApiPaginatedResponse(VehicleResponse)
  async getVehicles(@Query() query: CollectionQuery) {
    return this.ownerQuery.getVehicles(query);
  }
  @Get('get-unassigned-vehicles')
  @ApiPaginatedResponse(VehicleResponse)
  async getUnassignedVehicles(@Query() query: CollectionQuery) {
    return this.ownerQuery.getUnAssignedVehicles(query);
  }
  @Get('get-vehicles-by-category/:categoryId')
  @ApiPaginatedResponse(VehicleResponse)
  async getVehiclesByCategoryId(
    @Param('categoryId') categoryId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.ownerQuery.getVehiclesByCategoryId(categoryId, query);
  }
  @Get('count-vehicles-by-category')
  @ApiOkResponse({ type: CountByCategoryResponse, isArray: true })
  async countVehiclesByCategory(@Query() query: CollectionQuery) {
    return this.ownerQuery.countVehiclesByCategory(query);
  }
  @Get('group-vehicles-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupPassengersByStatus(@Query() query: CollectionQuery) {
    return this.ownerQuery.groupVehiclesByStatus(query);
  }

  @Get('count-vehicles-by-created-month')
  @ApiOkResponse({ type: CountByCreatedAtResponse, isArray: true })
  async countVehiclesByCreatedMonth(@Query() query: CollectionQuery) {
    return this.ownerQuery.countVehiclesByCreatedMonth(query);
  }
  @Get('get-archived-vehicles')
  @ApiPaginatedResponse(VehicleResponse)
  async getArchivedVehicles(@Query() query: CollectionQuery) {
    return this.ownerQuery.getArchivedVehicles(query);
  }
  @Get('get-archived-owner-vehicles/:ownerId')
  @ApiPaginatedResponse(VehicleResponse)
  async getArchivedOwnerVehicles(
    @Param('ownerId') ownerId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.ownerQuery.getArchivedOwnerVehicles(ownerId, query);
  }
  @Post('activate-or-block-owner/:id')
  @UseGuards(PermissionsGuard('activate-or-block-owner'))
  @ApiOkResponse({ type: OwnerResponse })
  async activateOrBlockOwner(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockOwner(id, user);
  }
  @Get('get-owner-vehicles/:ownerId')
  @ApiPaginatedResponse(VehicleResponse)
  async getOwnerVehicles(
    @Param('ownerId') ownerId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.ownerQuery.getOwnerVehicles(ownerId, query);
  }
  @Post('activate-or-block-vehicle/:id')
  @UseGuards(PermissionsGuard('activate-or-block-vehicle'))
  @ApiOkResponse({ type: VehicleResponse })
  async activateOrBlockVehicle(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockVehicles(id, user);
  }
  @Post('update-document')
  @UseGuards(PermissionsGuard('update-owner-document'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'logo', maxCount: 1 },
        { name: 'license', maxCount: 1 },
      ],
      {
        storage: diskStorage({
          destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
        }),
        fileFilter: (request, file, callback) => {
          if (
            !(file.mimetype.includes('image') || file.mimetype.includes('pdf'))
          ) {
            return callback(
              new BadRequestException('Provide a valid image or pdf file'),
              false,
            );
          }
          callback(null, true);
        },
        limits: { fileSize: Math.pow(1024, 2) },
      },
    ),
  )
  async updateDocument(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateDocument,
    @UploadedFiles()
    files: {
      logo?: Express.Multer.File;
      license?: Express.Multer.File;
    },
  ) {
    command.currentUser = user;
    if (files && files.logo) {
      const result = await this.fileManagerService.uploadFile(
        files.logo[0],
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        command.logo = result;
      }
    }
    if (files && files.license) {
      const result = await this.fileManagerService.uploadFile(
        files.license[0],
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        command.license = result;
      }
    }
    return await this.command.updateOwnerDocument(command);
  }
  @Post('update-vehicle-document')
  @UseGuards(PermissionsGuard('update-vehicle-document'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'bolo', maxCount: 1 },
        { name: 'insurance', maxCount: 1 },
      ],
      {
        storage: diskStorage({
          destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
        }),
        fileFilter: (request, file, callback) => {
          if (
            !(file.mimetype.includes('image') || file.mimetype.includes('pdf'))
          ) {
            return callback(
              new BadRequestException('Provide a valid image or pdf file'),
              false,
            );
          }
          callback(null, true);
        },
        limits: { fileSize: Math.pow(1024, 2) },
      },
    ),
  )
  async updateVehicleDocument(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateVehicleDocumentCommand,
    @UploadedFiles()
    files: {
      bolo?: Express.Multer.File;
      insurance?: Express.Multer.File;
    },
  ) {
    command.currentUser = user;
    if (files && files.bolo) {
      const result = await this.fileManagerService.uploadFile(
        files.bolo[0],
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        command.bolo = result;
      }
    }
    if (files && files.insurance) {
      const result = await this.fileManagerService.uploadFile(
        files.insurance[0],
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        command.insurance = result;
      }
    }
    return await this.command.updateVehicleDocument(command);
  }
  @Get('export-vehicles')
  @AllowAnonymous()
  async exportVehicles(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    if (!query.includes) {
      query.includes = [];
    }
    if (!query.includes.includes('owner')) {
      query.includes.push('owner');
    }
    if (!query.includes.includes('category')) {
      query.includes.push('category');
    }
    const fileName = `vehicles-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const vehicles = await this.ownerQuery.getVehicles(query);
    const vehicleData = vehicles.data;
    const rows = vehicleData.map((vehicle) => {
      return {
        Code: vehicle.code,
        'Plate Number': vehicle.plateNumber,
        Color: vehicle?.color,
        Model: vehicle?.model,
        Owner: vehicle?.owner?.name,
        'Owner Phone Number': vehicle?.owner?.phoneNumber,
        Category: vehicle?.category?.name,
        Capacity: vehicle?.category?.capacity,
        'Created Date': vehicle.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Vehicles`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [
        [
          'Code',
          'Plate Number',
          'Color',
          'Model',
          'Owner',
          'Owner Phone Number',
          'Category',
          'Capacity',
          'Created Date',
        ],
      ],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Owner'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
  @Get('export-owners')
  @AllowAnonymous()
  async exportOwners(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    const fileName = `owners-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const owners = await this.ownerQuery.getOwners(query);
    const ownerData = owners.data;
    const rows = ownerData.map((owner) => {
      return {
        Name: owner.name,
        'Phone Number': owner.phoneNumber,
        Email: owner?.email,
        TIN: owner?.tinNumber,
        'License Due Date': owner?.licenseDueDate,
        'Created Date': owner.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Owners`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [
        [
          'Name',
          'Phone Number',
          'Email',
          'TIN',
          'License Due Date',
          'Created Date',
        ],
      ],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
}
