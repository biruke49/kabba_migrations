import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { TransactionTotalResponse } from '@libs/common/corporate-transaction-spending';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ArchiveDriverFeeCommand,
  ChangeDriverFeeStatusCommand,
} from '@provider/usecases/driver-fees/driver-fee.commands';
import { DriverFeeResponse } from '@provider/usecases/driver-fees/driver-fee.response';
import { DriverFeeCommands } from '@provider/usecases/driver-fees/driver-fee.usecase.commands';
import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { FileManagerHelper } from '@libs/common/file-manager';
import * as XLSX from 'xlsx';
import { DriverFeeQuery } from '@provider/usecases/driver-fees/driver-fee.usecase.queries';
@Controller('driver-fees')
@ApiTags('driver-fees')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class DriverFeesController {
  constructor(
    private commands: DriverFeeCommands,
    private driverFeeQueries: DriverFeeQuery,
  ) {}
  @Get('get-driver-fee/:id')
  @ApiOkResponse({ type: DriverFeeResponse })
  async getDriverFee(@Param('id') id: string) {
    return this.driverFeeQueries.getDriverFee(id);
  }
  @Get('get-driver-fees')
  @ApiPaginatedResponse(DriverFeeResponse)
  async getDriverFees(@Query() query: CollectionQuery) {
    return this.driverFeeQueries.getDriverFees(query);
  }
  @Get('get-driver-fees-by-driver/:driverId')
  @ApiOkResponse({ type: DriverFeeResponse, isArray: true })
  async getDriverFeesByCarType(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.driverFeeQueries.getDriverFeesByDriverId(driverId, query);
  }
  @Get('get-my-fees')
  @ApiOkResponse({ type: DriverFeeResponse, isArray: true })
  async getMyFees(
    @CurrentUser() driver: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.driverFeeQueries.getDriverFeesByDriverId(driver.id, query);
  }
  @Get('get-total-driver-fees')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getTotalDriverFees(@Query() query: CollectionQuery) {
    return this.driverFeeQueries.getTotalDriverFees(query);
  }
  @Get('get-driver-earning/:driverId')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getDriverEarning(
    @Param('driverId') driverId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.driverFeeQueries.getDriverEarning(driverId, query);
  }
  @Get('get-my-earning')
  @ApiOkResponse({ type: TransactionTotalResponse })
  async getMyEarning(
    @CurrentUser() profile: UserInfo,
    @Query() query: CollectionQuery,
  ) {
    return this.driverFeeQueries.getDriverEarning(profile.id, query);
  }
  @Delete('archive-driver-fee')
  @UseGuards(PermissionsGuard('manage-driver-fee'))
  @ApiOkResponse({ type: Boolean })
  async archiveDriverFee(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveDriverFeeCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.commands.archiveDriverFee(archiveCommand);
  }
  @Delete('delete-driver-fee/:id')
  @UseGuards(PermissionsGuard('manage-driver-fee'))
  @ApiOkResponse({ type: Boolean })
  async deleteDriverFee(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.commands.deleteDriverFee(id, user);
  }
  @Post('change-driver-fee-status')
  @UseGuards(PermissionsGuard('manage-driver-fee'))
  @ApiOkResponse({ type: DriverFeeResponse })
  async openOrCloseJob(
    @CurrentUser() user: UserInfo,
    @Body() command: ChangeDriverFeeStatusCommand,
  ) {
    command.currentUser = user;
    return this.commands.changeDriverFeeStatus(command);
  }
  @Post('restore-driver-fee/:id')
  @UseGuards(PermissionsGuard('manage-driver-fee'))
  @ApiOkResponse({ type: DriverFeeResponse })
  async restoreDriverFee(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.commands.restoreDriverFee(id, user);
  }
  @Get('get-archived-driver-fees')
  @ApiPaginatedResponse(DriverFeeResponse)
  async getArchivedDriverFees(@Query() query: CollectionQuery) {
    return this.driverFeeQueries.getArchivedDriverFees(query);
  }
  @Get('export-data')
  @AllowAnonymous()
  async readData() {
    const fileName = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/Presidents.xlsx`;

    const rows = [
      {
        name: 'Aemiro Mekete',
        birthday: '2034-45-78',
      },
      {
        name: 'Aemiro Mekete',
        birthday: '2034-45-78',
      },
      {
        name: 'Aemiro Mekete',
        birthday: '2034-45-78',
      },
      {
        name: 'Aemiro Mekete',
        birthday: '2034-45-78',
      },
    ];
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `response`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(workSheet, [['Name', 'Birthday']], {
      origin: 'A1',
    });

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r.name.length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, fileName);
  }
}
