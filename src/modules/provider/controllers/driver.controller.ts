import { diskStorage } from 'multer';

import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { RolesGuard } from '@account/guards/role.quards';
import { GroupByStatusResponse } from '@credit/usecases/transactions/transaction.response';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { CountByGenderResponse } from '@libs/common/count-by-gender.response';
import {
  FileManagerHelper,
  FileManagerService,
  FileResponseDto,
} from '@libs/common/file-manager';
import { GroupByAddressResponse } from '@libs/common/group-by-address.response';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  StreamableFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ArchiveBankAccountCommand,
  CreateBankAccountCommand,
  DeleteBankAccountCommand,
  UpdateBankAccountCommand,
} from '@provider/usecases/drivers/bank-account.commands';
import { BankAccountResponse } from '@provider/usecases/drivers/bank-account.response';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';
import * as XLSX from 'xlsx';
import { DriverCommands } from '@provider/usecases/drivers/driver.usecase.commands';
import { DriverQuery } from '@provider/usecases/drivers/driver.usecase.queries';
import { DriverResponse } from '@provider/usecases/drivers/driver.response';
import {
  ArchiveDriverCommand,
  CreateDriverCommand,
  ReAssignVehicleCommand,
  RegisterDriverCommand,
  UpdateDocument,
  UpdateDriverCommand,
  UpdateDriverDatePreferenceCommand,
  UpdateMyProfileCommand,
} from '@provider/usecases/drivers/driver.commands';

@Controller('drivers')
@ApiTags('drivers')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class DriversController {
  constructor(
    private command: DriverCommands,
    private driverQuery: DriverQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-driver/:id')
  @ApiOkResponse({ type: DriverResponse })
  async getDriver(@Param('id') id: string) {
    return this.driverQuery.getDriver(id);
  }
  @Get('get-archived-driver/:id')
  @ApiOkResponse({ type: DriverResponse })
  async getArchivedDriver(@Param('id') id: string) {
    return this.driverQuery.getDriver(id, true);
  }
  @Get('get-my-profile')
  @UseGuards(RolesGuard('driver'))
  @ApiOkResponse({ type: DriverResponse })
  async getMyProfileDriver(@CurrentUser() driver: UserInfo) {
    return this.driverQuery.getDriver(driver.id);
  }
  @Get('get-drivers')
  @ApiPaginatedResponse(DriverResponse)
  async getDrivers(
    @Query() query: CollectionQuery,
    @Query('routeId') routeId?: string,
    @Query('datePreference') datePreference?: string,
    @Query('categoryId') categoryId?: string,
  ) {
    return this.driverQuery.getDrivers(
      query,
      routeId,
      datePreference,
      categoryId,
    );
  }
  @Get('get-drivers-by-category/:categoryId')
  @ApiOkResponse({ type: DriverResponse, isArray: true })
  async getDriversByCarType(
    @Param('categoryId') categoryId: string,
    @Query() query: CollectionQuery,
  ) {
    return this.driverQuery.getDriversByCarType(categoryId, query);
  }
  @Get('group-drivers-by-gender')
  @ApiOkResponse({ type: CountByGenderResponse, isArray: true })
  async groupDriversByGender(@Query() query: CollectionQuery) {
    return this.driverQuery.groupDriversByGender(query);
  }
  @Get('group-drivers-by-status')
  @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  async groupPassengersByStatus(@Query() query: CollectionQuery) {
    return this.driverQuery.groupDriversByStatus(query);
  }
  @Get('group-drivers-by-address/:address')
  @ApiOkResponse({ type: GroupByAddressResponse, isArray: true })
  async groupDriversByAddress(
    @Param('address') address: string,
    @Query() query: CollectionQuery,
  ) {
    return this.driverQuery.groupDriversByAddress(address, query);
  }
  @Post('create-driver')
  @UseGuards(PermissionsGuard('manage-drivers'))
  @ApiOkResponse({ type: DriverResponse })
  async createDriver(
    @CurrentUser() user: UserInfo,
    @Body() createDriverCommand: CreateDriverCommand,
  ) {
    createDriverCommand.currentUser = user;
    return this.command.createDriver(createDriverCommand);
  }
  @Post('register-driver')
  @AllowAnonymous()
  @ApiOkResponse({ type: DriverResponse })
  async registerDriver(@Body() registerCommand: RegisterDriverCommand) {
    return this.command.registerDriver(registerCommand);
  }

  @Put('update-driver')
  @UseGuards(PermissionsGuard('manage-drivers'))
  @ApiOkResponse({ type: DriverResponse })
  async updateDriver(
    @CurrentUser() user: UserInfo,
    @Body() updateDriverCommand: UpdateDriverCommand,
  ) {
    updateDriverCommand.currentUser = user;
    return this.command.updateDriver(updateDriverCommand);
  }
  @Post('update-driver-date-preference')
  @UseGuards(RolesGuard('driver'))
  @ApiOkResponse({ type: DriverResponse })
  async updateDriverDatePreference(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateDriverDatePreferenceCommand,
  ) {
    command.id = user.id;
    return this.command.updateDriverDatePreference(command);
  }
  @Put('update-my-profile')
  @UseGuards(RolesGuard('driver'))
  @ApiOkResponse({ type: DriverResponse })
  async updateMyProfile(
    @CurrentUser() user: UserInfo,
    @Body() updateDriverCommand: UpdateMyProfileCommand,
  ) {
    updateDriverCommand.currentUser = user;
    updateDriverCommand.id = user.id;
    return this.command.updateDriverProfile(updateDriverCommand);
  }
  @Put('re-assign-vehicle')
  @UseGuards(PermissionsGuard('re-assign-vehicle'))
  @ApiOkResponse({ type: DriverResponse })
  async reAssignVehicle(
    @CurrentUser() user: UserInfo,
    @Body() command: ReAssignVehicleCommand,
  ) {
    command.currentUser = user;
    return this.command.reAssignVehicle(command);
  }
  @Delete('archive-driver')
  @UseGuards(PermissionsGuard('manage-drivers'))
  @ApiOkResponse({ type: DriverResponse })
  async archiveDriver(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveDriverCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveDriver(archiveCommand);
  }
  @Delete('delete-driver/:id')
  @UseGuards(PermissionsGuard('manage-drivers'))
  @ApiOkResponse({ type: Boolean })
  async deleteDriver(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteDriver(id, user);
  }
  @Post('restore-driver/:id')
  @UseGuards(PermissionsGuard('manage-drivers'))
  @ApiOkResponse({ type: DriverResponse })
  async restoreDriver(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreDriver(id, user);
  }
  @Get('get-archived-drivers')
  @ApiPaginatedResponse(DriverResponse)
  async getArchivedDrivers(@Query() query: CollectionQuery) {
    return this.driverQuery.getArchivedDrivers(query);
  }
  @Post('add-driver-bank-account')
  @UseGuards(PermissionsGuard('manage-bank-account'))
  @ApiOkResponse({ type: BankAccountResponse })
  async addDriverBankAccount(
    @CurrentUser() user: UserInfo,
    @Body() createBankAccountCommand: CreateBankAccountCommand,
  ) {
    createBankAccountCommand.currentUser = user;
    return this.command.addBankAccount(createBankAccountCommand);
  }
  @Put('update-driver-bank-account')
  @UseGuards(PermissionsGuard('manage-bank-account'))
  @ApiOkResponse({ type: BankAccountResponse })
  async updateDriverBankAccount(
    @CurrentUser() user: UserInfo,
    @Body() updateBankAccountCommand: UpdateBankAccountCommand,
  ) {
    updateBankAccountCommand.currentUser = user;
    return this.command.updateBankAccount(updateBankAccountCommand);
  }
  @Delete('remove-driver-bank-account')
  @UseGuards(PermissionsGuard('manage-bank-account'))
  @ApiOkResponse({ type: Boolean })
  async removeDriverBankAccount(
    @CurrentUser() user: UserInfo,
    @Body() removeBankAccountCommand: DeleteBankAccountCommand,
  ) {
    removeBankAccountCommand.currentUser = user;
    return this.command.deleteBankAccount(removeBankAccountCommand);
  }
  @Delete('archive-driver-bank-account')
  @UseGuards(PermissionsGuard('manage-bank-account'))
  @ApiOkResponse({ type: Boolean })
  async archiveDriverBankAccount(
    @CurrentUser() user: UserInfo,
    @Body() archiveBankAccountCommand: ArchiveBankAccountCommand,
  ) {
    archiveBankAccountCommand.currentUser = user;
    return this.command.archiveBankAccount(archiveBankAccountCommand);
  }
  @Get('get-bank-account/:id')
  @ApiOkResponse({ type: BankAccountResponse })
  async getBankAccount(@Param('id') id: string) {
    return this.driverQuery.getBankAccount(id);
  }
  @Get('get-bank-accounts')
  @ApiPaginatedResponse(BankAccountResponse)
  async getBankAccounts(@Query() query: CollectionQuery) {
    return this.driverQuery.getBankAccounts(query);
  }
  @Get('get-archived-bank-accounts')
  @ApiPaginatedResponse(BankAccountResponse)
  async getArchivedBankAccounts(@Query() query: CollectionQuery) {
    return this.driverQuery.getArchivedBankAccounts(query);
  }
  @Post('restore-driver-bank-account')
  @UseGuards(PermissionsGuard('manage-bank-account'))
  @ApiOkResponse({ type: BankAccountResponse })
  async restoreDriverBankAccount(
    @CurrentUser() user: UserInfo,
    @Body() command: DeleteBankAccountCommand,
  ) {
    command.currentUser = user;
    return this.command.restoreDriverBankAccount(command);
  }
  @Post('activate-or-block-driver/:id')
  @UseGuards(PermissionsGuard('activate-or-block-driver'))
  @ApiOkResponse({ type: DriverResponse })
  async activateOrBlockDriver(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockDriver(id, user);
  }
  @Post('update-document')
  @UseGuards(PermissionsGuard('update-provider-document'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'profileImage', maxCount: 1 },
        { name: 'license', maxCount: 1 },
        { name: 'passport', maxCount: 1 },
      ],
      {
        storage: diskStorage({
          destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
        }),
        fileFilter: (request, file, callback) => {
          if (
            !(file.mimetype.includes('image') || file.mimetype.includes('pdf'))
          ) {
            return callback(
              new BadRequestException('Provide a valid image or pdf file'),
              false,
            );
          }
          callback(null, true);
        },
        limits: { fileSize: Math.pow(1024, 2) },
      },
    ),
  )
  async updateDocument(
    @CurrentUser() user: UserInfo,
    @Body() command: UpdateDocument,
    @UploadedFiles()
    files: {
      profileImage?: Express.Multer.File;
      license?: Express.Multer.File;
      passport?: Express.Multer.File;
    },
  ) {
    command.currentUser = user;
    if (files && files.profileImage) {
      const result = await this.fileManagerService.uploadFile(
        files.profileImage[0],
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        command.profileImage = result;
      }
    }
    if (files && files.passport) {
      const result = await this.fileManagerService.uploadFile(
        files.passport[0],
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        command.passport = result;
      }
    }
    if (files && files.license) {
      const result = await this.fileManagerService.uploadFile(
        files.license[0],
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        command.license = result;
      }
    }
    return await this.command.updateDocument(command);
  }

  @Get('check-driver-phone-number')
  @AllowAnonymous()
  async checkDriver(@Query('phoneNumber') phoneNumber: string) {
    if (!phoneNumber.startsWith('+')) {
      phoneNumber = '+' + phoneNumber.trim();
    }
    return this.command.isDriverExist(phoneNumber);
  }
  @Get('export-drivers')
  @AllowAnonymous()
  async exportPassengers(
    @Query() query: CollectionQuery,
    @Res({ passthrough: true }) response: Response,
  ): Promise<StreamableFile> {
    const fileName = `drivers-${uuidv4()}.xlsx`;
    const filePath = `${FileManagerHelper.UPLOADED_FILES_DESTINATION}/${fileName}`;
    const drivers = await this.driverQuery.getDrivers(query);
    const driverData = drivers.data;
    const rows = driverData.map((driver) => {
      return {
        Name: driver.name,
        'Phone Number': driver.phoneNumber,
        Email: driver.email,
        Gender: driver.gender,
        City: driver?.address?.city,
        'Sub City': driver?.address?.subCity,
        'Created Date': driver.createdAt,
      };
    });
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(rows);
    XLSX.utils.book_append_sheet(workBook, workSheet, `Drivers`);
    /* fix headers */
    XLSX.utils.sheet_add_aoa(
      workSheet,
      [
        [
          'Name',
          'Phone Number',
          'Email',
          'Gender',
          'City',
          'Sub City',
          'Created Date',
        ],
      ],
      {
        origin: 'A1',
      },
    );

    /* calculate column width */
    const max_width = rows.reduce((w, r) => Math.max(w, r['Name'].length), 10);
    workSheet['!cols'] = [{ wch: max_width }];
    XLSX.writeFile(workBook, filePath);
    const file: FileResponseDto = {
      filename: fileName,
      mimetype:
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      originalname: fileName,
      path: filePath,
    };
    const stream = await this.fileManagerService.downloadFile(
      file,
      FileManagerHelper.UPLOADED_FILES_DESTINATION,
      response,
      true,
    );

    response.set({
      'Content-Disposition': `inline; filename="${file.originalname}"`,
      'Content-Type': file.mimetype,
    });

    return stream;
  }
}
