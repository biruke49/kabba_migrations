import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { DriverEntity } from '../drivers/driver.entity';
import { DriverPaymentStatus } from '@router/domains/assignments/constants';
import { UserEntity } from '@user/persistence/users/user.entity';

@Entity('driver_fees')
export class DriverFeeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'driver_id', type: 'uuid' })
  driverId: string;
  @Column({ name: 'payment_method', nullable: true })
  paymentMethod: string;
  @Column({ type: 'double precision' })
  fee: number;
  @Column({ type: 'text', name: 'assignment_ids', array: true, default: [] })
  assignmentIds: string[];
  @Column({ name: 'payment_status', default: DriverPaymentStatus.INITIATED })
  paymentStatus: string;
  @Column({ nullable: true, name: 'account_number' })
  accountNumber?: string;
  @Column({ nullable: true, name: 'transaction_number' })
  transactionNumber?: string;
  @Column({ nullable: true, name: 'bank_name' })
  bankName?: string;
  @Column({ nullable: true, name: 'paid_by_id' })
  paidById?: string;
  @Column({ nullable: true, name: 'created_by' })
  createdBy?: string;
  @CreateDateColumn({
    type: 'timestamptz',
    name: 'created_at',
  })
  createdAt: Date;
  @DeleteDateColumn({ type: 'timestamptz', nullable: true, name: 'deleted_at' })
  deletedAt: Date;
  @Column({ nullable: true, name: 'deleted_by' })
  deletedBy: string;
  @ManyToOne(() => DriverEntity, (driver) => driver.payments)
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'paid_by_id' })
  paidBy: UserEntity;
}
