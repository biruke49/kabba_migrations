import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DriverFee } from '@provider/domains/driver-fees/driver-fee';
import { IDriverFeeRepository } from '@provider/domains/driver-fees/driver-fee.repository.interface';
import { Repository } from 'typeorm';
import { DriverFeeEntity } from './driver-fee.entity';
@Injectable()
export class DriverFeeRepository implements IDriverFeeRepository {
  constructor(
    @InjectRepository(DriverFeeEntity)
    private driverFeeRepository: Repository<DriverFeeEntity>,
  ) {}
  async insert(driverFee: DriverFee): Promise<DriverFee> {
    const driverFeeEntity = this.toDriverFeeEntity(driverFee);
    const result = await this.driverFeeRepository.save(driverFeeEntity);
    return result ? this.toDriverFee(result) : null;
  }
  async update(driverFee: DriverFee): Promise<DriverFee> {
    const driverFeeEntity = this.toDriverFeeEntity(driverFee);
    const result = await this.driverFeeRepository.save(driverFeeEntity);
    return result ? this.toDriverFee(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.driverFeeRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<DriverFee[]> {
    const driverFees = await this.driverFeeRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driverFees.length) {
      return null;
    }
    return driverFees.map((user) => this.toDriverFee(user));
  }
  async getById(id: string, withDeleted = false): Promise<DriverFee> {
    const driverFee = await this.driverFeeRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driverFee[0]) {
      return null;
    }
    return this.toDriverFee(driverFee[0]);
  }
  async getByDriverId(id: string, withDeleted = false): Promise<DriverFee> {
    const driverFee = await this.driverFeeRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driverFee[0]) {
      return null;
    }
    return this.toDriverFee(driverFee[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.driverFeeRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.driverFeeRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toDriverFee(driverFeeEntity: DriverFeeEntity): DriverFee {
    const driverFee = new DriverFee();
    driverFee.id = driverFeeEntity.id;
    driverFee.driverId = driverFeeEntity.driverId;
    driverFee.fee = driverFeeEntity.fee;
    driverFee.paymentMethod = driverFeeEntity.paymentMethod;
    driverFee.assignmentIds = driverFeeEntity.assignmentIds;
    driverFee.paymentStatus = driverFeeEntity.paymentStatus;
    driverFee.accountNumber = driverFeeEntity?.accountNumber;
    driverFee.transactionNumber = driverFeeEntity?.transactionNumber;
    driverFee.paidById = driverFeeEntity?.paidById;
    // driverFee.fromDate = driverFeeEntity.fromDate;
    // driverFee.toDate = driverFeeEntity.toDate;
    driverFee.createdBy = driverFeeEntity.createdBy;
    driverFee.deletedBy = driverFeeEntity.deletedBy;
    driverFee.createdAt = driverFeeEntity.createdAt;
    driverFee.deletedAt = driverFeeEntity.deletedAt;
    return driverFee;
  }
  toDriverFeeEntity(driverFee: DriverFee): DriverFeeEntity {
    const driverFeeEntity = new DriverFeeEntity();
    driverFeeEntity.id = driverFee.id;
    driverFeeEntity.driverId = driverFee.driverId;
    driverFeeEntity.fee = driverFee.fee;
    driverFeeEntity.paymentMethod = driverFee.paymentMethod;
    driverFeeEntity.assignmentIds = driverFee.assignmentIds;
    driverFeeEntity.paymentStatus = driverFee.paymentStatus;
    driverFeeEntity.accountNumber = driverFee?.accountNumber;
    driverFeeEntity.transactionNumber = driverFee?.transactionNumber;
    driverFeeEntity.paidById = driverFee?.paidById;
    // driverFeeEntity.fromDate = driverFee.fromDate;
    // driverFeeEntity.toDate = driverFee.toDate;
    driverFeeEntity.createdBy = driverFee.createdBy;
    driverFeeEntity.deletedBy = driverFee.deletedBy;
    driverFeeEntity.createdAt = driverFee.createdAt;
    driverFeeEntity.deletedAt = driverFee.deletedAt;
    return driverFeeEntity;
  }
}
