import { OwnerEntity } from '@provider/persistence/owners/owner.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IOwnerRepository } from '@provider/domains/owners/owner.repository.interface';
import { Owner } from '@provider/domains/owners/owner';
import { VehicleEntity } from './vehicle.entity';
import { Vehicle } from '@provider/domains/owners/vehicle';
@Injectable()
export class OwnerRepository implements IOwnerRepository {
  constructor(
    @InjectRepository(OwnerEntity)
    private ownerRepository: Repository<OwnerEntity>,
  ) {}
  async insert(owner: Owner): Promise<Owner> {
    const ownerEntity = this.toOwnerEntity(owner);
    const result = await this.ownerRepository.save(ownerEntity);
    return result ? this.toOwner(result) : null;
  }
  async update(owner: Owner): Promise<Owner> {
    const ownerEntity = this.toOwnerEntity(owner);
    const result = await this.ownerRepository.save(ownerEntity);
    return result ? this.toOwner(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.ownerRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Owner[]> {
    const owners = await this.ownerRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!owners.length) {
      return null;
    }
    return owners.map((user) => this.toOwner(user));
  }
  async getById(id: string, withDeleted = false): Promise<Owner> {
    const owner = await this.ownerRepository.find({
      where: { id: id },
      relations: ['vehicles'],
      withDeleted: withDeleted,
    });
    if (!owner[0]) {
      return null;
    }
    return this.toOwner(owner[0]);
  }
  async getByPhoneNumber(
    phoneNumber: string,
    withDeleted = false,
  ): Promise<Owner> {
    const owner = await this.ownerRepository.find({
      where: { phoneNumber: phoneNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!owner[0]) {
      return null;
    }
    return this.toOwner(owner[0]);
  }
  async getByEmail(email: string, withDeleted = false): Promise<Owner> {
    const owner = await this.ownerRepository.find({
      where: { email: email },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!owner[0]) {
      return null;
    }
    return this.toOwner(owner[0]);
  }
  async getByTinNumber(tinNumber: string, withDeleted = false): Promise<Owner> {
    const owner = await this.ownerRepository.find({
      where: { tinNumber: tinNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!owner[0]) {
      return null;
    }
    return this.toOwner(owner[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.ownerRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.ownerRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toOwner(ownerEntity: OwnerEntity): Owner {
    const owner = new Owner();
    owner.id = ownerEntity.id;
    owner.name = ownerEntity.name;
    owner.email = ownerEntity.email;
    owner.phoneNumber = ownerEntity.phoneNumber;
    owner.gender = ownerEntity.gender;
    owner.tinNumber = ownerEntity.tinNumber;
    owner.enabled = ownerEntity.enabled;
    owner.logo = ownerEntity.logo;
    owner.address = ownerEntity.address;
    owner.isCompany = ownerEntity.isCompany;
    owner.website = ownerEntity.website;
    owner.license = ownerEntity.license;
    owner.licenseDueDate = ownerEntity.licenseDueDate;
    owner.archiveReason = ownerEntity.archiveReason;
    owner.createdBy = ownerEntity.createdBy;
    owner.updatedBy = ownerEntity.updatedBy;
    owner.deletedBy = ownerEntity.deletedBy;
    owner.createdAt = ownerEntity.createdAt;
    owner.updatedAt = ownerEntity.updatedAt;
    owner.deletedAt = ownerEntity.deletedAt;
    owner.vehicles = ownerEntity.vehicles
      ? ownerEntity.vehicles.map((vehicle) => this.toVehicle(vehicle))
      : [];
    return owner;
  }
  toOwnerEntity(owner: Owner): OwnerEntity {
    const ownerEntity = new OwnerEntity();
    ownerEntity.id = owner.id;
    ownerEntity.name = owner.name;
    ownerEntity.email = owner.email;
    ownerEntity.phoneNumber = owner.phoneNumber;
    ownerEntity.gender = owner.gender;
    ownerEntity.tinNumber = owner.tinNumber;
    ownerEntity.enabled = owner.enabled;
    ownerEntity.logo = owner.logo;
    ownerEntity.address = owner.address;
    ownerEntity.isCompany = owner.isCompany;
    ownerEntity.website = owner.website;
    ownerEntity.license = owner.license;
    ownerEntity.licenseDueDate = owner.licenseDueDate;
    ownerEntity.archiveReason = owner.archiveReason;
    ownerEntity.createdBy = owner.createdBy;
    ownerEntity.updatedBy = owner.updatedBy;
    ownerEntity.deletedBy = owner.deletedBy;
    ownerEntity.createdAt = owner.createdAt;
    ownerEntity.updatedAt = owner.updatedAt;
    ownerEntity.deletedAt = owner.deletedAt;
    ownerEntity.vehicles = owner.vehicles
      ? owner.vehicles.map((vehicle) => this.toVehicleEntity(vehicle))
      : [];
    return ownerEntity;
  }
  toVehicle(vehicleEntity: VehicleEntity): Vehicle {
    const vehicle = new Vehicle();
    vehicle.id = vehicleEntity.id;
    vehicle.ownerId = vehicleEntity.ownerId;
    vehicle.code = vehicleEntity.code;
    vehicle.model = vehicleEntity.model;
    vehicle.plateNumber = vehicleEntity.plateNumber;
    vehicle.categoryId = vehicleEntity.categoryId;
    vehicle.enabled = vehicleEntity.enabled;
    vehicle.color = vehicleEntity.color;
    vehicle.ownerName = vehicleEntity.ownerName;
    vehicle.ownerPhoneNumber = vehicleEntity.ownerPhoneNumber;
    vehicle.bolo = vehicleEntity.bolo;
    vehicle.boloDueDate = vehicleEntity.boloDueDate;
    vehicle.insurance = vehicleEntity.insurance;
    vehicle.insuranceCertificateDueDate =
      vehicleEntity.insuranceCertificateDueDate;
    vehicle.archiveReason = vehicleEntity.archiveReason;
    vehicle.createdBy = vehicleEntity.createdBy;
    vehicle.updatedBy = vehicleEntity.updatedBy;
    vehicle.deletedBy = vehicleEntity.deletedBy;
    vehicle.createdAt = vehicleEntity.createdAt;
    vehicle.updatedAt = vehicleEntity.updatedAt;
    vehicle.deletedAt = vehicleEntity.deletedAt;
    return vehicle;
  }
  toVehicleEntity(vehicle: Vehicle): VehicleEntity {
    const vehicleEntity = new VehicleEntity();
    vehicleEntity.id = vehicle.id;
    vehicleEntity.ownerId = vehicle.ownerId;
    vehicleEntity.code = vehicle.code;
    vehicleEntity.model = vehicle.model;
    vehicleEntity.plateNumber = vehicle.plateNumber;
    vehicleEntity.categoryId = vehicle.categoryId;
    vehicleEntity.enabled = vehicle.enabled;
    vehicleEntity.color = vehicle.color;
    vehicleEntity.ownerName = vehicle.ownerName;
    vehicleEntity.ownerPhoneNumber = vehicle.ownerPhoneNumber;
    vehicleEntity.bolo = vehicle.bolo;
    vehicleEntity.boloDueDate = vehicle.boloDueDate;
    vehicleEntity.insurance = vehicle.insurance;
    vehicleEntity.insuranceCertificateDueDate =
      vehicle.insuranceCertificateDueDate;
    vehicleEntity.archiveReason = vehicle.archiveReason;
    vehicleEntity.createdBy = vehicle.createdBy;
    vehicleEntity.updatedBy = vehicle.updatedBy;
    vehicleEntity.deletedBy = vehicle.deletedBy;
    vehicleEntity.createdAt = vehicle.createdAt;
    vehicleEntity.updatedAt = vehicle.updatedAt;
    vehicleEntity.deletedAt = vehicle.deletedAt;
    return vehicleEntity;
  }
}
