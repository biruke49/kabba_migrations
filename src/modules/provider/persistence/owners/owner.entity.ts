import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { Address } from '@libs/common/address';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { FileDto } from '@libs/common/file-dto';
@Entity({ name: 'owners' })
export class OwnerEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ nullable: true })
  email: string;
  @Column({ name: 'phone_number', unique: true })
  phoneNumber: string;
  @Column({ nullable: true })
  gender: string;
  @Column({ default: true })
  enabled: boolean;
  @Column({ name: 'profile_image', nullable: true, type: 'jsonb' })
  logo: FileDto;
  @Column({ unique: true, name: 'tin_number' })
  tinNumber: string;
  @Column({ nullable: true })
  website: string;
  @Column({ type: 'jsonb', nullable: true })
  address: Address;
  @Column({ name: 'is_company', default: false })
  isCompany: boolean;
  @Column({ type: 'jsonb', nullable: true })
  license: FileDto;
  @Column({ name: 'license_due_date', nullable: true, type: 'date' })
  licenseDueDate: Date;
  @OneToMany(() => VehicleEntity, (vehicle) => vehicle.owner, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  vehicles: VehicleEntity[];
}
