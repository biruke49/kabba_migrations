import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { OwnerEntity } from '@provider/persistence/owners/owner.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { FileDto } from '@libs/common/file-dto';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';

@Entity('vehicles')
export class VehicleEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  code: string;
  @Column({ name: 'plate_number' })
  plateNumber: string;
  @Column()
  model: string;
  @Column({ name: 'category_id', type: 'uuid' })
  categoryId: string;
  @Column({ name: 'owner_name', nullable: true })
  ownerName: string;
  @Column({ name: 'owner_phone_number', nullable: true })
  ownerPhoneNumber: string;
  @Column()
  color: string;
  @Column({ type: 'jsonb', nullable: true })
  bolo: FileDto;
  @Column({ nullable: true, name: 'bolo_due_date', type: 'date' })
  boloDueDate: Date;
  @Column({ type: 'jsonb', nullable: true })
  insurance: FileDto;
  @Column({
    name: 'insurance_certificate_due_date',
    nullable: true,
    type: 'date',
  })
  insuranceCertificateDueDate: Date;
  @Column({ default: true })
  enabled: boolean;
  @ManyToOne(() => OwnerEntity, (owner) => owner.vehicles, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'owner_id' })
  owner: OwnerEntity;
  @Column({ name: 'owner_id', type: 'uuid' })
  ownerId: string;
  @OneToOne(() => DriverEntity, (driver) => driver.vehicle)
  driver: DriverEntity;
  @ManyToOne(() => CategoryEntity, (category) => category.vehicles, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'category_id' })
  category: CategoryEntity;
  @OneToMany(() => BookingEntity, (booking) => booking.vehicle, {
    onDelete: 'CASCADE',
  })
  bookings: BookingEntity[];
}
