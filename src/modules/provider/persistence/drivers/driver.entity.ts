import { DriverFeeEntity } from '@provider/persistence/driver-fees/driver-fee.entity';
import { VehicleEntity } from '@provider/persistence/owners/vehicle.entity';
import { FileDto } from '@libs/common/file-dto';
import { CommonEntity } from '@libs/common/common.entity';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Address } from '@libs/common/address';
import { AverageRate } from '@libs/common/average-rate';
import { BankAccountEntity } from './bank-account.entity';
import { ReviewEntity } from '@interaction/persistence/reviews/review.entity';
import { EmergencyContact } from '@libs/common/emergency-contact';
import { AssignmentEntity } from '@router/persistence/assignments/assignment.entity';
import { BookingEntity } from '@order/persistance/bookings/booking.entity';
import { PreferenceEntity } from '@interaction/persistence/preferences/preference.entity';
import { GroupAssignmentEntity } from '@assignment/persistence/groups/group-assignment.entity';
import { CandidateEntity } from '@job/persistence/jobs/candidate.entity';
import { DriverAssignmentEntity } from '@assignment/persistence/driver-assignments/driver-assignment.entity';
import { GroupEntity } from '@assignment/persistence/groups/group.entity';

@Entity('drivers')
export class DriverEntity extends CommonEntity {
  @Index()
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Index()
  @Column({ nullable: true })
  email: string;
  @Index()
  @Column({ name: 'phone_number', unique: true })
  phoneNumber: string;
  @Column({ nullable: true })
  gender: string;
  @Column({ name: 'profile_image', nullable: true, type: 'jsonb' })
  profileImage: FileDto;
  @Column({ nullable: true, type: 'jsonb' })
  address: Address;
  @Column({ name: 'is_active', default: false })
  isActive: boolean;
  @Column({ name: 'vehicle_id', type: 'uuid', nullable: true })
  vehicleId: string;
  @Column({ default: true })
  enabled: boolean;
  @Column({ name: 'passport', nullable: true, type: 'jsonb' })
  passport: FileDto;
  @Column({ name: 'license', nullable: true, type: 'jsonb' })
  license: FileDto;
  @Column({ name: 'license_due_date', nullable: true, type: 'date' })
  licenseDueDate: Date;
  @Column({ name: 'fcm_id', type: 'uuid', nullable: true })
  fcmId: string;
  @Column({ name: 'average_rate', nullable: true, type: 'jsonb' })
  averageRate: AverageRate;
  @Column({ nullable: true })
  lat: number;
  @Column({ nullable: true })
  lng: number;
  @Column({ name: 'is_approved', default: false })
  isApproved: boolean;
  @Column({
    name: 'emergency_contact',
    type: 'jsonb',
    default: {},
  })
  emergencyContact: EmergencyContact;
  @Column({ type: 'text', name: 'date_preferences', array: true, default: [] })
  datePreferences: string[];
  @OneToMany(() => BankAccountEntity, (bankAccount) => bankAccount.driver, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  bankAccounts: BankAccountEntity[];
  @OneToMany(() => ReviewEntity, (review) => review.driver, {
    cascade: ['remove', 'soft-remove', 'recover'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  reviews: ReviewEntity[];
  @OneToOne(() => VehicleEntity, (vehicle) => vehicle.driver, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'vehicle_id' })
  vehicle: VehicleEntity;

  @OneToMany(() => AssignmentEntity, (assignment) => assignment.driver, {
    // cascade: true,
    onDelete: 'CASCADE',
  })
  assignments: AssignmentEntity[];
  @OneToMany(() => BookingEntity, (booking) => booking.driver, {
    onDelete: 'CASCADE',
  })
  bookings: BookingEntity[];
  @OneToMany(() => PreferenceEntity, (preference) => preference.driver, {
    onDelete: 'CASCADE',
  })
  preferences: PreferenceEntity[];
  @OneToMany(() => DriverFeeEntity, (fee) => fee.driver)
  payments: DriverFeeEntity[];
  @OneToMany(
    () => GroupAssignmentEntity,
    (groupAssignment) => groupAssignment.driver,
    {
      cascade: true,
      onDelete: 'CASCADE',
    },
  )
  groupAssignments: GroupAssignmentEntity[];
  @OneToMany(() => GroupEntity, (groupAssignment) => groupAssignment.driver, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  group: GroupEntity[];
  @OneToMany(() => CandidateEntity, (candidate) => candidate.driver, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  candidates: CandidateEntity[];
  @OneToMany(
    () => DriverAssignmentEntity,
    (driverAssignment) => driverAssignment.driver,
    {
      cascade: ['remove', 'soft-remove', 'recover'],
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  driverAssignments: DriverAssignmentEntity[];
}
