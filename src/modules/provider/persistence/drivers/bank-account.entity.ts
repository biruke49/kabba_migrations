import { CommonEntity } from '@libs/common/common.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DriverEntity } from './driver.entity';

@Entity('bank_accounts')
export class BankAccountEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'driver_id' })
  driverId: string;
  @Column({ name: 'account_number', unique: true })
  accountNumber: string;
  @Column({ name: 'bank_name' })
  bankName: string;
  @Column({ name: 'is_preferred', default: false })
  isPreferred: boolean;
  @Column({ name: 'is_active', default: true })
  isActive: boolean;
  @ManyToOne(() => DriverEntity, (driver) => driver.bankAccounts, {
    orphanedRowAction: 'delete',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'driver_id' })
  driver: DriverEntity;
}
