import { DriverEntity } from '@provider/persistence/drivers/driver.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IDriverRepository } from '@provider/domains/drivers/driver.repository.interface';
import { Driver } from '@provider/domains/drivers/driver';
import { BankAccountEntity } from './bank-account.entity';
import { BankAccount } from '@provider/domains/drivers/bank-account';
import { Vehicle } from '@provider/domains/owners/vehicle';
import { VehicleEntity } from '../owners/vehicle.entity';
@Injectable()
export class DriverRepository implements IDriverRepository {
  constructor(
    @InjectRepository(DriverEntity)
    private driverRepository: Repository<DriverEntity>,
  ) {}
  async insert(driver: Driver): Promise<Driver> {
    const driverEntity = this.toDriverEntity(driver);
    const result = await this.driverRepository.save(driverEntity);
    return result ? this.toDriver(result) : null;
  }
  async update(driver: Driver): Promise<Driver> {
    const driverEntity = this.toDriverEntity(driver);
    const result = await this.driverRepository.save(driverEntity);
    return result ? this.toDriver(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.driverRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Driver[]> {
    const drivers = await this.driverRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!drivers.length) {
      return null;
    }
    return drivers.map((user) => this.toDriver(user));
  }
  async getById(id: string, withDeleted = false): Promise<Driver> {
    const driver = await this.driverRepository.find({
      where: { id: id },
      relations: ['bankAccounts', 'vehicle'],
      withDeleted: withDeleted,
    });
    if (!driver[0]) {
      return null;
    }
    return this.toDriver(driver[0]);
  }
  async getByVehicleId(
    vehicleId: string,
    withDeleted = false,
  ): Promise<Driver> {
    const driver = await this.driverRepository.find({
      where: { vehicleId: vehicleId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driver[0]) {
      return null;
    }
    return this.toDriver(driver[0]);
  }
  async getByIdNotification(id: string, withDeleted = false): Promise<Driver> {
    const driver = await this.driverRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driver[0]) {
      return null;
    }
    return this.toDriver(driver[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.driverRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.driverRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  async getByPhoneNumber(
    phoneNumber: string,
    withDeleted = false,
  ): Promise<Driver> {
    const driver = await this.driverRepository.find({
      where: { phoneNumber: phoneNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!driver[0]) {
      return null;
    }
    return this.toDriver(driver[0]);
  }
  toDriver(driverEntity: DriverEntity): Driver {
    const driver = new Driver();
    driver.id = driverEntity.id;
    driver.name = driverEntity.name;
    driver.email = driverEntity.email;
    driver.phoneNumber = driverEntity.phoneNumber;
    driver.gender = driverEntity.gender;
    driver.passport = driverEntity.passport;
    driver.enabled = driverEntity.enabled;
    driver.profileImage = driverEntity.profileImage;
    driver.address = driverEntity.address;
    driver.isActive = driverEntity.isActive;
    driver.averageRate = driverEntity.averageRate;
    driver.license = driverEntity.license;
    driver.emergencyContact = driverEntity.emergencyContact;
    driver.licenseDueDate = driverEntity.licenseDueDate;
    driver.lat = driverEntity.lat;
    driver.lng = driverEntity.lng;
    driver.vehicleId = driverEntity.vehicleId;
    driver.isApproved = driverEntity.isApproved;
    driver.datePreferences = driverEntity.datePreferences;
    driver.createdBy = driverEntity.createdBy;
    driver.updatedBy = driverEntity.updatedBy;
    driver.deletedBy = driverEntity.deletedBy;
    driver.createdAt = driverEntity.createdAt;
    driver.updatedAt = driverEntity.updatedAt;
    driver.deletedAt = driverEntity.deletedAt;
    driver.bankAccounts = driverEntity.bankAccounts
      ? driverEntity.bankAccounts.map((bankAccount) =>
          this.toBankAccount(bankAccount),
        )
      : [];
    if (driverEntity.vehicle) {
      driver.vehicle = this.toVehicle(driverEntity.vehicle);
    }

    return driver;
  }
  toDriverEntity(driver: Driver): DriverEntity {
    const driverEntity = new DriverEntity();
    driverEntity.id = driver.id;
    driverEntity.name = driver.name;
    driverEntity.email = driver.email;
    driverEntity.phoneNumber = driver.phoneNumber;
    driverEntity.gender = driver.gender;
    driverEntity.passport = driver.passport;
    driverEntity.enabled = driver.enabled;
    driverEntity.profileImage = driver.profileImage;
    driverEntity.address = driver.address;
    driverEntity.isActive = driver.isActive;
    driverEntity.emergencyContact = driver.emergencyContact;
    driverEntity.license = driver.license;
    driverEntity.licenseDueDate = driver.licenseDueDate;
    driverEntity.datePreferences = driver.datePreferences;
    driverEntity.vehicleId = driver.vehicleId;
    driverEntity.averageRate = driver.averageRate;
    driverEntity.lat = driver.lat;
    driverEntity.lng = driver.lng;
    driverEntity.isApproved = driver.isApproved;
    driverEntity.archiveReason = driver.archiveReason;
    driverEntity.createdBy = driver.createdBy;
    driverEntity.updatedBy = driver.updatedBy;
    driverEntity.deletedBy = driver.deletedBy;
    driverEntity.createdAt = driver.createdAt;
    driverEntity.updatedAt = driver.updatedAt;
    driverEntity.deletedAt = driver.deletedAt;
    driverEntity.bankAccounts = driver.bankAccounts
      ? driver.bankAccounts.map((bankAccount) =>
          this.toBankAccountEntity(bankAccount),
        )
      : [];
    if (driver.vehicle) {
      driverEntity.vehicle = this.toVehicleEntity(driver.vehicle);
    }
    return driverEntity;
  }
  toBankAccount(bankAccountEntity: BankAccountEntity): BankAccount {
    const bankAccount = new BankAccount();
    bankAccount.id = bankAccountEntity.id;
    bankAccount.driverId = bankAccountEntity.driverId;
    bankAccount.accountNumber = bankAccountEntity.accountNumber;
    bankAccount.bankName = bankAccountEntity.bankName;
    bankAccount.isPreferred = bankAccountEntity.isPreferred;
    bankAccount.isActive = bankAccountEntity.isActive;
    bankAccount.archiveReason = bankAccountEntity.archiveReason;
    bankAccount.createdBy = bankAccountEntity.createdBy;
    bankAccount.updatedBy = bankAccountEntity.updatedBy;
    bankAccount.deletedBy = bankAccountEntity.deletedBy;
    bankAccount.createdAt = bankAccountEntity.createdAt;
    bankAccount.updatedAt = bankAccountEntity.updatedAt;
    bankAccount.deletedAt = bankAccountEntity.deletedAt;
    return bankAccount;
  }
  toBankAccountEntity(bankAccount: BankAccount): BankAccountEntity {
    const bankAccountEntity = new BankAccountEntity();
    bankAccountEntity.id = bankAccount.id;
    bankAccountEntity.driverId = bankAccount.driverId;
    bankAccountEntity.accountNumber = bankAccount.accountNumber;
    bankAccountEntity.bankName = bankAccount.bankName;
    bankAccountEntity.isPreferred = bankAccount.isPreferred;
    bankAccountEntity.isActive = bankAccount.isActive;
    bankAccountEntity.archiveReason = bankAccount.archiveReason;
    bankAccountEntity.createdBy = bankAccount.createdBy;
    bankAccountEntity.updatedBy = bankAccount.updatedBy;
    bankAccountEntity.deletedBy = bankAccount.deletedBy;
    bankAccountEntity.createdAt = bankAccount.createdAt;
    bankAccountEntity.updatedAt = bankAccount.updatedAt;
    bankAccountEntity.deletedAt = bankAccount.deletedAt;
    return bankAccountEntity;
  }
  toVehicle(vehicleEntity: VehicleEntity): Vehicle {
    const vehicle = new Vehicle();
    vehicle.id = vehicleEntity.id;
    vehicle.ownerId = vehicleEntity.ownerId;
    vehicle.code = vehicleEntity.code;
    vehicle.model = vehicleEntity.model;
    vehicle.plateNumber = vehicleEntity.plateNumber;
    vehicle.categoryId = vehicleEntity.categoryId;
    vehicle.enabled = vehicleEntity.enabled;
    vehicle.color = vehicleEntity.color;
    vehicle.bolo = vehicleEntity.bolo;
    vehicle.boloDueDate = vehicleEntity.boloDueDate;
    vehicle.insurance = vehicleEntity.insurance;
    vehicle.insuranceCertificateDueDate =
      vehicleEntity.insuranceCertificateDueDate;
    vehicle.createdBy = vehicleEntity.createdBy;
    vehicle.updatedBy = vehicleEntity.updatedBy;
    vehicle.deletedBy = vehicleEntity.deletedBy;
    vehicle.createdAt = vehicleEntity.createdAt;
    vehicle.updatedAt = vehicleEntity.updatedAt;
    vehicle.deletedAt = vehicleEntity.deletedAt;
    return vehicle;
  }
  toVehicleEntity(vehicle: Vehicle): VehicleEntity {
    const vehicleEntity = new VehicleEntity();
    vehicleEntity.id = vehicle.id;
    vehicleEntity.ownerId = vehicle.ownerId;
    vehicleEntity.code = vehicle.code;
    vehicleEntity.model = vehicle.model;
    vehicleEntity.plateNumber = vehicle.plateNumber;
    vehicleEntity.categoryId = vehicle.categoryId;
    vehicleEntity.enabled = vehicle.enabled;
    vehicleEntity.color = vehicle.color;
    vehicleEntity.bolo = vehicle.bolo;
    vehicleEntity.boloDueDate = vehicle.boloDueDate;
    vehicleEntity.insurance = vehicle.insurance;
    vehicleEntity.insuranceCertificateDueDate =
      vehicle.insuranceCertificateDueDate;
    vehicleEntity.createdBy = vehicle.createdBy;
    vehicleEntity.updatedBy = vehicle.updatedBy;
    vehicleEntity.deletedBy = vehicle.deletedBy;
    vehicleEntity.createdAt = vehicle.createdAt;
    vehicleEntity.updatedAt = vehicle.updatedAt;
    vehicleEntity.deletedAt = vehicle.deletedAt;
    return vehicleEntity;
  }
}
