import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from '../db/data-source';
import { UserModule } from '@user/user.module';
import { ClassificationModule } from '@classification/classification.module';
import { ProviderModule } from '@provider/provider.module';
import { InteractionModule } from '@interaction/interaction.module';
import { PublicationModule } from '@publication/publication.module';
import { NotificationModule } from '@notification/notification.module';
import { AccountModule } from '@account/account.module';
import { CustomerModule } from '@customer/customer.module';
import { ActivityModule } from '@activity-logger/activity-logger.module';
import {
  FileManagerModule,
  FileManagerService,
} from '@libs/common/file-manager';
import { CreditModule } from '@credit/credit.module';
import { PaymentModule } from '@libs/payment/payment.module';
import { ServiceModule } from '@service/service.module';
import { OrderModule } from '@order/order.module';
import { ConfigurationsModule } from '@configurations/configurations.module';
import { AssignmentModule } from '@assignment/assignment.module';
import { JobModule } from '@job/job.module';
import { ChatModule } from '@chat/chat.module';
import { FaqModule } from 'modules/faq/faq.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CronService } from 'cron.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { ConfigModule } from '@nestjs/config';
import * as dotenv from 'dotenv';
import { RouterModule } from '@router/router.module';

dotenv.config({ path: '.env' });
@Module({
  imports: [
    EventEmitterModule.forRoot(),
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot(dataSourceOptions),
    UserModule,
    ClassificationModule,
    ProviderModule,
    InteractionModule,
    PublicationModule,
    NotificationModule,
    AccountModule,
    CustomerModule,
    ActivityModule,
    FileManagerModule,
    RouterModule,
    CreditModule,
    PaymentModule,
    ServiceModule,
    OrderModule,
    ConfigurationsModule,
    AssignmentModule,
    JobModule,
    ChatModule,
    ScheduleModule.forRoot(),
    FaqModule,
  ],
  controllers: [AppController],
  providers: [FileManagerService, AppService, CronService],
})
export class AppModule {}
