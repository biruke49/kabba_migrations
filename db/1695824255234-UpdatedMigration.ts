import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdatedMigration1695824255234 implements MigrationInterface {
    name = 'UpdatedMigration1695824255234'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "faqs" ("created_by" character varying, "updated_by" character varying, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "deleted_by" character varying, "archive_reason" text, "id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "description" text, "tags" jsonb, CONSTRAINT "PK_2ddf4f2c910f8e8fa2663a67bf0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "notifications" ADD "status" character varying`);
        await queryRunner.query(`ALTER TABLE "notifications" ADD "method" character varying`);
        await queryRunner.query(`ALTER TABLE "bank_accounts" ADD "chapa_subaccount_id" character varying`);
        await queryRunner.query(`ALTER TABLE "bank_accounts" ADD CONSTRAINT "UQ_5d1c870a3650ae94394f1d7dc11" UNIQUE ("chapa_subaccount_id")`);
        await queryRunner.query(`ALTER TABLE "drivers" ADD "split_type" character varying`);
        await queryRunner.query(`ALTER TABLE "drivers" ADD "split_amount" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "drivers" DROP COLUMN "split_amount"`);
        await queryRunner.query(`ALTER TABLE "drivers" DROP COLUMN "split_type"`);
        await queryRunner.query(`ALTER TABLE "bank_accounts" DROP CONSTRAINT "UQ_5d1c870a3650ae94394f1d7dc11"`);
        await queryRunner.query(`ALTER TABLE "bank_accounts" DROP COLUMN "chapa_subaccount_id"`);
        await queryRunner.query(`ALTER TABLE "notifications" DROP COLUMN "method"`);
        await queryRunner.query(`ALTER TABLE "notifications" DROP COLUMN "status"`);
        await queryRunner.query(`DROP TABLE "faqs"`);
    }

}
